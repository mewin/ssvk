
#ifndef DEBUG_GLSL_INCLUDED
#define DEBUG_GLSL_INCLUDED 1
#ifndef NDEBUG

const uint SSVK_DEBUG_MAX_LINES = 1024;
const uint SSVK_DEBUG_LINE_LENGTH = 1024;

layout(binding = 1, set = 1) uniform SSVkDebugOutputBuffer {
    uint numLines;
    uint chars[SSVK_DEBUG_MAX_LINES * SSVK_DEBUG_LINE_LENGTH / 4];
} u_debug;

bool g_printingActive = false;

void print()
{
    
}

#endif // NDEBUG
#endif // DEBUG_GLSL_INCLUDED
