#ifndef LIGHTS_GLSL_INCLUDED
#define LIGHTS_GLSL_INCLUDED 1

struct DirectionalLight
{
    vec3 color;
    vec3 direction;
    float ambient;
};

struct PointLight
{
    vec4 colorAndAmbient;  // use vec4s for alignment reasons (std140)
    vec4 positionAndRange;
    float attenuation;
    uint shadowCacheBufferIndex;
    // 2 floats/12 byte padding
};

#endif // LIGHTS_GLSL_INCLUDED
