#ifndef SSVK_GLSL_INCLUDED
#define SSVK_GLSL_INCLUDED 1

#extension GL_EXT_scalar_block_layout : enable

#include "lights.glsl"

#define SSVK_NUM_POINT_LIGHTS 8

#define DEBUG_RENDER_MODE_NONE         0
#define DEBUG_RENDER_MODE_ALBEDO       1
#define DEBUG_RENDER_MODE_ROUGHNESS    2
#define DEBUG_RENDER_MODE_METALLIC     3
#define DEBUG_RENDER_MODE_UV0          4
#define DEBUG_RENDER_MODE_UV1          5
#define DEBUG_RENDER_MODE_BARYCENTRICS 6
#define DEBUG_RENDER_MODE_MATERIAL_ID  7
#define DEBUG_RENDER_MODE_INSTANCE_ID  8
#define DEBUG_RENDER_MODE_PRIMITIVE_ID 9
#define DEBUG_RENDER_MODE_NORMAL_MAP   10
#define DEBUG_RENDER_MODE_DEPTH        11

#define DEBUG_RENDER_FLAG_SKIP_NORMAL_MAP (1 << 0)
#define DEBUG_RENDER_FLAG_PPROCESS_SKIP   (1 << 1)
#define DEBUG_RENDER_FLAG_PPROCESS_SPLIT  (1 << 2)

const vec3[] DEBUG_RENDER_COLORS = vec3[](
    vec3(1.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, 1.0),
    vec3(1.0, 1.0, 0.0), vec3(1.0, 0.0, 1.0), vec3(0.0, 1.0, 1.0),
    vec3(1.0, 1.0, 1.0), vec3(0.7, 0.7, 0.7), vec3(0.4, 0.4, 0.4)
);
#define DEBUG_RENDER_COLOR(materialID) DEBUG_RENDER_COLORS[materialID % DEBUG_RENDER_COLORS.length()]

// global state
#define SSVK_BINDING_UNI_GLOBAL_STATE        0
#define SSVK_BINDING_IA_COLOR                3
#define SSVK_BINDING_UNI_MESHDATA_BUFFER     4
#define SSVK_BINDING_UNI_VERTEXDATA_BUFFER   5
#define SSVK_BINDING_UNI_MATERIALDATA_BUFFER 6
#define SSVK_BINDING_UNI_SDC_BUFFER          7
#define SSVK_BINDING_UNI_MATERIAL_TEXTURES   10

#define SSVK_DECLARE_GLOBAL_STATE \
    layout(std140, binding = SSVK_BINDING_UNI_GLOBAL_STATE) uniform SSVkGlobalState { \
        mat4 view; \
        mat4 projection; \
        PointLight[SSVK_NUM_POINT_LIGHTS] pointLights; \
        vec4 eyePosAndTime; \
        uint debugRenderMode; \
        uint debugRenderFlags; \
        uint environmentMapTextureIndex; \
    } u_global;

///////////////
/// prepass ///
///////////////

// vertex shader
#define SSVK_PREPASS_IN_LOC_POSITION    0
#define SSVK_PREPASS_IN_LOC_NORMAL      1
#define SSVK_PREPASS_IN_LOC_UV0         2
#define SSVK_PREPASS_IN_LOC_UV1         3
#define SSVK_PREPASS_IN_LOC_MATERIAL_ID 4
#define SSVK_PREPASS_IN_LOC_INSTANCE_ID 5

#define SSVK_DECLARE_PREPASS_VERTEX_INPUTS \
    layout(location = SSVK_PREPASS_IN_LOC_POSITION) in vec3 in_position; \
    layout(location = SSVK_PREPASS_IN_LOC_NORMAL) in vec3 in_normal; \
    layout(location = SSVK_PREPASS_IN_LOC_UV0) in vec2 in_uv0; \
    layout(location = SSVK_PREPASS_IN_LOC_UV1) in vec2 in_uv1;

// fragment shader
#define SSVK_PREPASS_OUTPUT_LOCATION_POSITION_NORMAL  0 // vec4
#define SSVK_PREPASS_OUTPUT_LOCATION_UV_MATID_ALPHA   1 // uvec4

// both
#define SSVK_DECLARE_PREPASS_GLOBAL_STATE \
    SSVK_DECLARE_GLOBAL_STATE \
    layout(std140, push_constant) uniform SSVkModelState { \
        mat4 model; \
        uint materialID; \
        uint instanceID; \
    } u_perModel;


/////////////////
/// draw pass ///
/////////////////

// vertex shader
// not too much apparently

#define SSVK_OUTPUT_LOCATION_COLOR         0

#define SSVK_DECLARE_DRAWPASS_FRAGMENT_OUTPUTS \
    layout(location = SSVK_OUTPUT_LOCATION_COLOR) out vec4 out_color;

// both
#define SSVK_DECLARE_DRAWPASS_GLOBAL_STATE \
    SSVK_DECLARE_GLOBAL_STATE \
    layout(std140, push_constant) uniform SSVkMaterialState { \
        uint materialID; \
    } u_perMaterial;

#endif // SSVK_GLSL_INCLUDED
