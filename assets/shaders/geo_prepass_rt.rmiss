#version 460
#extension GL_ARB_separate_shader_objects : require
#extension GL_GOOGLE_include_directive : require
#extension GL_NV_ray_tracing : require
#extension GL_EXT_nonuniform_qualifier : require

#include "common.glsl"
#include "ray_tracing.glsl"
#include "spherical_conversion.glsl"
#include "ssvk.glsl"
#include "data.glsl"

SSVK_DECLARE_GLOBAL_STATE
SSVK_DECLARE_TEXTURES

layout(location = 0) rayPayloadInNV SSVkRayTracingPayload payload;

void main()
{
    if (u_global.environmentMapTextureIndex == 0)
    {
        payload.color          = vec4(0.0, 0.0, 0.0, 1.0);
    }
    else
    {
        vec2 sphere = directionToSphere(gl_WorldRayDirectionNV);
        vec2 uv = vec2(
            unmix(sphere.x, -0.5 * PI, 0.5 * PI),
            unmix(sphere.y, 0, PI)
        );
        payload.color = texture(u_textures[nonuniformEXT(u_global.environmentMapTextureIndex - 1)], uv);
    }

    payload.nextDirection  = vec3(0.0);
    payload.nextOrigin     = vec3(0.0);
    payload.factor         = vec4(0.0);
    
    payload.nextDirection2 = vec3(0.0);
    payload.nextOrigin2    = vec3(0.0);
    payload.factor2        = vec4(0.0);

    payload.depth          = gl_RayTmaxNV;
}
