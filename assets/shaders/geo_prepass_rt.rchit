#version 460
#extension GL_ARB_separate_shader_objects : require
#extension GL_GOOGLE_include_directive : require
#extension GL_NV_ray_tracing : require
#extension GL_EXT_nonuniform_qualifier : require

#include "ray_tracing.glsl"
#include "ssvk.glsl"
#include "pbr.glsl"
#include "util.glsl"
#include "data.glsl"

SSVK_DECLARE_GLOBAL_STATE

SSVK_DECLARE_MESH_DATA
SSVK_DECLARE_VERTEX_DATA
SSVK_DECLARE_MATERIAL_DATA
SSVK_DECLARE_TEXTURES

layout(binding = 1, set = 0) uniform accelerationStructureNV u_topAccelerationStructure;
layout(location = 0) rayPayloadInNV SSVkRayTracingPayload payload;
layout(location = 1) rayPayloadNV SSVkRayTracingShadowPayload shadowHitValue;

hitAttributeNV vec2 baryCoord;

vec4 calculateColor();

void main()
{
    payload.color = calculateColor();
}

vec3 getRestLight(vec3 origin, uint idx)
{
    vec3 lightPosition = u_global.pointLights[idx].positionAndRange.xyz;
    vec3 dir = lightPosition - origin;
    float dist = length(dir);
    vec3 direction = dir / dist;
    uint rayFlags = gl_RayFlagsOpaqueNV | gl_RayFlagsTerminateOnFirstHitNV;
    uint cullMask = 0xff;
    float tmin = 0.001;
    float tmax = 0.99 * dist;
    uint sbtRecordOffset = 1; // shadow_ray.rchit
    uint sbtRecordStride = 0;
    uint missIndex = 1; // shadow_ray.rmiss

    vec3 restLight = vec3(1.0);

    if (dist > u_global.pointLights[idx].positionAndRange.w) {
        return vec3(0.0);
    }

    // TODO: do this with a single ray with multiple intersections
    for (int i = 0; i < SSVK_MAX_SHADOW_PASSES; ++i)
    {
        traceNV(u_topAccelerationStructure, rayFlags, cullMask, sbtRecordOffset, sbtRecordStride, missIndex, origin, tmin, direction, tmax, 1);

        if (shadowHitValue.restLight.a >= 0.99 * tmax) { // miss
            return restLight;
        }
        restLight *= shadowHitValue.restLight.rgb;
        tmin = shadowHitValue.restLight.a + 0.1;

        if (length(restLight) < 0.01) {
            return vec3(0.0);
        }
    }

    return vec3(0.0);
}

vec3 doShading(vec3 worldPos, vec3 worldNormal, vec2 uv0, vec2 uv1, vec3 albedo, float metallic, float roughness, float ao, vec3 eyeDir)
{
    vec3 f0 = vec3(0.04);
    f0 = mix(f0, albedo, metallic);
    vec3 irradiance = vec3(0.0);

    for (int i = 0; i < SSVK_NUM_POINT_LIGHTS; ++i)
    {
        vec3 restLight = getRestLight(worldPos, i);
        irradiance += restLight * pbrPointLightIrradiance(u_global.pointLights[i], worldPos, worldNormal, eyeDir, albedo, metallic, roughness, f0);
    }
    
    vec3 ambient = vec3(0.03) * albedo * ao;
    vec3 color = ambient + irradiance;

    return color;
}

vec3 applyNormalTexture(mat3 tbn, vec3 normalMapValue)
{
    vec3 normal = normalMapValue * 2.0 - vec3(1.0);
    return normalize(tbn * normal);
}

vec3 calculatePortalDirection(vec3 dir, mat3 tbnSource, mat3 tbnDestination)
{
    // basically reflect the ray and transform it into the portals tangent space
    vec3 dirTangentSpace = transpose(tbnSource) * dir;
    dirTangentSpace.xz *= -1.0; // mirror and flip x/u direction, keep y/v
    return tbnDestination * dirTangentSpace;
}

vec4 calculateColor()
{
    MeshData meshData = meshes[gl_InstanceCustomIndexNV];
    MaterialData materialData = materials[meshData.materialID];

    VertexData v0 = vertices[meshData.offset + 3 * gl_PrimitiveID];
    VertexData v1 = vertices[meshData.offset + 3 * gl_PrimitiveID + 1];
    VertexData v2 = vertices[meshData.offset + 3 * gl_PrimitiveID + 2];

    vec3 eyeDir = -gl_WorldRayDirectionNV;

    // mesh properties
    vec3 bary            = vec3(1.0 - baryCoord.x - baryCoord.y, baryCoord.x, baryCoord.y);
    vec3 worldPos        = gl_WorldRayOriginNV + gl_WorldRayDirectionNV * gl_HitTNV; // (bary.x * v0.position + bary.y * v1.position + bary.z * v2.position).xyz;
    vec3 worldNormal     = mat3(gl_ObjectToWorldNV) * (bary.x * v0.normal + bary.y * v1.normal + bary.z * v2.normal).xyz;
    vec3 worldTangent    = mat3(gl_ObjectToWorldNV) * (bary.x * v0.tangent + bary.y * v1.tangent + bary.z * v2.tangent).xyz;
    vec3 worldBitangent  = mat3(gl_ObjectToWorldNV) * (bary.x * v0.bitangent + bary.y * v1.bitangent + bary.z * v2.bitangent).xyz;
    vec4 uv01            = bary.x * v0.uv01 + bary.y * v1.uv01 + bary.z * v2.uv01;
    vec2 uv0             = uv01.xy;
    vec2 uv1             = uv01.zw;
    bool isPortal        = v0.portalOutputLocation.w > 0.5;

    // normalize normal and tangents
    worldNormal = normalize(worldNormal);
    worldTangent = normalize(worldTangent);
    worldBitangent = normalize(worldBitangent);

    // material properties
    vec4  albedoAlpha       = texture(u_textures[nonuniformEXT(materialData.albedoTextureIndex)], uv0) * materialData.albedoFactor;
    vec3  normalMapValue    = texture(u_textures[nonuniformEXT(materialData.normalTextureIndex)], uv0).rgb;
    vec3  albedo            = pow(albedoAlpha.rgb, vec3(2.2));
    float alpha             = albedoAlpha.a;
    vec2  metallicRoughness = texture(u_textures[nonuniformEXT(materialData.metallicRoughnessTextureIndex)], uv0).rg;
    float metallic          = metallicRoughness.r * materialData.metallicRoughnessFactorRefractiveIndex.r;
    float roughness         = metallicRoughness.g * materialData.metallicRoughnessFactorRefractiveIndex.g;
    float refractiveIndex   = 1.3; // materialData.metallicRoughnessFactorRefractiveIndex.b;
    float ao                = texture(u_textures[nonuniformEXT(materialData.ambientOcclusionTextureIndex)], uv0).r;
    float reflectivity      = alpha * metallic * (1.0 - roughness);
    bool  backSide          = dot(worldNormal, eyeDir) < 0.0;

    // tangent space -> world space
    mat3 tbn = mat3(worldTangent, worldBitangent, worldNormal);

    if ((u_global.debugRenderFlags & DEBUG_RENDER_FLAG_SKIP_NORMAL_MAP) == 0) {
        worldNormal = applyNormalTexture(tbn, normalMapValue);
    }

    payload.depth = gl_HitTNV;

    vec3 color = doShading(worldPos, worldNormal, uv0, uv1, albedo, metallic, roughness, ao, eyeDir);

#ifndef NDEBUG
    if (u_global.debugRenderMode == 0 || u_global.debugRenderMode == DEBUG_RENDER_MODE_DEPTH)
    {
#endif
        // using -eyeDir instead of gl_WorldRayDirectionNV as the latter apparently changes when calling traceNV for shadow calculations
        // (bug??)

        if (isPortal)
        {
            vec3 portalLocation  = (bary.x * v0.portalOutputLocation + bary.y * v1.portalOutputLocation + bary.z * v2.portalOutputLocation).xyz;
            vec3 portalNormal = (bary.x * v0.portalOutputNormal + bary.y * v1.portalOutputNormal + bary.z * v2.portalOutputNormal).xyz;
            vec3 portalTangent = (bary.x * v0.portalOutputTangent + bary.y * v1.portalOutputTangent + bary.z * v2.portalOutputTangent).xyz;
            vec3 portalBitangent = (bary.x * v0.portalOutputBitangent + bary.y * v1.portalOutputBitangent + bary.z * v2.portalOutputBitangent).xyz;
            vec3 newDirection = calculatePortalDirection(
                -eyeDir,
                tbn,
                mat3(normalize(portalTangent), normalize(portalBitangent), normalize(portalNormal))
            );

            color = vec3(0.0);

            payload.nextDirection  = newDirection;
            payload.nextOrigin     = portalLocation;
            payload.factor         = vec4(1.0);
        }
        else
        {
            if (reflectivity > 0.1 && !backSide)
            {
                vec3 reflectNormal = worldNormal;
                // reflectNormal = normalize(reflectNormal + mix(vec3(0.0), randomDirection(worldPos), 0.5 * roughness));

                payload.nextDirection  = reflect(-eyeDir, reflectNormal);
                payload.nextOrigin     = worldPos + 0.01 * payload.nextDirection;
                payload.factor         = vec4(vec3(reflectivity) * albedo, reflectivity);
            }

            if (alpha < 0.99)
            {
                if (alpha < 0.01 || backSide) {
                    payload.factor2   = vec4(1.0);
                }
                else {
                    payload.factor2   = vec4(1.0 - alpha) * vec4(mix(albedo, vec3(1.0), 0.1), 1.0);
                }
                // vec3 refractNormal = normalize(worldNormal);

                // if (backSide) {
                //     refractiveIndex = 1.0 / refractiveIndex;
                //     refractNormal = -refractNormal;
                // }

                payload.nextDirection2 = -eyeDir; // refract(-eyeDir, refractNormal, refractiveIndex);
                payload.nextOrigin2    = worldPos + 0.01 * -eyeDir;
            } 
        }
#ifndef NDEBUG
    }
    else {
        alpha = 1.0;
    }
#endif

#ifndef NDEBUG
    switch (u_global.debugRenderMode)
    {
        case DEBUG_RENDER_MODE_ALBEDO:
            color = albedo;
            break;
        case DEBUG_RENDER_MODE_ROUGHNESS:
            color = vec3(roughness);
            break;
        case DEBUG_RENDER_MODE_METALLIC:
            color = vec3(metallic);
            break;
        case DEBUG_RENDER_MODE_UV0:
            color = vec3(uv0, 0.0);
            break;
        case DEBUG_RENDER_MODE_UV1:
            color = vec3(uv1, 0.0);
            break;
        case DEBUG_RENDER_MODE_BARYCENTRICS:
            color = bary;
            break;
        case DEBUG_RENDER_MODE_MATERIAL_ID:
            color = DEBUG_RENDER_COLOR(meshData.materialID);
            break;
        case DEBUG_RENDER_MODE_INSTANCE_ID:
            color = DEBUG_RENDER_COLOR(gl_InstanceCustomIndexNV);
            break;
        case DEBUG_RENDER_MODE_PRIMITIVE_ID:
            color = DEBUG_RENDER_COLOR(gl_PrimitiveID);
            break;
        case DEBUG_RENDER_MODE_NORMAL_MAP:
            color = normalMapValue;
            break;
    }
#endif
    return vec4(color, alpha);
}
