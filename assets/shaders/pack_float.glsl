#ifndef PACK_FLOAT_GLSL_INCLUDED
#define PACK_FLOAT_GLSL_INCLUDED 1

#include "util.glsl"

uint packFloatToUint16(float value, float minValue, float maxValue)
{
    float relative = unmix(value, minValue, maxValue);
    return clamp(uint(65535 * relative), 0, 65535);
}

uint packVec2ToUint(vec2 value, vec2 minValues, vec2 maxValues)
{
    uint x = packFloatToUint16(value.x, minValues.x, maxValues.x);
    uint y = packFloatToUint16(value.y, minValues.y, maxValues.y);

    return (x << 16) | y;
}

float unpackFloatFromUint16(uint packed, float minValue, float maxValue)
{
    float relative = float(packed) / 65535.0;
    return mix(minValue, maxValue, relative);
}

vec2 unpackVec2FromUint(uint packed, vec2 minValues, vec2 maxValues)
{
    uint x = packed >> 16;
    uint y = packed & 0xFFFF;

    return vec2(
        unpackFloatFromUint16(x, minValues.x, maxValues.x),
        unpackFloatFromUint16(y, minValues.y, maxValues.y)
    );
}

#endif // PACK_FLOAT_GLSL_INCLUDED
