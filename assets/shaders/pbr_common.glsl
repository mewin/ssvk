
#ifndef PBR_COMMON_INCLUDED
#define PBR_COMMON_INCLUDED 1

vec3 fresnelSchlick(float cosTheta, vec3 f0)
{
    return f0 + (1.0 - f0) * pow(1.0 - cosTheta, 5.0);
}

#endif // PBR_COMMON_INCLUDED
