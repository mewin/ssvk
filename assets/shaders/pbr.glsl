#ifndef PBR_GLSL_INCLUDED
#define PBR_GLSL_INCLUDED 1

#include "common.glsl"
#include "lights.glsl"

#define SSVK_PBR_TEXTURE_INDEX_ALBEDO             0
#define SSVK_PBR_TEXTURE_INDEX_NORMAL             1
#define SSVK_PBR_TEXTURE_INDEX_METALLIC_ROUGHNESS 2
#define SSVK_PBR_TEXTURE_INDEX_AMBIENT_OCCLUSION  3

float calculateAttenuationFactor(float distance, float range, float attenuation)
{
    // f(d) = a ((d - r)^4 / r^4) + (1 - a) (1 - d / r)
    // f: factor: 0 <= f <= 1
    // d: distance: 0 <= d <= r
    // a: attenuation: 0 <= a <= 1
    // r: range: 0 < d
    
    if (distance > range) {
        return 0.0;
    }

    float factor =
        attenuation         * (pow(distance - range, 4) / pow(range, 4))
        + (1 - attenuation) * (1 - distance / range);
    return factor;
}


float distributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness * roughness;
    float a2     = a * a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH * NdotH;
	
    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return num / denom;
}

float beckmannDistribution(vec3 normal, vec3 halfway, float roughness)
{
    float nDotH = dot(normal, halfway);
    float alpha = acos(nDotH);
    float roughness2 = pow(roughness, 2);
    float numerator = exp(-pow(tan(alpha), 2) / roughness2);
    float denominator = PI * roughness2 * pow(nDotH, 4);

    return numerator / denominator;
}

float geometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r * r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
	
    return num / denom;
}
float geometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = geometrySchlickGGX(NdotV, roughness);
    float ggx1  = geometrySchlickGGX(NdotL, roughness);
	
    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 f0)
{
    return f0 + (1.0 - f0) * pow(1.0 - cosTheta, 5.0);
}


vec3 pbrPointLightIrradiance(PointLight light, vec3 worldPos, vec3 worldNormal, vec3 eyeDir, vec3 albedo, float metallic, float roughness, vec3 f0)
{
    vec3  lightColor = light.colorAndAmbient.rgb;
    float lightAmbient = light.colorAndAmbient.a;
    vec3  lightPosition = light.positionAndRange.xyz;
    float lightRange = light.positionAndRange.w;
    float lightAttenuation = light.attenuation;

    vec3 lightDir = normalize(lightPosition - worldPos);
    vec3 H = normalize(eyeDir + lightDir);
    float lightDistance = length(lightPosition - worldPos);
    float attenuationFactor = calculateAttenuationFactor(lightDistance, lightRange, lightAttenuation);
    vec3 radiance = lightColor * attenuationFactor;

    // cook-torrance brdf
    float NDF = distributionGGX(worldNormal, H, roughness);
    float G   = geometrySmith(worldNormal, eyeDir, lightDir, roughness);
    vec3  F   = fresnelSchlick(max(dot(H, eyeDir), 0.0), f0);
    
    vec3  nominator   = NDF * G * F;
    float denominator = 4.0 * max(dot(worldNormal, eyeDir), 0.0) * max(dot(worldNormal, lightDir), 0.0);
    vec3  specular    = nominator / max(denominator, 0.001);

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;
    kD *= 1.0 - metallic;

    float normDotLight = max(dot(worldNormal, lightDir), 0.0);  
    return (kD * albedo / PI + specular) * radiance * normDotLight;
}

#endif // PBR_GLSL_INCLUDED
