#ifndef SPHERICAL_CONVERSION_GLSL_INCLUDED
#define SPHERICAL_CONVERSION_GLSL_INCLUDED 1

#include "common.glsl"

// ([-PI,PI], [0, PI]
vec2 directionToSphere(vec3 direction)
{   
    float phi = atan(direction.z, direction.x);
    float theta = acos(direction.y);

    return vec2(phi, theta);
}

vec3 sphereToDirection(vec2 sphere)
{
    float phi = sphere.x;
    float theta = sphere.y;

    float x = sin(theta) * cos(phi);
    float y = cos(theta);
    float z = sin(theta) * sin(phi);

    return vec3(x, y, z);
}

#endif // SPHERICAL_CONVERSION_GLSL_INCLUDED
