#ifndef LIGHT_VISIBILITY_COMMON_GLSL_INCLUDED
#define LIGHT_VISIBILITY_COMMON_GLSL_INCLUDED 1

const uint GRID_SIZE = 128;

struct GridCell
{
    uint visibility;
};

layout(binding = 2) buffer ProjectedVertices
{
    vec4[] projectedVertices;
};

layout(std140, binding = 3) buffer Grid
{
    GridCell grid[GRID_SIZE * GRID_SIZE * GRID_SIZE];
};

#endif // LIGHT_VISIBILITY_COMMON_GLSL_INCLUDED
