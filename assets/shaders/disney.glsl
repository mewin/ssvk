#ifndef DISNEY_GLSL_INCLUDED
#define DISNEY_GLSL_INCLUDED 1

#include "pbr_common.glsl"

// implementation of Disneys "principled" BRDF

struct DisneyBRDFMaterial
{
    vec3 baseColor;
    float subsurface;
    float metallic;
    float specular;
    float specularTint;
    float roughness;
    float anisotropic;
    float sheen;
    float sheenTint;
    float clearcoat;
    float clearcoatGloss;
};

vec3 brdfDisney(vec3 surfaceNormal, vec3 lightDirection, vec3 eyeDirection, DisneyBRDFMaterial material)
{
    vec3 halfwayVector = normalize(lightDirection + eyeDirection);
    vec3 diffuse = ...;
    vec3 

    return material.baseColor;
}

#endif // DISNEY_GLSL_INCLUDED
