
#ifndef DATA_GLSL_INCLUDED
#define DATA_GLSL_INCLUDED 1

#include "ssvk.glsl"

struct MeshData
{
    uint offset;
    uint materialID;
};

struct MaterialData
{
    uint albedoTextureIndex;
    uint normalTextureIndex;
    uint metallicRoughnessTextureIndex;
    uint ambientOcclusionTextureIndex; // TODO: why is this even an own texture?
    vec4 albedoFactor;
    vec4 metallicRoughnessFactorRefractiveIndex;
};

struct VertexData
{
    vec4 position;
    vec4 color;
    vec4 normal;
    vec4 tangent;
    vec4 bitangent;
    vec4 portalOutputLocation;
    vec4 portalOutputNormal;
    vec4 portalOutputTangent;
    vec4 portalOutputBitangent;
    vec4 uv01;
};

struct PortalData
{
    vec4 position;
    vec4 normal;
};

#define SSVK_DECLARE_MESH_DATA layout(std140, binding = SSVK_BINDING_UNI_MESHDATA_BUFFER) readonly buffer MeshDataBuffer \
    { \
        MeshData[] meshes; \
    };

#define SSVK_DECLARE_VERTEX_DATA layout(std140, binding = SSVK_BINDING_UNI_VERTEXDATA_BUFFER) readonly buffer VertexDataBuffer \
    { \
        VertexData[] vertices; \
    };

#define SSVK_DECLARE_MATERIAL_DATA layout(std140, binding = SSVK_BINDING_UNI_MATERIALDATA_BUFFER) readonly buffer MaterialDataBuffer \
    { \
        MaterialData[] materials; \
    };

#define SSVK_DECLARE_TEXTURES layout(binding = SSVK_BINDING_UNI_MATERIAL_TEXTURES) uniform sampler2D u_textures[];

#endif // DATA_GLSL_INCLUDED
