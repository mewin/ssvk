
#ifndef SHADOW_TRIANGLE_CACHE_GLSL_INCLUDED
#define SHADOW_TRIANGLE_CACHE_GLSL_INCLUDED 1

#include "spherical_conversion.glsl"
#include "ssvk.glsl"
#include "util.glsl"

const uint SHADOW_TRIANGLE_CACHE_BUFFER_SIZE_HORZ = 16;
const uint SHADOW_TRIANGLE_CACHE_BUFFER_SIZE_VERT = 8;
const uint SHADOW_TRIANGLE_CACHE_BUFFER_NUM_ELES = 10;
const uint SHADOW_TRIANGLE_CACHE_BUFFER_ELEMENTS = 
        SHADOW_TRIANGLE_CACHE_BUFFER_SIZE_HORZ
        * SHADOW_TRIANGLE_CACHE_BUFFER_SIZE_VERT
        * SHADOW_TRIANGLE_CACHE_BUFFER_NUM_ELES;

struct ShadowTriangleCacheElement
{
    vec4 points[3];
    uint meshId;
    uint frame;
    uint __padding[2];
};

struct ShadowTriangleCache
{
    ShadowTriangleCacheElement elements[SHADOW_TRIANGLE_CACHE_BUFFER_ELEMENTS];
};

// #define SSVK_DECLARE_SHADOW_TRIANGLE_CACHE
layout(std140, binding = SSVK_BINDING_UNI_SDC_BUFFER) buffer ShadowTriangleCacheBuffer
{
    ShadowTriangleCache[] shadowTriangleCache;
};

bool checkTriangleCut(vec4 points[3], vec3 origin, vec3 dest)
{
    vec3 dir = dest - origin;
    // make a plane
    vec3 d0 = points[1].xyz - points[0].xyz;
    vec3 d1 = points[2].xyz - points[0].xyz;
    // vec3 n = normalize(cross(d0, d1));
    // a * d0 + b * d1 = origin + t * dir
    
}

// lightToWorldPos: vector lightPosition -> worldPosition
uint shadowTriangleCacheIndex(vec3 lightToWorldPos)
{
    vec2 spherical = directionToSphere(lightToWorldPos);
    spherical = unmix(spherical, vec2(-PI, PI), vec2(0, PI));
    spherical = mix(vec2(0, 0), vec2(SHADOW_TRIANGLE_CACHE_BUFFER_SIZE_HORZ, SHADOW_TRIANGLE_CACHE_BUFFER_SIZE_VERT), spherical);
    ivec2 idx = ivec2(spherical);
    return idx.y * SHADOW_TRIANGLE_CACHE_BUFFER_SIZE_HORZ * SHADOW_TRIANGLE_CACHE_BUFFER_NUM_ELES
         + idx.x * SHADOW_TRIANGLE_CACHE_BUFFER_NUM_ELES;
}

bool shadowTriangleCacheLookup(vec3 worldPos, vec3 lightPos, uint bufferIndex, out vec4[3] triangle)
{
    vec3 dir = worldPos - lightPos;
    uint idx = shadowTriangleCacheIndex(normalize(dir));
    
    for (int i = 0; i < SHADOW_TRIANGLE_CACHE_BUFFER_NUM_ELES; i++)
    {
        if ((shadowTriangleCache[bufferIndex].elements[idx + i].frame > 0)
                && checkTriangleCut(shadowTriangleCache[bufferIndex].elements[idx + i].points, worldPos, lightPos)) {
            return true;
        }
    }

    return false;
}

void shadowTriangleCacheInsert(vec3 worldPos, vec3 lightPos, uint bufferIndex, vec4[3] triangle)
{
    vec3 dir = worldPos - lightPos;
    uint idx = shadowTriangleCacheIndex(normalize(dir));
    
    shadowTriangleCache[bufferIndex].elements[idx].frame = 1;
}

#endif // SHADOW_TRIANGLE_CACHE_GLSL_INCLUDED
