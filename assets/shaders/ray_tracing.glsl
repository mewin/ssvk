#ifndef RAY_TRACING_GLSL_INCLUDED
#define RAY_TRACING_GLSL_INCLUDED 1

#include "pack_float.glsl"
#include "spherical_conversion.glsl"

#define SSVK_MAX_RECURSION_DEPTH 4
#define SSVK_MAX_SHADOW_PASSES   2
#define SSVK_RAY_LENGTH          100.0

struct SSVkRayTracingPayload
{
    vec4 color;
    float depth;

    vec3 nextOrigin;
    vec3 nextDirection;
    vec4 factor;

    vec3 nextOrigin2;
    vec3 nextDirection2;
    vec4 factor2;
};

struct SSVkRayTracingShadowPayload
{
    vec4 restLight;
    vec4 hitTriangle[3];
};

#endif // RAY_TRACING_GLSL_INCLUDED
