#version 460
#extension GL_ARB_separate_shader_objects : require
#extension GL_NV_ray_tracing : require
#extension GL_GOOGLE_include_directive : require
#extension GL_EXT_nonuniform_qualifier : require

#include "data.glsl"
#include "ray_tracing.glsl"

SSVK_DECLARE_MESH_DATA
SSVK_DECLARE_VERTEX_DATA
SSVK_DECLARE_MATERIAL_DATA
SSVK_DECLARE_TEXTURES

layout(location = 1) rayPayloadInNV SSVkRayTracingShadowPayload shadowHitValue;

hitAttributeNV vec2 baryCoord;

vec4 calculateColor();

MeshData meshData;
MaterialData materialData;
VertexData v0;
VertexData v1;
VertexData v2;

void main()
{
    meshData = meshes[gl_InstanceCustomIndexNV];
    materialData = materials[meshData.materialID];
    v0 = vertices[meshData.offset + 3 * gl_PrimitiveID];
    v1 = vertices[meshData.offset + 3 * gl_PrimitiveID + 1];
    v2 = vertices[meshData.offset + 3 * gl_PrimitiveID + 2];

    vec4 color = calculateColor();
    if (color.a < 0.01) {
        shadowHitValue.restLight.rgb = vec3(1.0);
    }
    else {
        shadowHitValue.restLight.rgb = vec3(1.0 - color.a) * color.rgb;
    }
    shadowHitValue.restLight.a = gl_HitTNV;
    shadowHitValue.hitTriangle[0] = v0.position;
    shadowHitValue.hitTriangle[1] = v1.position;
    shadowHitValue.hitTriangle[2] = v2.position;
}

vec4 calculateColor()
{
    vec3 eyeDir = -gl_WorldRayDirectionNV;

    // mesh properties
    vec3 bary           = vec3(1.0 - baryCoord.x - baryCoord.y, baryCoord.x, baryCoord.y);
    vec3 worldNormal    = mat3(gl_ObjectToWorldNV) * (bary.x * v0.normal + bary.y * v1.normal + bary.z * v2.normal).xyz;
    vec4 uv01           = bary.x * v0.uv01 + bary.y * v1.uv01 + bary.z * v2.uv01;
    vec2 uv0            = uv01.xy;

    // material properties
    vec4  albedoAlpha       = texture(u_textures[nonuniformEXT(materialData.albedoTextureIndex)], uv0) * materialData.albedoFactor;

    return albedoAlpha;
}