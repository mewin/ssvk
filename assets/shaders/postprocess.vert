#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : enable

// #include "ssvk.glsl"

// SSVK_DECLARE_GLOBAL_STATE

layout(location = 0) out vec2 fragUV;

const vec2[4] RECT_POINTS = vec2[](
    vec2(-1.0, -1.0),
    vec2( 1.0, -1.0),
    vec2(-1.0,  1.0),
    vec2( 1.0,  1.0)
);

void main() {
    gl_Position = vec4(RECT_POINTS[gl_VertexIndex], 0.0, 1.0);
    fragUV = 0.5 * (vec2(1.0) + RECT_POINTS[gl_VertexIndex]);
}
