#version 460
#extension GL_NV_ray_tracing : require

layout(location = 1) rayPayloadInNV vec4 shadowHitValue;

void main()
{
    shadowHitValue = vec4(1.0, 1.0, 1.0, gl_RayTmaxNV);
}
