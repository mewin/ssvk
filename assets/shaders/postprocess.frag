#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : enable

#include "ray_tracing.glsl" // SSVK_RAY_LENGTH
#include "ssvk.glsl"

SSVK_DECLARE_GLOBAL_STATE

layout(input_attachment_index = 3, binding = SSVK_BINDING_IA_COLOR)
uniform subpassInput ia_color;

layout(location = 0) in vec2 fragUV;
layout(location = 0) out vec4 out_color;

void main()
{
    vec3 hdrColor = subpassLoad(ia_color).rgb;
    float depth = subpassLoad(ia_color).a / SSVK_RAY_LENGTH;

    const float EXPOSURE = 0.2;
    const float GAMMA = 2.2;

    // tone mapping
    vec3 mapped = vec3(1.0) - exp(-hdrColor * EXPOSURE);
    
    // gamma correction
    // not necessary if writing to a SRGB swapchain image
    // TODO: detect the above fact and do conversion if necessary
    // mapped = pow(mapped, vec3(1.0 / GAMMA));

    out_color = vec4(mapped, 1.0);

#ifndef NDEBUG
    if ((u_global.debugRenderFlags & DEBUG_RENDER_FLAG_PPROCESS_SKIP) != 0)
    {
        out_color = vec4(hdrColor, 1.0);
    }
    else if ((u_global.debugRenderFlags & DEBUG_RENDER_FLAG_PPROCESS_SPLIT) != 0 && fragUV.x > 0.5)
    {
        out_color = vec4(hdrColor, 1.0);
    }
    switch (u_global.debugRenderMode)
    {
        case DEBUG_RENDER_MODE_DEPTH:
            out_color = vec4(depth, depth, depth, 1.0);
            break;
    }
#endif
}
