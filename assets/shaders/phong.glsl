#ifndef PHONG_GLSL_INCLUDED
#define PHONG_GLSL_INCLUDED 1

#include "lights.glsl"

vec3 phongDirectional(DirectionalLight light, vec3 fragPos, vec3 fragColor, vec3 fragNormal, vec3 eyePos)
{
    const float SPECULAR_STRENTH = 0.5;

    vec3 ambient = light.ambient * light.color * fragColor;
    float diffuseFactor = max(dot(fragNormal, light.direction), 0.0);
    vec3 diffuse = diffuseFactor * light.color * fragColor;

    vec3 viewDir = normalize(eyePos - fragPos);
    vec3 reflectDir = reflect(-light.direction, fragNormal);
    float specularFactor = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = SPECULAR_STRENTH * specularFactor * light.color;

    return specular + diffuse + ambient;
}

vec3 phongPoint(PointLight light, vec3 fragPos, vec3 fragColor, vec3 fragNormal, vec3 eyePos)
{
    vec3 color = light.colorAndAmbient.rgb;
    float ambient = light.colorAndAmbient.a;
    vec3 position = light.positionAndRange.xyz;
    float range = light.positionAndRange.w;
    float attenuation = light.attenuation;

    DirectionalLight dirLight = DirectionalLight(color, normalize(fragPos - position), ambient);
    vec3 dirColor = phongDirectional(dirLight, fragPos, fragColor, fragNormal, eyePos);
    float distance = length(fragPos - position);
    if (distance >= range) {
        return vec3(0.0);
    }

    // f(d) = a ((d - r)^4 / r^4) + (1 - a) (1 - d / r)
    // f: factor: 0 <= f <= 1
    // d: distance: 0 <= d <= r
    // a: attenuation: 0 <= a <= 1
    // r: range: 0 < d
    float factor =
        attenuation         * (pow(distance - range, 4) / pow(range, 4))
        + (1 - attenuation) * (1 - distance / range);
    vec3 result = factor * dirColor;
    return result;
}

#endif // PHONG_GLSL_INCLUDED
