#ifndef UTIL_GLSL_INCLUDED
#define UTIL_GLSL_INCLUDED 1

float unmix(float value, float minValue, float maxValue)
{
    return (value - minValue) / (maxValue - minValue);
}

vec2 unmix(vec2 value, vec2 minValue, vec2 maxValue)
{
    return (value - minValue) / (maxValue - minValue);
}

vec3 unmix(vec3 value, vec3 minValue, vec3 maxValue)
{
    return (value - minValue) / (maxValue - minValue);
}

vec4 unmix(vec4 value, vec4 minValue, vec4 maxValue)
{
    return (value - minValue) / (maxValue - minValue);
}

float luminance(vec3 rgb)
{
    return 0.2126 * rgb.r + 0.7152 * rgb.g + 0.0723 * rgb.b;
}

// 
//get a scalar random value from a 3d value
float rand3dTo1d(vec3 value, vec3 dotDir)
{
    //make value smaller to avoid artefacts
    vec3 smallValue = sin(value);
    //get scalar value from 3d vector
    float random = dot(smallValue, dotDir);
    //make value more random by making it bigger and then taking teh factional part
    random = fract(sin(random) * 143758.5453);
    return random;
}

float rand3dTo1d(vec3 value)
{
    return rand3dTo1d(value, vec3(12.9898, 78.233, 37.719));
}

//get a 3d random value from a 3d value
vec3 rand3dTo3d(vec3 value){
    return vec3(
        rand3dTo1d(value, vec3(12.989, 78.233, 37.719)),
        rand3dTo1d(value, vec3(39.346, 11.135, 83.155)),
        rand3dTo1d(value, vec3(73.156, 52.235, 09.151))
    );
}

vec3 randomDirection(vec3 seed)
{
    return vec3(1.0) - 2.0 * rand3dTo3d(seed);
}

#endif // UTIL_GLSL_INCLUDED
