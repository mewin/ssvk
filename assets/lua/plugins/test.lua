
local module = {}

local function test(i)
    return i, i * i, i * i * i
end

local function lel(...)
    print(...)
end

function module.initialize(application)
    print(application.window)
    application.testFunc(test)

    application.window.connect("key_down", lel)
end

return module
