module spirv.tools;

// Copyright (c) 2015-2020 The Khronos Group Inc.
// Modifications Copyright (C) 2020 Advanced Micro Devices, Inc. All rights
// reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern (C):

// Helpers

extern (D) auto SPV_BIT(T)(auto ref T shift)
{
    return 1 << shift;
}

// Enumerations

enum spv_result_t
{
    SPV_SUCCESS = 0,
    SPV_UNSUPPORTED = 1,
    SPV_END_OF_STREAM = 2,
    SPV_WARNING = 3,
    SPV_FAILED_MATCH = 4,
    SPV_REQUESTED_TERMINATION = 5, // Success, but signals early termination.
    SPV_ERROR_INTERNAL = -1,
    SPV_ERROR_OUT_OF_MEMORY = -2,
    SPV_ERROR_INVALID_POINTER = -3,
    SPV_ERROR_INVALID_BINARY = -4,
    SPV_ERROR_INVALID_TEXT = -5,
    SPV_ERROR_INVALID_TABLE = -6,
    SPV_ERROR_INVALID_VALUE = -7,
    SPV_ERROR_INVALID_DIAGNOSTIC = -8,
    SPV_ERROR_INVALID_LOOKUP = -9,
    SPV_ERROR_INVALID_ID = -10,
    SPV_ERROR_INVALID_CFG = -11,
    SPV_ERROR_INVALID_LAYOUT = -12,
    SPV_ERROR_INVALID_CAPABILITY = -13,
    SPV_ERROR_INVALID_DATA = -14, // Indicates data rules validation failure.
    SPV_ERROR_MISSING_EXTENSION = -15,
    SPV_ERROR_WRONG_VERSION = -16, // Indicates wrong SPIR-V version
    _spv_result_t = 2147483647
}

// Severity levels of messages communicated to the consumer.
enum spv_message_level_t
{
    SPV_MSG_FATAL = 0, // Unrecoverable error due to environment.
    // Will exit the program immediately. E.g.,
    // out of memory.
    SPV_MSG_INTERNAL_ERROR = 1, // Unrecoverable error due to SPIRV-Tools
    // internals.
    // Will exit the program immediately. E.g.,
    // unimplemented feature.
    SPV_MSG_ERROR = 2, // Normal error due to user input.
    SPV_MSG_WARNING = 3, // Warning information.
    SPV_MSG_INFO = 4, // General information.
    SPV_MSG_DEBUG = 5 // Debug information.
}

enum spv_endianness_t
{
    SPV_ENDIANNESS_LITTLE = 0,
    SPV_ENDIANNESS_BIG = 1,
    _spv_endianness_t = 2147483647
}

// The kinds of operands that an instruction may have.
//
// Some operand types are "concrete".  The binary parser uses a concrete
// operand type to describe an operand of a parsed instruction.
//
// The assembler uses all operand types.  In addition to determining what
// kind of value an operand may be, non-concrete operand types capture the
// fact that an operand might be optional (may be absent, or present exactly
// once), or might occur zero or more times.
//
// Sometimes we also need to be able to express the fact that an operand
// is a member of an optional tuple of values.  In that case the first member
// would be optional, and the subsequent members would be required.
enum spv_operand_type_t
{
    // A sentinel value.
    SPV_OPERAND_TYPE_NONE = 0,

    // Set 1:  Operands that are IDs.
    SPV_OPERAND_TYPE_ID = 1,
    SPV_OPERAND_TYPE_TYPE_ID = 2,
    SPV_OPERAND_TYPE_RESULT_ID = 3,
    SPV_OPERAND_TYPE_MEMORY_SEMANTICS_ID = 4, // SPIR-V Sec 3.25
    SPV_OPERAND_TYPE_SCOPE_ID = 5, // SPIR-V Sec 3.27

    // Set 2:  Operands that are literal numbers.
    SPV_OPERAND_TYPE_LITERAL_INTEGER = 6, // Always unsigned 32-bits.
    // The Instruction argument to OpExtInst. It's an unsigned 32-bit literal
    // number indicating which instruction to use from an extended instruction
    // set.
    SPV_OPERAND_TYPE_EXTENSION_INSTRUCTION_NUMBER = 7,
    // The Opcode argument to OpSpecConstantOp. It determines the operation
    // to be performed on constant operands to compute a specialization constant
    // result.
    SPV_OPERAND_TYPE_SPEC_CONSTANT_OP_NUMBER = 8,
    // A literal number whose format and size are determined by a previous operand
    // in the same instruction.  It's a signed integer, an unsigned integer, or a
    // floating point number.  It also has a specified bit width.  The width
    // may be larger than 32, which would require such a typed literal value to
    // occupy multiple SPIR-V words.
    SPV_OPERAND_TYPE_TYPED_LITERAL_NUMBER = 9,

    // Set 3:  The literal string operand type.
    SPV_OPERAND_TYPE_LITERAL_STRING = 10,

    // Set 4:  Operands that are a single word enumerated value.
    SPV_OPERAND_TYPE_SOURCE_LANGUAGE = 11, // SPIR-V Sec 3.2
    SPV_OPERAND_TYPE_EXECUTION_MODEL = 12, // SPIR-V Sec 3.3
    SPV_OPERAND_TYPE_ADDRESSING_MODEL = 13, // SPIR-V Sec 3.4
    SPV_OPERAND_TYPE_MEMORY_MODEL = 14, // SPIR-V Sec 3.5
    SPV_OPERAND_TYPE_EXECUTION_MODE = 15, // SPIR-V Sec 3.6
    SPV_OPERAND_TYPE_STORAGE_CLASS = 16, // SPIR-V Sec 3.7
    SPV_OPERAND_TYPE_DIMENSIONALITY = 17, // SPIR-V Sec 3.8
    SPV_OPERAND_TYPE_SAMPLER_ADDRESSING_MODE = 18, // SPIR-V Sec 3.9
    SPV_OPERAND_TYPE_SAMPLER_FILTER_MODE = 19, // SPIR-V Sec 3.10
    SPV_OPERAND_TYPE_SAMPLER_IMAGE_FORMAT = 20, // SPIR-V Sec 3.11
    SPV_OPERAND_TYPE_IMAGE_CHANNEL_ORDER = 21, // SPIR-V Sec 3.12
    SPV_OPERAND_TYPE_IMAGE_CHANNEL_DATA_TYPE = 22, // SPIR-V Sec 3.13
    SPV_OPERAND_TYPE_FP_ROUNDING_MODE = 23, // SPIR-V Sec 3.16
    SPV_OPERAND_TYPE_LINKAGE_TYPE = 24, // SPIR-V Sec 3.17
    SPV_OPERAND_TYPE_ACCESS_QUALIFIER = 25, // SPIR-V Sec 3.18
    SPV_OPERAND_TYPE_FUNCTION_PARAMETER_ATTRIBUTE = 26, // SPIR-V Sec 3.19
    SPV_OPERAND_TYPE_DECORATION = 27, // SPIR-V Sec 3.20
    SPV_OPERAND_TYPE_BUILT_IN = 28, // SPIR-V Sec 3.21
    SPV_OPERAND_TYPE_GROUP_OPERATION = 29, // SPIR-V Sec 3.28
    SPV_OPERAND_TYPE_KERNEL_ENQ_FLAGS = 30, // SPIR-V Sec 3.29
    SPV_OPERAND_TYPE_KERNEL_PROFILING_INFO = 31, // SPIR-V Sec 3.30
    SPV_OPERAND_TYPE_CAPABILITY = 32, // SPIR-V Sec 3.31
    SPV_OPERAND_TYPE_RAY_FLAGS = 33, // SPIR-V Sec 3.RF
    SPV_OPERAND_TYPE_RAY_QUERY_INTERSECTION = 34, // SPIR-V Sec 3.RQIntersection
    SPV_OPERAND_TYPE_RAY_QUERY_COMMITTED_INTERSECTION_TYPE = 35, // SPIR-V Sec
    // 3.RQCommitted
    SPV_OPERAND_TYPE_RAY_QUERY_CANDIDATE_INTERSECTION_TYPE = 36, // SPIR-V Sec
    // 3.RQCandidate

    // Set 5:  Operands that are a single word bitmask.
    // Sometimes a set bit indicates the instruction requires still more operands.
    SPV_OPERAND_TYPE_IMAGE = 37, // SPIR-V Sec 3.14
    SPV_OPERAND_TYPE_FP_FAST_MATH_MODE = 38, // SPIR-V Sec 3.15
    SPV_OPERAND_TYPE_SELECTION_CONTROL = 39, // SPIR-V Sec 3.22
    SPV_OPERAND_TYPE_LOOP_CONTROL = 40, // SPIR-V Sec 3.23
    SPV_OPERAND_TYPE_FUNCTION_CONTROL = 41, // SPIR-V Sec 3.24
    SPV_OPERAND_TYPE_MEMORY_ACCESS = 42, // SPIR-V Sec 3.26

    // The remaining operand types are only used internally by the assembler.
    // There are two categories:
    //    Optional : expands to 0 or 1 operand, like ? in regular expressions.
    //    Variable : expands to 0, 1 or many operands or pairs of operands.
    //               This is similar to * in regular expressions.

    // Macros for defining bounds on optional and variable operand types.
    // Any variable operand type is also optional.

    // An optional operand represents zero or one logical operands.
    // In an instruction definition, this may only appear at the end of the
    // operand types.
    SPV_OPERAND_TYPE_OPTIONAL_ID = 43,
    SPV_OPERAND_TYPE_FIRST_OPTIONAL_TYPE = 43,
    // An optional image operand type.
    SPV_OPERAND_TYPE_OPTIONAL_IMAGE = 44,
    // An optional memory access type.
    SPV_OPERAND_TYPE_OPTIONAL_MEMORY_ACCESS = 45,
    // An optional literal integer.
    SPV_OPERAND_TYPE_OPTIONAL_LITERAL_INTEGER = 46,
    // An optional literal number, which may be either integer or floating point.
    SPV_OPERAND_TYPE_OPTIONAL_LITERAL_NUMBER = 47,
    // Like SPV_OPERAND_TYPE_TYPED_LITERAL_NUMBER, but optional, and integral.
    SPV_OPERAND_TYPE_OPTIONAL_TYPED_LITERAL_INTEGER = 48,
    // An optional literal string.
    SPV_OPERAND_TYPE_OPTIONAL_LITERAL_STRING = 49,
    // An optional access qualifier
    SPV_OPERAND_TYPE_OPTIONAL_ACCESS_QUALIFIER = 50,
    // An optional context-independent value, or CIV.  CIVs are tokens that we can
    // assemble regardless of where they occur -- literals, IDs, immediate
    // integers, etc.
    SPV_OPERAND_TYPE_OPTIONAL_CIV = 51,

    // A variable operand represents zero or more logical operands.
    // In an instruction definition, this may only appear at the end of the
    // operand types.
    SPV_OPERAND_TYPE_VARIABLE_ID = 52,
    SPV_OPERAND_TYPE_FIRST_VARIABLE_TYPE = 52,
    SPV_OPERAND_TYPE_VARIABLE_LITERAL_INTEGER = 53,
    // A sequence of zero or more pairs of (typed literal integer, Id).
    // Expands to zero or more:
    //  (SPV_OPERAND_TYPE_TYPED_LITERAL_INTEGER, SPV_OPERAND_TYPE_ID)
    // where the literal number must always be an integer of some sort.
    SPV_OPERAND_TYPE_VARIABLE_LITERAL_INTEGER_ID = 54,
    // A sequence of zero or more pairs of (Id, Literal integer)
    SPV_OPERAND_TYPE_VARIABLE_ID_LITERAL_INTEGER = 55,
    SPV_OPERAND_TYPE_LAST_VARIABLE_TYPE = 55,
    SPV_OPERAND_TYPE_LAST_OPTIONAL_TYPE = 55,

    // The following are concrete enum types from the DebugInfo extended
    // instruction set.
    SPV_OPERAND_TYPE_DEBUG_INFO_FLAGS = 56, // DebugInfo Sec 3.2.  A mask.
    SPV_OPERAND_TYPE_DEBUG_BASE_TYPE_ATTRIBUTE_ENCODING = 57, // DebugInfo Sec 3.3
    SPV_OPERAND_TYPE_DEBUG_COMPOSITE_TYPE = 58, // DebugInfo Sec 3.4
    SPV_OPERAND_TYPE_DEBUG_TYPE_QUALIFIER = 59, // DebugInfo Sec 3.5
    SPV_OPERAND_TYPE_DEBUG_OPERATION = 60, // DebugInfo Sec 3.6

    // The following are concrete enum types from the OpenCL.DebugInfo.100
    // extended instruction set.
    SPV_OPERAND_TYPE_CLDEBUG100_DEBUG_INFO_FLAGS = 61, // Sec 3.2. A Mask
    SPV_OPERAND_TYPE_CLDEBUG100_DEBUG_BASE_TYPE_ATTRIBUTE_ENCODING = 62, // Sec 3.3
    SPV_OPERAND_TYPE_CLDEBUG100_DEBUG_COMPOSITE_TYPE = 63, // Sec 3.4
    SPV_OPERAND_TYPE_CLDEBUG100_DEBUG_TYPE_QUALIFIER = 64, // Sec 3.5
    SPV_OPERAND_TYPE_CLDEBUG100_DEBUG_OPERATION = 65, // Sec 3.6
    SPV_OPERAND_TYPE_CLDEBUG100_DEBUG_IMPORTED_ENTITY = 66, // Sec 3.7

    // This is a sentinel value, and does not represent an operand type.
    // It should come last.
    SPV_OPERAND_TYPE_NUM_OPERAND_TYPES = 67,

    _spv_operand_type_t = 2147483647
}

enum spv_ext_inst_type_t
{
    SPV_EXT_INST_TYPE_NONE = 0,
    SPV_EXT_INST_TYPE_GLSL_STD_450 = 1,
    SPV_EXT_INST_TYPE_OPENCL_STD = 2,
    SPV_EXT_INST_TYPE_SPV_AMD_SHADER_EXPLICIT_VERTEX_PARAMETER = 3,
    SPV_EXT_INST_TYPE_SPV_AMD_SHADER_TRINARY_MINMAX = 4,
    SPV_EXT_INST_TYPE_SPV_AMD_GCN_SHADER = 5,
    SPV_EXT_INST_TYPE_SPV_AMD_SHADER_BALLOT = 6,
    SPV_EXT_INST_TYPE_DEBUGINFO = 7,
    SPV_EXT_INST_TYPE_OPENCL_DEBUGINFO_100 = 8,

    // Multiple distinct extended instruction set types could return this
    // value, if they are prefixed with NonSemantic. and are otherwise
    // unrecognised
    SPV_EXT_INST_TYPE_NONSEMANTIC_UNKNOWN = 9,

    _spv_ext_inst_type_t = 2147483647
}

// This determines at a high level the kind of a binary-encoded literal
// number, but not the bit width.
// In principle, these could probably be folded into new entries in
// spv_operand_type_t.  But then we'd have some special case differences
// between the assembler and disassembler.
enum spv_number_kind_t
{
    SPV_NUMBER_NONE = 0, // The default for value initialization.
    SPV_NUMBER_UNSIGNED_INT = 1,
    SPV_NUMBER_SIGNED_INT = 2,
    SPV_NUMBER_FLOATING = 3
}

enum spv_text_to_binary_options_t
{
    SPV_TEXT_TO_BINARY_OPTION_NONE = SPV_BIT(0),
    // Numeric IDs in the binary will have the same values as in the source.
    // Non-numeric IDs are allocated by filling in the gaps, starting with 1
    // and going up.
    SPV_TEXT_TO_BINARY_OPTION_PRESERVE_NUMERIC_IDS = SPV_BIT(1),
    _spv_text_to_binary_options_t = 2147483647
}

enum spv_binary_to_text_options_t
{
    SPV_BINARY_TO_TEXT_OPTION_NONE = SPV_BIT(0),
    SPV_BINARY_TO_TEXT_OPTION_PRINT = SPV_BIT(1),
    SPV_BINARY_TO_TEXT_OPTION_COLOR = SPV_BIT(2),
    SPV_BINARY_TO_TEXT_OPTION_INDENT = SPV_BIT(3),
    SPV_BINARY_TO_TEXT_OPTION_SHOW_BYTE_OFFSET = SPV_BIT(4),
    // Do not output the module header as leading comments in the assembly.
    SPV_BINARY_TO_TEXT_OPTION_NO_HEADER = SPV_BIT(5),
    // Use friendly names where possible.  The heuristic may expand over
    // time, but will use common names for scalar types, and debug names from
    // OpName instructions.
    SPV_BINARY_TO_TEXT_OPTION_FRIENDLY_NAMES = SPV_BIT(6),
    _spv_binary_to_text_options_t = 2147483647
}

// Constants

// The default id bound is to the minimum value for the id limit
// in the spir-v specification under the section "Universal Limits".
extern __gshared const uint kDefaultMaxIdBound;

// Structures

// Information about an operand parsed from a binary SPIR-V module.
// Note that the values are not included.  You still need access to the binary
// to extract the values.
struct spv_parsed_operand_t
{
    // Location of the operand, in words from the start of the instruction.
    ushort offset;
    // Number of words occupied by this operand.
    ushort num_words;
    // The "concrete" operand type.  See the definition of spv_operand_type_t
    // for details.
    spv_operand_type_t type;
    // If type is a literal number type, then number_kind says whether it's
    // a signed integer, an unsigned integer, or a floating point number.
    spv_number_kind_t number_kind;
    // The number of bits for a literal number type.
    uint number_bit_width;
}

// An instruction parsed from a binary SPIR-V module.
struct spv_parsed_instruction_t
{
    // An array of words for this instruction, in native endianness.
    const(uint)* words;
    // The number of words in this instruction.
    ushort num_words;
    ushort opcode;
    // The extended instruction type, if opcode is OpExtInst.  Otherwise
    // this is the "none" value.
    spv_ext_inst_type_t ext_inst_type;
    // The type id, or 0 if this instruction doesn't have one.
    uint type_id;
    // The result id, or 0 if this instruction doesn't have one.
    uint result_id;
    // The array of parsed operands.
    const(spv_parsed_operand_t)* operands;
    ushort num_operands;
}

struct spv_const_binary_t
{
    const(uint)* code;
    const size_t wordCount;
}

struct spv_binary_t
{
    uint* code;
    size_t wordCount;
}

struct spv_text_t
{
    const(char)* str;
    size_t length;
}

struct spv_position_t
{
    size_t line;
    size_t column;
    size_t index;
}

struct spv_diagnostic_t
{
    spv_position_t position;
    char* error;
    bool isTextSource;
}

// Opaque struct containing the context used to operate on a SPIR-V module.
// Its object is used by various translation API functions.
struct spv_context_t;

struct spv_validator_options_t;

struct spv_optimizer_options_t;

struct spv_reducer_options_t;

struct spv_fuzzer_options_t;

// Type Definitions

alias spv_const_binary = spv_const_binary_t*;
alias spv_binary = spv_binary_t*;
alias spv_text = spv_text_t*;
alias spv_position = spv_position_t*;
alias spv_diagnostic = spv_diagnostic_t*;
alias spv_const_context = const(spv_context_t)*;
alias spv_context = spv_context_t*;
alias spv_validator_options = spv_validator_options_t*;
alias spv_const_validator_options = const(spv_validator_options_t)*;
alias spv_optimizer_options = spv_optimizer_options_t*;
alias spv_const_optimizer_options = const(spv_optimizer_options_t)*;
alias spv_reducer_options = spv_reducer_options_t*;
alias spv_const_reducer_options = const(spv_reducer_options_t)*;
alias spv_fuzzer_options = spv_fuzzer_options_t*;
alias spv_const_fuzzer_options = const(spv_fuzzer_options_t)*;

// Platform API

// Returns the SPIRV-Tools software version as a null-terminated string.
// The contents of the underlying storage is valid for the remainder of
// the process.
const(char)* spvSoftwareVersionString ();
// Returns a null-terminated string containing the name of the project,
// the software version string, and commit details.
// The contents of the underlying storage is valid for the remainder of
// the process.
const(char)* spvSoftwareVersionDetailsString ();

// Certain target environments impose additional restrictions on SPIR-V, so it's
// often necessary to specify which one applies.  SPV_ENV_UNIVERSAL_* implies an
// environment-agnostic SPIR-V.
//
// When an API method needs to derive a SPIR-V version from a target environment
// (from the spv_context object), the method will choose the highest version of
// SPIR-V supported by the target environment.  Examples:
//    SPV_ENV_VULKAN_1_0           ->  SPIR-V 1.0
//    SPV_ENV_VULKAN_1_1           ->  SPIR-V 1.3
//    SPV_ENV_VULKAN_1_1_SPIRV_1_4 ->  SPIR-V 1.4
//    SPV_ENV_VULKAN_1_2           ->  SPIR-V 1.5
// Consult the description of API entry points for specific rules.
enum spv_target_env
{
    SPV_ENV_UNIVERSAL_1_0 = 0, // SPIR-V 1.0 latest revision, no other restrictions.
    SPV_ENV_VULKAN_1_0 = 1, // Vulkan 1.0 latest revision.
    SPV_ENV_UNIVERSAL_1_1 = 2, // SPIR-V 1.1 latest revision, no other restrictions.
    SPV_ENV_OPENCL_2_1 = 3, // OpenCL Full Profile 2.1 latest revision.
    SPV_ENV_OPENCL_2_2 = 4, // OpenCL Full Profile 2.2 latest revision.
    SPV_ENV_OPENGL_4_0 = 5, // OpenGL 4.0 plus GL_ARB_gl_spirv, latest revisions.
    SPV_ENV_OPENGL_4_1 = 6, // OpenGL 4.1 plus GL_ARB_gl_spirv, latest revisions.
    SPV_ENV_OPENGL_4_2 = 7, // OpenGL 4.2 plus GL_ARB_gl_spirv, latest revisions.
    SPV_ENV_OPENGL_4_3 = 8, // OpenGL 4.3 plus GL_ARB_gl_spirv, latest revisions.
    // There is no variant for OpenGL 4.4.
    SPV_ENV_OPENGL_4_5 = 9, // OpenGL 4.5 plus GL_ARB_gl_spirv, latest revisions.
    SPV_ENV_UNIVERSAL_1_2 = 10, // SPIR-V 1.2, latest revision, no other restrictions.
    SPV_ENV_OPENCL_1_2 = 11, // OpenCL Full Profile 1.2 plus cl_khr_il_program,
    // latest revision.
    SPV_ENV_OPENCL_EMBEDDED_1_2 = 12, // OpenCL Embedded Profile 1.2 plus
    // cl_khr_il_program, latest revision.
    SPV_ENV_OPENCL_2_0 = 13, // OpenCL Full Profile 2.0 plus cl_khr_il_program,
    // latest revision.
    SPV_ENV_OPENCL_EMBEDDED_2_0 = 14, // OpenCL Embedded Profile 2.0 plus
    // cl_khr_il_program, latest revision.
    SPV_ENV_OPENCL_EMBEDDED_2_1 = 15, // OpenCL Embedded Profile 2.1 latest revision.
    SPV_ENV_OPENCL_EMBEDDED_2_2 = 16, // OpenCL Embedded Profile 2.2 latest revision.
    SPV_ENV_UNIVERSAL_1_3 = 17, // SPIR-V 1.3 latest revision, no other restrictions.
    SPV_ENV_VULKAN_1_1 = 18, // Vulkan 1.1 latest revision.
    SPV_ENV_WEBGPU_0 = 19, // Work in progress WebGPU 1.0.
    SPV_ENV_UNIVERSAL_1_4 = 20, // SPIR-V 1.4 latest revision, no other restrictions.

    // Vulkan 1.1 with VK_KHR_spirv_1_4, i.e. SPIR-V 1.4 binary.
    SPV_ENV_VULKAN_1_1_SPIRV_1_4 = 21,

    SPV_ENV_UNIVERSAL_1_5 = 22, // SPIR-V 1.5 latest revision, no other restrictions.
    SPV_ENV_VULKAN_1_2 = 23 // Vulkan 1.2 latest revision.
}

// SPIR-V Validator can be parameterized with the following Universal Limits.
enum spv_validator_limit
{
    spv_validator_limit_max_struct_members = 0,
    spv_validator_limit_max_struct_depth = 1,
    spv_validator_limit_max_local_variables = 2,
    spv_validator_limit_max_global_variables = 3,
    spv_validator_limit_max_switch_branches = 4,
    spv_validator_limit_max_function_args = 5,
    spv_validator_limit_max_control_flow_nesting_depth = 6,
    spv_validator_limit_max_access_chain_indexes = 7,
    spv_validator_limit_max_id_bound = 8
}

// Returns a string describing the given SPIR-V target environment.
const(char)* spvTargetEnvDescription (spv_target_env env);

// Parses s into *env and returns true if successful.  If unparsable, returns
// false and sets *env to SPV_ENV_UNIVERSAL_1_0.
bool spvParseTargetEnv (const(char)* s, spv_target_env* env);

// Determines the target env value with the least features but which enables
// the given Vulkan and SPIR-V versions. If such a target is supported, returns
// true and writes the value to |env|, otherwise returns false.
//
// The Vulkan version is given as an unsigned 32-bit number as specified in
// Vulkan section "29.2.1 Version Numbers": the major version number appears
// in bits 22 to 21, and the minor version is in bits 12 to 21.  The SPIR-V
// version is given in the SPIR-V version header word: major version in bits
// 16 to 23, and minor version in bits 8 to 15.
bool spvParseVulkanEnv (uint vulkan_ver, uint spirv_ver, spv_target_env* env);

// Creates a context object for most of the SPIRV-Tools API.
// Returns null if env is invalid.
//
// See specific API calls for how the target environment is interpeted
// (particularly assembly and validation).
spv_context spvContextCreate (spv_target_env env);

// Destroys the given context object.
void spvContextDestroy (spv_context context);

// Creates a Validator options object with default options. Returns a valid
// options object. The object remains valid until it is passed into
// spvValidatorOptionsDestroy.
spv_validator_options spvValidatorOptionsCreate ();

// Destroys the given Validator options object.
void spvValidatorOptionsDestroy (spv_validator_options options);

// Records the maximum Universal Limit that is considered valid in the given
// Validator options object. <options> argument must be a valid options object.
void spvValidatorOptionsSetUniversalLimit (
    spv_validator_options options,
    spv_validator_limit limit_type,
    uint limit);

// Record whether or not the validator should relax the rules on types for
// stores to structs.  When relaxed, it will allow a type mismatch as long as
// the types are structs with the same layout.  Two structs have the same layout
// if
//
// 1) the members of the structs are either the same type or are structs with
// same layout, and
//
// 2) the decorations that affect the memory layout are identical for both
// types.  Other decorations are not relevant.
void spvValidatorOptionsSetRelaxStoreStruct (
    spv_validator_options options,
    bool val);

// Records whether or not the validator should relax the rules on pointer usage
// in logical addressing mode.
//
// When relaxed, it will allow the following usage cases of pointers:
// 1) OpVariable allocating an object whose type is a pointer type
// 2) OpReturnValue returning a pointer value
void spvValidatorOptionsSetRelaxLogicalPointer (
    spv_validator_options options,
    bool val);

// Records whether or not the validator should relax the rules because it is
// expected that the optimizations will make the code legal.
//
// When relaxed, it will allow the following:
// 1) It will allow relaxed logical pointers.  Setting this option will also
//    set that option.
// 2) Pointers that are pass as parameters to function calls do not have to
//    match the storage class of the formal parameter.
// 3) Pointers that are actaul parameters on function calls do not have to point
//    to the same type pointed as the formal parameter.  The types just need to
//    logically match.
void spvValidatorOptionsSetBeforeHlslLegalization (
    spv_validator_options options,
    bool val);

// Records whether the validator should use "relaxed" block layout rules.
// Relaxed layout rules are described by Vulkan extension
// VK_KHR_relaxed_block_layout, and they affect uniform blocks, storage blocks,
// and push constants.
//
// This is enabled by default when targeting Vulkan 1.1 or later.
// Relaxed layout is more permissive than the default rules in Vulkan 1.0.
void spvValidatorOptionsSetRelaxBlockLayout (
    spv_validator_options options,
    bool val);

// Records whether the validator should use standard block layout rules for
// uniform blocks.
void spvValidatorOptionsSetUniformBufferStandardLayout (
    spv_validator_options options,
    bool val);

// Records whether the validator should use "scalar" block layout rules.
// Scalar layout rules are more permissive than relaxed block layout.
//
// See Vulkan extnesion VK_EXT_scalar_block_layout.  The scalar alignment is
// defined as follows:
// - scalar alignment of a scalar is the scalar size
// - scalar alignment of a vector is the scalar alignment of its component
// - scalar alignment of a matrix is the scalar alignment of its component
// - scalar alignment of an array is the scalar alignment of its element
// - scalar alignment of a struct is the max scalar alignment among its
//   members
//
// For a struct in Uniform, StorageClass, or PushConstant:
// - a member Offset must be a multiple of the member's scalar alignment
// - ArrayStride or MatrixStride must be a multiple of the array or matrix
//   scalar alignment
void spvValidatorOptionsSetScalarBlockLayout (
    spv_validator_options options,
    bool val);

// Records whether or not the validator should skip validating standard
// uniform/storage block layout.
void spvValidatorOptionsSetSkipBlockLayout (
    spv_validator_options options,
    bool val);

// Creates an optimizer options object with default options. Returns a valid
// options object. The object remains valid until it is passed into
// |spvOptimizerOptionsDestroy|.
spv_optimizer_options spvOptimizerOptionsCreate ();

// Destroys the given optimizer options object.
void spvOptimizerOptionsDestroy (spv_optimizer_options options);

// Records whether or not the optimizer should run the validator before
// optimizing.  If |val| is true, the validator will be run.
void spvOptimizerOptionsSetRunValidator (
    spv_optimizer_options options,
    bool val);

// Records the validator options that should be passed to the validator if it is
// run.
void spvOptimizerOptionsSetValidatorOptions (
    spv_optimizer_options options,
    spv_validator_options val);

// Records the maximum possible value for the id bound.
void spvOptimizerOptionsSetMaxIdBound (spv_optimizer_options options, uint val);

// Records whether all bindings within the module should be preserved.
void spvOptimizerOptionsSetPreserveBindings (
    spv_optimizer_options options,
    bool val);

// Records whether all specialization constants within the module
// should be preserved.
void spvOptimizerOptionsSetPreserveSpecConstants (
    spv_optimizer_options options,
    bool val);

// Creates a reducer options object with default options. Returns a valid
// options object. The object remains valid until it is passed into
// |spvReducerOptionsDestroy|.
spv_reducer_options spvReducerOptionsCreate ();

// Destroys the given reducer options object.
void spvReducerOptionsDestroy (spv_reducer_options options);

// Sets the maximum number of reduction steps that should run before the reducer
// gives up.
void spvReducerOptionsSetStepLimit (
    spv_reducer_options options,
    uint step_limit);

// Sets the fail-on-validation-error option; if true, the reducer will return
// kStateInvalid if a reduction step yields a state that fails SPIR-V
// validation. Otherwise, an invalid state is treated as uninteresting and the
// reduction backtracks and continues.
void spvReducerOptionsSetFailOnValidationError (
    spv_reducer_options options,
    bool fail_on_validation_error);

// Creates a fuzzer options object with default options. Returns a valid
// options object. The object remains valid until it is passed into
// |spvFuzzerOptionsDestroy|.
spv_fuzzer_options spvFuzzerOptionsCreate ();

// Destroys the given fuzzer options object.
void spvFuzzerOptionsDestroy (spv_fuzzer_options options);

// Enables running the validator after every transformation is applied during
// a replay.
void spvFuzzerOptionsEnableReplayValidation (spv_fuzzer_options options);

// Sets the seed with which the random number generator used by the fuzzer
// should be initialized.
void spvFuzzerOptionsSetRandomSeed (spv_fuzzer_options options, uint seed);

// Sets the maximum number of steps that the shrinker should take before giving
// up.
void spvFuzzerOptionsSetShrinkerStepLimit (
    spv_fuzzer_options options,
    uint shrinker_step_limit);

// Enables running the validator after every pass is applied during a fuzzing
// run.
void spvFuzzerOptionsEnableFuzzerPassValidation (spv_fuzzer_options options);

// Encodes the given SPIR-V assembly text to its binary representation. The
// length parameter specifies the number of bytes for text. Encoded binary will
// be stored into *binary. Any error will be written into *diagnostic if
// diagnostic is non-null, otherwise the context's message consumer will be
// used. The generated binary is independent of the context and may outlive it.
// The SPIR-V binary version is set to the highest version of SPIR-V supported
// by the context's target environment.
spv_result_t spvTextToBinary (
    const spv_const_context context,
    const(char)* text,
    const size_t length,
    spv_binary* binary,
    spv_diagnostic* diagnostic);

// Encodes the given SPIR-V assembly text to its binary representation. Same as
// spvTextToBinary but with options. The options parameter is a bit field of
// spv_text_to_binary_options_t.
spv_result_t spvTextToBinaryWithOptions (
    const spv_const_context context,
    const(char)* text,
    const size_t length,
    const uint options,
    spv_binary* binary,
    spv_diagnostic* diagnostic);

// Frees an allocated text stream. This is a no-op if the text parameter
// is a null pointer.
void spvTextDestroy (spv_text text);

// Decodes the given SPIR-V binary representation to its assembly text. The
// word_count parameter specifies the number of words for binary. The options
// parameter is a bit field of spv_binary_to_text_options_t. Decoded text will
// be stored into *text. Any error will be written into *diagnostic if
// diagnostic is non-null, otherwise the context's message consumer will be
// used.
spv_result_t spvBinaryToText (
    const spv_const_context context,
    const(uint)* binary,
    const size_t word_count,
    const uint options,
    spv_text* text,
    spv_diagnostic* diagnostic);

// Frees a binary stream from memory. This is a no-op if binary is a null
// pointer.
void spvBinaryDestroy (spv_binary binary);

// Validates a SPIR-V binary for correctness. Any errors will be written into
// *diagnostic if diagnostic is non-null, otherwise the context's message
// consumer will be used.
//
// Validate for SPIR-V spec rules for the SPIR-V version named in the
// binary's header (at word offset 1).  Additionally, if the context target
// environment is a client API (such as Vulkan 1.1), then validate for that
// client API version, to the extent that it is verifiable from data in the
// binary itself.
spv_result_t spvValidate (
    const spv_const_context context,
    const spv_const_binary binary,
    spv_diagnostic* diagnostic);

// Validates a SPIR-V binary for correctness. Uses the provided Validator
// options. Any errors will be written into *diagnostic if diagnostic is
// non-null, otherwise the context's message consumer will be used.
//
// Validate for SPIR-V spec rules for the SPIR-V version named in the
// binary's header (at word offset 1).  Additionally, if the context target
// environment is a client API (such as Vulkan 1.1), then validate for that
// client API version, to the extent that it is verifiable from data in the
// binary itself, or in the validator options.
spv_result_t spvValidateWithOptions (
    const spv_const_context context,
    const spv_const_validator_options options,
    const spv_const_binary binary,
    spv_diagnostic* diagnostic);

// Validates a raw SPIR-V binary for correctness. Any errors will be written
// into *diagnostic if diagnostic is non-null, otherwise the context's message
// consumer will be used.
spv_result_t spvValidateBinary (
    const spv_const_context context,
    const(uint)* words,
    const size_t num_words,
    spv_diagnostic* diagnostic);

// Creates a diagnostic object. The position parameter specifies the location in
// the text/binary stream. The message parameter, copied into the diagnostic
// object, contains the error message to display.
spv_diagnostic spvDiagnosticCreate (
    const spv_position position,
    const(char)* message);

// Destroys a diagnostic object.  This is a no-op if diagnostic is a null
// pointer.
void spvDiagnosticDestroy (spv_diagnostic diagnostic);

// Prints the diagnostic to stderr.
spv_result_t spvDiagnosticPrint (const spv_diagnostic diagnostic);

// Gets the name of an instruction, without the "Op" prefix.
const(char)* spvOpcodeString (const uint opcode);

// The binary parser interface.

// A pointer to a function that accepts a parsed SPIR-V header.
// The integer arguments are the 32-bit words from the header, as specified
// in SPIR-V 1.0 Section 2.3 Table 1.
// The function should return SPV_SUCCESS if parsing should continue.
alias spv_parsed_header_fn_t = spv_result_t function (
    void* user_data,
    spv_endianness_t endian,
    uint magic,
    uint version_,
    uint generator,
    uint id_bound,
    uint reserved);

// A pointer to a function that accepts a parsed SPIR-V instruction.
// The parsed_instruction value is transient: it may be overwritten
// or released immediately after the function has returned.  That also
// applies to the words array member of the parsed instruction.  The
// function should return SPV_SUCCESS if and only if parsing should
// continue.
alias spv_parsed_instruction_fn_t = spv_result_t function (
    void* user_data,
    const(spv_parsed_instruction_t)* parsed_instruction);

// Parses a SPIR-V binary, specified as counted sequence of 32-bit words.
// Parsing feedback is provided via two callbacks provided as function
// pointers.  Each callback function pointer can be a null pointer, in
// which case it is never called.  Otherwise, in a valid parse the
// parsed-header callback is called once, and then the parsed-instruction
// callback once for each instruction in the stream.  The user_data parameter
// is supplied as context to the callbacks.  Returns SPV_SUCCESS on successful
// parse where the callbacks always return SPV_SUCCESS.  For an invalid parse,
// returns a status code other than SPV_SUCCESS, and if diagnostic is non-null
// also emits a diagnostic. If diagnostic is null the context's message consumer
// will be used to emit any errors. If a callback returns anything other than
// SPV_SUCCESS, then that status code is returned, no further callbacks are
// issued, and no additional diagnostics are emitted.
spv_result_t spvBinaryParse (
    const spv_const_context context,
    void* user_data,
    const(uint)* words,
    const size_t num_words,
    spv_parsed_header_fn_t parse_header,
    spv_parsed_instruction_fn_t parse_instruction,
    spv_diagnostic* diagnostic);

// INCLUDE_SPIRV_TOOLS_LIBSPIRV_H_
