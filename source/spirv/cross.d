module spirv.cross;

/*
 * Copyright 2019-2020 Hans-Kristian Arntzen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import spirv;

extern (C):

/*
 * C89-compatible wrapper for SPIRV-Cross' API.
 * Documentation here is sparse unless the behavior does not map 1:1 with C++ API.
 * It is recommended to look at the canonical C++ API for more detailed information.
 */

/* Bumped if ABI or API breaks backwards compatibility. */
enum SPVC_C_API_VERSION_MAJOR = 0;
/* Bumped if APIs or enumerations are added in a backwards compatible way. */
enum SPVC_C_API_VERSION_MINOR = 30;
/* Bumped if internal implementation details change. */
enum SPVC_C_API_VERSION_PATCH = 0;

/* Exports symbols. Standard C calling convention is used. */

/*
 * Gets the SPVC_C_API_VERSION_* used to build this library.
 * Can be used to check for ABI mismatch if so-versioning did not catch it.
 */
void spvc_get_version (uint* major, uint* minor, uint* patch);

/* Gets a human readable version string to identify which commit a particular binary was created from. */
const(char)* spvc_get_commit_revision_and_timestamp ();

/* These types are opaque to the user. */
struct spvc_context_s;
alias spvc_context = spvc_context_s*;
struct spvc_parsed_ir_s;
alias spvc_parsed_ir = spvc_parsed_ir_s*;
struct spvc_compiler_s;
alias spvc_compiler = spvc_compiler_s*;
struct spvc_compiler_options_s;
alias spvc_compiler_options = spvc_compiler_options_s*;
struct spvc_resources_s;
alias spvc_resources = spvc_resources_s*;
struct spvc_type_s;
alias spvc_type = const(spvc_type_s)*;
struct spvc_constant_s;
alias spvc_constant = spvc_constant_s*;
struct spvc_set_s;
alias spvc_set = const(spvc_set_s)*;

/*
 * Shallow typedefs. All SPIR-V IDs are plain 32-bit numbers, but this helps communicate which data is used.
 * Maps to a SPIRType.
 */
alias spvc_type_id = uint;
/* Maps to a SPIRVariable. */
alias spvc_variable_id = uint;
/* Maps to a SPIRConstant. */
alias spvc_constant_id = uint;

/* See C++ API. */
struct spvc_reflected_resource
{
    spvc_variable_id id;
    spvc_type_id base_type_id;
    spvc_type_id type_id;
    const(char)* name;
}

/* See C++ API. */
struct spvc_entry_point
{
    SpvExecutionModel execution_model;
    const(char)* name;
}

/* See C++ API. */
struct spvc_combined_image_sampler
{
    spvc_variable_id combined_id;
    spvc_variable_id image_id;
    spvc_variable_id sampler_id;
}

/* See C++ API. */
struct spvc_specialization_constant
{
    spvc_constant_id id;
    uint constant_id;
}

/* See C++ API. */
struct spvc_buffer_range
{
    uint index;
    size_t offset;
    size_t range;
}

/* See C++ API. */
struct spvc_hlsl_root_constants
{
    uint start;
    uint end;
    uint binding;
    uint space;
}

/* See C++ API. */
struct spvc_hlsl_vertex_attribute_remap
{
    uint location;
    const(char)* semantic;
}

/*
 * Be compatible with non-C99 compilers, which do not have stdbool.
 * Only recent MSVC compilers supports this for example, and ideally SPIRV-Cross should be linkable
 * from a wide range of compilers in its C wrapper.
 */
alias spvc_bool = ubyte;
enum SPVC_TRUE = cast(spvc_bool) 1;
enum SPVC_FALSE = cast(spvc_bool) 0;

enum spvc_result
{
    /* Success. */
    SPVC_SUCCESS = 0,

    /* The SPIR-V is invalid. Should have been caught by validation ideally. */
    SPVC_ERROR_INVALID_SPIRV = -1,

    /* The SPIR-V might be valid or invalid, but SPIRV-Cross currently cannot correctly translate this to your target language. */
    SPVC_ERROR_UNSUPPORTED_SPIRV = -2,

    /* If for some reason we hit this, new or malloc failed. */
    SPVC_ERROR_OUT_OF_MEMORY = -3,

    /* Invalid API argument. */
    SPVC_ERROR_INVALID_ARGUMENT = -4,

    SPVC_ERROR_INT_MAX = 0x7fffffff
}

enum spvc_capture_mode
{
    /* The Parsed IR payload will be copied, and the handle can be reused to create other compiler instances. */
    SPVC_CAPTURE_MODE_COPY = 0,

    /*
    	 * The payload will now be owned by the compiler.
    	 * parsed_ir should now be considered a dead blob and must not be used further.
    	 * This is optimal for performance and should be the go-to option.
    	 */
    SPVC_CAPTURE_MODE_TAKE_OWNERSHIP = 1,

    SPVC_CAPTURE_MODE_INT_MAX = 0x7fffffff
}

enum spvc_backend
{
    /* This backend can only perform reflection, no compiler options are supported. Maps to spirv_cross::Compiler. */
    SPVC_BACKEND_NONE = 0,
    SPVC_BACKEND_GLSL = 1, /* spirv_cross::CompilerGLSL */
    SPVC_BACKEND_HLSL = 2, /* CompilerHLSL */
    SPVC_BACKEND_MSL = 3, /* CompilerMSL */
    SPVC_BACKEND_CPP = 4, /* CompilerCPP */
    SPVC_BACKEND_JSON = 5, /* CompilerReflection w/ JSON backend */
    SPVC_BACKEND_INT_MAX = 0x7fffffff
}

/* Maps to C++ API. */
enum spvc_resource_type
{
    SPVC_RESOURCE_TYPE_UNKNOWN = 0,
    SPVC_RESOURCE_TYPE_UNIFORM_BUFFER = 1,
    SPVC_RESOURCE_TYPE_STORAGE_BUFFER = 2,
    SPVC_RESOURCE_TYPE_STAGE_INPUT = 3,
    SPVC_RESOURCE_TYPE_STAGE_OUTPUT = 4,
    SPVC_RESOURCE_TYPE_SUBPASS_INPUT = 5,
    SPVC_RESOURCE_TYPE_STORAGE_IMAGE = 6,
    SPVC_RESOURCE_TYPE_SAMPLED_IMAGE = 7,
    SPVC_RESOURCE_TYPE_ATOMIC_COUNTER = 8,
    SPVC_RESOURCE_TYPE_PUSH_CONSTANT = 9,
    SPVC_RESOURCE_TYPE_SEPARATE_IMAGE = 10,
    SPVC_RESOURCE_TYPE_SEPARATE_SAMPLERS = 11,
    SPVC_RESOURCE_TYPE_ACCELERATION_STRUCTURE = 12,
    SPVC_RESOURCE_TYPE_INT_MAX = 0x7fffffff
}

/* Maps to spirv_cross::SPIRType::BaseType. */
enum spvc_basetype
{
    SPVC_BASETYPE_UNKNOWN = 0,
    SPVC_BASETYPE_VOID = 1,
    SPVC_BASETYPE_BOOLEAN = 2,
    SPVC_BASETYPE_INT8 = 3,
    SPVC_BASETYPE_UINT8 = 4,
    SPVC_BASETYPE_INT16 = 5,
    SPVC_BASETYPE_UINT16 = 6,
    SPVC_BASETYPE_INT32 = 7,
    SPVC_BASETYPE_UINT32 = 8,
    SPVC_BASETYPE_INT64 = 9,
    SPVC_BASETYPE_UINT64 = 10,
    SPVC_BASETYPE_ATOMIC_COUNTER = 11,
    SPVC_BASETYPE_FP16 = 12,
    SPVC_BASETYPE_FP32 = 13,
    SPVC_BASETYPE_FP64 = 14,
    SPVC_BASETYPE_STRUCT = 15,
    SPVC_BASETYPE_IMAGE = 16,
    SPVC_BASETYPE_SAMPLED_IMAGE = 17,
    SPVC_BASETYPE_SAMPLER = 18,
    SPVC_BASETYPE_ACCELERATION_STRUCTURE = 19,

    SPVC_BASETYPE_INT_MAX = 0x7fffffff
}

enum SPVC_COMPILER_OPTION_COMMON_BIT = 0x1000000;
enum SPVC_COMPILER_OPTION_GLSL_BIT = 0x2000000;
enum SPVC_COMPILER_OPTION_HLSL_BIT = 0x4000000;
enum SPVC_COMPILER_OPTION_MSL_BIT = 0x8000000;
enum SPVC_COMPILER_OPTION_LANG_BITS = 0x0f000000;
enum SPVC_COMPILER_OPTION_ENUM_BITS = 0xffffff;

extern (D) auto SPVC_MAKE_MSL_VERSION(T0, T1, T2)(auto ref T0 major, auto ref T1 minor, auto ref T2 patch)
{
    return major * 10000 + minor * 100 + patch;
}

/* Maps to C++ API. */
enum spvc_msl_platform
{
    SPVC_MSL_PLATFORM_IOS = 0,
    SPVC_MSL_PLATFORM_MACOS = 1,
    SPVC_MSL_PLATFORM_MAX_INT = 0x7fffffff
}

/* Maps to C++ API. */
enum spvc_msl_vertex_format
{
    SPVC_MSL_VERTEX_FORMAT_OTHER = 0,
    SPVC_MSL_VERTEX_FORMAT_UINT8 = 1,
    SPVC_MSL_VERTEX_FORMAT_UINT16 = 2
}

/* Maps to C++ API. */
struct spvc_msl_vertex_attribute
{
    uint location;
    uint msl_buffer;
    uint msl_offset;
    uint msl_stride;
    spvc_bool per_instance;
    spvc_msl_vertex_format format;
    SpvBuiltIn builtin;
}

/*
 * Initializes the vertex attribute struct.
 */
void spvc_msl_vertex_attribute_init (spvc_msl_vertex_attribute* attr);

/* Maps to C++ API. */
struct spvc_msl_resource_binding
{
    SpvExecutionModel stage;
    uint desc_set;
    uint binding;
    uint msl_buffer;
    uint msl_texture;
    uint msl_sampler;
}

/*
 * Initializes the resource binding struct.
 * The defaults are non-zero.
 */
void spvc_msl_resource_binding_init (spvc_msl_resource_binding* binding);

enum SPVC_MSL_PUSH_CONSTANT_DESC_SET = ~(0u);
enum SPVC_MSL_PUSH_CONSTANT_BINDING = 0;
enum SPVC_MSL_SWIZZLE_BUFFER_BINDING = ~(1u);
enum SPVC_MSL_BUFFER_SIZE_BUFFER_BINDING = ~(2u);
enum SPVC_MSL_ARGUMENT_BUFFER_BINDING = ~(3u);

/* Obsolete. Sticks around for backwards compatibility. */
enum SPVC_MSL_AUX_BUFFER_STRUCT_VERSION = 1;

/* Runtime check for incompatibility. Obsolete. */
uint spvc_msl_get_aux_buffer_struct_version ();

/* Maps to C++ API. */
enum spvc_msl_sampler_coord
{
    SPVC_MSL_SAMPLER_COORD_NORMALIZED = 0,
    SPVC_MSL_SAMPLER_COORD_PIXEL = 1,
    SPVC_MSL_SAMPLER_INT_MAX = 0x7fffffff
}

/* Maps to C++ API. */
enum spvc_msl_sampler_filter
{
    SPVC_MSL_SAMPLER_FILTER_NEAREST = 0,
    SPVC_MSL_SAMPLER_FILTER_LINEAR = 1,
    SPVC_MSL_SAMPLER_FILTER_INT_MAX = 0x7fffffff
}

/* Maps to C++ API. */
enum spvc_msl_sampler_mip_filter
{
    SPVC_MSL_SAMPLER_MIP_FILTER_NONE = 0,
    SPVC_MSL_SAMPLER_MIP_FILTER_NEAREST = 1,
    SPVC_MSL_SAMPLER_MIP_FILTER_LINEAR = 2,
    SPVC_MSL_SAMPLER_MIP_FILTER_INT_MAX = 0x7fffffff
}

/* Maps to C++ API. */
enum spvc_msl_sampler_address
{
    SPVC_MSL_SAMPLER_ADDRESS_CLAMP_TO_ZERO = 0,
    SPVC_MSL_SAMPLER_ADDRESS_CLAMP_TO_EDGE = 1,
    SPVC_MSL_SAMPLER_ADDRESS_CLAMP_TO_BORDER = 2,
    SPVC_MSL_SAMPLER_ADDRESS_REPEAT = 3,
    SPVC_MSL_SAMPLER_ADDRESS_MIRRORED_REPEAT = 4,
    SPVC_MSL_SAMPLER_ADDRESS_INT_MAX = 0x7fffffff
}

/* Maps to C++ API. */
enum spvc_msl_sampler_compare_func
{
    SPVC_MSL_SAMPLER_COMPARE_FUNC_NEVER = 0,
    SPVC_MSL_SAMPLER_COMPARE_FUNC_LESS = 1,
    SPVC_MSL_SAMPLER_COMPARE_FUNC_LESS_EQUAL = 2,
    SPVC_MSL_SAMPLER_COMPARE_FUNC_GREATER = 3,
    SPVC_MSL_SAMPLER_COMPARE_FUNC_GREATER_EQUAL = 4,
    SPVC_MSL_SAMPLER_COMPARE_FUNC_EQUAL = 5,
    SPVC_MSL_SAMPLER_COMPARE_FUNC_NOT_EQUAL = 6,
    SPVC_MSL_SAMPLER_COMPARE_FUNC_ALWAYS = 7,
    SPVC_MSL_SAMPLER_COMPARE_FUNC_INT_MAX = 0x7fffffff
}

/* Maps to C++ API. */
enum spvc_msl_sampler_border_color
{
    SPVC_MSL_SAMPLER_BORDER_COLOR_TRANSPARENT_BLACK = 0,
    SPVC_MSL_SAMPLER_BORDER_COLOR_OPAQUE_BLACK = 1,
    SPVC_MSL_SAMPLER_BORDER_COLOR_OPAQUE_WHITE = 2,
    SPVC_MSL_SAMPLER_BORDER_COLOR_INT_MAX = 0x7fffffff
}

/* Maps to C++ API. */
enum spvc_msl_format_resolution
{
    SPVC_MSL_FORMAT_RESOLUTION_444 = 0,
    SPVC_MSL_FORMAT_RESOLUTION_422 = 1,
    SPVC_MSL_FORMAT_RESOLUTION_420 = 2,
    SPVC_MSL_FORMAT_RESOLUTION_INT_MAX = 0x7fffffff
}

/* Maps to C++ API. */
enum spvc_msl_chroma_location
{
    SPVC_MSL_CHROMA_LOCATION_COSITED_EVEN = 0,
    SPVC_MSL_CHROMA_LOCATION_MIDPOINT = 1,
    SPVC_MSL_CHROMA_LOCATION_INT_MAX = 0x7fffffff
}

/* Maps to C++ API. */
enum spvc_msl_component_swizzle
{
    SPVC_MSL_COMPONENT_SWIZZLE_IDENTITY = 0,
    SPVC_MSL_COMPONENT_SWIZZLE_ZERO = 1,
    SPVC_MSL_COMPONENT_SWIZZLE_ONE = 2,
    SPVC_MSL_COMPONENT_SWIZZLE_R = 3,
    SPVC_MSL_COMPONENT_SWIZZLE_G = 4,
    SPVC_MSL_COMPONENT_SWIZZLE_B = 5,
    SPVC_MSL_COMPONENT_SWIZZLE_A = 6,
    SPVC_MSL_COMPONENT_SWIZZLE_INT_MAX = 0x7fffffff
}

/* Maps to C++ API. */
enum spvc_msl_sampler_ycbcr_model_conversion
{
    SPVC_MSL_SAMPLER_YCBCR_MODEL_CONVERSION_RGB_IDENTITY = 0,
    SPVC_MSL_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_IDENTITY = 1,
    SPVC_MSL_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_BT_709 = 2,
    SPVC_MSL_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_BT_601 = 3,
    SPVC_MSL_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_BT_2020 = 4,
    SPVC_MSL_SAMPLER_YCBCR_MODEL_CONVERSION_INT_MAX = 0x7fffffff
}

/* Maps to C+ API. */
enum spvc_msl_sampler_ycbcr_range
{
    SPVC_MSL_SAMPLER_YCBCR_RANGE_ITU_FULL = 0,
    SPVC_MSL_SAMPLER_YCBCR_RANGE_ITU_NARROW = 1,
    SPVC_MSL_SAMPLER_YCBCR_RANGE_INT_MAX = 0x7fffffff
}

/* Maps to C++ API. */
struct spvc_msl_constexpr_sampler
{
    spvc_msl_sampler_coord coord;
    spvc_msl_sampler_filter min_filter;
    spvc_msl_sampler_filter mag_filter;
    spvc_msl_sampler_mip_filter mip_filter;
    spvc_msl_sampler_address s_address;
    spvc_msl_sampler_address t_address;
    spvc_msl_sampler_address r_address;
    spvc_msl_sampler_compare_func compare_func;
    spvc_msl_sampler_border_color border_color;
    float lod_clamp_min;
    float lod_clamp_max;
    int max_anisotropy;

    spvc_bool compare_enable;
    spvc_bool lod_clamp_enable;
    spvc_bool anisotropy_enable;
}

/*
 * Initializes the constexpr sampler struct.
 * The defaults are non-zero.
 */
void spvc_msl_constexpr_sampler_init (spvc_msl_constexpr_sampler* sampler);

/* Maps to the sampler Y'CbCr conversion-related portions of MSLConstexprSampler. See C++ API for defaults and details. */
struct spvc_msl_sampler_ycbcr_conversion
{
    uint planes;
    spvc_msl_format_resolution resolution;
    spvc_msl_sampler_filter chroma_filter;
    spvc_msl_chroma_location x_chroma_offset;
    spvc_msl_chroma_location y_chroma_offset;
    spvc_msl_component_swizzle[4] swizzle;
    spvc_msl_sampler_ycbcr_model_conversion ycbcr_model;
    spvc_msl_sampler_ycbcr_range ycbcr_range;
    uint bpc;
}

/*
 * Initializes the constexpr sampler struct.
 * The defaults are non-zero.
 */
void spvc_msl_sampler_ycbcr_conversion_init (spvc_msl_sampler_ycbcr_conversion* conv);

/* Maps to C++ API. */
enum spvc_hlsl_binding_flag_bits
{
    SPVC_HLSL_BINDING_AUTO_NONE_BIT = 0,
    SPVC_HLSL_BINDING_AUTO_PUSH_CONSTANT_BIT = 1 << 0,
    SPVC_HLSL_BINDING_AUTO_CBV_BIT = 1 << 1,
    SPVC_HLSL_BINDING_AUTO_SRV_BIT = 1 << 2,
    SPVC_HLSL_BINDING_AUTO_UAV_BIT = 1 << 3,
    SPVC_HLSL_BINDING_AUTO_SAMPLER_BIT = 1 << 4,
    SPVC_HLSL_BINDING_AUTO_ALL = 0x7fffffff
}

alias spvc_hlsl_binding_flags = uint;

enum SPVC_HLSL_PUSH_CONSTANT_DESC_SET = ~(0u);
enum SPVC_HLSL_PUSH_CONSTANT_BINDING = 0;

/* Maps to C++ API. */
struct spvc_hlsl_resource_binding_mapping
{
    uint register_space;
    uint register_binding;
}

struct spvc_hlsl_resource_binding
{
    SpvExecutionModel stage;
    uint desc_set;
    uint binding;

    spvc_hlsl_resource_binding_mapping cbv;
    spvc_hlsl_resource_binding_mapping uav;
    spvc_hlsl_resource_binding_mapping srv;
    spvc_hlsl_resource_binding_mapping sampler;
}

/*
 * Initializes the resource binding struct.
 * The defaults are non-zero.
 */
void spvc_hlsl_resource_binding_init (spvc_hlsl_resource_binding* binding);

/* Maps to the various spirv_cross::Compiler*::Option structures. See C++ API for defaults and details. */
enum spvc_compiler_option
{
    SPVC_COMPILER_OPTION_UNKNOWN = 0,

    SPVC_COMPILER_OPTION_FORCE_TEMPORARY = 1 | SPVC_COMPILER_OPTION_COMMON_BIT,
    SPVC_COMPILER_OPTION_FLATTEN_MULTIDIMENSIONAL_ARRAYS = 2 | SPVC_COMPILER_OPTION_COMMON_BIT,
    SPVC_COMPILER_OPTION_FIXUP_DEPTH_CONVENTION = 3 | SPVC_COMPILER_OPTION_COMMON_BIT,
    SPVC_COMPILER_OPTION_FLIP_VERTEX_Y = 4 | SPVC_COMPILER_OPTION_COMMON_BIT,

    SPVC_COMPILER_OPTION_GLSL_SUPPORT_NONZERO_BASE_INSTANCE = 5 | SPVC_COMPILER_OPTION_GLSL_BIT,
    SPVC_COMPILER_OPTION_GLSL_SEPARATE_SHADER_OBJECTS = 6 | SPVC_COMPILER_OPTION_GLSL_BIT,
    SPVC_COMPILER_OPTION_GLSL_ENABLE_420PACK_EXTENSION = 7 | SPVC_COMPILER_OPTION_GLSL_BIT,
    SPVC_COMPILER_OPTION_GLSL_VERSION = 8 | SPVC_COMPILER_OPTION_GLSL_BIT,
    SPVC_COMPILER_OPTION_GLSL_ES = 9 | SPVC_COMPILER_OPTION_GLSL_BIT,
    SPVC_COMPILER_OPTION_GLSL_VULKAN_SEMANTICS = 10 | SPVC_COMPILER_OPTION_GLSL_BIT,
    SPVC_COMPILER_OPTION_GLSL_ES_DEFAULT_FLOAT_PRECISION_HIGHP = 11 | SPVC_COMPILER_OPTION_GLSL_BIT,
    SPVC_COMPILER_OPTION_GLSL_ES_DEFAULT_INT_PRECISION_HIGHP = 12 | SPVC_COMPILER_OPTION_GLSL_BIT,

    SPVC_COMPILER_OPTION_HLSL_SHADER_MODEL = 13 | SPVC_COMPILER_OPTION_HLSL_BIT,
    SPVC_COMPILER_OPTION_HLSL_POINT_SIZE_COMPAT = 14 | SPVC_COMPILER_OPTION_HLSL_BIT,
    SPVC_COMPILER_OPTION_HLSL_POINT_COORD_COMPAT = 15 | SPVC_COMPILER_OPTION_HLSL_BIT,
    SPVC_COMPILER_OPTION_HLSL_SUPPORT_NONZERO_BASE_VERTEX_BASE_INSTANCE = 16 | SPVC_COMPILER_OPTION_HLSL_BIT,

    SPVC_COMPILER_OPTION_MSL_VERSION = 17 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_TEXEL_BUFFER_TEXTURE_WIDTH = 18 | SPVC_COMPILER_OPTION_MSL_BIT,

    /* Obsolete, use SWIZZLE_BUFFER_INDEX instead. */
    SPVC_COMPILER_OPTION_MSL_AUX_BUFFER_INDEX = 19 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_SWIZZLE_BUFFER_INDEX = 19 | SPVC_COMPILER_OPTION_MSL_BIT,

    SPVC_COMPILER_OPTION_MSL_INDIRECT_PARAMS_BUFFER_INDEX = 20 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_SHADER_OUTPUT_BUFFER_INDEX = 21 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_SHADER_PATCH_OUTPUT_BUFFER_INDEX = 22 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_SHADER_TESS_FACTOR_OUTPUT_BUFFER_INDEX = 23 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_SHADER_INPUT_WORKGROUP_INDEX = 24 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_ENABLE_POINT_SIZE_BUILTIN = 25 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_DISABLE_RASTERIZATION = 26 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_CAPTURE_OUTPUT_TO_BUFFER = 27 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_SWIZZLE_TEXTURE_SAMPLES = 28 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_PAD_FRAGMENT_OUTPUT_COMPONENTS = 29 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_TESS_DOMAIN_ORIGIN_LOWER_LEFT = 30 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_PLATFORM = 31 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_ARGUMENT_BUFFERS = 32 | SPVC_COMPILER_OPTION_MSL_BIT,

    SPVC_COMPILER_OPTION_GLSL_EMIT_PUSH_CONSTANT_AS_UNIFORM_BUFFER = 33 | SPVC_COMPILER_OPTION_GLSL_BIT,

    SPVC_COMPILER_OPTION_MSL_TEXTURE_BUFFER_NATIVE = 34 | SPVC_COMPILER_OPTION_MSL_BIT,

    SPVC_COMPILER_OPTION_GLSL_EMIT_UNIFORM_BUFFER_AS_PLAIN_UNIFORMS = 35 | SPVC_COMPILER_OPTION_GLSL_BIT,

    SPVC_COMPILER_OPTION_MSL_BUFFER_SIZE_BUFFER_INDEX = 36 | SPVC_COMPILER_OPTION_MSL_BIT,

    SPVC_COMPILER_OPTION_EMIT_LINE_DIRECTIVES = 37 | SPVC_COMPILER_OPTION_COMMON_BIT,

    SPVC_COMPILER_OPTION_MSL_MULTIVIEW = 38 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_VIEW_MASK_BUFFER_INDEX = 39 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_DEVICE_INDEX = 40 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_VIEW_INDEX_FROM_DEVICE_INDEX = 41 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_DISPATCH_BASE = 42 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_DYNAMIC_OFFSETS_BUFFER_INDEX = 43 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_TEXTURE_1D_AS_2D = 44 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_ENABLE_BASE_INDEX_ZERO = 45 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_IOS_FRAMEBUFFER_FETCH_SUBPASS = 46 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_INVARIANT_FP_MATH = 47 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_EMULATE_CUBEMAP_ARRAY = 48 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_ENABLE_DECORATION_BINDING = 49 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_FORCE_ACTIVE_ARGUMENT_BUFFER_RESOURCES = 50 | SPVC_COMPILER_OPTION_MSL_BIT,
    SPVC_COMPILER_OPTION_MSL_FORCE_NATIVE_ARRAYS = 51 | SPVC_COMPILER_OPTION_MSL_BIT,

    SPVC_COMPILER_OPTION_ENABLE_STORAGE_IMAGE_QUALIFIER_DEDUCTION = 52 | SPVC_COMPILER_OPTION_COMMON_BIT,

    SPVC_COMPILER_OPTION_HLSL_FORCE_STORAGE_BUFFER_AS_UAV = 53 | SPVC_COMPILER_OPTION_HLSL_BIT,

    SPVC_COMPILER_OPTION_FORCE_ZERO_INITIALIZED_VARIABLES = 54 | SPVC_COMPILER_OPTION_COMMON_BIT,

    SPVC_COMPILER_OPTION_HLSL_NONWRITABLE_UAV_TEXTURE_AS_SRV = 55 | SPVC_COMPILER_OPTION_HLSL_BIT,

    SPVC_COMPILER_OPTION_INT_MAX = 0x7fffffff
}

/*
 * Context is the highest-level API construct.
 * The context owns all memory allocations made by its child object hierarchy, including various non-opaque structs and strings.
 * This means that the API user only has to care about one "destroy" call ever when using the C API.
 * All pointers handed out by the APIs are only valid as long as the context
 * is alive and spvc_context_release_allocations has not been called.
 */
spvc_result spvc_context_create (spvc_context* context);

/* Frees all memory allocations and objects associated with the context and its child objects. */
void spvc_context_destroy (spvc_context context);

/* Frees all memory allocations and objects associated with the context and its child objects, but keeps the context alive. */
void spvc_context_release_allocations (spvc_context context);

/* Get the string for the last error which was logged. */
const(char)* spvc_context_get_last_error_string (spvc_context context);

/* Get notified in a callback when an error triggers. Useful for debugging. */
alias spvc_error_callback = void function (void* userdata, const(char)* error);
void spvc_context_set_error_callback (spvc_context context, spvc_error_callback cb, void* userdata);

/* SPIR-V parsing interface. Maps to Parser which then creates a ParsedIR, and that IR is extracted into the handle. */
spvc_result spvc_context_parse_spirv (
    spvc_context context,
    const(SpvId)* spirv,
    size_t word_count,
    spvc_parsed_ir* parsed_ir);

/*
 * Create a compiler backend. Capture mode controls if we construct by copy or move semantics.
 * It is always recommended to use SPVC_CAPTURE_MODE_TAKE_OWNERSHIP if you only intend to cross-compile the IR once.
 */
spvc_result spvc_context_create_compiler (
    spvc_context context,
    spvc_backend backend,
    spvc_parsed_ir parsed_ir,
    spvc_capture_mode mode,
    spvc_compiler* compiler);

/* Maps directly to C++ API. */
uint spvc_compiler_get_current_id_bound (spvc_compiler compiler);

/* Create compiler options, which will initialize defaults. */
spvc_result spvc_compiler_create_compiler_options (
    spvc_compiler compiler,
    spvc_compiler_options* options);
/* Override options. Will return error if e.g. MSL options are used for the HLSL backend, etc. */
spvc_result spvc_compiler_options_set_bool (
    spvc_compiler_options options,
    spvc_compiler_option option,
    spvc_bool value);
spvc_result spvc_compiler_options_set_uint (
    spvc_compiler_options options,
    spvc_compiler_option option,
    uint value);
/* Set compiler options. */
spvc_result spvc_compiler_install_compiler_options (
    spvc_compiler compiler,
    spvc_compiler_options options);

/* Compile IR into a string. *source is owned by the context, and caller must not free it themselves. */
spvc_result spvc_compiler_compile (spvc_compiler compiler, const(char*)* source);

/* Maps to C++ API. */
spvc_result spvc_compiler_add_header_line (spvc_compiler compiler, const(char)* line);
spvc_result spvc_compiler_require_extension (spvc_compiler compiler, const(char)* ext);
spvc_result spvc_compiler_flatten_buffer_block (spvc_compiler compiler, spvc_variable_id id);

spvc_bool spvc_compiler_variable_is_depth_or_compare (spvc_compiler compiler, spvc_variable_id id);

/*
 * HLSL specifics.
 * Maps to C++ API.
 */
spvc_result spvc_compiler_hlsl_set_root_constants_layout (
    spvc_compiler compiler,
    const(spvc_hlsl_root_constants)* constant_info,
    size_t count);
spvc_result spvc_compiler_hlsl_add_vertex_attribute_remap (
    spvc_compiler compiler,
    const(spvc_hlsl_vertex_attribute_remap)* remap,
    size_t remaps);
spvc_variable_id spvc_compiler_hlsl_remap_num_workgroups_builtin (spvc_compiler compiler);

spvc_result spvc_compiler_hlsl_set_resource_binding_flags (
    spvc_compiler compiler,
    spvc_hlsl_binding_flags flags);

spvc_result spvc_compiler_hlsl_add_resource_binding (
    spvc_compiler compiler,
    const(spvc_hlsl_resource_binding)* binding);
spvc_bool spvc_compiler_hlsl_is_resource_used (
    spvc_compiler compiler,
    SpvExecutionModel model,
    uint set,
    uint binding);

/*
 * MSL specifics.
 * Maps to C++ API.
 */
spvc_bool spvc_compiler_msl_is_rasterization_disabled (spvc_compiler compiler);

/* Obsolete. Renamed to needs_swizzle_buffer. */
spvc_bool spvc_compiler_msl_needs_aux_buffer (spvc_compiler compiler);
spvc_bool spvc_compiler_msl_needs_swizzle_buffer (spvc_compiler compiler);
spvc_bool spvc_compiler_msl_needs_buffer_size_buffer (spvc_compiler compiler);

spvc_bool spvc_compiler_msl_needs_output_buffer (spvc_compiler compiler);
spvc_bool spvc_compiler_msl_needs_patch_output_buffer (spvc_compiler compiler);
spvc_bool spvc_compiler_msl_needs_input_threadgroup_mem (spvc_compiler compiler);
spvc_result spvc_compiler_msl_add_vertex_attribute (
    spvc_compiler compiler,
    const(spvc_msl_vertex_attribute)* attrs);
spvc_result spvc_compiler_msl_add_resource_binding (
    spvc_compiler compiler,
    const(spvc_msl_resource_binding)* binding);
spvc_result spvc_compiler_msl_add_discrete_descriptor_set (spvc_compiler compiler, uint desc_set);
spvc_result spvc_compiler_msl_set_argument_buffer_device_address_space (spvc_compiler compiler, uint desc_set, spvc_bool device_address);
spvc_bool spvc_compiler_msl_is_vertex_attribute_used (spvc_compiler compiler, uint location);
spvc_bool spvc_compiler_msl_is_resource_used (
    spvc_compiler compiler,
    SpvExecutionModel model,
    uint set,
    uint binding);
spvc_result spvc_compiler_msl_remap_constexpr_sampler (spvc_compiler compiler, spvc_variable_id id, const(spvc_msl_constexpr_sampler)* sampler);
spvc_result spvc_compiler_msl_remap_constexpr_sampler_by_binding (spvc_compiler compiler, uint desc_set, uint binding, const(spvc_msl_constexpr_sampler)* sampler);
spvc_result spvc_compiler_msl_remap_constexpr_sampler_ycbcr (spvc_compiler compiler, spvc_variable_id id, const(spvc_msl_constexpr_sampler)* sampler, const(spvc_msl_sampler_ycbcr_conversion)* conv);
spvc_result spvc_compiler_msl_remap_constexpr_sampler_by_binding_ycbcr (spvc_compiler compiler, uint desc_set, uint binding, const(spvc_msl_constexpr_sampler)* sampler, const(spvc_msl_sampler_ycbcr_conversion)* conv);
spvc_result spvc_compiler_msl_set_fragment_output_components (spvc_compiler compiler, uint location, uint components);

uint spvc_compiler_msl_get_automatic_resource_binding (spvc_compiler compiler, spvc_variable_id id);
uint spvc_compiler_msl_get_automatic_resource_binding_secondary (spvc_compiler compiler, spvc_variable_id id);

spvc_result spvc_compiler_msl_add_dynamic_buffer (spvc_compiler compiler, uint desc_set, uint binding, uint index);

spvc_result spvc_compiler_msl_add_inline_uniform_block (spvc_compiler compiler, uint desc_set, uint binding);

/*
 * Reflect resources.
 * Maps almost 1:1 to C++ API.
 */
spvc_result spvc_compiler_get_active_interface_variables (spvc_compiler compiler, spvc_set* set);
spvc_result spvc_compiler_set_enabled_interface_variables (spvc_compiler compiler, spvc_set set);
spvc_result spvc_compiler_create_shader_resources (spvc_compiler compiler, spvc_resources* resources);
spvc_result spvc_compiler_create_shader_resources_for_active_variables (
    spvc_compiler compiler,
    spvc_resources* resources,
    spvc_set active);
spvc_result spvc_resources_get_resource_list_for_type (
    spvc_resources resources,
    spvc_resource_type type,
    const(spvc_reflected_resource*)* resource_list,
    size_t* resource_size);

/*
 * Decorations.
 * Maps to C++ API.
 */
void spvc_compiler_set_decoration (
    spvc_compiler compiler,
    SpvId id,
    SpvDecoration decoration,
    uint argument);
void spvc_compiler_set_decoration_string (
    spvc_compiler compiler,
    SpvId id,
    SpvDecoration decoration,
    const(char)* argument);
void spvc_compiler_set_name (spvc_compiler compiler, SpvId id, const(char)* argument);
void spvc_compiler_set_member_decoration (
    spvc_compiler compiler,
    spvc_type_id id,
    uint member_index,
    SpvDecoration decoration,
    uint argument);
void spvc_compiler_set_member_decoration_string (
    spvc_compiler compiler,
    spvc_type_id id,
    uint member_index,
    SpvDecoration decoration,
    const(char)* argument);
void spvc_compiler_set_member_name (
    spvc_compiler compiler,
    spvc_type_id id,
    uint member_index,
    const(char)* argument);
void spvc_compiler_unset_decoration (spvc_compiler compiler, SpvId id, SpvDecoration decoration);
void spvc_compiler_unset_member_decoration (
    spvc_compiler compiler,
    spvc_type_id id,
    uint member_index,
    SpvDecoration decoration);

spvc_bool spvc_compiler_has_decoration (spvc_compiler compiler, SpvId id, SpvDecoration decoration);
spvc_bool spvc_compiler_has_member_decoration (
    spvc_compiler compiler,
    spvc_type_id id,
    uint member_index,
    SpvDecoration decoration);
const(char)* spvc_compiler_get_name (spvc_compiler compiler, SpvId id);
uint spvc_compiler_get_decoration (spvc_compiler compiler, SpvId id, SpvDecoration decoration);
const(char)* spvc_compiler_get_decoration_string (
    spvc_compiler compiler,
    SpvId id,
    SpvDecoration decoration);
uint spvc_compiler_get_member_decoration (
    spvc_compiler compiler,
    spvc_type_id id,
    uint member_index,
    SpvDecoration decoration);
const(char)* spvc_compiler_get_member_decoration_string (
    spvc_compiler compiler,
    spvc_type_id id,
    uint member_index,
    SpvDecoration decoration);
const(char)* spvc_compiler_get_member_name (spvc_compiler compiler, spvc_type_id id, uint member_index);

/*
 * Entry points.
 * Maps to C++ API.
 */
spvc_result spvc_compiler_get_entry_points (
    spvc_compiler compiler,
    const(spvc_entry_point*)* entry_points,
    size_t* num_entry_points);
spvc_result spvc_compiler_set_entry_point (
    spvc_compiler compiler,
    const(char)* name,
    SpvExecutionModel model);
spvc_result spvc_compiler_rename_entry_point (
    spvc_compiler compiler,
    const(char)* old_name,
    const(char)* new_name,
    SpvExecutionModel model);
const(char)* spvc_compiler_get_cleansed_entry_point_name (
    spvc_compiler compiler,
    const(char)* name,
    SpvExecutionModel model);
void spvc_compiler_set_execution_mode (spvc_compiler compiler, SpvExecutionMode mode);
void spvc_compiler_unset_execution_mode (spvc_compiler compiler, SpvExecutionMode mode);
void spvc_compiler_set_execution_mode_with_arguments (
    spvc_compiler compiler,
    SpvExecutionMode mode,
    uint arg0,
    uint arg1,
    uint arg2);
spvc_result spvc_compiler_get_execution_modes (
    spvc_compiler compiler,
    const(SpvExecutionMode*)* modes,
    size_t* num_modes);
uint spvc_compiler_get_execution_mode_argument (spvc_compiler compiler, SpvExecutionMode mode);
uint spvc_compiler_get_execution_mode_argument_by_index (
    spvc_compiler compiler,
    SpvExecutionMode mode,
    uint index);
SpvExecutionModel spvc_compiler_get_execution_model (spvc_compiler compiler);

/*
 * Type query interface.
 * Maps to C++ API, except it's read-only.
 */
spvc_type spvc_compiler_get_type_handle (spvc_compiler compiler, spvc_type_id id);

/* Pulls out SPIRType::self. This effectively gives the type ID without array or pointer qualifiers.
 * This is necessary when reflecting decoration/name information on members of a struct,
 * which are placed in the base type, not the qualified type.
 * This is similar to spvc_reflected_resource::base_type_id. */
spvc_type_id spvc_type_get_base_type_id (spvc_type type);

spvc_basetype spvc_type_get_basetype (spvc_type type);
uint spvc_type_get_bit_width (spvc_type type);
uint spvc_type_get_vector_size (spvc_type type);
uint spvc_type_get_columns (spvc_type type);
uint spvc_type_get_num_array_dimensions (spvc_type type);
spvc_bool spvc_type_array_dimension_is_literal (spvc_type type, uint dimension);
SpvId spvc_type_get_array_dimension (spvc_type type, uint dimension);
uint spvc_type_get_num_member_types (spvc_type type);
spvc_type_id spvc_type_get_member_type (spvc_type type, uint index);
SpvStorageClass spvc_type_get_storage_class (spvc_type type);

/* Image type query. */
spvc_type_id spvc_type_get_image_sampled_type (spvc_type type);
SpvDim spvc_type_get_image_dimension (spvc_type type);
spvc_bool spvc_type_get_image_is_depth (spvc_type type);
spvc_bool spvc_type_get_image_arrayed (spvc_type type);
spvc_bool spvc_type_get_image_multisampled (spvc_type type);
spvc_bool spvc_type_get_image_is_storage (spvc_type type);
SpvImageFormat spvc_type_get_image_storage_format (spvc_type type);
SpvAccessQualifier spvc_type_get_image_access_qualifier (spvc_type type);

/*
 * Buffer layout query.
 * Maps to C++ API.
 */
spvc_result spvc_compiler_get_declared_struct_size (spvc_compiler compiler, spvc_type struct_type, size_t* size);
spvc_result spvc_compiler_get_declared_struct_size_runtime_array (
    spvc_compiler compiler,
    spvc_type struct_type,
    size_t array_size,
    size_t* size);
spvc_result spvc_compiler_get_declared_struct_member_size (spvc_compiler compiler, spvc_type type, uint index, size_t* size);

spvc_result spvc_compiler_type_struct_member_offset (
    spvc_compiler compiler,
    spvc_type type,
    uint index,
    uint* offset);
spvc_result spvc_compiler_type_struct_member_array_stride (
    spvc_compiler compiler,
    spvc_type type,
    uint index,
    uint* stride);
spvc_result spvc_compiler_type_struct_member_matrix_stride (
    spvc_compiler compiler,
    spvc_type type,
    uint index,
    uint* stride);

/*
 * Workaround helper functions.
 * Maps to C++ API.
 */
spvc_result spvc_compiler_build_dummy_sampler_for_combined_images (spvc_compiler compiler, spvc_variable_id* id);
spvc_result spvc_compiler_build_combined_image_samplers (spvc_compiler compiler);
spvc_result spvc_compiler_get_combined_image_samplers (
    spvc_compiler compiler,
    const(spvc_combined_image_sampler*)* samplers,
    size_t* num_samplers);

/*
 * Constants
 * Maps to C++ API.
 */
spvc_result spvc_compiler_get_specialization_constants (
    spvc_compiler compiler,
    const(spvc_specialization_constant*)* constants,
    size_t* num_constants);
spvc_constant spvc_compiler_get_constant_handle (
    spvc_compiler compiler,
    spvc_constant_id id);

spvc_constant_id spvc_compiler_get_work_group_size_specialization_constants (
    spvc_compiler compiler,
    spvc_specialization_constant* x,
    spvc_specialization_constant* y,
    spvc_specialization_constant* z);

/*
 * Buffer ranges
 * Maps to C++ API.
 */
spvc_result spvc_compiler_get_active_buffer_ranges (
    spvc_compiler compiler,
    spvc_variable_id id,
    const(spvc_buffer_range*)* ranges,
    size_t* num_ranges);

/*
 * No stdint.h until C99, sigh :(
 * For smaller types, the result is sign or zero-extended as appropriate.
 * Maps to C++ API.
 * TODO: The SPIRConstant query interface and modification interface is not quite complete.
 */
float spvc_constant_get_scalar_fp16 (spvc_constant constant, uint column, uint row);
float spvc_constant_get_scalar_fp32 (spvc_constant constant, uint column, uint row);
double spvc_constant_get_scalar_fp64 (spvc_constant constant, uint column, uint row);
uint spvc_constant_get_scalar_u32 (spvc_constant constant, uint column, uint row);
int spvc_constant_get_scalar_i32 (spvc_constant constant, uint column, uint row);
uint spvc_constant_get_scalar_u16 (spvc_constant constant, uint column, uint row);
int spvc_constant_get_scalar_i16 (spvc_constant constant, uint column, uint row);
uint spvc_constant_get_scalar_u8 (spvc_constant constant, uint column, uint row);
int spvc_constant_get_scalar_i8 (spvc_constant constant, uint column, uint row);
void spvc_constant_get_subconstants (spvc_constant constant, const(spvc_constant_id*)* constituents, size_t* count);
spvc_type_id spvc_constant_get_type (spvc_constant constant);

/*
 * Misc reflection
 * Maps to C++ API.
 */
spvc_bool spvc_compiler_get_binary_offset_for_decoration (
    spvc_compiler compiler,
    spvc_variable_id id,
    SpvDecoration decoration,
    uint* word_offset);

spvc_bool spvc_compiler_buffer_is_hlsl_counter_buffer (spvc_compiler compiler, spvc_variable_id id);
spvc_bool spvc_compiler_buffer_get_hlsl_counter_buffer (
    spvc_compiler compiler,
    spvc_variable_id id,
    spvc_variable_id* counter_id);

spvc_result spvc_compiler_get_declared_capabilities (
    spvc_compiler compiler,
    const(SpvCapability*)* capabilities,
    size_t* num_capabilities);
spvc_result spvc_compiler_get_declared_extensions (
    spvc_compiler compiler,
    const(char**)* extensions,
    size_t* num_extensions);

const(char)* spvc_compiler_get_remapped_declared_block_name (spvc_compiler compiler, spvc_variable_id id);
spvc_result spvc_compiler_get_buffer_block_decorations (
    spvc_compiler compiler,
    spvc_variable_id id,
    const(SpvDecoration*)* decorations,
    size_t* num_decorations);

