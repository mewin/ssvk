module test_application;

import core.time;
import std.algorithm : max;
import std.experimental.logger;
import std.format;
import std.math;

import gfm.math;

import ssvk.core.application;
import ssvk.core.types;
import ssvk.core.utility.math;
import ssvk.loaders.bitmap;
import ssvk.loaders.scene;
import ssvk.loaders.shader;
import ssvk.window;
import ssvk.scene.mesh_instance;
import ssvk.scene.node3d;
import ssvk.scene.point_light3d;
import ssvk.vulkan.types : DebugRenderMode;
import ssvk.tools.mesh;

enum CameraMode
{
    TARGET,
    FLY
}

class TestApplication : Application
{
    mixin(implementBaseObject());

    private Node3D root3D;
    private vec3f center = vec3f(0.0);
    private float cameraDistance = 20.0;
    private float cameraAngleX = 0.0;
    private float cameraAngleY = -0.25 * PI;
    private bool keyDownW, keyDownA, keyDownS, keyDownD;
    private CameraMode cameraMode = CameraMode.TARGET;

    import std.variant : Variant;
    public void testFunc(Variant[] delegate(Variant[]) func)
    {
        for (int i = 0; i < 5; ++i)
        {
            info(func([Variant(i)]));
        }
    }

    ////////////////////////
    /// public interface ///
    ////////////////////////
    public override void initialize()
    {
        super.initialize();

        connectSignals();
        setupScene();
    }

    public override void tick(float delta)
    {
        float camSpeed = 10.0;
        vec3f cameraMove = vec3f(0.0);
        if (keyDownW != keyDownS) {
            if (keyDownW) {
                cameraMove += vec3f(-sin(cameraAngleX), 0.0, -cos(cameraAngleX));
            } else {
                cameraMove += vec3f(sin(cameraAngleX), 0.0, cos(cameraAngleX));
            }
        }
        if (keyDownA != keyDownD) {
            if (keyDownA) {
                cameraMove += vec3f(-cos(cameraAngleX), 0.0, sin(cameraAngleX));
            }
            else {
                cameraMove += vec3f(cos(cameraAngleX), 0.0, -sin(cameraAngleX));
            }
        }
        if (cameraMove != vec3f(0.0)) {
            center += camSpeed * delta * cameraMove;
            updateCamera();
        }

        super.tick(delta);

        // int fps = 1.seconds.total!"hnsecs" / max(renderer.frameDuration.total!"hnsecs", 1);
        // window.title = format("FPS: %s", fps);
    }

    //////////////////////
    /// initialization ///
    //////////////////////
    private void connectSignals()
    {
        window.connect(Window.SIG_CLOSE, &onWindowClose);
        window.connect(Window.SIG_KEY_DOWN, &onKeyDown);
        window.connect(Window.SIG_KEY_UP, &onKeyUp);
        window.connect(window.SIG_MOUSE_BUTTON_DOWN, &onMouseButtonDown);
        window.connect(Window.SIG_MOUSE_BUTTON_UP, &onMouseButtonUp);
        window.connect(Window.SIG_MOUSE_MOTION, &onMouseMotion);
        window.connect(Window.SIG_MOUSE_WHEEL, &onMouseWheel);
    }

    private void setupScene()
    {
        renderer.environmentMap = loadBitmap("assets/images/evening_in_park_4096.jpg");
        root3D = new Node3D();
        // meshes
        // auto root = loadScene("assets/models/two_cubes.obj");
        // root3D.addChild(loadScene("assets/models/cone.obj"));
        // root3D.addChild(loadScene("assets/models/teapot.obj"));
        // root3D.addChild(loadScene("assets/models/directions.obj"));

        // root3D.addChild(loadScene("assets/models/helmet/source/HEML1.fbx"));
        // root3D.addChild(loadScene("assets/models/kasatka/scene.gltf"));
        // root3D.addChild(loadScene("assets/models/sci_fi_biolab_corridor_lights/scene.gltf"));
        // root3D.addChild(loadScene("assets/models/cube_and_plane/scene.gltf"));
        // root3D.addChild(loadScene("assets/models/portal/portal.gltf"));
        // root3D.addChild(loadScene("assets/models/projection/scene.gltf"));
        root3D.addChild(loadScene("assets/models/portal4/scene.gltf"));
        
        // auto cone = loadScene("assets/models/cone.obj");
        // cone.transform.translation = cone.transform.translation + vec3f(5.0, 0.0, 0.0);
        // cone.transform.scale = vec3f(3.0);
        // root3D.addChild(cone);
        sceneTree.rootNode.addChild(root3D);
        // root3D.transform.translation = vec3f(0.0, 0.0, -5.0);
        root3D.transform.scale = vec3f(root3D.transform.scale.x * 10.0 / root3D.aabb.maxExtent);
        
        if (root3D.firstNodeOfType!PointLight3D() is null)
        {
            float distFactor = 10.0;
            // light
            auto light = new PointLight3D();
            light.transform = Transform3D.fromTranslation(distFactor * vec3f(1.0, 1.0, -1.5));
            light.range = 40.0;
            light.attenuation = 0.5;
            light.energy = 40.0;
            sceneTree.rootNode.addChild(light);

            auto light2 = new PointLight3D();
            light2.transform = Transform3D.fromTranslation(distFactor * vec3f(-0.4, 1.0, 1.5));
            light2.range = 20.0;
            light2.attenuation = 0.5;
            sceneTree.rootNode.addChild(light2);

            auto light3 = new PointLight3D();
            light3.transform = Transform3D.fromTranslation(distFactor * vec3f(0.4, 1.0, 1.5));
            light3.range = 20.0;
            light3.attenuation = 0.5;
            sceneTree.rootNode.addChild(light3);

            auto light4 = new PointLight3D();
            light4.transform = Transform3D.fromTranslation(distFactor * vec3f(-0.8, 1.0, 1.5));
            light4.range = 20.0;
            light4.attenuation = 0.5;
            sceneTree.rootNode.addChild(light4);
        }

        // update portals manually (not done automatically yet)
        MeshInstance[] meshInstances = sceneTree.rootNode.allNodesOfType!MeshInstance();
        updatePortals(meshInstances);

        // camera
        sceneTree.rootNode.addChild(camera);
        updateCamera();
        
        sceneTree.rootNode.prettyPrint();
    }

    /////////////
    // utility //
    /////////////
    private void updateCamera()
    {
        vec3f cameraPos = center + mat3f.rotateY(cameraAngleX) * mat3f.rotateX(cameraAngleY) *
                vec3f(0.0, 0.0, cameraDistance);
        camera.transform = Transform3D.lookAt(cameraPos, center, vec3f(0.0, 1.0, 0.0));
    }

    ////////////////////
    // event handling //
    ////////////////////
    private void onWindowClose()
    {
        stop = true;
    }

    private void onKeyDown(KeyEvent event)
    {
        bool shift = (event.modifiers & KeyModifierBits.SHIFT) != KeyModifierFlags.init;

        switch (event.key)
        {
            case KeyEvent.KEY_Q:
                root3D.transform = root3D.transform.rotated(vec3f(0.0, 1.0, 0.0), 0.1);
                break;
            case KeyEvent.KEY_E:
                root3D.transform = root3D.transform.rotated(vec3f(0.0, 1.0, 0.0), -0.1);
                break;
            case KeyEvent.KEY_0:
                renderer.debugRenderMode = DebugRenderMode.NONE;
                break;
            case KeyEvent.KEY_1:
                renderer.debugRenderMode = shift ? DebugRenderMode.NORMAL_MAP : DebugRenderMode.ALBEDO;
                break;
            case KeyEvent.KEY_2:
                renderer.debugRenderMode = shift ? DebugRenderMode.DEPTH : DebugRenderMode.ROUGHNESS;
                break;
            case KeyEvent.KEY_3:
                renderer.debugRenderMode = DebugRenderMode.METALLIC;
                break;
            case KeyEvent.KEY_4:
                renderer.debugRenderMode = DebugRenderMode.UV0;
                break;
            case KeyEvent.KEY_5:
                renderer.debugRenderMode = DebugRenderMode.UV1;
                break;
            case KeyEvent.KEY_6:
                renderer.debugRenderMode = DebugRenderMode.BARYCENTRICS;
                break;
            case KeyEvent.KEY_7:
                renderer.debugRenderMode = DebugRenderMode.MATERIAL_ID;
                break;
            case KeyEvent.KEY_8:
                renderer.debugRenderMode = DebugRenderMode.INSTANCE_ID;
                break;
            case KeyEvent.KEY_9:
                renderer.debugRenderMode = DebugRenderMode.PRIMITIVE_ID;
                break;
            case KeyEvent.KEY_W:
                keyDownW = true;
                break;
            case KeyEvent.KEY_S:
                keyDownS = true;
                break;
            case KeyEvent.KEY_A:
                keyDownA = true;
                break;
            case KeyEvent.KEY_D:
                keyDownD = true;
                break;
            case KeyEvent.KEY_F:
                window.fullscreen = !window.fullscreen;
                break;
            case KeyEvent.KEY_R:
                renderer.reloadShaders();
                break;
            case KeyEvent.KEY_F1:
                renderer.debugRenderFlags.SKIP_NORMAL_MAP = !renderer.debugRenderFlags.SKIP_NORMAL_MAP;
                break;
            case KeyEvent.KEY_F2:
                if (renderer.debugRenderFlags.PPROCESS_SKIP) {
                    renderer.debugRenderFlags.PPROCESS_SKIP = false;
                    renderer.debugRenderFlags.PPROCESS_SPLIT = true;
                    info("post processing: split");
                }
                else if (renderer.debugRenderFlags.PPROCESS_SPLIT) {
                    renderer.debugRenderFlags.PPROCESS_SPLIT = false;
                    info("post processing: normal");
                }
                else {
                    renderer.debugRenderFlags.PPROCESS_SKIP = true;
                    info("post processing: skip");
                }
                break;
            default:
                break;
        }
    }

    private void onKeyUp(KeyEvent event)
    {
        switch (event.key)
        {
            case KeyEvent.KEY_W:
                keyDownW = false;
                break;
            case KeyEvent.KEY_S:
                keyDownS = false;
                break;
            case KeyEvent.KEY_A:
                keyDownA = false;
                break;
            case KeyEvent.KEY_D:
                keyDownD = false;
                break;
            default:
                break;
        }
    }

    private void onMouseButtonDown(MouseButtonEvent event)
    {
        if (event.button == MouseButtonEvent.BUTTON_LEFT) {
            window.relativeMouseMode = true;
        }
    }

    private void onMouseButtonUp(MouseButtonEvent event)
    {
        if (event.button == MouseButtonEvent.BUTTON_LEFT) {
            window.relativeMouseMode = false;
        }
    }

    private void onMouseMotion(MouseMotionEvent event)
    {
        if (window.relativeMouseMode)
        {
            if (event.motion.x != 0) {
                cameraAngleX = normalizeAngle(cameraAngleX - 0.01 * event.motion.x);
                updateCamera();
            }
            if (event.motion.y != 0) {
                cameraAngleY = clamp(cameraAngleY - 0.01 * event.motion.y, -0.49 * PI, 0.49 * PI);
                updateCamera();
            }
        }
    }

    private void onMouseWheel(MouseWheelEvent event)
    {
        if (event.scroll.y != 0) {
            cameraDistance = clamp(cameraDistance - 0.5 * event.scroll.y, 0.5, 50.0);
            updateCamera();
        }
    }
}
