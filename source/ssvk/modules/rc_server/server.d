module ssvk.modules.rc_server.server;

version(kirei_mod_rc_server):

version(kirei_mod_console) {}
else
{
    static assert(false, "RC server module requires console module.");
}

import core.time;
import std.concurrency;
import std.experimental.logger;
import std.socket;

private Tid threadId;

private enum
{
    BACKLOG_SIZE = 10,
    MAX_CONNECTIONS = 60,
    BUFFER_LENGTH = 4096
}

private void listenerThread(ushort port)
{
    Server server;

    try
    {
        server.initialize(port);
        server.run();
    }
    catch(Exception ex)
    {
        error("Exception in RC server thread:");
        error(ex);
    }
}

public void startServer(ushort port)
{
    threadId = spawn(&listenerThread, port);
}

public void stopServer()
{
    send(threadId, true);
}

struct Client
{
    string name;
    Socket socket;
    ubyte[BUFFER_LENGTH] buffer;
    size_t bufferPos = 0;

    this(Socket socket_)
    {
        name = socket_.remoteAddress.toAddrString();
        socket = socket_;
    }
}

struct Server
{
    TcpSocket serverSocket;
    Client[] connectedClients;

    public void initialize(ushort port)
    {
        serverSocket = new TcpSocket();
        serverSocket.blocking = false;
        serverSocket.setOption(SocketOptionLevel.SOCKET, SocketOption.REUSEADDR, true);
        serverSocket.bind(new InternetAddress(port));
        serverSocket.listen(BACKLOG_SIZE);

        info("RC server listening on port ", port);
    }

    public void run()
    {
        bool running = true;

        auto socketSet = new SocketSet(MAX_CONNECTIONS + 1);

        while (running)
        {
            receiveTimeout(Duration.zero, (bool){
                running = false;
            });

            socketSet.reset();
            socketSet.add(serverSocket);
            foreach (client; connectedClients) {
                socketSet.add(client.socket);
            }

            if (Socket.select(socketSet, null, null, dur!"msecs"(10)) > 0)
            {
                if (socketSet.isSet(serverSocket)) {
                    acceptConnection();
                }

                size_t[] clientsToRemove;
                foreach (idx, client; connectedClients)
                {
                    if (socketSet.isSet(client.socket))
                    {
                        if (!receiveFromClient(client))
                        {
                            clientsToRemove ~= idx;
                        }
                    }
                }

                removeClients(clientsToRemove);
            }
        }
    }

    private void acceptConnection()
    {
        Socket socket = serverSocket.accept();
        connectedClients ~= Client(socket);

        socket.send("Connected to SSVk remote console.\n");
    }

    private bool receiveFromClient(ref Client client)
    {

        ptrdiff_t received = client.socket.receive(client.buffer[client.bufferPos .. $]);
        if (received == 0)
        {
            info("RC server: client disconnected: ", client.name);
            return false;
        }
        if (received == Socket.ERROR)
        {
            warning("RC server: error while receiving from client: ", client.name);
            return false;
        }

        client.bufferPos += cast(size_t) received;

        processBuffer(client);
        return true;
    }

    private void removeClients(const(size_t[]) indices)
    {
        import std.algorithm : remove, SwapStrategy;
        
        foreach (idx; indices)
        {
            connectedClients[idx].socket.shutdown(SocketShutdown.BOTH);
            connectedClients = connectedClients.remove!(SwapStrategy.unstable)(idx);
        }
    }

    private void processBuffer(ref Client client)
    {
        import core.stdc.string : memmove;
        import std.algorithm.searching : countUntil;

        if (client.bufferPos < 1) {
            return;
        }
        
        size_t processed = 0;
        ubyte[] msg = client.buffer[0 .. client.bufferPos];
        ptrdiff_t lineEnd = msg.countUntil('\n');

        while (lineEnd > -1)
        {
            processed += lineEnd;

            processMessage(client, cast(string) msg[0 .. lineEnd]);

            msg = msg[lineEnd + 1 .. $];
            lineEnd = msg.countUntil('\n');
        }

        size_t restLength = client.bufferPos - processed;
        memmove(&client.buffer[0], &client.buffer[processed], restLength);
        client.bufferPos = restLength;
    }

    private void processMessage(ref Client client, string msg)
    {
        import ssvk.core.inter_thread.object_reference;
        info("RC server: received command: ", msg);

        auto app = getApplicationReference();
        app.queryProperties!(["stop", "totalTicks", "time", "window"])();
        info(app);
    }
}