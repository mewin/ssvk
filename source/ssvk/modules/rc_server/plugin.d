module ssvk.modules.rc_server.plugin;

version(kirei_mod_rc_server):

import ssvk.core.application;
import ssvk.core.plugin;
import ssvk.modules.rc_server.server;

class PluginRCServer : Plugin
{
    mixin(implementBaseObject());

    public override void initialize(Application application)
    {
        immutable(ushort) port = 11_387;

        startServer(port);
    }

    public override void cleanup()
    {
        stopServer();
    }
}
