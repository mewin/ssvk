module ssvk.modules.lua.lua_plugin;

version(kirei_mod_lua):

import std.algorithm.searching;
import std.conv;
import std.exception;
import std.experimental.logger;
import std.string;
import std.traits;
import std.variant;

import bindbc.lua;

import ssvk.core.application;
import ssvk.core.base_object;
import ssvk.core.plugin;
import ssvk.modules.lua.lua_util;
import ssvk.reflection.binding;

private void valueToLua(T)(lua_State* L, T value)
{
    mixin(checkStack(1));

    static if (is(T == bool))
    {
        lua_pushboolean(L, value ? 1 : 0);
    }
    else static if (__traits(isArithmetic, value))
    {
        lua_pushnumber(L, cast(double) value);
    }
    else static if (isAssignable!(string, T))
    {
        const(char)* zstr = value.toStringz();
        lua_pushstring(L, zstr);
    }
    else static if (isAssignable!(BaseObject, T))
    {
        LuaPlugin plugin = pluginFromState(L);
        if (plugin is null) {
            lua_pushnil(L);
        }
        else {
            plugin.pushWrappedObject!T(value);
        }
    }
    else
    {
        lua_pushnil(L);
    }
}

private int nextWrappedId = 0;

private Variant[] callWrappedLuaFunction(lua_State* L, int funcId, ref ParameterList params)
{
    int stackSize = lua_gettop(L);
    scope(exit) lua_settop(L, stackSize);

    // retrieve function from registry
    lua_pushnumber(L, funcId);
    lua_gettable(L, LUA_REGISTRYINDEX);

    // convert parameters to lua values
    foreach (param; params.values) {
        luaD_pushvariant(L, param);
    }
    
    // call function
    int err = lua_pcall(L, cast(int) params.values.length /* nargs */, LUA_MULTRET /* nresults */, 0 /* errfunc */);
    if (err)
    {
        error("Error in Lua function:");
        error(lua_tostring(L, -1).to!string());
        return [];
    }

    // convert results back from lua
    int nres = lua_gettop(L) - stackSize;
    Variant[] results;
    for (int i = 0; i < nres; ++i) {
        results ~= luaD_tovariant(L, -nres + i);
    }
    return results;
}

private LuaPlugin pluginFromState(lua_State* L)
{
    mixin(checkStack());

    lua_pushstring(L, PLUGIN_INSTANCE_KEY);
    lua_gettable(L, LUA_REGISTRYINDEX);
    if (lua_type(L, -1) != LUA_TLIGHTUSERDATA) {
        lua_pop(L, 1);
        return null;
    }
    LuaPlugin plugin = cast(LuaPlugin) lua_touserdata(L, -1);
    lua_pop(L, 1);
    return plugin;
}

struct GlobalFunctions
{
    static void print(ref ParameterList params)
    {
        string[] strs;

        foreach (val; params.values)
        {
            if (val.hasValue()) {
                strs ~= val.toString();
            }
            else {
                strs ~= "nil";
            }
        }
        
        info("Lua> ", strs.join("\t"));
    }
}

class LuaPlugin : Plugin
{
    private lua_State* L;
    private BaseObject[] usedObjects;

    public this(string filename)
    {
        immutable(char)* zstr = filename.toStringz();
        L = luaL_newstate();
        luaL_openlibs(L);
        luaD_initstate(L);
        
        initRegistry();
        initFunctions();
        int err = luaL_loadfile(L, zstr);
        if (!err) {
            err = lua_pcall(L, 0 /* nargs */, 1 /* nresults */, 0 /* errfunc */);
        }
        if (err)
        {
            error("Error loading Lua plugin:");
            error(lua_tostring(L, -1).to!string);
            throw new Exception("Error loading Lua plugin.");
        }

        // plugin should have pushed a table (the module) onto the stack (via return)
        enforce(lua_gettop(L) == 1, "Invalid Lua plugin, did not return module.");
        int toptype = lua_type(L, 1);
        if (toptype == LUA_TSTRING) {
            const(char)* msg = lua_tostring(L, 1);
            throw new Exception("Error loading Lua plugin:", msg.to!string());
        }
        enforce(toptype == LUA_TTABLE, "Invalid Lua plugin, module must be a table.");
    }

    public override void initialize(Application application)
    {
        enforce(lua_gettop(L) == 1 && lua_type(L, 1) == LUA_TTABLE, "Module table should be on top of the stack.");

        // the module (table) should always be on top of the stack
        lua_pushstring(L, "initialize");
        lua_gettable(L, 1);
        int type = lua_type(L, -1);

        // just in case there is no initialize() function in the module
        if (type != LUA_TNIL)
        {
            luaD_pushbaseobject(L, application); // push first parameter (the application) onto the stack
            int err = lua_pcall(L, 1 /* nargs */, 1 /* nresults */, 0 /* errfunc */);
            
            switch (err)
            {
                case LUA_ERRRUN:
                    error("Error initializing Lua plugin:");
                    error(lua_tostring(L, -1).to!string);
                    throw new Exception("Error initializing Lua plugin.");
                case LUA_ERRMEM:
                    error("Lua memory allocation error.");
                    break;
                default:
                    break;
            }
        }

        lua_settop(L, 1); // remove anything but the module from the stack
    }

    public override void cleanup()
    {

    }

    private void initRegistry()
    {
        lua_pushstring(L, PLUGIN_INSTANCE_KEY);
        lua_pushlightuserdata(L, cast(void*) this);
        lua_settable(L, LUA_REGISTRYINDEX);
    }

    private void initFunctions()
    {
        // lua_pushcfunction(L, &l_print);
        luaD_pushdfunction(L, &GlobalFunctions.print);
        lua_setglobal(L, "print");
    }
}
