module ssvk.modules.lua.lua_util;

version(kirei_mod_lua):

import core.memory;
import std.conv;
import std.exception;
import std.experimental.logger;
import std.functional;
import std.string;
import std.traits;
import std.variant;

import bindbc.lua;

import ssvk.core.base_object;
import ssvk.reflection.binding;
import ssvk.scripting.common;

enum
{
    PLUGIN_INSTANCE_KEY = "__plugin__",
    DELEGATE_TABLE_KEY = "__delegates__",
    DELEGATE_COUNTER_KEY = "__next_delegate_id__",
    METATABLE_KEY_UDATA_TYPE = "__type__",
    METATABLE_KEY_CLASS_NAME = "__class__",
    UDATA_TYPE_BASE_OBJECT = "__base_object__",
    UDATA_TYPE_DELEGATE = "__delegate__"
}

struct WrappedObject
{
    BaseObject object;
}

class LuaException : ScriptException
{
    const(int) error;

    public this(string msg, int error_, string file = __FILE__, size_t line = __LINE__)
    {
        super(msg, file, line);
        
        error = error_;
    }
}

package debug int oldStackValue;
pragma(inline, true)
{
package string checkStack(int difference = 0)
{
    debug {
        return ("
        int oldStackValue = lua_gettop(L);
        scope(exit) assert(lua_gettop(L) == oldStackValue + %d, \"Lua stack check failed in\" ~ __FUNCTION__ ~ \".\");
        ").format(difference);
    }
    else {
        return "";
    }
}
}

package int normalizeStackIndex(lua_State* L, int stackIdx) nothrow
{
    if (stackIdx < 0) {
        return lua_gettop(L) + stackIdx + 1;
    }
    return stackIdx;
}

private int l_baseobject_index(T)(lua_State* L) nothrow
{
    mixin(checkStack(1));
    luaL_checktype(L, 1, LUA_TUSERDATA);

    if (!checkMetatable!T(L, 1))
    {
        luaL_error(L, "Invalid metatable.");
        return 0;
    }

    WrappedObject* ptr = cast(WrappedObject*) lua_touserdata(L, 1);
    T object = cast(T) ptr.object;
    
    assert(object !is null);

    BaseClass clazz = assumeWontThrow(object.getClass());
    string memberName = luaL_checkstring(L, 2).to!string();
    if (memberName in clazz.functions)
    {
        luaD_pushdvarargfunction(L, (Variant[] args)
        {
            return clazz.functions[memberName].invoke(object, args);
        });
    }
    else if (memberName in clazz.properties)
    {
        auto prop = clazz.properties[memberName];
        if (!prop.getter)
        {
            const(char)* zstr = memberName.toStringz();
            luaL_error(L, "Property %s is write-only!", zstr);
        }
        else
        {
            try {
                luaD_pushvariant(L, prop.getter(object));
            }
            catch(Exception ex)
            {
                const(char)* zstrMember = memberName.toStringz();
                const(char)* zstrMsg = ex.msg.toStringz();
                luaL_error(L, "Exception in getter of %s: %s.", zstrMember, zstrMsg);
            }
        }
    }
    else {
        lua_pushnil(L);
    }

    return 1;
}

private int l_baseobject_newindex(T)(lua_State* L) nothrow
{
    mixin(checkStack());
    luaL_checktype(L, 1, LUA_TUSERDATA);

    if (!checkMetatable!T(L, 1))
    {
        luaL_error(L, "Invalid metatable.");
        return 0;
    }

    WrappedObject* ptr = cast(WrappedObject*) lua_touserdata(L, 1);
    T object = cast(T) ptr.object;
    
    assert(object);

    BaseClass clazz = assumeWontThrow(object.getClass());
    const(char)* zstr = luaL_checkstring(L, 2);
    string memberName = zstr.to!string();

    if (memberName in clazz.properties)
    {
        auto prop = clazz.properties[memberName];
        if (!prop.setter)
        {
            luaL_error(L, "Property %s is read-only!", zstr);
        }
        else
        {
            try
            {
                Variant value = luaD_tovariant(L, 3);
                prop.setter(object, value);
            }
            catch(Exception ex)
            {
                const(char)* zstrMember = memberName.toStringz();
                const(char)* zstrMsg = ex.msg.toStringz();
                luaL_error(L, "Exception in setter of %s: %s.", zstrMember, zstrMsg);
            }
            return 0;
        }
    }

    luaL_error(L, "Error setting value, invalid member name: %s", zstr);
    return 0;
}

private int l_baseobject_gc(lua_State* L) nothrow
{
    mixin(checkStack());
    luaL_checktype(L, 1, LUA_TUSERDATA);

    WrappedObject* ptr = cast(WrappedObject*) lua_touserdata(L, 1);

    GC.disable();
    GC.removeRoot(ptr);
    GC.removeRoot(ptr);
    GC.enable();

    return 0;
}

private int l_call_delegate(TDelegate)(lua_State* L) nothrow
{
    mixin(checkStack());
    
    // params: <delegate>, ...
    TDelegate* dele = cast(TDelegate*) lua_touserdata(L, 1);
    if (dele !is null)
    {
        ParameterList params;
        params.values = luaD_tovariantarray(L, 2, lua_gettop(L));

        try {
            params.unwrap(*dele);
        }
        catch(Exception e)
        {
            luaL_error(L, "Error in function call: %s.", e.msg.ptr);
        }
    }

    return 0;
}

private int l_call_vararg_delegate(lua_State* L) nothrow
{
    alias TDelegate = Variant delegate(Variant[]);
    mixin(checkStack(1));

    // params: <delegate>, ...
    TDelegate* dele = cast(TDelegate*) lua_touserdata(L, 1);
    if (dele !is null)
    {
        try
        {
            Variant[] params = luaD_tovariantarray(L, 2, lua_gettop(L));
            Variant result = (*dele)(params);
            luaD_pushvariant(L, result);
        }
        catch(Exception ex)
        {
            luaD_exception(L, "Exception while calling wrapped function: %s", ex);
        }
    }

    return 1;
}

private int l_delete_delegate(TDelegate)(lua_State* L) nothrow
{
    mixin(checkStack());

    // params: <delegate>
    TDelegate* dele = cast(TDelegate*) lua_touserdata(L, 1);
    if (dele !is null)
    {
        GC.disable();
        GC.removeRoot(dele);
        GC.removeRange(dele);
        GC.enable();
    }

    return 0;
}

private bool checkMetatable(T)(lua_State* L, int stackIdx) nothrow
{
    mixin(checkStack());
    int stackSize = lua_gettop(L);
    scope(exit) lua_settop(L, stackSize);

    // retrieve metatable
    lua_getmetatable(L, stackIdx);
    if (lua_type(L, -1) != LUA_TTABLE) {
        assumeWontThrow(error("checkMetatable: value does not have a metatable."));
        return false;
    }

    // check userdata type
    lua_pushstring(L, METATABLE_KEY_UDATA_TYPE);
    lua_gettable(L, -2);
    if (lua_type(L, -1) != LUA_TSTRING || lua_tostring(L, -1).to!string() != UDATA_TYPE_BASE_OBJECT) {
        assumeWontThrow(error("checkMetatable: metatable is missing correct userdata type."));
        return false;
    }
    lua_pop(L, 1);

    // check class
    lua_pushstring(L, METATABLE_KEY_CLASS_NAME);
    lua_gettable(L, -2);
    if (lua_type(L, -1) != LUA_TSTRING || lua_tostring(L, -1).to!string() != T.stringof) {
        assumeWontThrow(error("checkMetatable: incorrect or missing class name in metatable."));
        return false;
    }
    lua_pop(L, 1);

    // check __index
    lua_pushstring(L, "__index");
    lua_gettable(L, -2);
    if (lua_type(L, -1) != LUA_TFUNCTION || lua_tocfunction(L, -1) != &l_baseobject_index!T) {
        assumeWontThrow(error("checkMetatable: incorrect or missing __index function in metatable."));
        return false;
    }
    lua_pop(L, 1);

    // check __newindex
    lua_pushstring(L, "__newindex");
    lua_gettable(L, -2);
    if (lua_type(L, -1) != LUA_TFUNCTION || lua_tocfunction(L, -1) != &l_baseobject_newindex!T) {
        assumeWontThrow(error("checkMetatable: incorrect or missing __newindex function in metatable."));
        return false;
    }
    lua_pop(L, 1);

    return true;
}

enum UserDataType
{
    UNKNOWN,
    BASE_OBJECT,
    DELEGATE
}

UserDataType luaD_userdatatype(lua_State* L, int stackIdx) nothrow
{
    mixin(checkStack());

    int stackSize = lua_gettop(L);
    scope(exit) lua_settop(L, stackSize);
    
    if (!lua_getmetatable(L, stackIdx)) {
        return UserDataType.UNKNOWN;
    }

    lua_pushstring(L, METATABLE_KEY_UDATA_TYPE);
    lua_gettable(L, -2);
    if (lua_type(L, -1) != LUA_TSTRING) {
        return UserDataType.UNKNOWN;
    }

    switch (lua_tostring(L, -1).to!string())
    {
        case UDATA_TYPE_BASE_OBJECT:
            return UserDataType.BASE_OBJECT;
        case UDATA_TYPE_DELEGATE:
            return UserDataType.DELEGATE;
        default:
            return UserDataType.UNKNOWN;
    }
}

Variant[] delegate(Variant[]) luaD_todelegate(lua_State* L, int stackIdx) nothrow
{
    mixin(checkStack());
    stackIdx = normalizeStackIndex(L, stackIdx);

    // get next delegate id
    lua_pushstring(L, DELEGATE_COUNTER_KEY);
    lua_gettable(L, LUA_REGISTRYINDEX);
    immutable(int) delegateID = cast(int) lua_tonumber(L, -1);
    lua_pop(L, 1);

    // increase counter
    lua_pushstring(L, DELEGATE_COUNTER_KEY);
    lua_pushnumber(L, delegateID + 1);
    lua_settable(L, LUA_REGISTRYINDEX);

    // retrieve delegate table
    lua_pushstring(L, DELEGATE_TABLE_KEY);
    lua_gettable(L, LUA_REGISTRYINDEX);

    // push function onto delegate table
    lua_pushnumber(L, delegateID);
    lua_pushvalue(L, stackIdx);
    lua_settable(L, -3);
    lua_pop(L, 1);

    return (Variant[] args)
    {
        immutable(int) oldTop = lua_gettop(L);
        scope(exit) lua_settop(L, oldTop);

        // retrieve delegate table
        lua_pushstring(L, DELEGATE_TABLE_KEY);
        lua_gettable(L, LUA_REGISTRYINDEX);

        // retrieve delegate from table
        lua_pushnumber(L, delegateID);
        lua_gettable(L, -2);
        immutable(int) resStackIdx = lua_gettop(L);

        // push arguments
        luaD_pushvariantarray(L, args);
        immutable(int) err = lua_pcall(L, cast(int) args.length, LUA_MULTRET, 0);

        switch (err)
        {
            case LUA_ERRRUN:
                immutable(string) error = lua_tostring(L, -1).to!string();
                throw new LuaException("Lua runtime error: " ~ error, err);
            case LUA_ERRMEM:
                throw new LuaException("Lua memory error.", err);
            default:
                break;
        }

        return luaD_tovariantarray(L, resStackIdx, lua_gettop(L));
    };
}

Variant luaD_tovariant(lua_State* L, int stackIdx) nothrow
{
    mixin(checkStack());
    stackIdx = normalizeStackIndex(L, stackIdx);

    try
    {
        switch (lua_type(L, stackIdx))
        {
            case LUA_TBOOLEAN:
                return Variant(cast(bool) lua_toboolean(L, stackIdx));
            case LUA_TFUNCTION:
                return Variant(luaD_todelegate(L, stackIdx));
            case LUA_TNUMBER:
                return Variant(lua_tonumber(L, stackIdx));
            case LUA_TSTRING:
                return Variant(lua_tostring(L, stackIdx).to!string());
            case LUA_TUSERDATA, LUA_TLIGHTUSERDATA:
                if (luaD_userdatatype(L, stackIdx) == UserDataType.BASE_OBJECT) {
                    auto wrapped = cast(WrappedObject*) lua_touserdata(L, stackIdx);
                    return Variant(wrapped.object);
                }
                return Variant();
            case LUA_TTABLE:
                Variant[string] array;
                lua_pushnil(L); // ..., table, ..., nil
                while (lua_next(L, stackIdx) != 0)
                {
                    // ..., table, ..., key, value
                    lua_pushvalue(L, -2); // duplicate value, as lua_tostring() might change type and confuse lua_next()
                    // ..., table, ..., key, value, key
                    string key = lua_tostring(L, -1).to!string();
                    Variant value = luaD_tovariant(L, -2); // TODO: check for recursion
                    array[key] = value;
                    lua_pop(L, 2);
                    // ..., table, ..., key
                }
                // ..., table, ...
                return Variant(array);
            default:
                return Variant();
        }
    }
    catch(Exception e)
    {
        luaD_exception(L, "Error while converting argument %s:", e);
    }
    return Variant();
}

Variant[] luaD_tovariantarray(lua_State* L, int first, int last) nothrow
{
    Variant[] values;

    for (int i = first; i <= last; ++i)
    {
        values ~= luaD_tovariant(L, i);
    }

    return values;
}

void luaD_pushvariant(lua_State* L, Variant variant)
{
    mixin(checkStack(1));

    if (!variant.hasValue()) {
        lua_pushnil(L);
    }
    else if (variant.convertsTo!string()) {
        const(char)* zstr = variant.get!string().toStringz();
        lua_pushstring(L, zstr);
    }
    else if (variant.type() == typeid(bool)) {
        lua_pushboolean(L, variant.get!bool() ? 1 : 0);
    }
    else if (variant.convertsTo!double()) {
        lua_pushnumber(L, variant.get!double());
    }
    else if (variant.convertsTo!BaseObject()) {
        luaD_pushbaseobject(L, variant.get!BaseObject());
    }
    else if (variant.convertsTo!(Variant[string])) {
        luaD_pushvariantmap(L, variant.get!(Variant[string]));
    }
    else
    {
        warning("Do not know how to push value to lua stack: ", variant.toString());
        lua_pushnil(L);
    }
}

void luaD_pushvariantarray(lua_State* L, Variant[] variants)
{
    foreach (variant; variants) {
        luaD_pushvariant(L, variant);
    }
}

void luaD_pushvariantmap(lua_State* L, Variant[string] values)
{
    mixin(checkStack(1));

    lua_newtable(L);

    foreach (key, value; values)
    {
        const(char)* zstr = key.toStringz();
        lua_pushstring(L, zstr);
        luaD_pushvariant(L, value);
        lua_settable(L, -3);
    }
}

void luaD_pushbaseobject(T : BaseObject)(lua_State* L, T object)
{
    mixin(checkStack(1));

    WrappedObject* ptr = cast(WrappedObject*) lua_newuserdata(L, WrappedObject.sizeof);
    ptr.object = object;

    GC.disable();
    GC.addRange(ptr, WrappedObject.sizeof, typeid(WrappedObject));
    GC.addRoot(ptr);
    GC.enable();

    // create meta table
    lua_newtable(L);

    // __index table
    lua_pushstring(L, "__index");
    lua_pushcfunction(L, &l_baseobject_index!T);
    lua_settable(L, -3);

    // __newindex table
    lua_pushstring(L, "__newindex");
    lua_pushcfunction(L, &l_baseobject_newindex!T);
    lua_settable(L, -3);

    // __gc (deleter)
    lua_pushstring(L, "__gc");
    lua_pushcfunction(L, &l_baseobject_gc);
    lua_settable(L, -3);

    // type info
    lua_pushstring(L, METATABLE_KEY_UDATA_TYPE);
    lua_pushstring(L, UDATA_TYPE_BASE_OBJECT);
    lua_settable(L, -3);

    // class name (for debugging)
    lua_pushstring(L, METATABLE_KEY_CLASS_NAME);
    lua_pushstring(L, T.stringof);
    lua_settable(L, -3);

    lua_setmetatable(L, -2);
}

void luaD_pushdfunction(T, bool vararg = false)(lua_State* L, T func)
{
    mixin(checkStack(1));

    auto asDelegate = toDelegate(func);
    alias TDelegate = typeof(asDelegate);
    void* ud = lua_newuserdata(L, asDelegate.sizeof);
    TDelegate* udDelegate = cast(TDelegate*) ud;
    // udDelegate.__ctor();
    *udDelegate = asDelegate;

    // build metatable
    lua_newtable(L);

    // __call function
    lua_pushstring(L, "__call");
    static if (vararg)
    {
        static assert(is(ReturnType!TDelegate == Variant)
            && Parameters!TDelegate.length == 1 && is(Parameters!TDelegate[0] == Variant[]));
        lua_pushcfunction(L, &l_call_vararg_delegate);
    }
    else {
        lua_pushcfunction(L, &l_call_delegate!TDelegate);
    }
    lua_settable(L, -3);

    // __gc ("destructor")
    lua_pushstring(L, "__gc");
    lua_pushcfunction(L, &l_delete_delegate!TDelegate);
    lua_settable(L, -3);

    // set metatable
    lua_setmetatable(L, -2);

    // dont delete anything referenced by the delegate
    GC.addRange(ud, asDelegate.sizeof, typeid(asDelegate));
    GC.addRoot(ud);
}

void luaD_pushdvarargfunction(lua_State* L, Variant delegate(Variant[]) func) nothrow
{
    luaD_pushdfunction!(Variant delegate(Variant[]), true)(L, func);
}

int luaD_exception(lua_State* L, const(char)* msg, Exception ex) nothrow
{
    const(char)* zstr = ex.msg.toStringz();
    return luaL_error(L, msg, zstr);
}

void luaD_initstate(lua_State* L)
{
    mixin(checkStack());

    lua_pushstring(L, DELEGATE_TABLE_KEY);
    lua_newtable(L);
    lua_settable(L, LUA_REGISTRYINDEX);

    lua_pushstring(L, DELEGATE_COUNTER_KEY);
    lua_pushnumber(L, 1);
    lua_settable(L, LUA_REGISTRYINDEX);
}