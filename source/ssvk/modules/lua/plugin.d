module ssvk.modules.lua.plugin;

version(kirei_mod_lua):

import std.algorithm.searching;
import std.experimental.logger;
import std.file;
import std.path;

import bindbc.lua;

import ssvk.core.application;
import ssvk.core.plugin;
import ssvk.modules.lua.lua_plugin;

class PluginLua : Plugin
{
    private Application application;
    private LuaPlugin[] plugins;

    public override void initialize(Application application)
    {
        this.application = application;

        immutable(LuaSupport) ret = loadLua();

        if(ret == LuaSupport.noLibrary)
        {
            warning("Failed to load lua!");
            return;
        }
        else if(LuaSupport.badLibrary)
        {
            warning("Bad lua library, this may cause problems!");
        }

        // load lua "plugins"
        try
        {
            foreach (DirEntry entry; dirEntries("assets/lua/plugins", SpanMode.shallow))
            {
                if (entry.isFile() && entry.name.endsWith(".lua")) {
                    loadLuaPlugin(entry.name);
                }
                else if (entry.isDir()) {
                    string path = buildPath(entry.name, "init.lua");
                    if (path.exists()) {
                        loadLuaPlugin(path);
                    }
                }
            }
        }
        catch(Exception ex)
        {
            warning(ex.msg);
        }

        foreach (plugin; plugins)
        {
            plugin.initialize(application);
        }
    }

    private void loadLuaPlugin(string filename)
    {
        plugins ~= new LuaPlugin(filename);
    }
}
