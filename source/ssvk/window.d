module ssvk.window;

import std.array;
import std.conv;
import std.exception;
import std.string;
import std.typecons;
import std.traits;

import bindbc.sdl;

import ssvk.core.application;
import ssvk.core.base_object;
import ssvk.core.types;

struct WindowCreateInfo
{
    public uint width = 1280;
    public uint height = 720;
}

enum KeyModifierBits
{
    NONE      = 0,
    LSHIFT    = (1 << 0),
    RSHIFT    = (1 << 1),
    LCTRL     = (1 << 2),
    RCTRL     = (1 << 3),
    LMETA     = (1 << 4),
    RMETA     = (1 << 5),
    LALT      = (1 << 6),
    RALT      = (1 << 7),
    ALT_GR    = (1 << 8),
    CAPS_LOCK = (1 << 9),
    NUM_LOCK  = (1 << 10),
    
    SHIFT  = LSHIFT | RSHIFT,
    CTRL   = LCTRL | RCTRL,
    META   = LMETA | RMETA,
    ALT    = LALT | RALT
}

alias KeyModifierFlags = BitFlags!(KeyModifierBits, Yes.unsafe);

// TODO: input server or something?
struct KeyEvent
{
    public bool pressed = false;
    public bool repeat = false;
    public int key = 0;
    public int scancode = 0;
    public KeyModifierFlags modifiers;

    // lazy mans way of providing the key event values
    // TODO: typing this out might be a good idea
    static int opDispatch(string s)() if (s.length > 4 && s[0..4] == "KEY_")
    {
        // SDL defines SDLK_a - SDLK_z for letters
        // i dont like that
        static if (s.length == 5 && s[4] >= 'A' && s[4] <= 'Z') {
            return mixin("SDLK_", cast(char) (s[4] - 'A' + 'a'));
        }
        else {
            return mixin("SDLK_", s[4..$]);
        }
    }

    static int opDispatch(string s)() if (s.length > 9 && s[0..9] == "SCANCODE_")
    {
        return mixin("SDL_SCANCODE_", s[9..$]);
    }
}

struct MouseButtonEvent
{
    public enum BUTTON_LEFT = SDL_BUTTON_LEFT;
    public enum BUTTON_RIGHT = SDL_BUTTON_RIGHT;
    public enum BUTTON_MIDDLE = SDL_BUTTON_MIDDLE;

    public bool pressed = false;
    public int clicks = 1;
    public int button = BUTTON_LEFT;
    public Point2DI point;
}

struct MouseMotionEvent
{
    public Point2DI point;
    public Distance2DI motion;
}

struct MouseWheelEvent
{
    public Distance2DI scroll;
}

/**
 * Signals:
 *  - close()
 *  - key_down(KeyEvent)
 *  - key_up(KeyEvent)
 *  - mouse_down(MouseEvent)
 *  - mouse_up(MouseEvent)
 */
class Window : BaseObject
{
    mixin(implementBaseObject());
    
    enum
    {
        SIG_CLOSE             = "close",
        SIG_KEY_DOWN          = "key_down",
        SIG_KEY_UP            = "key_up",
        SIG_MOUSE_BUTTON_DOWN = "mouse_down",
        SIG_MOUSE_BUTTON_UP   = "mouse_up",
        SIG_MOUSE_MOTION      = "mouse_motion",
        SIG_MOUSE_WHEEL       = "mouse_wheel"
    }

    private Application app;
    public SDL_Window* sdlHandle;

    public void create()(Application app, auto ref const(WindowCreateInfo) createInfo)
    {
        this.app = app;

        // load SDL
        SDLSupport ret = loadSDL();
        enforce(ret == sdlSupport);

        // init SDL
        if (SDL_Init(SDL_INIT_VIDEO) != 0) {
            throw new Exception("Could not init SDL.");
        }

        // create the window
        sdlHandle = SDL_CreateWindow("SSVk", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, createInfo.width,
                createInfo.height, SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);
    }

    public void update()
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            handleEvent(event);
        }
    }

    public void cleanup()
    {
        SDL_DestroyWindow(sdlHandle);
        SDL_Quit();
    }

    public Extent2DI getFramebufferSize()
    {
        int width, height;
        SDL_Vulkan_GetDrawableSize(sdlHandle, &width, &height);

        return Extent2DI(width, height);
    }
    
    @property
    public bool inputGrabbed()
    {
        return SDL_GetWindowGrab(sdlHandle) == SDL_TRUE;
    }

    @property
    public void inputGrabbed(bool value)
    {
        SDL_SetWindowGrab(sdlHandle, value ? SDL_TRUE : SDL_FALSE);
    }

    @property
    public bool cursorVisible()
    {
        return SDL_ShowCursor(SDL_QUERY) == SDL_ENABLE;
    }
    
    @property
    public void cursorVisible(bool value)
    {
        SDL_ShowCursor(value ? SDL_ENABLE : SDL_DISABLE);
    }

    @property
    public bool relativeMouseMode()
    {
        return SDL_GetRelativeMouseMode() == SDL_TRUE;
    }

    @property
    public void relativeMouseMode(bool value)
    {
        SDL_SetRelativeMouseMode(value ? SDL_TRUE : SDL_FALSE);
    }

    @property
    public string title()
    {
        return SDL_GetWindowTitle(sdlHandle).to!string;
    }

    @property
    public void title(string value)
    {
        SDL_SetWindowTitle(sdlHandle, value.toStringz);
    }

    @property
    public bool fullscreen()
    {
        return (SDL_GetWindowFlags(sdlHandle) & SDL_WINDOW_FULLSCREEN) != 0;
    }

    @property
    public void fullscreen(bool value)
    {
        SDL_SetWindowFullscreen(sdlHandle, value ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
    }

    private void handleEvent(ref const(SDL_Event) event)
    {
        switch (event.type)
        {
            case SDL_KEYDOWN:
                KeyEvent keyEvent = {
                    pressed: true,
                    repeat: event.key.repeat > 0,
                    scancode: event.key.keysym.scancode,
                    key: event.key.keysym.sym,
                    modifiers: convertSDLKeyModifiers(event.key.keysym.mod)
                };
                emitSignal(SIG_KEY_DOWN, keyEvent);
                break;
            case SDL_KEYUP:
                KeyEvent keyEvent = {
                    pressed: false,
                    repeat: false,
                    scancode: event.key.keysym.scancode,
                    key: event.key.keysym.sym,
                    modifiers: convertSDLKeyModifiers(event.key.keysym.mod)
                };
                emitSignal(SIG_KEY_UP, keyEvent);
                break;
            case SDL_QUIT:
                emitSignal(SIG_CLOSE);
                break;
            case SDL_MOUSEBUTTONDOWN:
                MouseButtonEvent mouseButtonEvent = {
                    pressed: true,
                    clicks: event.button.clicks,
                    button: event.button.button,
                    point: {
                        x: event.button.x,
                        y: event.button.y
                    }
                };
                emitSignal(SIG_MOUSE_BUTTON_DOWN, mouseButtonEvent);
                break;
            case SDL_MOUSEBUTTONUP:
                MouseButtonEvent mouseMotionEvent = {
                    pressed: false,
                    clicks: event.button.clicks,
                    button: event.button.button,
                    point: {
                        x: event.button.x,
                        y: event.button.y
                    }
                };
                emitSignal(SIG_MOUSE_BUTTON_UP, mouseMotionEvent);
                break;
            case SDL_MOUSEMOTION:
                MouseMotionEvent mouseMotionEvent = {
                    point: {
                        x: event.motion.x,
                        y: event.motion.y
                    },
                    motion: {
                        x: event.motion.xrel,
                        y: event.motion.yrel
                    }
                };
                emitSignal(SIG_MOUSE_MOTION, mouseMotionEvent);
                break;
            case SDL_MOUSEWHEEL:
                MouseWheelEvent mouseWheelEvent = {
                    scroll: {
                        x: event.wheel.x,
                        y: event.wheel.y
                    }
                };
                emitSignal(SIG_MOUSE_WHEEL, mouseWheelEvent);
                break;
            default:
                break;
        }
    }
}

KeyModifierFlags convertSDLKeyModifiers(Uint16 sdlValue)
{
    KeyModifierFlags result;

    if (sdlValue & KMOD_LSHIFT) {
        result.LSHIFT = true;
    }
    if (sdlValue & KMOD_RSHIFT) {
        result.RSHIFT = true;
    }
    if (sdlValue & KMOD_LCTRL) {
        result.LCTRL = true;
    }
    if (sdlValue & KMOD_RCTRL) {
        result.RCTRL = true;
    }
    if (sdlValue & KMOD_LALT) {
        result.LALT = true;
    }
    if (sdlValue & KMOD_RALT) {
        result.RALT = true;
    }
    if (sdlValue & KMOD_LGUI) {
        result.LMETA = true;
    }
    if (sdlValue & KMOD_RGUI) {
        result.RMETA = true;
    }
    if (sdlValue & KMOD_NUM) {
        result.NUM_LOCK = true;
    }
    if (sdlValue & KMOD_CAPS) {
        result.CAPS_LOCK = true;
    }
    if (sdlValue & KMOD_MODE) {
        result.ALT_GR = true;
    }

    return result;
}
