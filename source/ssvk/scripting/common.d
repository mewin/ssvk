module ssvk.scripting.common;

class ScriptException : Exception
{
    public this(string msg, string file = __FILE__, size_t line = __LINE__)
    {
        super(msg, file, line);
    }
}
