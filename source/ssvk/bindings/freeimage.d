module ssvk.bindings.freeimage;

import std.exception;
import std.experimental.logger;

import bindbc.freeimage : FISupport, fiSupport, loadFreeImage, FreeImage_Initialise, FreeImage_DeInitialise;

import ssvk.core.application;
import ssvk.core.plugin;

class PluginFreeImage : Plugin
{
    public override void initialize(Application application)
    {
		immutable(FISupport) ret = loadFreeImage();
		if (ret != fiSupport)
		{
			error("Missing FreeImage library.");
			throw new Exception("Missing FreeImage library.");
		}
		FreeImage_Initialise();
    }

    public override void cleanup()
    {
		FreeImage_DeInitialise();
    }
}