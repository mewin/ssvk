module ssvk.bindings.assimp;

import std.experimental.logger;

import bindbc.assimp;

import ssvk.core.application;
import ssvk.core.plugin;

class PluginAssimp : Plugin
{
    public override void initialize(Application application)
    {
		immutable(AssimpSupport) assimp = loadAssimp();
		if (assimp == AssimpSupport.noLibrary)
		{
			error("Missing Assimp library");
			throw new Exception("Missing Assimp library.");
		}
		if (assimp == AssimpSupport.badLibrary) {
			warning("Bad Assimp library version, this may cause problems.");
		}
		infof("Assimp version: %d.%d.%d", aiGetVersionMajor(), aiGetVersionMinor(), aiGetVersionRevision());
    }
}
