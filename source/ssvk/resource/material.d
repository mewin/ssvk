module ssvk.resource.material;

import std.variant;

import ssvk.loaders.bitmap;
import ssvk.loaders.shader;
import ssvk.resource.resource;
import ssvk.resource.shader;
import ssvk.resource.bitmap;

enum
{
    BINDING_PBR_ALBEDO             = 0,
    BINDING_PBR_NORMAL             = 1,
    BINDING_PBR_METALLIC_ROUGHNESS = 2,
    BINDINB_PBR_AMBIENT_OCCLUSION  = 3
}

enum
{
    PARAMETER_PBR_ALBEDO       = "albedo",
    PARAMETER_PBR_ROUGHNESS    = "roughness",
    PARAMETER_PBR_METALLIC     = "metallic",
    PARAMETER_REFRACTIVE_INDEX = "refractiveIndex"
}

class Material : Resource
{
    mixin(implementBaseObject());
    
    public Bitmap[uint] boundTextures; // TODO: Texture class as proxy to allow render-to-texture etc
    public Variant[string] parameters;

    public T getParameterAs(alias T)(string name, T def = T.init)
    {
        if (name !in parameters) {
            return def;
        }
        if (parameters[name].convertsTo!T()) {
            return parameters[name].get!T();
        }
        return def;
    }
}

private Material defaultMaterial;

@property
Material DEFAULT_MATERIAL()
{
    if (defaultMaterial is null)
    {
        defaultMaterial = new Material();
        defaultMaterial.boundTextures[BINDING_PBR_ALBEDO]             = loadBitmap("assets/images/dirty_gold_01/albedo.png");
        defaultMaterial.boundTextures[BINDING_PBR_NORMAL]             = loadBitmap("assets/images/dirty_gold_01/normal.png");
        defaultMaterial.boundTextures[BINDING_PBR_METALLIC_ROUGHNESS] = loadBitmap("assets/images/dirty_gold_01/metallic_roughness.png");
        defaultMaterial.boundTextures[BINDINB_PBR_AMBIENT_OCCLUSION]  = loadBitmap("assets/images/dirty_gold_01/ao.png");
    }
    return defaultMaterial;
}
