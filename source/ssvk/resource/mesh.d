module ssvk.resource.mesh;

import std.algorithm;

import gfm.math;

import ssvk.core.types;
import ssvk.resource.material;
import ssvk.resource.resource;

class Mesh : Resource
{
    mixin(implementBaseObject());
    
    public Material material;
    private Vertex[] vertices_;
    private uint[] indices_;

    public this(size_t nVertices, size_t nIndices)
    {
        vertices_.length = nVertices;
        indices_.length = nIndices;
    }

    public this(Vertex[] vertices, uint[] indices = [])
    {
        vertices_ = vertices;
        indices_ = indices;
    }

    @property
    public inout(Vertex[]) vertices() inout
    {
        return vertices_;
    }

    @property
    public inout(uint[]) indices() inout
    {
        return indices_;
    }

    @property
    public AABB aabb() const
    {
        AABB result;

        foreach (vertex; vertices_)
        {
            result.pointMin.x = min(result.pointMin.x, vertex.position.x);
            result.pointMin.y = min(result.pointMin.y, vertex.position.y);
            result.pointMin.z = min(result.pointMin.z, vertex.position.z);
            
            result.pointMax.x = max(result.pointMax.x, vertex.position.x);
            result.pointMax.y = max(result.pointMax.y, vertex.position.y);
            result.pointMax.z = max(result.pointMax.z, vertex.position.z);
        }

        return result;
    }

    public Vertex[] unpackVertices() const
    {
        Vertex[] unpacked;

        unpacked.length = indices.length;
        foreach (i, index; indices)
        {
            unpacked[i] = vertices[index];
        }

        return unpacked;
    }
}
