module ssvk.resource.resource_server;

import std.path;
import std.typecons;

import ssvk.resource.resource;

class ResourceLoader
{
    public abstract TypeInfo_Class getType();
    public abstract Resource load(string filename);
    public bool canLoad(string filename)
    {
        return true;
    }
}

class ResourceException : Exception
{
    private string filename_;

    public this(string filename, string file = __FILE__, size_t line = __LINE__)
    {
        super("Failed to load resource at " ~ filename, file, line);
        this.filename_ = filename;
    }

    @property
    public string filename() const
    {
        return filename_;
    }
}

struct ResourceServer
{
    private ResourceLoader[][TypeInfo_Class] loaders;
    private Resource[string] loadedResources;

    public T tryLoad(T = Resource)(string filename) nothrow
    {
        filename = filename.asNormalizedPath();

        // check if the resource has already been loaded from that path
        if (filename in loadedResources) {
            return loadedResources[filename];
        }

        TypeInfo_Class = typeid(T);
        // check if it is loadable by an loader
        if (tp !in loaders) {
            return null;
        }

        // try to find a suitable loader
        foreach (loader; loaders[tp])
        {
            if (loader.canLoad(filename))
            {
                T resource = cast(T) loader.load(filename);
                if (resource) {
                    loadedResources[filename] = resource;
                }
                return resource;
            }
        }

        return null;
    }

    public T load(T = Resource)(string filename)
    {
        T loaded = tryLoad!T(filename);
        if (loaded is null) {
            throw new ResourceException(filename);
        }
        return loaded;
    }

    public void addLoader(ResourceLoader loader)
    {
        loaders.require(loader.getType()) ~= loader;
    }
}

__gshared ResourceServer resourceServer;
