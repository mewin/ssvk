module ssvk.resource.shader;

import std.algorithm;
import std.array;
import std.conv;
import std.exception;
import std.experimental.logger;
import std.string;
import std.traits;
import std.typecons;

import spirv;
import spirv.cross;

import ssvk.resource.resource;

private void enforceSPVC(spvc_result result)
{
    enforce(result == spvc_result.SPVC_SUCCESS);
}

extern(C) private void spvcErrorCallback(void* userdata, const(char)* errorMsg)
{
    error(errorMsg);
}

enum DataBaseType
{
    UNKNOWN,
    VOID,
    BOOLEAN,
    INT8,
    UINT8,
    INT16,
    UINT16,
    INT32,
    UINT32,
    INT64,
    UINT64,
    ATOMIC_COUNTER,
    FLOAT16,
    FLOAT32,
    FLOAT64,
    STRUCT,
    IMAGE,
    SAMPLED_IMAGE,
    SAMPLER
}

enum ShaderStage
{
    UNKNOWN                = 0,
    VERTEX                 = 1,
    TESSELATION_CONTROL    = 2,
    TESSELATION_EVALUATION = 3,
    GEOMETRY               = 4,
    FRAGMENT               = 5,
    COMPUTE                = 6,
    KERNEL                 = 7,
    TASK                   = 8,
    MESH                   = 9,
    RAY_GENERATION         = 10,
    INTERSECTION           = 11,
    ANY_HIT                = 12,
    CLOSEST_HIT            = 13,
    MISS                   = 14,
    CALLABLE               = 15
}

template EnumToFlags(T)
{
    mixin(generateEnumMembers());

    string generateEnumMembers()
    {
        string result = "enum EnumToFlags { NONE = 0,";
        static foreach (i, member; EnumMembers!T) {
            result ~= format("%s = (1 << %d),", member, i);
        }
        result ~= "}";
        return result;
    }
}
EnumToFlags!T flagFromEnum(T)(T flag) if (is(T Base == enum))
{
    return cast(EnumToFlags!T) (1 << flag);
}

alias ShaderStageBits = EnumToFlags!ShaderStage;
alias ShaderStageFlags = BitFlags!ShaderStageBits;

struct DataType
{
    DataBaseType baseType;
    uint vectorSize = 1;
    uint columns = 1;
    uint arraySize = 1;
    DataMember[] structMembers;
}

struct DataMember
{
    string name;
    size_t offset;
    DataType type;
}

struct UniformBufferAttachment
{
    uint binding;
    uint set;
    DataMember[] members;
    ShaderStageFlags stages;
}
alias StorageBufferAttachment = UniformBufferAttachment;

struct InputAttachment
{
    uint binding;
    uint set;
    ShaderStageFlags stages = ShaderStageBits.FRAGMENT;
}

struct AccelerationStructureAttachment
{
    uint binding;
    uint set;
    ShaderStageFlags stages;
}

struct SampledImage
{
    string name;
    uint binding;
    uint set;
    ShaderStageFlags stages;
}
alias StorageImage = SampledImage;

struct Variable
{
    string name;
    uint location;
    uint binding;
    uint set;
    DataType type;
}

struct PushConstant
{
    string name;
    uint offset;
    DataType type;
    ShaderStageFlags stages;
}

struct SpecializationConstant
{
    uint id;
    DataType type;
}

struct SpirVParser
{
    UniformBufferAttachment[] uniformBuffers;
    StorageBufferAttachment[] storageBuffers;
    InputAttachment[] inputAttachments;
    AccelerationStructureAttachment[] accelerationStructures;
    SampledImage[] sampledImages;
    StorageImage[] storageImages;
    PushConstant[] pushConstants;
    SpecializationConstant[] specializationConstants;
    Variable[] inputVariables;
    Variable[] outputVariables;
    ShaderStage shaderStage = shaderStage.UNKNOWN;

    private spvc_context context;
    private spvc_compiler compiler;
    private spvc_resources resources;

    void parse(const(uint[]) code)
    {
        // setup spvc context
        spvc_context_create(&context).enforceSPVC();
        scope(exit) spvc_context_destroy(context);
        spvc_context_set_error_callback(context, &spvcErrorCallback, null);

        // parse spirv
        spvc_parsed_ir ir;
        spvc_context_parse_spirv(context, code.ptr, code.length, &ir).enforceSPVC();

        // create compiler
        spvc_context_create_compiler(context, spvc_backend.SPVC_BACKEND_NONE, ir,
                spvc_capture_mode.SPVC_CAPTURE_MODE_TAKE_OWNERSHIP, &compiler).enforceSPVC();
        
        // create resources
        spvc_compiler_create_shader_resources(compiler, &resources).enforceSPVC();

        parseShaderInfo();
        parseUniformBuffers();
        parseStorageBuffers();
        parseInputAttachments();
        parseAccelerationStructures();
        parseSampledImages();
        parseStorageImages();
        parsePushConstants();
        parseSpecializationConstants();
        parseInputs();
        parseOutputs();
    }

    private void parseShaderInfo()
    {
        shaderStage = shaderStageFromSPVC(spvc_compiler_get_execution_model(compiler));
    }

    private void parseUniformBuffers()
    {
        const(spvc_reflected_resource)* list = null;
        size_t count;
        spvc_resources_get_resource_list_for_type(resources, spvc_resource_type.SPVC_RESOURCE_TYPE_UNIFORM_BUFFER,
                &list, &count).enforceSPVC();
        uniformBuffers.length = count;

        for (size_t i = 0; i < count; ++i)
        {
            spvc_type_id typeId = list[i].type_id;
            spvc_type type = spvc_compiler_get_type_handle(compiler, typeId);

            uniformBuffers[i].members = parseStruct(type);
            uniformBuffers[i].binding = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationBinding);
            uniformBuffers[i].set = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationDescriptorSet);
        }
    }

    private void parseStorageBuffers()
    {
        const(spvc_reflected_resource)* list = null;
        size_t count;
        spvc_resources_get_resource_list_for_type(resources, spvc_resource_type.SPVC_RESOURCE_TYPE_STORAGE_BUFFER,
                &list, &count).enforceSPVC();
        storageBuffers.length = count;

        for (size_t i = 0; i < count; ++i)
        {
            spvc_type_id typeId = list[i].type_id;
            spvc_type type = spvc_compiler_get_type_handle(compiler, typeId);

            storageBuffers[i].members = parseStruct(type);
            storageBuffers[i].binding = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationBinding);
            storageBuffers[i].set = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationDescriptorSet);
        }
    }

    private void parseInputAttachments()
    {
        const(spvc_reflected_resource)* list = null;
        size_t count;
        spvc_resources_get_resource_list_for_type(resources,
                spvc_resource_type.SPVC_RESOURCE_TYPE_ACCELERATION_STRUCTURE, &list, &count).enforceSPVC();
        accelerationStructures.length = count;

        for (size_t i = 0; i < count; ++i)
        {
            accelerationStructures[i].binding = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationBinding);
            accelerationStructures[i].set = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationDescriptorSet);
        }
    }

    private void parseAccelerationStructures()
    {
        const(spvc_reflected_resource)* list = null;
        size_t count;
        spvc_resources_get_resource_list_for_type(resources, spvc_resource_type.SPVC_RESOURCE_TYPE_SUBPASS_INPUT,
                &list, &count).enforceSPVC();
        inputAttachments.length = count;

        for (size_t i = 0; i < count; ++i)
        {
            inputAttachments[i].binding = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationBinding);
            inputAttachments[i].set = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationDescriptorSet);
        }
    }

    private void parseSampledImages()
    {
        const(spvc_reflected_resource)* list = null;
        size_t count;
        spvc_resources_get_resource_list_for_type(resources, spvc_resource_type.SPVC_RESOURCE_TYPE_SAMPLED_IMAGE,
                &list, &count).enforceSPVC();
        sampledImages.length = count;

        for (size_t i = 0; i < count; ++i)
        {
            sampledImages[i].name = to!string(list[i].name);
            sampledImages[i].binding = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationBinding);
            sampledImages[i].set = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationDescriptorSet);
        }
    }

    private void parseStorageImages()
    {
        const(spvc_reflected_resource)* list = null;
        size_t count;
        spvc_resources_get_resource_list_for_type(resources, spvc_resource_type.SPVC_RESOURCE_TYPE_STORAGE_IMAGE,
                &list, &count).enforceSPVC();
        storageImages.length = count;

        for (size_t i = 0; i < count; ++i)
        {
            storageImages[i].name = to!string(list[i].name);
            storageImages[i].binding = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationBinding);
            storageImages[i].set = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationDescriptorSet);
        }
    }

    private void parsePushConstants()
    {
        const(spvc_reflected_resource)* list = null;
        size_t count;
        spvc_resources_get_resource_list_for_type(resources, spvc_resource_type.SPVC_RESOURCE_TYPE_PUSH_CONSTANT,
                &list, &count).enforceSPVC();
        pushConstants.length = count;

        for (size_t i = 0; i < count; ++i)
        {
            spvc_type_id typeId = list[i].type_id;
            spvc_type type = spvc_compiler_get_type_handle(compiler, typeId);

            pushConstants[i].name = to!string(list[i].name);
            pushConstants[i].offset = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationOffset);
            pushConstants[i].type = parseType(type);
        }
    }

    private void parseSpecializationConstants()
    {
        const(spvc_specialization_constant)* list = null;
        size_t count;
        spvc_compiler_get_specialization_constants(compiler, &list, &count).enforceSPVC();
        specializationConstants.length = count;

        for (size_t i = 0; i < count; ++i)
        {
            spvc_constant handle = spvc_compiler_get_constant_handle(compiler, list[i].id);
            spvc_type_id typeId = spvc_constant_get_type(handle);
            spvc_type type = spvc_compiler_get_type_handle(compiler, typeId);

            specializationConstants[i].id = list[i].constant_id;
            specializationConstants[i].type = parseType(type);
        }
    }

    private void parseInputs()
    {
        inputVariables = parseVariables(spvc_resource_type.SPVC_RESOURCE_TYPE_STAGE_INPUT);
    }

    private void parseOutputs()
    {
        outputVariables = parseVariables(spvc_resource_type.SPVC_RESOURCE_TYPE_STAGE_OUTPUT);
    }
    
    private Variable[] parseVariables(spvc_resource_type resourceType)
    {
        Variable[] variables;
        
        const(spvc_reflected_resource)* list = null;
        size_t count;
        spvc_resources_get_resource_list_for_type(resources, resourceType,
                &list, &count).enforceSPVC();
        variables.length = count;

        for (size_t i = 0; i < count; ++i) {
            spvc_type_id typeId = list[i].type_id;
            spvc_type type = spvc_compiler_get_type_handle(compiler, typeId);

            variables[i].name = to!string(list[i].name);
            variables[i].type = parseType(type);
            variables[i].location = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationLocation);
            variables[i].binding = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationBinding);
            variables[i].set = spvc_compiler_get_decoration(compiler, list[i].id,
                    SpvDecoration.SpvDecorationDescriptorSet);
        }

        return variables;
    }

    private DataMember[] parseStruct(spvc_type type)
    in {
        assert(spvc_type_get_basetype(type) == spvc_basetype.SPVC_BASETYPE_STRUCT);
    }
    body
    {
        DataMember[] dataMembers;
        uint nMembers = spvc_type_get_num_member_types(type);
        dataMembers.length = nMembers;

        foreach (j, ref dataMember; dataMembers)
        {
            immutable(uint) index = cast(uint) j;
            
            // name
            dataMember.name = getStructMemberName(type, index);

            // struct offset
            uint offset;
            spvc_compiler_type_struct_member_offset(compiler, type, index, &offset).enforceSPVC();
            dataMember.offset = offset;

            // type
            spvc_type_id memberTypeId = spvc_type_get_member_type(type, index);
            spvc_type memberType = spvc_compiler_get_type_handle(compiler, memberTypeId);

            dataMember.type = parseType(memberType);
        }

        return dataMembers;
    }

    private DataType parseType(spvc_type type)
    {
        DataType dataType;

        dataType.baseType = baseTypeFromSPVC(type);
        dataType.vectorSize = spvc_type_get_vector_size(type);
        dataType.columns = spvc_type_get_columns(type);
        dataType.arraySize = spvc_type_get_num_array_dimensions(type);

        if (dataType.baseType == DataBaseType.STRUCT) {
            dataType.structMembers = parseStruct(type);
            // TODO: check for recursion (?)
            // this would definitely be a bug in SPIRV-Cross
        }

        return dataType;
    }

    private string getStructMemberName(spvc_type type, uint index)
    in {
        assert(spvc_type_get_basetype(type) == spvc_basetype.SPVC_BASETYPE_STRUCT);
    }
    body
    {
        spvc_type_id self = spvc_type_get_base_type_id(type);
        const(char)* name = spvc_compiler_get_member_name(compiler, self, index);
        if (name is null || name[0] == '\0') {
            return format("m_%u", index);
        }
        else {
            return name.to!string;
        }
    }

    private DataBaseType baseTypeFromSPVC(spvc_type type)
    {
        immutable(spvc_basetype) baseType = spvc_type_get_basetype(type);
        switch (baseType)
        {
            default:
            case spvc_basetype.SPVC_BASETYPE_UNKNOWN:
                return DataBaseType.UNKNOWN;
            case spvc_basetype.SPVC_BASETYPE_VOID:
                return DataBaseType.VOID;
            case spvc_basetype.SPVC_BASETYPE_BOOLEAN:
                return DataBaseType.BOOLEAN;
            case spvc_basetype.SPVC_BASETYPE_INT8:
                return DataBaseType.INT8;
            case spvc_basetype.SPVC_BASETYPE_UINT8:
                return DataBaseType.UINT8;
            case spvc_basetype.SPVC_BASETYPE_INT16:
                return DataBaseType.INT16;
            case spvc_basetype.SPVC_BASETYPE_UINT16:
                return DataBaseType.UINT16;
            case spvc_basetype.SPVC_BASETYPE_INT32:
                return DataBaseType.INT32;
            case spvc_basetype.SPVC_BASETYPE_UINT32:
                return DataBaseType.UINT32;
            case spvc_basetype.SPVC_BASETYPE_INT64:
                return DataBaseType.INT64;
            case spvc_basetype.SPVC_BASETYPE_UINT64:
                return DataBaseType.UINT64;
            case spvc_basetype.SPVC_BASETYPE_ATOMIC_COUNTER:
                return DataBaseType.ATOMIC_COUNTER;
            case spvc_basetype.SPVC_BASETYPE_FP16:
                return DataBaseType.FLOAT16;
            case spvc_basetype.SPVC_BASETYPE_FP32:
                return DataBaseType.FLOAT32;
            case spvc_basetype.SPVC_BASETYPE_FP64:
                return DataBaseType.FLOAT64;
            case spvc_basetype.SPVC_BASETYPE_STRUCT:
                return DataBaseType.STRUCT;
            case spvc_basetype.SPVC_BASETYPE_IMAGE:
                return DataBaseType.IMAGE;
            case spvc_basetype.SPVC_BASETYPE_SAMPLED_IMAGE:
                return DataBaseType.SAMPLED_IMAGE;
            case spvc_basetype.SPVC_BASETYPE_SAMPLER:
                return DataBaseType.SAMPLER;
        }
    }

    private ShaderStage shaderStageFromSPVC(SpvExecutionModel executionModel)
    {
        switch (executionModel)
        {
            case SpvExecutionModel.SpvExecutionModelVertex:
                return ShaderStage.VERTEX;
            case SpvExecutionModel.SpvExecutionModelTessellationControl:
                return ShaderStage.TESSELATION_CONTROL;
            case SpvExecutionModel.SpvExecutionModelTessellationEvaluation:
                return ShaderStage.TESSELATION_EVALUATION;
            case SpvExecutionModel.SpvExecutionModelGeometry:
                return ShaderStage.TESSELATION_EVALUATION;
            case SpvExecutionModel.SpvExecutionModelFragment:
                return ShaderStage.FRAGMENT;
            case SpvExecutionModel.SpvExecutionModelGLCompute:
                return ShaderStage.COMPUTE;
            case SpvExecutionModel.SpvExecutionModelKernel:
                return ShaderStage.KERNEL;
            case SpvExecutionModel.SpvExecutionModelTaskNV:
                return ShaderStage.TASK;
            case SpvExecutionModel.SpvExecutionModelMeshNV:
                return ShaderStage.MESH;
            case SpvExecutionModel.SpvExecutionModelRayGenerationNV:
                return ShaderStage.RAY_GENERATION;
            case SpvExecutionModel.SpvExecutionModelIntersectionNV:
                return ShaderStage.INTERSECTION;
            case SpvExecutionModel.SpvExecutionModelAnyHitNV:
                return ShaderStage.ANY_HIT;
            case SpvExecutionModel.SpvExecutionModelClosestHitNV:
                return ShaderStage.CLOSEST_HIT;
            case SpvExecutionModel.SpvExecutionModelMissNV:
                return ShaderStage.MISS;
            case SpvExecutionModel.SpvExecutionModelCallableNV:
                return ShaderStage.CALLABLE;
            default:
                return ShaderStage.UNKNOWN;
        }
    }
}

struct ShaderCodeModule
{
    ShaderStage stage;
    const uint[] code;
}

class Shader : Resource
{
    mixin(implementBaseObject());
    
    private ShaderCodeModule[] modules_;
    private ShaderStage[] stages_;
    private UniformBufferAttachment[] uniformBuffers_;
    private StorageBufferAttachment[] storageBuffers_;
    private InputAttachment[] inputAttachments_;
    private AccelerationStructureAttachment[] accelerationStructures_;
    private SampledImage[] sampledImages_;
    private StorageImage[] storageImages_;
    private PushConstant[] pushConstants_;
    private SpecializationConstant[] specializationConstants_;
    private Variable[] inputVariables_;
    private Variable[] outputVariables_;

    public this(const(uint[])[] modules)
    {
        SpirVParser parser;
        foreach (module_; modules)
        {
            parser.parse(module_);

            enforce(parser.shaderStage != ShaderStage.UNKNOWN);

            ShaderCodeModule shaderModule = {
                stage: parser.shaderStage,
                code: module_
            };
            modules_ ~= shaderModule;
            if (!stages_.canFind(parser.shaderStage)) {
                stages_ ~= parser.shaderStage;
            }

            appendAttachments(uniformBuffers_, parser.uniformBuffers, parser.shaderStage);
            appendAttachments(storageBuffers_, parser.storageBuffers, parser.shaderStage);
            appendAttachments(inputAttachments_, parser.inputAttachments, parser.shaderStage);
            appendAttachments(accelerationStructures_, parser.accelerationStructures, parser.shaderStage);
            appendAttachments(sampledImages_, parser.sampledImages, parser.shaderStage);
            appendAttachments(storageImages_, parser.storageImages, parser.shaderStage);
            appendAttachments!(PushConstant, ["offset"])(pushConstants_, parser.pushConstants, parser.shaderStage);
            appendAttachments(outputVariables_, parser.outputVariables, parser.shaderStage);
            appendAttachments!(SpecializationConstant, ["id"])(specializationConstants_,
                    parser.specializationConstants, parser.shaderStage);
        }
    }

    @property
    public const(ShaderCodeModule[]) modules() const
    {
        return modules_;
    }

    @property
    public const(ShaderStage[]) stages() const
    {
        return stages_;
    }

    @property
    public const(UniformBufferAttachment[]) uniformBuffers() const
    {
        return uniformBuffers_;
    }

    @property
    public const(StorageBufferAttachment[]) storageBuffers() const
    {
        return storageBuffers_;
    }

    @property
    public const(InputAttachment[]) inputAttachments() const
    {
        return inputAttachments_;
    }

    @property
    public const (SampledImage[]) sampledImages() const
    {
        return sampledImages_;
    }

    @property
    public const (StorageImage[]) storageImages() const
    {
        return storageImages_;
    }

    @property
    public const(PushConstant[]) pushConstants() const
    {
        return pushConstants_;
    }

    @property
    public const (SpecializationConstant[]) specializationConstants() const
    {
        return specializationConstants_;
    }

    @property
    public const(AccelerationStructureAttachment[]) accelerationStructures() const
    {
        return accelerationStructures_;
    }

    @property
    public const(Variable[]) inputVariables() const
    {
        return inputVariables_;
    }

    @property
    public const(Variable[]) outputVariables() const
    {
        return outputVariables_;
    }

    public int findUniformBuffer(uint binding, uint set = 0)
    {
        foreach (i, uniformBuffer; uniformBuffers)
        {
            if (uniformBuffer.binding == binding && uniformBuffer.set == set)
            {
                return cast(int) i;
            }
        }
        return -1;
    }

    private void appendAttachments(T, string[] compareMembers = ["binding", "set"])(ref T[] attachments, T[] appendix,
            ShaderStage shaderStage)
    {
        foreach (attachment; appendix) {
            appendAttachment!(T, compareMembers)(attachments, attachment, shaderStage);
        }
    }

    private void appendAttachment(T, string[] compareMembers = ["binding", "set"])(ref T[] attachments, T attachment,
        ShaderStage shaderStage)
    {
FOREACH_ATTACHMENTS:
        foreach (ref existing; attachments)
        {
            static foreach (compareMember; compareMembers) {
                if (__traits(getMember, existing, compareMember) != __traits(getMember, attachment, compareMember)) {
                    continue FOREACH_ATTACHMENTS;
                }
            }
            static if (__traits(hasMember, existing, "stages")) {
                existing.stages |= flagFromEnum(shaderStage);
            }
            return;
        }
        static if (__traits(hasMember, attachment, "stages")) {
            attachment.stages = flagFromEnum(shaderStage);
        }
        attachments ~= attachment;
    }
}

public uint scalarSize(DataBaseType dataBaseType)
{
    switch (dataBaseType)
    {
        case DataBaseType.BOOLEAN:
            // ...
            return 1;
        case DataBaseType.INT8, DataBaseType.UINT8:
            return 1;
        case DataBaseType.INT16, DataBaseType.UINT16, DataBaseType.FLOAT16:
            return 2;
        case DataBaseType.INT32, DataBaseType.UINT32, DataBaseType.FLOAT32:
            return 4;
        case DataBaseType.INT64, DataBaseType.UINT64, DataBaseType.FLOAT64:
            return 8;
        default:
            assert(false, "invalid DataBaseType for scalarSize");
            // return 1;
    }
}

public size_t structSizeStd140(const(DataType) dataType)
{
    size_t size = 0;
    foreach (member; dataType.structMembers)
    {
        size_t memberAlign = member.type.alignStd140();
        size = max(size, member.offset + memberAlign * member.type.columns * max(1, member.type.arraySize));
    }

    size += ((size + 16 - 1) / 16) * 16; // round up to muitple of 16

    return size;
}

public size_t structAlignStd140(const(DataType) dataType)
{
    size_t baseAlign = 16;
    foreach (member; dataType.structMembers)
    {
        baseAlign = max(baseAlign, member.type.alignStd140());
    }
    return baseAlign;
}

public size_t alignStd140(const(DataType) dataType)
{
    if (dataType.baseType == DataBaseType.STRUCT) {
        return dataType.structAlignStd140();
    }
    else
    {
        size_t baseAlign = scalarSize(dataType.baseType);
        uint vectorElements = dataType.vectorSize == 3 ? 4 : dataType.vectorSize;
        size_t fullAlign = vectorElements * baseAlign;
        if (dataType.arraySize > 0 || dataType.columns > 1) {
            fullAlign = max(16, fullAlign);
        }
        return fullAlign;
    }
}

public size_t sizeStd140(const(DataType) dataType)
{
    size_t baseSize = 0;
    size_t baseAlign = dataType.alignStd140();
    if (dataType.baseType == DataBaseType.STRUCT) {
        baseSize = dataType.structSizeStd140();
    }
    else {
        baseSize = baseAlign;
    }
    
    return baseSize * max(1, dataType.arraySize) * dataType.columns;
}
