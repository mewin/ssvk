module ssvk.resource;

public import ssvk.resource.bitmap;
public import ssvk.resource.material;
public import ssvk.resource.mesh;
public import ssvk.resource.resource;
public import ssvk.resource.shader;