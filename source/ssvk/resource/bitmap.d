module ssvk.resource.bitmap;

import std.exception;
import std.traits;

import ssvk.core.types;
import ssvk.core.utility.bitmap;
import ssvk.resource.resource;

immutable int BITMAP_PITCH_AUTOMATIC = -1;

enum ColorChannels
{
    R,
    RG,
    RGB,
    RGBA
}

enum ComponentType
{
    UINT8,
    INT8,
    UINT16,
    INT16,
    UINT32,
    INT32,
    FLOAT16,
    FLOAT32
}

template ComponentDataType(ComponentType componentType)
{
    static if (componentType == ComponentType.UINT8) {
        alias ComponentDataType = ubyte;
    }
    else static if (componentType == ComponentType.INT8) {
        alias ComponentDataType = byte;
    }
    else static if (componentType == ComponentType.UINT16) {
        alias ComponentDataType = ushort;
    }
    else static if (componentType == ComponentType.INT16) {
        alias ComponentDataType = short;
    }
    else static if (componentType == ComponentType.UINT32) {
        alias ComponentDataType = uint;
    }
    else static if (componentType == ComponentType.INT32) {
        alias ComponentDataType = int;
    }
    else static if (componentType == ComponentType.FLOAT32) {
        alias ComponentDataType = float;
    }
}

ComponentType getComponentType(T)()
{
    static if (is(T == ubyte)) {
        return ComponentType.UINT8;
    }
    else static if (is(T == byte)) {
        return ComponentType.INT8;
    }
    else static if (is(T == ushort)) {
        return ComponentType.UINT16;
    }
    else static if (is(T == short)) {
        return ComponentType.INT16;
    }
    else static if (is(T == uint)) {
        return ComponentType.UINT32;
    }
    else static if (is(T == int)) {
        return ComponentType.INT32;
    }
    else static if (is(T == float)) {
        return ComponentType.FLOAT32;
    }
    else {
        static assert(!"Cannot directly access this data type.");
    }
}

uint getChannelCount(ColorChannels channels)
{
    final switch (channels)
    {
        case ColorChannels.RGBA:
            return 4;
        case ColorChannels.RGB:
            return 3;
        case ColorChannels.RG:
            return 2;
        case ColorChannels.R:
            return 1;
    }
}

uint getComponentBytes(ComponentType componentType)
{
    final switch (componentType)
    {
        case ComponentType.UINT8:
        case ComponentType.INT8:
            return 1;
        case ComponentType.UINT16:
        case ComponentType.INT16:
        case ComponentType.FLOAT16:
            return 2;
        case ComponentType.UINT32:
        case ComponentType.INT32:
        case ComponentType.FLOAT32:
            return 4;
    }
}

uint getBitsPerChannel(ComponentType componentType)
{
    return componentType.getComponentBytes() * 8;
}

uint getBPP(ColorChannels colorChannels, ComponentType componentType = ComponentType.UINT8)
{
    immutable(uint) perChannel = componentType.getBitsPerChannel();
    final switch (colorChannels)
    {
        case ColorChannels.R:
            return perChannel;
        case ColorChannels.RG:
            return 2 * perChannel;
        case ColorChannels.RGB:
            return 3 * perChannel;
        case ColorChannels.RGBA:
            return 4 * perChannel;
    }
}

ColorChannels channelsFromBPPAndType(uint bpp, ComponentType type)
{
    enforce(bpp % type.getBitsPerChannel() == 0, "invalid parameters to channelsFromBPPAndType");
    immutable(uint) channels = bpp / type.getBitsPerChannel();
    enforce(channels >= 1 && channels <= 4, "invalid parameters to channelsFromBPPAndType");

    final switch (channels)
    {
        case 1:
            return ColorChannels.R;
        case 2:
            return ColorChannels.RG;
        case 3:
            return ColorChannels.RGB;
        case 4:
            return ColorChannels.RGBA;
    }
}

T convertColorChannel(alias T)(float value)
{
    static if (is(T == float) || is(T == double))
    {
        return value;
    }
    else static if (is(T == ubyte))
    {
        return cast(ubyte) (255 * value);
    }
    else
    {
        assert(false, "No known color channel conversion for this component type.");
    }
}

enum CHANNEL_IDX_R = 0;
enum CHANNEL_IDX_G = 1;
enum CHANNEL_IDX_B = 2;
enum CHANNEL_IDX_A = 3;

struct Pixel(T = ubyte, int channelCount = 4)
{
    T[channelCount] values;
    
    @property
    public T r() const {
        return values[0];
    }
    @property
    public void r(T v) {
        values[0] = v;
    }
    static if (channelCount > 1) {
        @property
        public T g() const {
            return values[1];
        }
        @property
        void g(T v) {
            values[1] = v;
        }
    }
    static if (channelCount > 2) {
        @property
        public T b() const {
            return values[2];
        }
        @property
        void b(T v) {
            values[2] = v;
        }
    }
    static if (channelCount > 3) {
        @property
        public T a() const {
            return values[3];
        }
        @property
        void a(T v) {
            values[3] = v;
        }
    }

    ref auto opAssign(Color color)
    {
        r = convertColorChannel!T(color.r);
        static if (channelCount > 1) {
            g = convertColorChannel!T(color.g);
        }
        static if (channelCount > 2) {
            b = convertColorChannel!T(color.b);
        }
        static if (channelCount > 3) {
            a = convertColorChannel!T(color.a);
        }
        return this;
    }
}

template visitBitmap(string visitor, string params = "", string bitmap = "bitmap")
{
    const char[] visitBitmap = "
    static foreach (colorChannels; [1, 2, 3, 4])
    {
        import std.traits : EnumMembers;

        static foreach (componentType; EnumMembers!ComponentType) {
            if (" ~ bitmap ~ ".colorChannels.getChannelCount() == colorChannels &&
                    " ~ bitmap ~ ".componentType == componentType)
            {
                alias DataType = ComponentDataType!(componentType);
                static if (is(DataType))
                {
                    auto view = " ~ bitmap ~ ".getView!(DataType, colorChannels)();"
                    ~ visitor ~ "(view, " ~ params ~ ");
                }
            }
        }
    }";
}

struct BitmapView(T = ubyte, int channelCount = 4)
{
    Extent2DUI extent;
    Pixel!(T, channelCount)[] pixels;
}

class Bitmap : Resource
{
    mixin(implementBaseObject());
    
    private ColorChannels colorChannels_;
    private ComponentType componentType_;

    protected Extent2DUI extent_;
    protected ubyte[] data;

    public this(uint width, uint height, ColorChannels colorChannels = ColorChannels.RGBA,
            ComponentType componentType = ComponentType.UINT8)
    {
        colorChannels_ = colorChannels;
        componentType_ = componentType;

        extent_.width = width;
        extent_.height = height;
        data.length = width * height * getChannelCount(colorChannels) * getComponentBytes(componentType);
    }

    @property
    public Extent2DUI extent() const {
        return extent_;
    }
    @property
    public ColorChannels colorChannels() const {
        return colorChannels_;
    }
    @property
    public ComponentType componentType() const {
        return componentType_;
    }
    @property
    public uint bpp() const {
        return getChannelCount(colorChannels) * getComponentBytes(componentType) * 8;
    }
    @property
    public uint pitch() const {
        return cast(uint) getChannelCount(colorChannels) * getComponentBytes(componentType) * extent.width;
    }

    public inout(Pixel!(T, channelCount)[]) getPixels(T = ubyte, int channelCount = 4)() inout
    {
        alias PXL = Pixel!(T, channelCount);

        enforce(getComponentType!(T)() == componentType);
        enforce(getChannelCount(colorChannels_) == channelCount);
        
        inout(PXL)* pxls = cast(inout(PXL)*) cast(void*) data.ptr;
        return pxls[0..data.length / PXL.sizeof];
    }

    public BitmapView!(T, channelCount) getView(T = ubyte, int channelCount = 4)()
    {
        return BitmapView!(T, channelCount)(extent_, getPixels!(T, channelCount)());
    }
}
