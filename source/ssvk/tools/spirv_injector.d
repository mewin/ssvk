module ssvk.tools.spirv_injector;

import std.conv;
import std.exception;
import std.experimental.logger;

import spirv.tools;

private void enforceSPV(spv_result_t result)
{
    enforce(result == spv_result_t.SPV_SUCCESS);
}

private extern(C) spv_result_t parseHeaderCallback(
        void* user_data, spv_endianness_t endian, uint magic, uint version_,
        uint generator, uint id_bound, uint reserved)
{
    return spv_result_t.SPV_SUCCESS;
}

private extern(C) spv_result_t parseInstructionCallbackCallback(void* user_data,
        const(spv_parsed_instruction_t)* parsed_instruction)
{
    info(spvOpcodeString(parsed_instruction.opcode).to!string);
    return spv_result_t.SPV_SUCCESS;
}

struct SpirVInjector
{
    public void parse(const(uint[]) code)
    {
        spv_context context = spvContextCreate(spv_target_env.SPV_ENV_VULKAN_1_2);

        spvBinaryParse(context, &this, code.ptr, code.length, &parseHeaderCallback, &parseInstructionCallbackCallback, null)
            .enforceSPV();

        spvContextDestroy (context);
    }
}
