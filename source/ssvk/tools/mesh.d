module ssvk.tools.mesh;

import std.format;
import std.typecons;

import gfm.math;

import ssvk.core.types.math3d;
import ssvk.resource.mesh;
import ssvk.scene.mesh_instance;

struct Triangle
{
    uint index0;
    uint index1;
    uint index2;

    bool opCast(T)() const if (is(T == bool))
    {
        return index0 != 0 || index1 != 0 || index2 != 0;
    }
}

Triangle findTriangleByUV(const(Mesh) mesh, vec2f uv)
{
    // the triangles are not sorted in any way, no other option but brute-forcing here
    // TODO: maybe create some kind of wrapper for triangle mesh search/manipulation to speed this up if necessary

    for (uint i = 0; i + 2 < mesh.indices.length; i += 3)
    {
        uint index0 = mesh.indices[i];
        uint index1 = mesh.indices[i + 1];
        uint index2 = mesh.indices[i + 2];
        vec2f uv0 = mesh.vertices[index0].uv0;
        vec2f uv1 = mesh.vertices[index1].uv0;
        vec2f uv2 = mesh.vertices[index2].uv0;
        bool leftOf0 = (uv1 - uv0).dot(uv - uv0) >= 0;
        bool leftOf1 = (uv2 - uv1).dot(uv - uv1) >= 0;
        bool leftOf2 = (uv0 - uv2).dot(uv - uv2) >= 0;

        if (leftOf0 == leftOf1 && leftOf1 == leftOf2)
        {
            return Triangle(index0, index1, index2);
        }
    }
    return Triangle.init;
}

auto interpolateVertexValue(string memberName)(const(Mesh) mesh, vec2f uv)
{
    // 1. find the containing triangle
    Triangle triangle = mesh.findTriangleByUV(uv);
    if (!triangle) {
        return __traits(getMember, Vertex.init, memberName);
    }
    Vertex v0 = mesh.vertices[triangle.index0];
    Vertex v1 = mesh.vertices[triangle.index1];
    Vertex v2 = mesh.vertices[triangle.index2];

    // 2. calculate barycentrics from uv
    float det = (v1.uv0.y - v2.uv0.y) * (v0.uv0.x - v2.uv0.x) + (v2.uv0.x - v1.uv0.x) * (v0.uv0.y - v2.uv0.y);
    float bary0 = ((v1.uv0.y - v2.uv0.y) * (uv.x - v2.uv0.x) + (v2.uv0.x - v1.uv0.x) * (uv.y - v2.uv0.y)) / det;
    float bary1 = ((v2.uv0.y - v0.uv0.y) * (uv.x - v2.uv0.x) + (v0.uv0.x - v2.uv0.x) * (uv.y - v2.uv0.y)) / det;
    float bary2 = 1.0 - bary1 - bary0;

    // 3. calculate the result
    return bary0 * __traits(getMember, v0, memberName)
        + bary1 * __traits(getMember, v1, memberName)
        + bary2 * __traits(getMember, v2, memberName);
}

private void connectPortals(MeshInstance source, const(MeshInstance) destination)
{
    // TODO: manage usage counter or something for meshes
    // if mesh is used multiple times, it has to be duplicated here
    
    mat4f destinationTransform = cast(mat4f) destination.globalTransform;
    mat3f destinationRotation = cast(mat3f) destinationTransform;

    foreach (ref vertex; source.mesh.vertices)
    {
        vec2f uv = vertex.uv0;
        uv.x = 1.0 - uv.x;
        vertex.portalOutputPosition  = destinationTransform * vec4f(destination.mesh.interpolateVertexValue!"position"(uv), 1.0);
        vertex.portalOutputNormal    = destinationRotation * destination.mesh.interpolateVertexValue!"normal"(uv);
        vertex.portalOutputTangent   = destinationRotation * destination.mesh.interpolateVertexValue!"tangent"(uv);
        vertex.portalOutputBitangent = destinationRotation * destination.mesh.interpolateVertexValue!"bitangent"(uv);
    }
}

void updatePortals(MeshInstance[] meshInstances)
{
    // TODO: actually store the portal relationship instead of doing this every time
    alias MIPair = Tuple!(MeshInstance, "a", MeshInstance, "b");
    MIPair[string] pairs;

    foreach (meshInstance; meshInstances)
    {
        string pair;
        string ab;
        try
        {
            if (meshInstance.name.formattedRead!"portal_%s_%s"(pair, ab) == 2)
            {
                if (ab == "a") {
                    pairs.require(pair).a = meshInstance;
                }
                else {
                    pairs.require(pair).b = meshInstance;
                }
            }
        }
        catch(FormatException ignore) {} // TODO: is there a better way? ...
    }

    foreach (pair; pairs)
    {
        if (pair.a is null || pair.b is null) {
            continue;
        }
        connectPortals(pair.a, pair.b);
        connectPortals(pair.b, pair.a);
    }
}
