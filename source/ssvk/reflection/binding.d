module ssvk.reflection.binding;

import std.algorithm;
import std.array;
import std.conv;
import std.functional;
import std.traits;
import std.variant;

import ssvk.core.base_object;
import ssvk.core.utility.functional;
import ssvk.core.utility.traits;
import ssvk.reflection.hints;

struct MemberProperty
{
    TypeInfo type;
    BaseClass baseClass;
    Variant delegate(BaseObject) getter;
    void delegate(BaseObject, Variant) setter;
}

struct MemberFunction
{
    Variant delegate(BaseObject, Variant[]) invoke;
    TypeInfo_Function[] overloads;
}

struct BaseClass
{
    string name;
    MemberProperty[string] properties;
    MemberFunction[string] functions;
}

private Variant wrapValue(T)(auto ref T value)
{
    return Variant(value);
}

private MemberProperty wrapProperty(T, string member)()
{
    alias TMember = typeof(mixin("T." ~ member));

    MemberProperty property;

    property.type = typeid(TMember);
    static if (isAssignable!(BaseObject, T))
    {
        property.baseClass = wrapClass!T();
    }
    static if (canGet!(T, member))
    {
        property.getter = (BaseObject obj)
        {
            T tObj = cast(T) obj;
            return wrapValue(__traits(getMember, tObj, member));
        };   
    }
    static if (canSet!(T, member))
    {
        property.setter = (BaseObject obj, Variant val)
        {
            T tObj = cast(T) obj;
            if (!val.convertsTo!(TMember)) {
                return;
            }
            __traits(getMember, tObj, member) = val.get!TMember();
        };
    }

    return property;
}

private bool isCallable(paramTypes...)(Variant[] params)
{
    if (paramTypes.length != params.length) {
        return false;
    }
    static foreach (i, paramType; paramTypes) {
        static if (is(TParam == struct))
        {
            if (!params[i].convertsTo!(Variant[string])) {
                return false;
            }
        }
        else
        {
            if (!params[i].convertsTo!paramType()) {
                return false;
            }
        }
    }
    return true;
}

private Variant call(T, string member, returnType, paramTypes...)(T obj, Variant[] params)
{
    static if (is(returnType == void))
    {
        mixin("obj." ~ member ~ "(" ~ unpackVariantArray!paramTypes ~ ");");
        return Variant();
    }
    else
    {
        mixin("auto res = obj." ~ member ~ "(" ~ unpackVariantArray!paramTypes ~ ");");
        return Variant(res);
    }
}

private Variant callTemplate(T, string member, templArgs...)(T obj, Variant[] params)
{
    alias overload = typeof(mixin("&obj." ~ member ~ "!(" ~ tupleToString!templArgs() ~ ")"));
    alias paramTypes = Parameters!overload;
    alias returnType = ReturnType!overload;
    
    static if (is(returnType == void))
    {
        mixin("obj." ~ member ~ "!(" ~ tupleToString!templArgs() ~ ")(" ~ unpackVariantArray!paramTypes ~ ");");
        return Variant();
    }
    else
    {
        mixin("auto res = obj." ~ member ~ "(" ~ unpackVariantArray!paramTypes ~ ");");
        return Variant(res);
    }
}

private Variant memberFunctionWrapper(T, string member)(BaseObject obj, Variant[] params)
{
    T tObj = cast(T) obj;
    
    static foreach (TOverload; __traits(getOverloads, T, member))
    {
        alias paramTypes = Parameters!TOverload;
        alias returnType = ReturnType!TOverload;

        if (isCallable!paramTypes(params))
        {
            return call!(T, member, returnType, paramTypes)(tObj, params);
        }
    }

    return Variant();
}

private string tupleToString(tuple...)()
{
    string[] parts;
    static foreach (ele; tuple) {
        parts ~= ele.stringof;
    }
    return parts.join(",");
}

private Variant memberTemplateFunctionWrapper(T, string member)(BaseObject obj, Variant[] params)
{
    T tObj = cast(T) obj;
    
    static foreach (attr; __traits(getAttributes, mixin("tObj." ~ member)))
    {
        static if (is(attr : ReflectionHintTemplateTypes!Types, Types...))
        {
            alias templArgs = TemplateArgsOf!attr;
            alias overload = typeof(mixin("&tObj." ~ member ~ "!(" ~ tupleToString!templArgs() ~ ")"));
            alias paramTypes = Parameters!overload;

            if (isCallable!paramTypes(params))
            {
                return callTemplate!(T, member, templArgs)(tObj, params);
            }
        }
    }

    return Variant();
}

private MemberFunction wrapFunction(T, string member)()
{
    MemberFunction func;

    func.invoke = toDelegate(&memberFunctionWrapper!(T, member));

    static foreach (overload; typeof(__traits(getOverloads, T, member)))
    {
        static if (is(typeof(typeid(overload)) == TypeInfo_Const)) {
            func.overloads ~= cast(TypeInfo_Function) typeid(overload).base;
        }
        else {
            func.overloads ~= typeid(overload);
        }
    }

    return func;
}

private MemberFunction wrapTemplateFunction(T, string member)()
{
    MemberFunction func;

    func.invoke = toDelegate(&memberTemplateFunctionWrapper!(T, member));

    return func;
}

private void wrapMember(T, string member)(ref BaseClass cls)
{
    T object;

    static if (__traits(isVirtualFunction, mixin("object." ~ member)))
    {
        static if ([__traits(getFunctionAttributes, mixin("object." ~ member))[]].canFind("@property"))
        {
            cls.properties[member] = wrapProperty!(T, member)();
        }
        else
        {
            cls.functions[member] = wrapFunction!(T, member)();
        }
    }
    else static if (__traits(isTemplate, mixin("object." ~ member)))
    {
        cls.functions[member] = wrapTemplateFunction!(T, member)();
    }
    else static if (__traits(isPOD, typeof(mixin("object." ~ member))))
    {
        cls.properties[member] = wrapProperty!(T, member);
    }
}

private void wrapMembers(T)(ref BaseClass cls)
{
    static foreach (member; __traits(allMembers, T))
    {
        // ignore anything that is not public and special values
        static if (__traits(getProtection, mixin("T." ~ member)) == "public"
            && !hasStaticMember!(T, member)
            && member != "__ctor" && member != "__dtor" && member != "__xdtor"
            && member != "Monitor" && member != "factory" && member != "opCmp"
            && member != "opEquals")
        {
            wrapMember!(T, member)(cls);
        }
    }
}

private BaseClass[TypeInfo_Class] classDB;

public BaseClass wrapClass(T)(const(T) object = null)
{
    return classDB.require(typeid(T), ()
    {
        BaseClass baseClass;
        baseClass.name = T.stringof;
        wrapMembers!T(baseClass);
        return baseClass;
    }());
}
