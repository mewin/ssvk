module ssvk.vulkan.renderer;

import core.stdc.string : memcpy;
import core.thread.osthread;
import core.time;
import std.algorithm;
import std.exception;
import std.experimental.logger;
import std.typecons;

import erupted;
import erupted.vulkan_lib_loader;
import gfm.math;

import ssvk.core.application;
import ssvk.core.types;
import ssvk.loaders.shader;
import ssvk.resource.bitmap;
import ssvk.resource.material;
import ssvk.resource.mesh;
import ssvk.resource.shader;
import ssvk.scene.camera3d;
import ssvk.scene.mesh_instance;
import ssvk.scene.point_light3d;
import ssvk.scene.scene_tree;
import ssvk.vulkan;
import ssvk.vulkan.cmd.synchronization;
import ssvk.vulkan.debug_utils_messenger;
import ssvk.vulkan.render_modules;
import ssvk.vulkan.resource.material;
import ssvk.vulkan.resource.mesh;
import ssvk.vulkan.resource.shader;
import ssvk.vulkan.resource.texture;

immutable(int) MAX_FRAMES_IN_FLIGHT = 2;

struct ExtensionInfo
{
    public string[] requiredInstanceExtensions;
    public string[] requiredInstanceLayers;
    public string[] optionalInstanceExtensions;
    public string[] optionalInstanceLayers;
    public string[] requiredDeviceExtensions;
    public string[] optionalDeviceExtensions;
}

struct RendererCreateInfo
{
    public ExtensionInfo extensions;
    public uint32_t windowWidth = 1280;
    public uint32_t windowHeight = 720;
}

struct ImageResource
{
    public Image image;
    public ImageView imageView;
    public DeviceMemory imageMemory;
}

class Renderer
{
    // TODO: split renderer and viewport to enable stuff like render-to-texture
    
    public Application app;
    public Device device;
    public Instance instance;
    public Surface surface;
    public Swapchain swapchain;
    debug public DebugUtilsMessenger debugUtilsMessenger;
    public Duration frameDuration;

    public CommandPool commandPoolDrawing;
    public CommandPool commandPoolTransient;
    public CommandBuffer[] commandBuffersDrawing;
    public DescriptorPool descriptorPool;
    public Semaphore[] imageAvailableSemaphores;
    public Semaphore[] renderFinishedSemaphores;
    public Buffer[] uniformBuffers;
    public DeviceMemory[] uniformBufferMemories;
    
    public ImageResource intermediateImage;
    private Fence[] inFlightFences;
    private Fence[] imagesInFlight;
    public UBOGlobalState uboGlobalState;
    public DebugRenderMode debugRenderMode;
    public DebugRenderFlags debugRenderFlags;
    private size_t currentFrame = 0;
    public bool framebufferChanged = false;
    public int totalTicks = 0;

    // manually managed vertex data buffer
    public TypedDynamicBuffer!MeshInfo meshDataBuffer;
    public TypedDynamicBuffer!Vertex vertexDataBuffer;
    public TypedDynamicBuffer!MaterialInfo materialDataBuffer;
    public uint32_t nextTextureIndex; // TODO: do some real resource managment instead of a counter
    
    public ResourceManager resourceManager;
    public Bitmap environmentMap;
    public RenderList renderList;

    // sub modules
    public PostProcessor postProcessor;
    public RayTracer rayTracer;
    public LightVisibilityMap lightVisibilityMap;
    public UserInterface userInterface;

    ////////////////////////
    /// public interface ///
    ////////////////////////
    public void create(Application app, RendererCreateInfo createInfo)
    {
        this.app = app;

        createInfo.extensions.requiredDeviceExtensions ~= VK_KHR_SWAPCHAIN_EXTENSION_NAME;
        createInfo.extensions.requiredDeviceExtensions ~= VK_NV_RAY_TRACING_EXTENSION_NAME;
        createInfo.extensions.requiredDeviceExtensions ~= VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME;

        debug
        {
            info("This is a debug version, enabling validation layers.");
            createInfo.extensions.optionalInstanceLayers ~= "VK_LAYER_KHRONOS_validation";
            createInfo.extensions.optionalInstanceExtensions ~= VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
        }


        // load global level vulkan functions
        loadGlobalLevelFunctions().enforce();
        
        // init surface before retrieving vulkan extensions
        surface.initialize(this, app.window);

        // retrieve required extensions for surface creation
        surface.getRequiredExtensions(createInfo.extensions);

        // create instance
        instance.create(this, createInfo.extensions);

        // install debug messenger
        debug createDebugUtilsMessenger();

        // create surface
        surface.create();
        
        // choose a physical device
        PhysicalDevice[] physicalDevices = instance.enumeratePhysicalDevices();
        Nullable!PhysicalDevice physicalDevice = choosePhysicalDevice(physicalDevices, surface, createInfo.extensions);
        if (physicalDevice.isNull())
        {
            fatal("No suitable physical devices found.");
            throw new Exception("No physical devices.");
        }

        device.create(this, physicalDevice.get(), createInfo.extensions);

        createCommandPools();
        createSynchronizationObjects();

        createSwapchain();

        resourceManager.create(this);

        // other global objects
        meshDataBuffer.create(this, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, 1024);
        vertexDataBuffer.create(this, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, 1024 * 1024 * 1024);
        materialDataBuffer.create(this, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, 1024);
        
        // sub modules
        rayTracer.create(this);
        postProcessor.create(this);
        lightVisibilityMap.create(this);
        userInterface.create(this);
    }

    public void cleanup()
    {
        device.waitIdle();

        userInterface.cleanup();
        lightVisibilityMap.cleanup(),
        rayTracer.cleanup();
        postProcessor.cleanup();

        resourceManager.cleanup();

        renderFinishedSemaphores.each!((ref sem) => sem.cleanup());
        imageAvailableSemaphores.each!((ref sem) => sem.cleanup());
        inFlightFences.each!((ref fence) => fence.cleanup());

        cleanupSwapchain();

        commandPoolTransient.cleanup();
        commandPoolDrawing.cleanup();

        // other global stuff
        meshDataBuffer.cleanup();
        vertexDataBuffer.cleanup();
        materialDataBuffer.cleanup();

        device.cleanup();
        surface.cleanup();

        debug debugUtilsMessenger.cleanup();
        instance.cleanup();
    }

    public void update()
    {
    }

    public void reloadShaders()
    {
        device.waitIdle();
        
        postProcessor.reloadShaders();
        rayTracer.reloadShaders();
    }

    //////////////////////
    /// initialization ///
    //////////////////////
    private void createSwapchain()
    {
        swapchain.create(this);
        createIntermediateImage();
        createUniformBuffers();
        createDescriptorPool();
        createCommandBuffersDrawing();

        // prepare in-flight image fences
        imagesInFlight.length = swapchain.images.length;
        framebufferChanged = false;
    }

    public void recreateSwapchain()
    {
        device.waitIdle();
        cleanupSwapchain();
        createSwapchain();
        rayTracer.onRecreateSwapchain();
        postProcessor.onRecreateSwapchain();
    }

    private void createDescriptorPool()
    {
        // TODO: variable size
        enum DESCRIPTOR_SET_MULTIPLIER = 128;

        DescriptorPoolCreateInfo descriptorPoolCreateInfo = {
            poolSizes: [
                {
                    type: VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    descriptorCount: cast(uint32_t) swapchain.images.length * DESCRIPTOR_SET_MULTIPLIER
                },
                {
                    type: VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                    descriptorCount: cast(uint32_t) swapchain.images.length * DESCRIPTOR_SET_MULTIPLIER
                },
                {
                    type: VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV,
                    descriptorCount: cast(uint32_t) swapchain.images.length * DESCRIPTOR_SET_MULTIPLIER
                }
            ],
            maxSets: cast(uint32_t) swapchain.images.length * DESCRIPTOR_SET_MULTIPLIER
        };
        descriptorPool.create(this, descriptorPoolCreateInfo);
    }

    private void createCommandPools()
    {
        VkCommandPoolCreateInfo commandPoolCreateInfo = {
            queueFamilyIndex: device.graphicsQueue.familyIndex,
            flags: VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT
        };
        commandPoolDrawing.create(this, commandPoolCreateInfo);

        commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
        commandPoolTransient.create(this, commandPoolCreateInfo);
    }

    private void createCommandBuffersDrawing()
    {
        commandBuffersDrawing.length = MAX_FRAMES_IN_FLIGHT;
        commandBuffersDrawing.each!((ref cb) => cb.allocate(this, commandPoolDrawing));
    }

    private void recordCommandBuffer(uint32_t imageIndex, Camera3D camera)
    {
        CommandBuffer commandBuffer = commandBuffersDrawing[currentFrame];
        commandBuffer.reset();

        commandBuffer.begin();

        lightVisibilityMap.record(commandBuffer);

        memoryBarrier(commandBuffer, intermediateImage.image, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL,
                VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_ACCESS_SHADER_WRITE_BIT);
        
        rayTracer.record(commandBuffer, imageIndex, (cast(mat4f) camera.globalTransform));

        // TODO: optimize barriers?
        memoryBarrier(commandBuffer, intermediateImage.image, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_GENERAL,
                VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_ACCESS_SHADER_WRITE_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                VK_ACCESS_SHADER_READ_BIT);
        memoryBarrier(commandBuffer, swapchain.images[imageIndex], VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_GENERAL,
                VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                VK_ACCESS_SHADER_WRITE_BIT);
        
        // post processing
        postProcessor.record(commandBuffer, imageIndex);

        userInterface.record(commandBuffer, imageIndex);

        memoryBarrier(commandBuffer, swapchain.images[imageIndex], VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_ACCESS_SHADER_WRITE_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                0);

        commandBuffer.end();
    }

    private void createSynchronizationObjects()
    {
        // semaphores
        imageAvailableSemaphores.length = MAX_FRAMES_IN_FLIGHT;
        renderFinishedSemaphores.length = MAX_FRAMES_IN_FLIGHT;

        imageAvailableSemaphores.each!((ref sem) => sem.create(this));
        renderFinishedSemaphores.each!((ref sem) => sem.create(this));

        // fences
        VkFenceCreateInfo fenceCreateInfo = {
            flags: VK_FENCE_CREATE_SIGNALED_BIT
        };
        inFlightFences.length = MAX_FRAMES_IN_FLIGHT;
        inFlightFences.each!((ref fence) => fence.create(this, fenceCreateInfo));
    }
    
    private void createUniformBuffers()
    {
        VkDeviceSize bufferSize = uboGlobalState.sizeof;

        uniformBuffers.length = swapchain.images.length;
        uniformBufferMemories.length = swapchain.images.length;

        for (uint32_t i = 0; i < uniformBuffers.length; ++i)
        {
            createBuffer(this, bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                    uniformBuffers[i], uniformBufferMemories[i]);
        }
    }

    debug private void createDebugUtilsMessenger()
    {
        debugUtilsMessenger.create(this);
    }
    
    private void createIntermediateImage()
    {
        VkFormat formatVec4 = device.physicalDevice.findIntermediateImageFormatVec4();

        createImage(this, swapchain.extent.width, swapchain.extent.height, formatVec4, VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, intermediateImage.image, intermediateImage.imageMemory);
        createImageView(this, intermediateImage.image, intermediateImage.imageView, formatVec4,
                VK_IMAGE_ASPECT_COLOR_BIT);
    }

    private void cleanupIntermediateImage()
    {
        intermediateImage.imageView.cleanup();
        intermediateImage.image.cleanup();
        intermediateImage.imageMemory.free();
    }

    ///////////////
    /// drawing ///
    ///////////////
    public void drawFrame(SceneNode rootNode, Camera3D camera)
    {
        auto startTime  = MonoTime.currTime;
        
        // wait for frame to finish
        inFlightFences[currentFrame].wait();

        uint32_t imageIndex;
        if (swapchain.acquireNextImage(uint64_t.max, imageAvailableSemaphores[currentFrame], Fence.init, &imageIndex)
                == VkResult.VK_ERROR_OUT_OF_DATE_KHR)
        {
            recreateSwapchain();
            return;
        }

        renderList.rebuild(this, rootNode);
        updateUniformBufferObject(camera);

        rayTracer.update();
        lightVisibilityMap.update();

        // check if there is a corresponding fence and wait for it
        if (imagesInFlight[imageIndex]) {
            imagesInFlight[imageIndex].wait();
        }
        imagesInFlight[imageIndex] = inFlightFences[currentFrame];

        updateUniformData(imageIndex);

        recordCommandBuffer(imageIndex, camera);
        // submit commands to graphics queue
        const(QueueSubmitInfo) submitInfo = {
            waitSemaphores: [imageAvailableSemaphores[currentFrame]],
            waitStages: [VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT],
            commandBuffers: [commandBuffersDrawing[currentFrame]],
            signalSemaphores: [renderFinishedSemaphores[currentFrame]]
        };

        inFlightFences[currentFrame].reset();
        device.graphicsQueue.submit(submitInfo, inFlightFences[currentFrame]);

        // present results
        const(PresentInfo) presentInfo = {
            waitSemaphores: [renderFinishedSemaphores[currentFrame]],
            swapchains: [swapchain],
            imageIndices: [imageIndex]
        };
        if (device.presentQueue.present(presentInfo) != VkResult.VK_SUCCESS || framebufferChanged) {
            recreateSwapchain();
        }
        
        currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;

        frameDuration = MonoTime.currTime - startTime;

        Thread.sleep(10.msecs);
    }
    
    private void updateUniformBufferObject(Camera3D camera)
    {
        TextureData* environmentMapTD = null;
        if (environmentMap !is null) {
            environmentMapTD = resourceManager.getTextureData(environmentMap);
        }
        mat4f viewMat = (cast(mat4f) camera.globalTransform).inverse();
        float aspect = cast(float) swapchain.extent.width / cast(float) swapchain.extent.height;
        uboGlobalState.view = viewMat.transposed();
        uboGlobalState.projection = mat4f.perspective(radians(45.0), aspect, 0.1, 100.0).transposed(); // TODO: OpenGL Z vs. Vulkan Z
        uboGlobalState.eyePos = camera.globalTransform.translation;
        uboGlobalState.time = app.time;
        uboGlobalState.debugRenderMode = debugRenderMode;
        uboGlobalState.debugRenderFlags = debugRenderFlags;
        uboGlobalState.environmentMapTextureIndex = environmentMapTD ? environmentMapTD.textureArrayIndex + 1 : 0; // 0 -> unset, always add 1, subtract in shader
        
        // lights
        foreach (i, ref light; uboGlobalState.pointLights)
        {
            if (i < renderList.pointLights.length)
            {
                // auto lightEle = renderList.pointLights[i];
                // lightEle.position = (viewMat * vec4f(lightEle.position, 1.0)).xyz;
                light = renderList.pointLights[i];
            }
            else
            {
                light.color = vec3f(0.0);
                light.ambient = 0.0;
                light.position = vec3f(0.0);
                light.range = 0.0;
                light.attenuation = 0.0;
            }
        }
    }

    private void updateUniformData(uint32_t imageIndex)
    {
        void* data = uniformBufferMemories[imageIndex].map(0, uboGlobalState.sizeof);
        memcpy(data, &uboGlobalState, uboGlobalState.sizeof);
        uniformBufferMemories[imageIndex].unmap();
    }

    private void cleanupSwapchain()
    {
        cleanupIntermediateImage();

        commandBuffersDrawing.each!((ref cb) => cb.free());
        uniformBuffers.each!((ref ub) => ub.cleanup());
        uniformBufferMemories.each!((ref mem) => mem.free());

        descriptorPool.cleanup();
        swapchain.cleanup();
    }

    /////////////////////
    /// utility stuff ///
    /////////////////////
    private int scorePhysicalDevice(PhysicalDevice physicalDevice, Surface surface,
            ref const(ExtensionInfo) extensionInfo)
    {
        // first check if all required queue families are available
        if (!findQueueFamilies(physicalDevice, surface).isComplete()) {
            return -1;
        }

        // then check device features
        VkPhysicalDeviceFeatures features = physicalDevice.getFeatures();
        if (!features.samplerAnisotropy) {
            return -1;
        }

        // check additional features
        VkPhysicalDeviceDescriptorIndexingFeatures descriptorIndexingFeatures = physicalDevice.getDescriptorIndexingFeatures();
        if (!descriptorIndexingFeatures.runtimeDescriptorArray
                || !descriptorIndexingFeatures.shaderSampledImageArrayNonUniformIndexing
                || !descriptorIndexingFeatures.descriptorBindingVariableDescriptorCount) {
            return -1;
        }
        
        // after that check for required device extensions
        VkExtensionProperties[] extensions = physicalDevice.enumerateExtensions();
        foreach (requiredExtension; extensionInfo.requiredDeviceExtensions) {
            if (!checkExtension(extensions, requiredExtension)) {
                return -1;
            }
        }
        // check if swapchain support is adequate
        const(SwapchainSupportDetails) ssd = physicalDevice.getSwapchainSupportDetails(surface);
        if (ssd.formats.length < 1 || ssd.presentModes.length < 1) {
            return -1;
        }
        
        // everything required is available -> calculate a score
        int score = 0;
        foreach (optionalExtension; extensionInfo.optionalDeviceExtensions) {
            if (checkExtension(extensions, optionalExtension)) {
                ++score;
            }
        }

        return score;
    }

    private Nullable!PhysicalDevice choosePhysicalDevice(PhysicalDevice[] devices, Surface surface,
            ref const(ExtensionInfo) extensionInfo)
    {
        Nullable!PhysicalDevice bestDevice;
        int bestScore = -1;

        foreach (device; devices)
        {
            immutable(int) score = scorePhysicalDevice(device, surface, extensionInfo);
            if (score > bestScore)
            {
                bestScore = score;
                bestDevice = device;
            }
        }

        return bestDevice;
    }
}
