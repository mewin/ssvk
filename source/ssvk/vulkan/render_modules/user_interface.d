module ssvk.vulkan.render_modules.user_interface;

import erupted;

import ssvk.vulkan;

struct UserInterface
{
    private Renderer renderer;

    public void create(Renderer renderer)
    {
        this.renderer = renderer;
    }

    public void cleanup()
    {

    }

    public void record(ref CommandBuffer commandBuffer, uint32_t imageIndex)
    {
        
    }
}
