module ssvk.vulkan.render_modules.ray_tracer;

import std.algorithm;
import std.typecons;

import erupted;
import gfm.math;

import ssvk.resource.mesh;
import ssvk.vulkan;
import ssvk.vulkan.cmd.ray_tracing;
import ssvk.vulkan.cmd.render;
import ssvk.vulkan.cmd.synchronization;
import ssvk.vulkan.loaders.shader_pipeline;
import ssvk.vulkan.resource;
import ssvk.vulkan.util;

struct RayTracer
{
    private Renderer renderer;

    // shaders
    public ShaderData raytracingShader;
    public DescriptorSet[] raytracingDescriptorSets;

    // Ray Tracing Acceleration Structures
    public AccelerationStructureNV topAccelerationStructure;
    public DeviceMemory topAccelerationStructureMemory;
    public Buffer accelerationStructureInstanceBuffer;
    public DeviceMemory accelerationStructureInstanceBufferMemory;
    public VkPhysicalDeviceRayTracingPropertiesNV raytracingProperties;
    public Buffer shaderBindingTableBuffer;
    public DeviceMemory shaderBindingTableBufferMemory;

    public void create(Renderer renderer)
    {
        this.renderer = renderer;

        raytracingProperties = renderer.device.physicalDevice.getRaytracingProperties();
        createShader();
    }
    
    public void cleanup()
    {
        raytracingShader.cleanup();

        topAccelerationStructure.cleanup();
        topAccelerationStructureMemory.free();

        accelerationStructureInstanceBuffer.cleanup();
        accelerationStructureInstanceBufferMemory.free();

        shaderBindingTableBuffer.cleanup();
        shaderBindingTableBufferMemory.free();
    }

    public void update()
    {
        // (re)create acceleration structure from render list
        if (topAccelerationStructure.vkHandle is null) {
            createAccelerationStructures();
        }
    }

    public void onRecreateSwapchain()
    {
        createDescriptorSets();
    }

    public void record(ref CommandBuffer commandBuffer, uint32_t imageIndex, mat4f cameraTransform)
    {
        commandBuffer.cmdBindPipeline(VK_PIPELINE_BIND_POINT_RAY_TRACING_NV, raytracingShader.pipeline);
        commandBuffer.cmdBindDescriptorSets(VK_PIPELINE_BIND_POINT_RAY_TRACING_NV, raytracingShader.pipelineLayout, 0,
                [raytracingDescriptorSets[imageIndex].vkHandle], []);

        CameraState cameraState = {
            transform: cameraTransform.transposed()
        };

        commandBuffer.cmdPushConstants(raytracingShader.pipelineLayout, VK_SHADER_STAGE_RAYGEN_BIT_NV, 0,
                CameraState.sizeof, &cameraState);
        commandBuffer.cmdTraceRaysNV(
            shaderBindingTableBuffer, 0,
            shaderBindingTableBuffer, 3 * raytracingProperties.shaderGroupHandleSize, raytracingProperties.shaderGroupHandleSize,
            shaderBindingTableBuffer, 1 * raytracingProperties.shaderGroupHandleSize, raytracingProperties.shaderGroupHandleSize,
            Buffer(), 0, 0,
            renderer.swapchain.extent.width, renderer.swapchain.extent.height, 1);
    }

    public void reloadShaders()
    {
        raytracingShader.cleanup();
        createShader();
        createDescriptorSets();

        shaderBindingTableBuffer.cleanup();
        shaderBindingTableBufferMemory.free();
        createShaderBindingTable();
    }

    private void createShader()
    {
        raytracingShader = loadShaderPipeline(renderer, "assets/pipelines/raytracing.json");
    }

    private void createAccelerationStructures()
    {
        // 1. create VkGeometryNVs and VkGeometryInstances
        VkAccelerationStructureInstanceNV[] instances;
        AccelerationStructureNV[] bottomAccelerationStructures;
        VkGeometryNV[] geometries;
        
        foreach (ref renderMeshElement; renderer.renderList.meshList)
        {
            MeshData* md = renderer.resourceManager.getMeshData(renderMeshElement.mesh);

            Mesh mesh = renderMeshElement.mesh;
            vec3f[] vertices;
            vertices.length = mesh.vertices.length;
            foreach (i, ref vtx; mesh.vertices) {
                vertices[i] = vtx.position;
            }
            VkGeometryNV geometry = {
                geometryType: VK_GEOMETRY_TYPE_TRIANGLES_NV,
                geometry: {
                    triangles: {
                        vertexData: md.vertexBuffer.vkHandle,
                        vertexOffset: 0,
                        vertexCount: md.numVertices,
                        vertexStride: vec3f.sizeof,
                        vertexFormat: VK_FORMAT_R32G32B32_SFLOAT,
                        indexData: md.indexBuffer.vkHandle,
                        indexCount: md.numIndices,
                        indexType: VK_INDEX_TYPE_UINT32,
                        transformData: VK_NULL_HANDLE,
                        transformOffset: 0
                    }
                }
            };
            md.geometry = geometry;
            geometries ~= geometry;

            // create bottom level acceleration structure
            createAccelerationStructure(renderer, VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_NV, [geometry],
                    0, md.accelerationStructure, md.accelerationStructureMemory);
            bottomAccelerationStructures ~= md.accelerationStructure;

            // one instance per geometry
            VkAccelerationStructureInstanceNV instance;
            instance.transform = convertTransformMatrix(cast(mat4f) renderMeshElement.transform);
            instance.instanceCustomIndex = md.meshDataBufferIndex;
            instance.mask = 0xFF;
            instance.instanceShaderBindingTableRecordOffset = 0;
            instance.flags = VK_GEOMETRY_INSTANCE_TRIANGLE_CULL_DISABLE_BIT_NV;
            instance.accelerationStructureReference = md.accelerationStructure.getHandle();

            instances ~= instance;
        }

        // build instance buffer
        createAndFillBuffer(renderer, instances, VK_BUFFER_USAGE_RAY_TRACING_BIT_NV, accelerationStructureInstanceBuffer,
                accelerationStructureInstanceBufferMemory);
        
        // create top level acceleration structure
        createAccelerationStructure(renderer, VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_NV, [],
                cast(uint32_t) instances.length, topAccelerationStructure, topAccelerationStructureMemory);
        
        // create scratch buffer
        VkDeviceSize scratchBufferSize = getScratchBufferSize(topAccelerationStructure);
        foreach (accelerationStructure; bottomAccelerationStructures) {
            scratchBufferSize = max(scratchBufferSize, getScratchBufferSize(accelerationStructure));
        }
        Buffer scratchBuffer;
        DeviceMemory scratchBufferMemory;

        createBuffer(renderer, scratchBufferSize, VK_BUFFER_USAGE_RAY_TRACING_BIT_NV, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                scratchBuffer, scratchBufferMemory);
        
        // build acceleration structures
        VkMemoryBarrier memoryBarrier = {
            srcAccessMask: VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_NV | VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_NV,
            dstAccessMask: VK_ACCESS_ACCELERATION_STRUCTURE_WRITE_BIT_NV | VK_ACCESS_ACCELERATION_STRUCTURE_READ_BIT_NV
        };

        CommandBuffer commandBuffer;
        commandBuffer.allocate(renderer, renderer.commandPoolTransient);

        commandBuffer.begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

        foreach (i, accelerationStructure; bottomAccelerationStructures)
        {
            VkGeometryNV geometry = geometries[i];
            VkAccelerationStructureInfoNV asInfoBottom = {
                type: VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_NV,
                geometryCount: 1,
                pGeometries: &geometry
            };
            commandBuffer.cmdBuildAccelerationStructureNV(asInfoBottom, Nullable!Buffer(), 0, false,
                    accelerationStructure, Nullable!AccelerationStructureNV(), scratchBuffer);
            commandBuffer.cmdPipelineBarrier(VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV,
                    VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV, 0, [memoryBarrier], [], []);
        }
        
        VkAccelerationStructureInfoNV asInfoTop = {
            type: VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_NV,
            instanceCount: cast(uint32_t) instances.length
        };
        commandBuffer.cmdBuildAccelerationStructureNV(asInfoTop, accelerationStructureInstanceBuffer.nullable, 0, false,
                topAccelerationStructure, Nullable!AccelerationStructureNV(), scratchBuffer);
        commandBuffer.cmdPipelineBarrier(VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV,
                VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_NV, 0, [memoryBarrier], [], []);

        commandBuffer.end();
        
        QueueSubmitInfo submitInfo = {
            commandBuffers: [commandBuffer]
        };
        renderer.device.graphicsQueue.submit(submitInfo);
        renderer.device.graphicsQueue.waitIdle();

        // free temporary resources
        commandBuffer.free();
        scratchBuffer.cleanup();
        scratchBufferMemory.free();

        // create shader binding table
        createShaderBindingTable();

        // (re)create descriptor sets
        createDescriptorSets();
    }

    private void createShaderBindingTable()
    {
        const uint32_t GROUP_NUM = cast(uint32_t) raytracingShader.shader.modules.length;
        const uint32_t SHADER_BINDING_TABLE_SIZE = raytracingProperties.shaderGroupHandleSize * GROUP_NUM;

        createBuffer(renderer, SHADER_BINDING_TABLE_SIZE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, shaderBindingTableBuffer, shaderBindingTableBufferMemory);
        void* mapped = shaderBindingTableBufferMemory.map();
        vkGetRayTracingShaderGroupHandlesNV(renderer.device.vkHandle, raytracingShader.pipeline.vkHandle, 0, GROUP_NUM,
                SHADER_BINDING_TABLE_SIZE, mapped).enforceVK();
        shaderBindingTableBufferMemory.unmap();
    }

    private void createDescriptorSets()
    {
        TextureData[] textureDatas = renderer.resourceManager.getAllTextures(); // size is required for allocation

        raytracingShader.cleanupDescriptorSetLayout();
        raytracingShader.cleanupPipeline();
        raytracingShader.createDescriptorSetLayout(cast(uint) textureDatas.length);
        // raytracingShader.createRaytracingPipeline(this);
        raytracingShader.recreatePipeline();

        raytracingDescriptorSets.length = renderer.swapchain.images.length;

        foreach (i, ref descriptorSet; raytracingDescriptorSets)
        {
            descriptorSet.allocate(renderer, renderer.descriptorPool, raytracingShader.descriptorSetLayout,
                    cast(uint) textureDatas.length);
            VkWriteDescriptorSet[] writeDescriptors;

            // uniform buffers
            VkDescriptorBufferInfo bufferInfoGlobalState = {
                buffer: renderer.uniformBuffers[i].vkHandle,
                offset: 0,
                range: UBOGlobalState.sizeof
            };
            VkWriteDescriptorSet writeDescriptorGlobalState = {
                dstSet: descriptorSet.vkHandle,
                dstBinding: 0,
                dstArrayElement: 0,
                descriptorType: VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                descriptorCount: 1,
                pBufferInfo: &bufferInfoGlobalState,
                pImageInfo: null,
                pTexelBufferView: null
            };
            writeDescriptors ~= writeDescriptorGlobalState;

            // acceleration structure
            VkWriteDescriptorSetAccelerationStructureNV descriptorAccelerationStructureInfo = {
                accelerationStructureCount: 1,
                pAccelerationStructures: &topAccelerationStructure.vkHandle
            };
            VkWriteDescriptorSet writeDescriptorAccelerationStructure = {
                pNext: &descriptorAccelerationStructureInfo,
                dstSet: descriptorSet.vkHandle,
                dstBinding: 1,
                descriptorType: VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV,
                descriptorCount: 1
            };
            writeDescriptors ~= writeDescriptorAccelerationStructure;

            // storage images
            VkDescriptorImageInfo outputIntermediateImage = {
                sampler: null,
                imageView: renderer.intermediateImage.imageView.vkHandle,
                imageLayout: VK_IMAGE_LAYOUT_GENERAL
            };
            VkWriteDescriptorSet writeDescriptorIntermediateImage = {
                dstSet: descriptorSet.vkHandle,
                dstBinding: 2,
                descriptorCount: 1,
                descriptorType: VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                pImageInfo: &outputIntermediateImage
            };
            writeDescriptors ~= writeDescriptorIntermediateImage;

            // storage buffers
            VkDescriptorBufferInfo bufferInfoMeshDataBuffer = {
                buffer: renderer.meshDataBuffer.vkHandle,
                offset: 0,
                range: VK_WHOLE_SIZE
            };
            VkWriteDescriptorSet writeDescriptormeshDataBuffer = {
                dstSet: descriptorSet.vkHandle,
                dstBinding: DRAWPASS_BINDING_MESHDATA_BUFFER,
                dstArrayElement: 0,
                descriptorType: VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                descriptorCount: 1,
                pBufferInfo: &bufferInfoMeshDataBuffer,
                pImageInfo: null,
                pTexelBufferView: null
            };
            writeDescriptors ~= writeDescriptormeshDataBuffer;

            VkDescriptorBufferInfo bufferInfoVertexDataBuffer = {
                buffer: renderer.vertexDataBuffer.vkHandle,
                offset: 0,
                range: VK_WHOLE_SIZE
            };
            VkWriteDescriptorSet writeDescriptorVertexDataBuffer = {
                dstSet: descriptorSet.vkHandle,
                dstBinding: DRAWPASS_BINDING_VERTEXDATA_BUFFER,
                dstArrayElement: 0,
                descriptorType: VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                descriptorCount: 1,
                pBufferInfo: &bufferInfoVertexDataBuffer,
                pImageInfo: null,
                pTexelBufferView: null
            };
            writeDescriptors ~= writeDescriptorVertexDataBuffer;

            VkDescriptorBufferInfo bufferInfoMaterialDataBuffer = {
                buffer: renderer.materialDataBuffer.vkHandle,
                offset: 0,
                range: VK_WHOLE_SIZE
            };
            VkWriteDescriptorSet writeDescriptorMaterialDataBuffer = {
                dstSet: descriptorSet.vkHandle,
                dstBinding: DRAWPASS_BINDING_MATERIALDATA_BUFFER,
                dstArrayElement: 0,
                descriptorType: VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                descriptorCount: 1,
                pBufferInfo: &bufferInfoMaterialDataBuffer,
                pImageInfo: null,
                pTexelBufferView: null
            };
            writeDescriptors ~= writeDescriptorMaterialDataBuffer;

            // texture array
            VkDescriptorImageInfo[] imageInfos;
            imageInfos.length = textureDatas.length; // TODO: what if a texture in the middle is removed?

            foreach (td; textureDatas) {
                VkDescriptorImageInfo imageInfo = {
                    imageLayout: VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                    imageView: td.texImageView.vkHandle,
                    sampler: td.texSampler.vkHandle
                };
                imageInfos[td.textureArrayIndex] = imageInfo;
            }

            VkWriteDescriptorSet writeDescriptorTextures = {
                dstSet: descriptorSet.vkHandle,
                dstBinding: DRAWPASS_BINDING_MATERIAL_TEXTURES,
                dstArrayElement: 0,
                descriptorType: VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                descriptorCount: cast(uint32_t) imageInfos.length,
                pBufferInfo: null,
                pImageInfo: imageInfos.ptr,
                pTexelBufferView: null
            };
            writeDescriptors ~= writeDescriptorTextures;

            // descriptor sets
            vkUpdateDescriptorSets(renderer.device.vkHandle, cast(uint32_t) writeDescriptors.length,
                    writeDescriptors.ptr, 0, null);
        }
    }
}
