module ssvk.vulkan.render_modules.post_processor;

import erupted;

import ssvk.vulkan;
import ssvk.vulkan.cmd.compute;
import ssvk.vulkan.cmd.render;
import ssvk.vulkan.loaders.shader_pipeline;
import ssvk.vulkan.resource.shader;

struct PostProcessor
{
    private Renderer renderer;
    public ShaderData shader;
    public DescriptorSet[] descriptorSets;

    public void create(Renderer renderer)
    {
        this.renderer = renderer;

        createShader();
        createDescriptorSets();
    }

    public void cleanup()
    {
        shader.cleanup();
    }

    public void onRecreateSwapchain()
    {
        shader.cleanupPipeline();
        shader.recreatePipeline();
        createDescriptorSets();
    }

    public void record(ref CommandBuffer commandBuffer, uint32_t imageIndex)
    {
        commandBuffer.cmdBindPipeline(VK_PIPELINE_BIND_POINT_COMPUTE, shader.pipeline);
        commandBuffer.cmdBindDescriptorSets(VK_PIPELINE_BIND_POINT_COMPUTE, shader.pipelineLayout, 0,
                [descriptorSets[imageIndex].vkHandle], []);
        commandBuffer.cmdDispatch((renderer.swapchain.extent.width + 15) / 16, (renderer.swapchain.extent.height + 15) / 16, 1);
    }

    public void reloadShaders()
    {
        shader.cleanup();
        createShader();
    }

    private void createShader()
    {
        shader = loadShaderPipeline(renderer, "assets/pipelines/postprocess.json");
    }

    private void createDescriptorSets()
    {
        descriptorSets.length = renderer.swapchain.images.length;

        foreach (i, ref descriptorSet; descriptorSets)
        {
            descriptorSet.allocate(renderer, renderer.descriptorPool, shader.descriptorSetLayout);
            VkWriteDescriptorSet[] writeDescriptors;

            // uniform buffers
            VkDescriptorBufferInfo bufferInfoGlobalState = {
                buffer: renderer.uniformBuffers[i].vkHandle,
                offset: 0,
                range: UBOGlobalState.sizeof
            };
            VkWriteDescriptorSet writeDescriptorGlobalState = {
                dstSet: descriptorSet.vkHandle,
                dstBinding: 0,
                dstArrayElement: 0,
                descriptorType: VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                descriptorCount: 1,
                pBufferInfo: &bufferInfoGlobalState,
                pImageInfo: null,
                pTexelBufferView: null
            };
            writeDescriptors ~= writeDescriptorGlobalState;

            // image attachments
            VkDescriptorImageInfo storageImageInfoInput = {
                imageLayout: VK_IMAGE_LAYOUT_GENERAL,
                imageView: renderer.intermediateImage.imageView.vkHandle
            };
            VkWriteDescriptorSet storageImageInfoInputDescSet = {
                dstSet: descriptorSet.vkHandle,
                dstBinding: cast(uint32_t) POSTPROCESS_BINDING_INPUT_IMAGE,
                descriptorType: VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                descriptorCount: 1,
                pImageInfo: &storageImageInfoInput
            };
            writeDescriptors ~= storageImageInfoInputDescSet;

            VkDescriptorImageInfo storageImageInfoOutput = {
                imageLayout: VK_IMAGE_LAYOUT_GENERAL,
                imageView: renderer.swapchain.imageViews[i].vkHandle
            };
            VkWriteDescriptorSet storageImageInfoOutputDescSet = {
                dstSet: descriptorSet.vkHandle,
                dstBinding: cast(uint32_t) POSTPROCESS_BINDING_OUTPUT_IMAGE,
                descriptorType: VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                descriptorCount: 1,
                pImageInfo: &storageImageInfoOutput
            };
            writeDescriptors ~= storageImageInfoOutputDescSet;

            // descriptor sets
            vkUpdateDescriptorSets(renderer.device.vkHandle, cast(uint32_t) writeDescriptors.length,
                    writeDescriptors.ptr, 0, null);
        }
    }
}
