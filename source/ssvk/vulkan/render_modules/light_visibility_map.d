module ssvk.vulkan.render_modules.light_visibility_map;

import erupted;
import gfm.math;

import ssvk.vulkan;
import ssvk.vulkan.loaders.shader_pipeline;
import ssvk.vulkan.resource.shader;

struct LightVisibilityMap
{
    private Renderer renderer;
    private ShaderData projectionShader;
    private TypedDynamicBuffer!vec4f projectedVerticesBuffer;
    private DescriptorSet[] projectionDescriptorSets;

    public void create(Renderer renderer)
    {
        this.renderer = renderer;

        createBuffers();
        createShaders();
        createDescriptorSets();
    }

    public void cleanup()
    {
        projectionShader.cleanup();
        projectedVerticesBuffer.cleanup();
    }

    public void update()
    {
        if (projectedVerticesBuffer.length == renderer.vertexDataBuffer.length) {
            return;
        }
        VkBuffer oldHandle = projectedVerticesBuffer.vkHandle;
        projectedVerticesBuffer.length = renderer.vertexDataBuffer.length;
        if (projectedVerticesBuffer.vkHandle != oldHandle)
        {
            createDescriptorSets();
        }
    }

    public void record(ref CommandBuffer commandBuffer)
    {
        bufferMemoryBarrier(commandBuffer, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
            VK_ACCESS_SHADER_WRITE_BIT, projectedVerticesBuffer);



        bufferMemoryBarrier(commandBuffer, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_ACCESS_SHADER_WRITE_BIT,
            VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_ACCESS_SHADER_READ_BIT, projectedVerticesBuffer);
    }

    private void createBuffers()
    {
        projectedVerticesBuffer.create(renderer, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, 1024);
    }

    private void createShaders()
    {
        projectionShader = loadShaderPipeline(renderer, "assets/pipelines/light_visibility_projection.json");
    }

    private void createDescriptorSets()
    {
        
    }
}
