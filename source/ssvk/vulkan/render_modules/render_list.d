module ssvk.vulkan.render_modules.render_list;

import std.experimental.logger;

import gfm.math;

import ssvk.resource.mesh;
import ssvk.scene.mesh_instance;
import ssvk.scene.point_light3d;
import ssvk.scene.scene_tree;
import ssvk.vulkan;
import ssvk.vulkan.resource.point_light;


struct RenderMeshElement
{
    Mesh  mesh;
    mat4f transform;
}

alias RenderPointLightElement = PointLightData;

struct RenderList
{
    RenderMeshElement[]       meshList;
    RenderPointLightElement[] pointLights;

    public void reset()
    {
        meshList.length = 0;
        pointLights.length = 0;
    }

    public void rebuild(Renderer renderer, SceneNode rootNode)
    {
        reset();

        build(renderer, rootNode);
    }

    private void build(Renderer renderer, SceneNode rootNode)
    {
        foreach (child; rootNode.children)
        {
            build(renderer, child);
        }

        if (MeshInstance meshInstance = cast(MeshInstance) rootNode)
        {
            // insert into render list
            if (meshInstance.mesh is null) {
                return;
            }

            RenderMeshElement meshEle;
            meshEle.mesh = meshInstance.mesh;
            meshEle.transform = cast(mat4f) meshInstance.globalTransform;
            meshList ~= meshEle;
        }
        else if (PointLight3D pointLight = cast(PointLight3D) rootNode)
        {
            if (pointLights.length >= PointLight.COUNT) {
                warning("Too many lights!");
                return;
            }
            RenderPointLightElement lightEle = *renderer.resourceManager.getPointLightData(pointLight); 
            pointLights ~= lightEle;
        }
    }
}