module ssvk.vulkan.dynamic_buffer;

import core.stdc.string : memcpy;
import std.math;

import erupted;

import ssvk.vulkan;

struct DynamicBuffer
{
    private Renderer renderer;
    private VkImageUsageFlags usage;
    public Buffer buffer;
    public DeviceMemory bufferMemory;
    public VkDeviceSize blockSize;
    private VkDeviceSize length_;
    public uint blockCount;

    public void create(Renderer renderer, VkBufferUsageFlags usage, VkDeviceSize blockSize, uint initialBlockCount = 1)
    {
        this.renderer = renderer;
        this.usage = usage  | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        this.blockSize = blockSize;
        this.blockCount = initialBlockCount;

        createBuffer(renderer, blockCount * blockSize, this.usage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buffer, bufferMemory);
    }

    public void cleanup()
    {
        buffer.cleanup();
        bufferMemory.free();
    }

    @property
    public VkDeviceSize capacity() const
    {
        return blockSize * blockCount;
    }

    @property
    public VkDeviceSize length() const
    {
        return length_;
    }

    @property
    public void length(VkDeviceSize value)
    {
        ensureCapacity(value);
        length_ = value;
    }

    @property
    public inout(VkBuffer) vkHandle() inout
    {
        return buffer.vkHandle;
    }

    public void append(void* data, VkDeviceSize bytes)
    {
        VkDeviceSize newSize = length_ + bytes;
        if (newSize > capacity) {
            ensureCapacity(newSize);
        }

        Buffer stagingBuffer;
        DeviceMemory stagingBufferMemory;
        createBuffer(renderer, bytes, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
                | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);
        void* stagingBufferData = stagingBufferMemory.map(0, bytes);
        memcpy(stagingBufferData, data, bytes);
        stagingBufferMemory.unmap();

        stagingBuffer.copyTo(buffer, bytes, 0, length_);
        length_ += bytes;

        stagingBuffer.cleanup();
        stagingBufferMemory.free();
    }

    public void ensureCapacity(VkDeviceSize bytes)
    {
        if (bytes <= capacity) {
            return;
        }
        uint requiredBlockCount = cast(uint) ceil(cast(double) bytes / cast(double) blockSize);
        
        Buffer newBuffer;
        DeviceMemory newBufferMemory;
        createBuffer(renderer, requiredBlockCount * blockSize, usage, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, newBuffer,
                newBufferMemory);
        if (length_ > 0) {
            buffer.copyTo(newBuffer, length_);
        }
        
        buffer.cleanup();
        bufferMemory.free();
        buffer = newBuffer;
        bufferMemory = newBufferMemory;
        blockCount = requiredBlockCount;
    }
}

struct TypedDynamicBuffer(T)
{
    public DynamicBuffer base;

    public void create(Renderer renderer, VkBufferUsageFlags usage, VkDeviceSize blockSize, uint initialBlockCount = 1)
    {
        base.create(renderer, usage, blockSize, initialBlockCount);
    }

    public void cleanup()
    {
        base.cleanup();
    }

    @property
    public size_t length() const
    {
        return base.length / T.sizeof;
    }

    @property
    public void length(size_t value)
    {
        base.length = value * T.sizeof;
    }

    @property
    public size_t capacity() const
    {
        return base.capacity / T.sizeof;
    }

    @property
    public inout(Buffer) buffer() inout
    {
        return base.buffer;
    }

    @property
    public inout(VkBuffer) vkHandle() inout
    {
        return base.vkHandle;
    }

    public void append(T element)
    {
        base.append(&element, T.sizeof);
    }

    public void append(T[] elements)
    {
        base.append(elements.ptr, elements.length * T.sizeof);
    }

    alias buffer this;
}
