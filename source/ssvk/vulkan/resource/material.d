module ssvk.vulkan.resource.material;

import std.algorithm;

import erupted;
import gfm.math;

import ssvk.resource.material;
import ssvk.vulkan;
import ssvk.vulkan.resource.shader;
import ssvk.vulkan.resource.texture;

struct MaterialData
{
    public DescriptorSet[] descriptorSets;
    public Material material;
    public uint32_t materialDataBufferIndex;

    public void create(Renderer renderer, Material material)
    {
        this.material = material;

        appendToStorageBuffer(renderer, material);
    }

    private void appendToStorageBuffer(Renderer renderer, Material material)
    {
        materialDataBufferIndex = cast(uint32_t) renderer.materialDataBuffer.length;
        
        MaterialInfo materialInfo = {
            albedoTextureIndex:            getTextureIndex(renderer, material, BINDING_PBR_ALBEDO),
            normalTextureIndex:            getTextureIndex(renderer, material, BINDING_PBR_NORMAL),
            roughnessMetallicTextureIndex: getTextureIndex(renderer, material, BINDING_PBR_METALLIC_ROUGHNESS),
            ambientOcclusionTextureIndex:  getTextureIndex(renderer, material, BINDINB_PBR_AMBIENT_OCCLUSION),
            albedoFactor:                  material.getParameterAs!vec4f(PARAMETER_PBR_ALBEDO, vec4f(1.0)),
            metallicFactor:                material.getParameterAs!float(PARAMETER_PBR_METALLIC, 1.0),
            roughnessFactor:               material.getParameterAs!float(PARAMETER_PBR_ROUGHNESS, 1.0),
            refractiveIndex:               material.getParameterAs!float(PARAMETER_REFRACTIVE_INDEX, 1.52)
        };
        renderer.materialDataBuffer.append(materialInfo);
    }

    public void cleanup()
    {
        // foreach (ref ds; descriptorSets) {
        //     ds.free();
        // }
    }

    private uint32_t getTextureIndex(Renderer renderer, Material material, uint binding)
    {
        TextureData* td = renderer.resourceManager.getTextureData(material.boundTextures[binding]);

        return td.textureArrayIndex;
    }
}
