module ssvk.vulkan.resource.mesh;

import std.algorithm;
import std.array;

import erupted;
import gfm.math;

import ssvk.core.types;
import ssvk.resource.mesh;
import ssvk.vulkan;
import ssvk.vulkan.resource.material;

struct MeshData
{
    // buffers are only used for the acceleration structure
    public Buffer vertexBuffer;
    public DeviceMemory vertexBufferMemory;
    public Buffer indexBuffer;
    public DeviceMemory indexBufferMemory;
    public uint numVertices;
    public uint numIndices;
    public uint32_t meshDataBufferIndex;
    public AccelerationStructureNV accelerationStructure;
    public DeviceMemory accelerationStructureMemory;
    public VkGeometryNV geometry;

    public void create(Renderer renderer, Mesh mesh)
    {
        createVertexBuffer(renderer, mesh);
        createIndexBuffer(renderer, mesh);

        appendToStorageBuffers(renderer, mesh);
    }

    public void cleanup()
    {
        vertexBuffer.cleanup();
        vertexBufferMemory.free();
        indexBuffer.cleanup();
        indexBufferMemory.free();
        accelerationStructure.cleanup();
        accelerationStructureMemory.free();

        // TODO: do some memory management in storage buffers to allow dynamically removing geometry
    }

    private void createVertexBuffer(Renderer renderer, Mesh mesh)
    {
        // only the vertices are required for the acceleration structure
        vec3f[] vertices = mesh.vertices.map!("a.position").array();

        createAndFillBuffer(renderer, vertices, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, vertexBuffer, vertexBufferMemory);
        
        numVertices = cast(uint) vertices.length;
    }

    private void createIndexBuffer(Renderer renderer, Mesh mesh)
    {
        VkDeviceSize bufferSize = uint32_t.sizeof * mesh.indices.length;

        createAndFillBuffer(renderer, mesh.indices.ptr, bufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, indexBuffer,
                indexBufferMemory);
        
        numIndices = cast(uint) mesh.indices.length;
    }

    private void appendToStorageBuffers(Renderer renderer, Mesh mesh)
    {
        meshDataBufferIndex = cast(uint32_t) renderer.meshDataBuffer.length;
        VkDeviceSize indexOffset = renderer.vertexDataBuffer.length;
        Vertex[] unpackedVertices = mesh.unpackVertices();

        MaterialData* matData = renderer.resourceManager.getMaterialData(mesh.material);
        MeshInfo meshInfo = {
            offset: cast(uint32_t) indexOffset,
            materialID: cast(uint32_t) matData.materialDataBufferIndex
        };

        renderer.meshDataBuffer.append(meshInfo);
        renderer.vertexDataBuffer.append(unpackedVertices);
    }
}
