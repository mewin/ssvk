module ssvk.vulkan.resource.shader;

import std.algorithm;
import std.traits;
import std.variant;

import erupted;

import ssvk.resource.shader;
import ssvk.vulkan;

VkShaderStageFlagBits stageToVulkan(ShaderStage stage)
{
    final switch(stage)
    {
        case ShaderStage.VERTEX:
            return VK_SHADER_STAGE_VERTEX_BIT;
        case ShaderStage.TESSELATION_CONTROL:
            return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
        case ShaderStage.TESSELATION_EVALUATION:
            return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
        case ShaderStage.GEOMETRY:
            return VK_SHADER_STAGE_GEOMETRY_BIT;
        case ShaderStage.FRAGMENT:
            return VK_SHADER_STAGE_FRAGMENT_BIT;
        case ShaderStage.COMPUTE:
            return VK_SHADER_STAGE_COMPUTE_BIT;
        case ShaderStage.TASK:
            return VK_SHADER_STAGE_TASK_BIT_NV;
        case ShaderStage.MESH:
            return VK_SHADER_STAGE_MESH_BIT_NV;
        case ShaderStage.RAY_GENERATION:
            return VK_SHADER_STAGE_RAYGEN_BIT_NV;
        case ShaderStage.INTERSECTION:
            return VK_SHADER_STAGE_INTERSECTION_BIT_NV;
        case ShaderStage.ANY_HIT:
            return VK_SHADER_STAGE_ANY_HIT_BIT_NV;
        case ShaderStage.CLOSEST_HIT:
            return VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV;
        case ShaderStage.MISS:
            return VK_SHADER_STAGE_MISS_BIT_NV;
        case ShaderStage.CALLABLE:
            return VK_SHADER_STAGE_CALLABLE_BIT_NV;
        case ShaderStage.UNKNOWN:
        case ShaderStage.KERNEL:
            throw new Exception("Unknown or invalid shader stage.");
    }
}

struct ShaderData
{
    private Renderer renderer;
    public Pipeline pipeline;
    public PipelineLayout pipelineLayout;
    public DescriptorSetLayout descriptorSetLayout;
    public Variant recreateInfo;
    public Shader shader;

    public void initialize(Renderer renderer)
    {
        this.renderer = renderer;
    }

    public void createDescriptorSetLayout(uint dynamicCount = 0)
    {
        VkDescriptorSetLayoutBinding[] bindings;

        foreach (inputAttachment; shader.inputAttachments)
        {
            VkDescriptorSetLayoutBinding iaBinding = {
                binding:  inputAttachment.binding,
                descriptorType: VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
                descriptorCount: 1,
                stageFlags: stagesToVk(inputAttachment.stages)
            };
            bindings ~= iaBinding;
        }

        foreach (uniformBuffer; shader.uniformBuffers)
        {
            VkDescriptorSetLayoutBinding uboBinding = {
                binding: uniformBuffer.binding,
                descriptorType: VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                descriptorCount: 1,
                stageFlags: stagesToVk(uniformBuffer.stages)
            };
            bindings ~= uboBinding;
        }

        foreach (storageBuffer; shader.storageBuffers)
        {
            VkDescriptorSetLayoutBinding sboBinding = {
                binding: storageBuffer.binding,
                descriptorType: VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                descriptorCount: 1,
                stageFlags: stagesToVk(storageBuffer.stages)
            };
            bindings ~= sboBinding;
        }

        foreach (sampledImage; shader.sampledImages)
        {
            VkDescriptorSetLayoutBinding imageBinding = {
                binding: sampledImage.binding,
                descriptorType: VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                descriptorCount: 1,
                stageFlags: stagesToVk(sampledImage.stages)
            };
            bindings ~= imageBinding;
        }

        foreach (storageImage; shader.storageImages)
        {
            VkDescriptorSetLayoutBinding imageBinding = {
                binding: storageImage.binding,
                descriptorType: VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                descriptorCount: 1,
                stageFlags: stagesToVk(storageImage.stages)
            };
            bindings ~= imageBinding;
        }

        foreach (accelerationStructure; shader.accelerationStructures)
        {
            VkDescriptorSetLayoutBinding accelerationStructureBinding = {
                binding: accelerationStructure.binding,
                descriptorType: VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_NV,
                descriptorCount: 1,
                stageFlags: stagesToVk(accelerationStructure.stages)
            };
            bindings ~= accelerationStructureBinding;
        }

        bindings.sort!("a.binding < b.binding")();

        DescriptorSetLayoutCreateInfo createInfo = {
            bindings: bindings
        };

        // dynamic size
        VkDescriptorSetLayoutBindingFlagsCreateInfo bindingFlagsCreateInfo;
        uint32_t[] bindingFlags;
        if (dynamicCount > 0)
        {
            bindingFlags.length = bindings.length;
            bindingFlags[$-1] = VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT_EXT;
            createInfo.bindings[$-1].descriptorCount = dynamicCount;
            bindingFlagsCreateInfo.bindingCount = cast(uint32_t) bindings.length;
            bindingFlagsCreateInfo.pBindingFlags = bindingFlags.ptr;
            createInfo.layoutBindingFlagsCreateInfo = bindingFlagsCreateInfo;
        }

        descriptorSetLayout.create(renderer, createInfo);
    }

    public void createPipelineLayout()
    in {
        assert(descriptorSetLayout.vkHandle);
    }
    body {
        VkPushConstantRange[] pushConstants = parsePushConstants();
        VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {
            setLayoutCount: 1,
            pSetLayouts: &descriptorSetLayout.vkHandle,
            pushConstantRangeCount: cast(uint32_t) pushConstants.length,
            pPushConstantRanges: pushConstants.ptr
        };
        pipelineLayout.create(renderer, pipelineLayoutCreateInfo);
    }

    public void recreatePipeline()
    in {
        assert(!pipelineLayout.vkHandle);
        assert(!pipeline.vkHandle);
        assert(recreateInfo.hasValue());
    }
    body {
        createPipelineLayout();

        if (auto createInfo = recreateInfo.peek!RayTracingPipelineCreateInfo())
        {   
            createInfo.layout = pipelineLayout;
            pipeline.createRaytracing(renderer, *createInfo);
        }
        else if (auto createInfo = recreateInfo.peek!GraphicsPipelineCreateInfo())
        {
            createInfo.layout = pipelineLayout;
            pipeline.createGraphics(renderer, *createInfo);
        }
        else if (auto createInfo = recreateInfo.peek!VkComputePipelineCreateInfo())
        {
            createInfo.layout = pipelineLayout.vkHandle;
            pipeline.createCompute(renderer, *createInfo);
        }
        else
        {
            throw new Exception("invalid recreateInfo");
        }
    }

    public void cleanupPipeline()
    {
        pipeline.cleanup();
        pipelineLayout.cleanup();
    }

    public void cleanupDescriptorSetLayout()
    {
        descriptorSetLayout.cleanup();
    }

    public void cleanup()
    {
        pipeline.cleanup();
        pipelineLayout.cleanup();
        descriptorSetLayout.cleanup();

        if (auto createInfo = recreateInfo.peek!RayTracingPipelineCreateInfo())
        {
            cleanupRecreateShaderModules(createInfo.stages);
        }
        else if (auto createInfo = recreateInfo.peek!GraphicsPipelineCreateInfo())
        {
            cleanupRecreateShaderModules(createInfo.stages);
        }
        else if (auto createInfo = recreateInfo.peek!VkComputePipelineCreateInfo())
        {
            cleanupRecreateShaderModules([createInfo.stage]);
        }
        else
        {
            throw new Exception("invalid recreateInfo");
        }
    }

    private void cleanupRecreateShaderModules(VkPipelineShaderStageCreateInfo[] stages)
    {
        foreach (stage; stages)
        {
            vkDestroyShaderModule(renderer.device.vkHandle, stage.module_, null);
        }
    }

    private VkShaderStageFlags stagesToVk(ShaderStageFlags stages)
    {
        VkShaderStageFlags vkFlags = 0;

        static foreach (member; EnumMembers!ShaderStage)
        {
            if (stages & flagFromEnum(member)) {
                vkFlags |= stageToVulkan(member);
            }
        }

        return vkFlags;
    }

    private VkPushConstantRange[] parsePushConstants()
    {
        VkPushConstantRange[] result;

        foreach (pushConstant; shader.pushConstants)
        {
            VkPushConstantRange pushConstantRange = {
                stageFlags: stagesToVk(pushConstant.stages),
                offset: pushConstant.offset,
                size: cast(uint32_t) sizeStd140(pushConstant.type)
            };
            result ~= pushConstantRange;
        }

        return result;
    }
}
