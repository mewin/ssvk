module ssvk.vulkan.resource;

public import ssvk.vulkan.resource.material;
public import ssvk.vulkan.resource.mesh;
public import ssvk.vulkan.resource.point_light;
public import ssvk.vulkan.resource.shader;
public import ssvk.vulkan.resource.texture;
