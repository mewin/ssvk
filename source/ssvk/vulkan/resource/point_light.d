module ssvk.vulkan.resource.point_light;

import erupted;
import gfm.math;

import ssvk.scene.point_light3d;
import ssvk.vulkan;

struct PointLightData
{
    private Renderer renderer;
    public PointLight pointLight;

    public void create(Renderer renderer, PointLight3D pointLight)
    {
        this.renderer = renderer;
        color = (cast(vec4f) pointLight.color).xyz * pointLight.energy;
        ambient = 0.0;
        position = pointLight.globalTransform.translation;
        range = pointLight.range;
        attenuation = pointLight.attenuation;
    }

    public void cleanup()
    {

    }

    alias pointLight this;
}
