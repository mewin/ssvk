module ssvk.vulkan.resource.texture;

import erupted;

import ssvk.resource.bitmap;
import ssvk.vulkan;

struct TextureData
{
    public Image texImage;
    public ImageView texImageView;
    public Sampler texSampler;
    public DeviceMemory texImageMemory;
    public uint32_t textureArrayIndex;

    public void create(Renderer renderer, Bitmap bmp)
    {
        createImage(renderer, bmp, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, texImage, texImageMemory);
        createImageView(renderer, texImage, texImageView, VK_FORMAT_R8G8B8A8_UNORM);
        createTextureSampler(renderer, texSampler);

        appendToTextureArray(renderer);
    }

    public void cleanup()
    {
        texSampler.cleanup();
        texImageView.cleanup();
        texImage.cleanup();
        texImageMemory.free();
    }

    private void appendToTextureArray(Renderer renderer)
    {
        textureArrayIndex = renderer.nextTextureIndex;
        ++renderer.nextTextureIndex;

        // TODO: invalidate prepass descriptor sets
    }
}
