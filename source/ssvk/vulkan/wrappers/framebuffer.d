module ssvk.vulkan.wrappers.framebuffer;

import erupted;

import ssvk.vulkan;

struct Framebuffer
{
    private Renderer renderer;
    public VkFramebuffer vkHandle;

    public void create(Renderer renderer, VkFramebufferCreateInfo createInfo)
    {
        this.renderer = renderer;

        vkCreateFramebuffer(renderer.device.vkHandle, &createInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        vkDestroyFramebuffer(renderer.device.vkHandle, vkHandle, null);
    }
}
