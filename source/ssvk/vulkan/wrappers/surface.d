module ssvk.vulkan.wrappers.surface;

import std.exception;
import std.conv;

import bindbc.sdl;
import erupted;

import ssvk.core.types;
import ssvk.window;
import ssvk.vulkan.renderer;
import ssvk.vulkan.util;

struct Surface
{
    private Renderer renderer;
    private Window window;
    public VkSurfaceKHR vkHandle;

    public void initialize(Renderer renderer, Window window)
    {
        this.renderer = renderer;
        this.window = window;
    }

    public void create()
    {
        SDL_Vulkan_CreateSurface(window.sdlHandle, renderer.instance.vkHandle, &vkHandle).enforce();
    }

    public void cleanup()
    {
        vkDestroySurfaceKHR(renderer.instance.vkHandle, vkHandle, null);
    }

    public void getRequiredExtensions(ref ExtensionInfo extensionInfo)
    {
        // query vulkan extensions required by SDL
        uint sdlRequiredExtensionsCount;
        const(char)*[] sdlRequiredExtensions;
        SDL_Vulkan_GetInstanceExtensions(window.sdlHandle, &sdlRequiredExtensionsCount, null).enforce();
        sdlRequiredExtensions.length = sdlRequiredExtensionsCount;
        SDL_Vulkan_GetInstanceExtensions(window.sdlHandle, &sdlRequiredExtensionsCount, sdlRequiredExtensions.ptr)
                .enforce();

        foreach (requiredExtension; sdlRequiredExtensions) {
            extensionInfo.requiredInstanceExtensions ~= to!string(requiredExtension);
        }
    }

    public VkExtent2D getFramebufferSize()
    {
        Extent2DI windowSize = window.getFramebufferSize();

        return VkExtent2D(windowSize.width, windowSize.height);
    }
}
