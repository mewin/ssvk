module ssvk.vulkan.wrappers.acceleration_structure;

import erupted;

import ssvk.vulkan;

struct AccelerationStructureNV
{
    private Renderer renderer;
    public VkAccelerationStructureNV vkHandle;

    public void create(Renderer renderer, ref const(VkAccelerationStructureCreateInfoNV) createInfo)
    {
        this.renderer = renderer;

        vkCreateAccelerationStructureNV(renderer.device.vkHandle, &createInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        if (vkHandle != VK_NULL_HANDLE) {
            vkDestroyAccelerationStructureNV(renderer.device.vkHandle, vkHandle, null);
        }
        vkHandle = VK_NULL_HANDLE;
    }

    public VkMemoryRequirements2 getMemoryRequirements(
            ref const(VkAccelerationStructureMemoryRequirementsInfoNV) memoryRequirementsInfo)
    {
        VkMemoryRequirements2 memoryRequirements;
        vkGetAccelerationStructureMemoryRequirementsNV(renderer.device.vkHandle, &memoryRequirementsInfo,
                &memoryRequirements);
        return memoryRequirements;
    }

    public void bindMemory(ref const(VkBindAccelerationStructureMemoryInfoNV) bindInfo)
    {
        vkBindAccelerationStructureMemoryNV(renderer.device.vkHandle, 1, &bindInfo).enforceVK();
    }

    public uint64_t getHandle()
    {
        uint64_t handle;
        vkGetAccelerationStructureHandleNV(renderer.device.vkHandle, vkHandle, uint64_t.sizeof, &handle).enforceVK();
        return handle;
    }
}
