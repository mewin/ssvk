module ssvk.vulkan.wrappers.instance;

import std.algorithm;
import std.array;
import std.conv;
import std.experimental.logger;

import erupted;

import ssvk.vulkan;

struct Instance
{
    public string[] loadedExtensions;
    public string[] loadedLayers;
    public Renderer renderer;
    public VkInstance vkHandle;

    ////////////////////////
    /// public functions ///
    ////////////////////////
    public void create(Renderer renderer, ref const(ExtensionInfo) extensionInfo)
    {
        this.renderer = renderer;

        // query available extensions and layers
        VkExtensionProperties[] extensions = enumerateExtensions();
        VkLayerProperties[] layers = enumerateLayers();

        VkApplicationInfo applicationInfo = {
            pApplicationName : "SSVk",
            apiVersion: VK_MAKE_VERSION(1, 1, 0)
        };

        // check required extensions
        const(char*)[] extensionNames;
        foreach (string requiredExtension; extensionInfo.requiredInstanceExtensions)
        {
            if (!checkExtension(extensions, requiredExtension))
            {
                errorf("Missing required extension: %s.", requiredExtension);
                throw new Exception("Missing required extension.");
            }
            loadedExtensions ~= requiredExtension;
            extensionNames ~= requiredExtension.ptr;
        }

        // check optional extensions
        foreach (string optionalExtension; extensionInfo.optionalInstanceExtensions)
        {
            if (!checkExtension(extensions, optionalExtension))
            {
                warningf("Missing optional extension: %s.", optionalExtension);
                continue;
            }
            loadedExtensions ~= optionalExtension;
            extensionNames ~= optionalExtension.ptr;
        }

        // check required layers
        const(char*)[] layerNames;
        foreach (string requiredLayer; extensionInfo.requiredInstanceLayers)
        {
            if (!checkLayer(layers, requiredLayer))
            {
                errorf("Missing required layers: %s.", requiredLayer);
                throw new Exception("Missing required layer.");
            }
            loadedLayers ~= requiredLayer;
            layerNames ~= requiredLayer.ptr;
        }

        // check optional layers
        foreach (string optionalLayer; extensionInfo.optionalInstanceLayers)
        {
            if (!checkLayer(layers, optionalLayer))
            {
                warningf("Missing optional layers: %s.", optionalLayer);
                continue;
            }
            loadedLayers ~= optionalLayer;
            layerNames ~= optionalLayer.ptr;
        }

        VkInstanceCreateInfo instanceCreateInfo = {
            pApplicationInfo: &applicationInfo,
            enabledExtensionCount: cast(uint32_t)(extensionNames.length),
            ppEnabledExtensionNames: extensionNames.ptr,
            enabledLayerCount: cast(uint32_t)(layerNames.length),
            ppEnabledLayerNames: layerNames.ptr
        };

        vkCreateInstance(&instanceCreateInfo, null, &vkHandle).enforceVK();
        loadInstanceLevelFunctions(vkHandle);
    }

    public void cleanup()
    {
        if (vkHandle != VK_NULL_HANDLE) {
            // vkDestroyInstance(vkHandle, null);
            warning("TODO: vkDestroyInstance segfaults");
        }
    }

    public PhysicalDevice[] enumeratePhysicalDevices()
    {
        uint32_t numPhysicalDevices;
        VkPhysicalDevice[] physicalDevices;

        vkEnumeratePhysicalDevices(vkHandle, &numPhysicalDevices, null).enforceVK();
        if (numPhysicalDevices < 1) {
            return [];
        }
        physicalDevices.length = numPhysicalDevices;
        vkEnumeratePhysicalDevices(vkHandle, &numPhysicalDevices, physicalDevices.ptr).enforceVK();
        debug {
             dumpPhysicalDevices(physicalDevices);
        }
        return map!(a => PhysicalDevice(renderer, a))(physicalDevices).array();
    }

    /////////////////////////
    /// private functions ///
    /////////////////////////
    private VkExtensionProperties[] enumerateExtensions()
    {
        uint32_t extensionCount;
        VkExtensionProperties[] extensions;
        vkEnumerateInstanceExtensionProperties(null, &extensionCount, null);
        extensions.length = extensionCount;
        vkEnumerateInstanceExtensionProperties(null, &extensionCount, extensions.ptr);
        debug
        {
	        info("Available instance extensions:");
            dumpExtensions(extensions);
        }
        return extensions;
    }

    private VkLayerProperties[] enumerateLayers()
    {
        uint32_t layerCount;
        VkLayerProperties[] layers;
        vkEnumerateInstanceLayerProperties(&layerCount, null);
        layers.length = layerCount;
        vkEnumerateInstanceLayerProperties(&layerCount, layers.ptr);
        debug
        {
	        info("Available instance layers:");
            dumpLayers(layers);
        }
        return layers;
    }

    ////////////////////
    /// debug output ///
    ////////////////////
    private void dumpPhysicalDevices(VkPhysicalDevice[] physicalDevices)
    {
        info("Detected physical devices:");
        foreach (VkPhysicalDevice physicalDevice; physicalDevices)
        {
            VkPhysicalDeviceProperties properties;
            vkGetPhysicalDeviceProperties(physicalDevice, &properties);
            info(to!string(properties.deviceName.ptr));
        }
    }
}
