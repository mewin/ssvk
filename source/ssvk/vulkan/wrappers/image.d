module ssvk.vulkan.wrappers.image;

import std.experimental.logger;

import erupted;

import ssvk.vulkan;
import ssvk.vulkan.cmd.synchronization;

struct Image
{
    private Renderer renderer;
    public VkImage vkHandle;

    public void initialize(Renderer renderer, VkImage vkHandle)
    {
        this.renderer = renderer;
        this.vkHandle = vkHandle;
    }

    public void create(Renderer renderer, VkImageCreateInfo createInfo)
    {
        this.renderer = renderer;
        
        vkCreateImage(renderer.device.vkHandle, &createInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        if (vkHandle != VK_NULL_HANDLE)
        {
            vkDestroyImage(renderer.device.vkHandle, vkHandle, null);
            vkHandle = VK_NULL_HANDLE;
        }
    }

    public VkMemoryRequirements getMemoryRequirements()
    {
        VkMemoryRequirements requirements;
        vkGetImageMemoryRequirements(renderer.device.vkHandle, vkHandle, &requirements);
        return requirements;
    }

    public void bindMemory(DeviceMemory memory, VkDeviceSize offset = 0)
    {
        vkBindImageMemory(renderer.device.vkHandle, vkHandle, memory.vkHandle, offset);
    }

    public void transitionLayout(VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout)
    {
        VkImageMemoryBarrier memoryBarrier = {
            oldLayout: oldLayout,
            newLayout: newLayout,
            srcQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED,
            dstQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED,
            image: vkHandle,
            subresourceRange: {
                aspectMask: VK_IMAGE_ASPECT_COLOR_BIT,
                baseMipLevel: 0,
                levelCount: 1,
                baseArrayLayer: 0,
                layerCount: 1
            },
            srcAccessMask: 0,
            dstAccessMask: 0
        };
        VkPipelineStageFlags srcStage, dstStage;
        genLayoutTransitionMasks(memoryBarrier, format, srcStage, dstStage);

        CommandBuffer cmdBuffer;
        cmdBuffer.allocate(renderer, renderer.commandPoolTransient);

        cmdBuffer.begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
        cmdBuffer.cmdPipelineBarrier(srcStage, dstStage, 0 /* dependencyFlags */, [], [], [memoryBarrier]);
        cmdBuffer.end();

        QueueSubmitInfo submitInfo = {
            commandBuffers: [cmdBuffer]
        };
        renderer.device.graphicsQueue.submit(submitInfo);
        renderer.device.graphicsQueue.waitIdle();

        cmdBuffer.free();
    }

    private void genLayoutTransitionMasks(ref VkImageMemoryBarrier memoryBarrier, VkFormat format,
            ref VkPipelineStageFlags srcStage, ref VkPipelineStageFlags dstStage)
    {
        if (memoryBarrier.oldLayout == VK_IMAGE_LAYOUT_UNDEFINED
                && memoryBarrier.newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
        {
            memoryBarrier.srcAccessMask = 0;
            memoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        }
        else if (memoryBarrier.oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
                && memoryBarrier.newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
        {
            memoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            memoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
            srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            dstStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        }
        else if (memoryBarrier.newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
        {
            memoryBarrier.srcAccessMask = 0;
            memoryBarrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT
                    | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
            memoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
            if (hasStencilComponent(format)) {
                memoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_STENCIL_BIT;
            }
            srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            dstStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        }
        else if (memoryBarrier.newLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
        {
            memoryBarrier.srcAccessMask = 0;
            memoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            memoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            dstStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        }
        else if (memoryBarrier.newLayout == VK_IMAGE_LAYOUT_GENERAL)
        {
            memoryBarrier.srcAccessMask = 0;
            memoryBarrier.dstAccessMask = 0;
            srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            dstStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
        }
        else
        {
            errorf("Unsupported image transition from %s to %s.", memoryBarrier.oldLayout, memoryBarrier.newLayout);
            throw new Exception("transition is unsupported yet");
        }
    }
}

struct ImageView
{
    private Renderer renderer;
    public VkImageView vkHandle;

    public void create(Renderer renderer, VkImageViewCreateInfo createInfo)
    {
        this.renderer = renderer;

        vkCreateImageView(renderer.device.vkHandle, &createInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        if (vkHandle != VK_NULL_HANDLE)
        {
            vkDestroyImageView(renderer.device.vkHandle, vkHandle, null);
            vkHandle = VK_NULL_HANDLE;
        }
    }
}

struct Sampler
{
    private Renderer renderer;
    public VkSampler vkHandle;

    public void create(Renderer renderer, VkSamplerCreateInfo createInfo)
    {
        this.renderer = renderer;

        vkCreateSampler(renderer.device.vkHandle, &createInfo, null, &vkHandle);
    }

    public void cleanup()
    {
        if (vkHandle != VK_NULL_HANDLE)
        {
            vkDestroySampler(renderer.device.vkHandle, vkHandle, null);
            vkHandle = VK_NULL_HANDLE;
        }
    }
}
