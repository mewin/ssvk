module ssvk.vulkan.wrappers.device;

import std.experimental.logger;

import erupted;

import ssvk.vulkan;

struct Device
{
    private Renderer renderer;
    public PhysicalDevice physicalDevice;
    public VkDevice vkHandle;
    public Queue graphicsQueue;
    public Queue computeQueue;
    public Queue presentQueue;
    public string[] loadedExtensions;

    public void create(Renderer renderer, PhysicalDevice physicalDevice, ref const(ExtensionInfo) extensionInfo)
    {
        this.renderer = renderer;
        this.physicalDevice = physicalDevice;

        // queues
        QueueFamilyIndices qfi = findQueueFamilies(physicalDevice, renderer.surface);
        if (!qfi.isComplete()) {
            fatal("Could not find suitable queue family indices!");
            throw new Exception("No suitable queue families.");
        }

        VkDeviceQueueCreateInfo[] queueCreateInfos;

        /// graphics queue
        immutable(float[]) graphicsQueuePriorities = [1.];
        immutable(VkDeviceQueueCreateInfo) queueCreateInfoGraphics = {
            queueFamilyIndex: qfi.graphicsFamily.get(),
            queueCount: cast(uint32_t) graphicsQueuePriorities.length,
            pQueuePriorities: graphicsQueuePriorities.ptr
        };
        queueCreateInfos ~= queueCreateInfoGraphics;

        /// compute queue
        immutable(float[]) computeQueuePriorities = [1.];
        if (qfi.computeFamily != qfi.graphicsFamily)
        {
            immutable(VkDeviceQueueCreateInfo) queueCreateInfoCompute = {
                queueFamilyIndex: qfi.computeFamily.get(),
                queueCount: cast(uint32_t) computeQueuePriorities.length,
                pQueuePriorities: computeQueuePriorities.ptr
            };
            queueCreateInfos ~= queueCreateInfoCompute;
        }

        /// present queue
        immutable(float[]) presentQueuePriorities = [1.];
        if (qfi.presentFamily != qfi.graphicsFamily)
        {
            immutable(VkDeviceQueueCreateInfo) queueCreateInfoPresent = {
                queueFamilyIndex: qfi.presentFamily.get(),
                queueCount: cast(uint32_t) graphicsQueuePriorities.length,
                pQueuePriorities: presentQueuePriorities.ptr
            };
            queueCreateInfos ~= queueCreateInfoPresent;
        }

        // physical device features
        VkPhysicalDeviceFeatures physicalDeviceFeatures;
        physicalDeviceFeatures.samplerAnisotropy = VK_TRUE;
        
        VkPhysicalDeviceDescriptorIndexingFeatures descriptorIndexingFeatures = {
            shaderSampledImageArrayNonUniformIndexing: VK_TRUE,
            descriptorBindingVariableDescriptorCount: VK_TRUE,
            runtimeDescriptorArray: VK_TRUE
        };

        // --- extensions --- //
        // query available extensions and layers
        VkExtensionProperties[] extensions = physicalDevice.enumerateExtensions();
        debug
        {
            info("Available device extensions:");
            dumpExtensions(extensions);
        }

        // check required extensions
        const(char*)[] extensionNames;
        foreach (string requiredExtension; extensionInfo.requiredDeviceExtensions)
        {
            // this should have been checked while choosing the device
            assert(checkExtension(extensions, requiredExtension));
            loadedExtensions ~= requiredExtension;
            extensionNames ~= requiredExtension.ptr;
        }

        // check optional extensions
        foreach (string optionalExtension; extensionInfo.optionalDeviceExtensions)
        {
            if (!checkExtension(extensions, optionalExtension))
            {
                warningf("Missing optional device extension: %s.", optionalExtension);
                continue;
            }
            loadedExtensions ~= optionalExtension;
            extensionNames ~= optionalExtension.ptr;
        }

        // create the device
        VkDeviceCreateInfo deviceCreateInfo = {
            pNext: &descriptorIndexingFeatures,
            queueCreateInfoCount: cast(uint32_t) queueCreateInfos.length,
            pQueueCreateInfos: queueCreateInfos.ptr,
            pEnabledFeatures: &physicalDeviceFeatures,
            enabledExtensionCount: cast(uint32_t) extensionNames.length,
            ppEnabledExtensionNames: extensionNames.ptr
        };
        vkCreateDevice(physicalDevice.vkHandle, &deviceCreateInfo, null, &vkHandle).enforceVK();
        loadDeviceLevelFunctions(vkHandle);

        // retrieve the queue handles
        graphicsQueue.initialize(renderer, this, qfi.graphicsFamily.get(), 0);
        computeQueue.initialize(renderer, this, qfi.computeFamily.get(), 0);
        presentQueue.initialize(renderer, this, qfi.presentFamily.get(), 0);
    }

    public void cleanup()
    {
        if (vkHandle != VK_NULL_HANDLE) {
            vkDeviceWaitIdle(vkHandle);
            vkDestroyDevice(vkHandle, null);
        }
    }

    public void waitIdle()
    {
        vkDeviceWaitIdle(vkHandle);
    }
}