module ssvk.vulkan.wrappers.shader;

import std.exception;
import std.file;
import std.stdio;

import erupted;

import ssvk.vulkan;

struct ShaderModule
{
    private Renderer renderer;
    public VkShaderModule vkHandle;

    public void create(Renderer renderer, VkShaderModuleCreateInfo createInfo)
    {
        this.renderer = renderer;

        vkCreateShaderModule(renderer.device.vkHandle, &createInfo, null, &vkHandle).enforceVK();
    }

    public void create(Renderer renderer, string shaderFileName)
    {
        ulong fileSize = getSize(shaderFileName);
        enforce(fileSize % uint32_t.sizeof == 0);
        uint32_t[] shaderCodeBuffer;
        shaderCodeBuffer.length = fileSize / uint32_t.sizeof;

        File file = File(shaderFileName, "r");
        const(uint32_t[]) shaderCodeSlice = file.rawRead(shaderCodeBuffer);
        
        create(renderer, shaderCodeSlice);
    }

    public void create(Renderer renderer, const(uint32_t[]) shaderCode)
    {
        VkShaderModuleCreateInfo shaderModuleCreateInfo = {
            codeSize: cast(uint32_t) shaderCode.length * uint32_t.sizeof,
            pCode: shaderCode.ptr
        };

        create(renderer, shaderModuleCreateInfo);
    }

    public void cleanup()
    {
        vkDestroyShaderModule(renderer.device.vkHandle, vkHandle, null);
    }
}
