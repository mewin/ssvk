module ssvk.vulkan.wrappers.queue;

import std.algorithm;
import std.array;

import ssvk.vulkan;

import erupted;

struct QueueSubmitInfo
{
    Semaphore[] waitSemaphores;
    VkPipelineStageFlags[] waitStages;
    CommandBuffer[] commandBuffers;
    Semaphore[] signalSemaphores;

    invariant
    {
        assert(waitSemaphores.length == waitStages.length);
    }
}

struct PresentInfo
{
    Semaphore[] waitSemaphores;
    Swapchain[] swapchains;
    uint32_t[] imageIndices;

    invariant
    {
        assert(swapchains.length == imageIndices.length);
    }
}

struct Queue
{
    private Renderer renderer;
    public VkQueue vkHandle;
    public uint32_t familyIndex;

    public void initialize(Renderer renderer, Device device, uint32_t familyIndex, uint32_t queueIndex)
    {
        this.renderer = renderer;
        this.familyIndex = familyIndex;

        vkGetDeviceQueue(device.vkHandle, familyIndex, queueIndex, &vkHandle);
    }

    public void submit(const(QueueSubmitInfo) submitInfo, Fence signalFence = Fence.init)
    {
        // map wrapped types to VK handles
        const(VkSemaphore[]) waitSemaphoreHandles = map!(sem => sem.vkHandle)(submitInfo.waitSemaphores).array();
        const(VkCommandBuffer[]) commandBufferHandles = map!(cb => cb.vkHandle)(submitInfo.commandBuffers).array();
        const(VkSemaphore[]) signalSemaphoreHandles = map!(sem => sem.vkHandle)(submitInfo.signalSemaphores).array();
        const(VkSubmitInfo) vkSubmitInfo = {
            waitSemaphoreCount: cast(uint32_t) waitSemaphoreHandles.length,
            pWaitSemaphores: waitSemaphoreHandles.ptr,
            pWaitDstStageMask: submitInfo.waitStages.ptr,
            commandBufferCount: cast(uint32_t) commandBufferHandles.length,
            pCommandBuffers: commandBufferHandles.ptr,
            signalSemaphoreCount: cast(uint32_t) signalSemaphoreHandles.length,
            pSignalSemaphores: signalSemaphoreHandles.ptr
        };
        vkQueueSubmit(vkHandle, 1, &vkSubmitInfo, signalFence.vkHandle).enforceVK();
    }

    public VkResult present(const(PresentInfo) presentInfo)
    {
        VkResult[] results;
        results.length = presentInfo.swapchains.length;

        // map wrapped types to VK handles
        const(VkSemaphore[]) waitSemaphoreHandles = map!(sem => sem.vkHandle)(presentInfo.waitSemaphores).array();
        const(VkSwapchainKHR[]) swapchainHandles = map!(sc => sc.vkHandle)(presentInfo.swapchains).array();
        const(VkPresentInfoKHR) vkPresentInfo = {
            waitSemaphoreCount: cast(uint32_t) waitSemaphoreHandles.length,
            pWaitSemaphores: waitSemaphoreHandles.ptr,
            swapchainCount: cast(uint32_t) swapchainHandles.length,
            pSwapchains: swapchainHandles.ptr,
            pImageIndices: presentInfo.imageIndices.ptr,
            pResults: results.ptr
        };

        vkQueuePresentKHR(vkHandle, &vkPresentInfo);

        foreach (result; results)
        {
            if (result != VkResult.VK_SUCCESS) {
                return enforceVK!([VkResult.VK_SUBOPTIMAL_KHR, VkResult.VK_ERROR_OUT_OF_DATE_KHR])(result);
            }
        }
        return VkResult.VK_SUCCESS;
    }

    public void waitIdle()
    {
        vkQueueWaitIdle(vkHandle);
    }
}