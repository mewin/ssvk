module ssvk.vulkan.wrappers.pipeline;

import std.typecons;

import erupted;

import ssvk.vulkan;

struct PipelineColorBlendStateCreateInfo
{
    VkBool32 logicOpEnable = VK_FALSE;
    VkLogicOp logicOp = VK_LOGIC_OP_COPY;
    float[4] blendConstants = [0., 0., 0., 0.];
}

struct GraphicsPipelineCreateInfo
{
    VkPipelineVertexInputStateCreateInfo vertexInput;
    VkPipelineInputAssemblyStateCreateInfo inputAssembly = {
        topology: VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        primitiveRestartEnable: VK_FALSE
    };
    VkViewport[] viewports = [
        {
            x: 0.0,
            y: 0.0,
            width: -1.0,
            height: -1.0,
            minDepth: 0.0,
            maxDepth: 1.0
        }
    ];
    VkRect2D[] scissors = [
        {
            extent: {
                width: uint32_t.max,
                height: uint32_t.max
            }
        }
    ];
    VkPipelineRasterizationStateCreateInfo rasterizer = {
        depthClampEnable: VK_FALSE,
        rasterizerDiscardEnable: VK_FALSE,
        polygonMode: VK_POLYGON_MODE_FILL,
        lineWidth: 1.0,
        cullMode: VK_CULL_MODE_NONE,
        frontFace: VK_FRONT_FACE_CLOCKWISE,
        depthBiasEnable: VK_FALSE,
        depthBiasClamp: 0.0
    };
    VkPipelineMultisampleStateCreateInfo multisampling = {
        sampleShadingEnable: VK_FALSE,
        rasterizationSamples: VK_SAMPLE_COUNT_1_BIT
    };
    PipelineColorBlendStateCreateInfo colorBlending;
    VkPipelineColorBlendAttachmentState[] colorBlendAttachments = [
        {
            colorWriteMask: VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT
                    | VK_COLOR_COMPONENT_A_BIT,
            blendEnable: VK_TRUE,
            srcColorBlendFactor: VK_BLEND_FACTOR_SRC_ALPHA,
            dstColorBlendFactor: VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
            colorBlendOp: VK_BLEND_OP_ADD,
            srcAlphaBlendFactor: VK_BLEND_FACTOR_ONE,
            dstAlphaBlendFactor: VK_BLEND_FACTOR_ZERO,
            alphaBlendOp: VK_BLEND_OP_ADD,
        }
    ];
    VkPipelineShaderStageCreateInfo[] stages;
    PipelineLayout layout;
    RenderPass renderPass;
    uint32_t subpass = 0;
    Pipeline basePipeline;
    int basePipelineIndex = -1;

    // GFM is designed for OpenGL and therefore expects OpenGL screen coordinates
    // therefore the screen is flipped (along the y-axis) by default
    // see https://www.saschawillems.de/blog/2019/03/29/flipping-the-vulkan-viewport/
    bool flipViewport = true;
}

struct RayTracingPipelineCreateInfo
{
    VkPipelineCreateFlags                 flags;
    VkPipelineShaderStageCreateInfo[]     stages;
    VkRayTracingShaderGroupCreateInfoNV[] groups;
    uint32_t                              maxRecursionDepth;
    PipelineLayout                        layout;
    Pipeline                              basePipeline;
    int32_t                               basePipelineIndex;
}

struct Pipeline
{
    private Renderer renderer;
    public VkPipeline vkHandle;

    public void createGraphics(Renderer renderer, GraphicsPipelineCreateInfo createInfo)
    {
        this.renderer = renderer;

        foreach (ref viewport; createInfo.viewports)
        {
            if (viewport.width < 0.0) {
                viewport.width = renderer.swapchain.extent.width;
            }
            if (viewport.height < 0.0) {
                viewport.height = renderer.swapchain.extent.height;
            }
            if (createInfo.flipViewport) {
                viewport.y = viewport.height;
                viewport.height = -viewport.height;
            }
        }
        foreach (ref scissor; createInfo.scissors)
        {
            if (scissor.extent.width == uint32_t.max) {
                scissor.extent.width = renderer.swapchain.extent.width;
            }
            if (scissor.extent.height == uint32_t.max) {
                scissor.extent.height = renderer.swapchain.extent.height;
            }
        }

        VkPipelineViewportStateCreateInfo viewportState = {
            viewportCount: cast(uint32_t) createInfo.viewports.length,
            pViewports: createInfo.viewports.ptr,
            scissorCount: cast(uint32_t) createInfo.scissors.length,
            pScissors: createInfo.scissors.ptr
        };

        VkPipelineColorBlendStateCreateInfo colorBlending = {
            logicOpEnable: createInfo.colorBlending.logicOpEnable,
            logicOp: createInfo.colorBlending.logicOp,
            blendConstants: createInfo.colorBlending.blendConstants,
            attachmentCount: cast(uint32_t) createInfo.colorBlendAttachments.length,
            pAttachments: createInfo.colorBlendAttachments.ptr
        };
        VkPipelineDepthStencilStateCreateInfo depthStencil = {
            depthTestEnable: VK_TRUE,
            depthWriteEnable: VK_TRUE,
            depthCompareOp: VK_COMPARE_OP_LESS,
            depthBoundsTestEnable: VK_FALSE,
            minDepthBounds: 0.0,
            maxDepthBounds: 1.0,
            stencilTestEnable: VK_FALSE
        };

        // build the actual VK struct
        VkGraphicsPipelineCreateInfo vkCreateInfo = {
            stageCount: cast(uint32_t) createInfo.stages.length,
            pStages: createInfo.stages.ptr,
            pVertexInputState: &createInfo.vertexInput,
            pInputAssemblyState: &createInfo.inputAssembly,
            pViewportState: &viewportState,
            pRasterizationState: &createInfo.rasterizer,
            pMultisampleState: &createInfo.multisampling,
            pDepthStencilState: &depthStencil,
            pColorBlendState: &colorBlending,
            pDynamicState: null,
            layout: createInfo.layout.vkHandle,
            renderPass: createInfo.renderPass.vkHandle,
            subpass: createInfo.subpass,
            basePipelineHandle: createInfo.basePipeline.vkHandle,
            basePipelineIndex: createInfo.basePipelineIndex
        };

        vkCreateGraphicsPipelines(renderer.device.vkHandle, null, 1, &vkCreateInfo, null, &vkHandle).enforceVK();
    }

    public void createCompute(Renderer renderer, ref const(VkComputePipelineCreateInfo) createInfo)
    {
        this.renderer = renderer;

        vkCreateComputePipelines(renderer.device.vkHandle, null, 1, &createInfo, null, &vkHandle);
    }

    public void createRaytracing(Renderer renderer, RayTracingPipelineCreateInfo createInfo)
    {
        this.renderer = renderer;
        
        VkRayTracingPipelineCreateInfoNV vkCreateInfo = {
            flags: createInfo.flags,
            stageCount: cast(uint32_t) createInfo.stages.length,
            pStages: createInfo.stages.ptr,
            groupCount: cast(uint32_t) createInfo.groups.length,
            pGroups: createInfo.groups.ptr,
            maxRecursionDepth: createInfo.maxRecursionDepth,
            layout: createInfo.layout.vkHandle,
            basePipelineHandle: createInfo.basePipeline.vkHandle,
            basePipelineIndex: createInfo.basePipelineIndex
        };
        vkCreateRayTracingPipelinesNV(renderer.device.vkHandle, null, 1, &vkCreateInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        if (vkHandle)
        {
            vkDestroyPipeline(renderer.device.vkHandle, vkHandle, null);
            vkHandle = VK_NULL_HANDLE;
        }
    }
}

struct PipelineLayout
{
    private Renderer renderer;
    public VkPipelineLayout vkHandle;

    public void create(Renderer renderer, VkPipelineLayoutCreateInfo createInfo)
    {
        this.renderer = renderer;

        vkCreatePipelineLayout(renderer.device.vkHandle, &createInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        if (vkHandle)
        {
            vkDestroyPipelineLayout(renderer.device.vkHandle, vkHandle, null);
            vkHandle = VK_NULL_HANDLE;
        }
    }
}

struct RenderPass
{
    private Renderer renderer;
    public VkRenderPass vkHandle;

    public void create(Renderer renderer, VkRenderPassCreateInfo createInfo)
    {
        this.renderer = renderer;

        vkCreateRenderPass(renderer.device.vkHandle, &createInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        if (vkHandle)
        {
            vkDestroyRenderPass(renderer.device.vkHandle, vkHandle, null);
            vkHandle = VK_NULL_HANDLE;
        }
    }
}

struct DescriptorSetLayoutCreateInfo
{
    VkDescriptorSetLayoutCreateFlags flags = 0;
    VkDescriptorSetLayoutBinding[] bindings;
    Nullable!VkDescriptorSetLayoutBindingFlagsCreateInfo layoutBindingFlagsCreateInfo;
}

struct DescriptorSetLayout
{
    private Renderer renderer;
    public VkDescriptorSetLayout vkHandle;

    public void create(Renderer renderer, DescriptorSetLayoutCreateInfo createInfo)
    {
        this.renderer = renderer;

        VkDescriptorSetLayoutCreateInfo vkCreateInfo = {
            flags: createInfo.flags,
            bindingCount: cast(uint32_t) createInfo.bindings.length,
            pBindings: createInfo.bindings.ptr
        };
        if (!createInfo.layoutBindingFlagsCreateInfo.isNull()) {
            vkCreateInfo.pNext = &createInfo.layoutBindingFlagsCreateInfo.get();
        }
        vkCreateDescriptorSetLayout(renderer.device.vkHandle, &vkCreateInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        if (vkHandle)
        {
            vkDestroyDescriptorSetLayout(renderer.device.vkHandle, vkHandle, null);
            vkHandle = VK_NULL_HANDLE;
        }
    }
}

struct DescriptorPoolCreateInfo
{
    VkDescriptorPoolCreateFlags flags = 0;
    uint32_t maxSets = 0;
    VkDescriptorPoolSize[] poolSizes;
}

struct DescriptorPool
{
    private Renderer renderer;
    public VkDescriptorPool vkHandle;

    public void create(Renderer renderer, DescriptorPoolCreateInfo createInfo)
    {
        this.renderer = renderer;

        VkDescriptorPoolCreateInfo vkCreateInfo = {
            flags: createInfo.flags,
            maxSets: createInfo.maxSets,
            poolSizeCount: cast(uint32_t) createInfo.poolSizes.length,
            pPoolSizes: createInfo.poolSizes.ptr
        };
        vkCreateDescriptorPool(renderer.device.vkHandle, &vkCreateInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        if (vkHandle)
        {
            vkDestroyDescriptorPool(renderer.device.vkHandle, vkHandle, null);
            vkHandle = VK_NULL_HANDLE;
        }
    }
}

struct DescriptorSet
{
    private Renderer renderer;
    private DescriptorPool descriptorPool;
    public VkDescriptorSet vkHandle;

    public void allocate(Renderer renderer, DescriptorPool descriptorPool, DescriptorSetLayout descriptorLayout,
            uint variableDescriptorCounts = 0)
    {
        this.renderer = renderer;
        this.descriptorPool = descriptorPool;

        VkDescriptorSetAllocateInfo allocInfo = {
            descriptorPool: descriptorPool.vkHandle,
            descriptorSetCount: 1,
            pSetLayouts: &descriptorLayout.vkHandle
        };

        VkDescriptorSetVariableDescriptorCountAllocateInfoEXT variableAllocInfo; // must be declared at this scope
        if (variableDescriptorCounts > 0)
        {
            allocInfo.pNext = &variableAllocInfo; // actually use it
            variableAllocInfo.descriptorSetCount = 1;
            variableAllocInfo.pDescriptorCounts = &variableDescriptorCounts;
        }
        vkAllocateDescriptorSets(renderer.device.vkHandle, &allocInfo, &vkHandle).enforceVK();
    }

    public void free()
    {
        vkFreeDescriptorSets(renderer.device.vkHandle, descriptorPool.vkHandle, 1, &vkHandle);
    }
}
