module ssvk.vulkan.wrappers.data;

import erupted;

import ssvk.vulkan;
import ssvk.vulkan.cmd.memory;

struct Buffer
{
    private Renderer renderer;
    public VkBuffer vkHandle;

    public void create(Renderer renderer, VkBufferCreateInfo createInfo)
    {
        this.renderer = renderer;

        vkCreateBuffer(renderer.device.vkHandle, &createInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        vkDestroyBuffer(renderer.device.vkHandle, vkHandle, null);
    }

    public void bindMemory(DeviceMemory memory, VkDeviceSize offset = 0)
    {
        vkBindBufferMemory(renderer.device.vkHandle, vkHandle, memory.vkHandle, offset);
    }

    public VkMemoryRequirements getMemoryRequirements()
    {
        VkMemoryRequirements requirements;
        vkGetBufferMemoryRequirements(renderer.device.vkHandle, vkHandle, &requirements);
        return requirements;
    }

    public void copyTo(Buffer dstBuffer, VkDeviceSize size, VkDeviceSize srcOffset = 0, VkDeviceSize dstOffset = 0)
    {
        VkBufferCopy copyRegion = {
            srcOffset: srcOffset,
            dstOffset: dstOffset,
            size: size
        };

        CommandBuffer cmdBuffer;
        cmdBuffer.allocate(renderer, renderer.commandPoolTransient);

        cmdBuffer.begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
        cmdBuffer.cmdCopyBuffer(this, dstBuffer, [copyRegion]);
        cmdBuffer.end();

        QueueSubmitInfo submitInfo = {
            commandBuffers: [cmdBuffer]
        };
        renderer.device.graphicsQueue.submit(submitInfo);
        renderer.device.graphicsQueue.waitIdle();

        cmdBuffer.free();
    }

    public void copyToImage(Image dstImage, uint width, uint height, uint depth = 1)
    {
        VkBufferImageCopy region = {
            bufferOffset: 0,
            bufferRowLength: 0,
            bufferImageHeight: 0,
            imageSubresource: {
                aspectMask: VK_IMAGE_ASPECT_COLOR_BIT,
                mipLevel: 0,
                baseArrayLayer: 0,
                layerCount: 1
            },
            imageOffset: {x: 0, y: 0, z: 0},
            imageExtent: {
                width: width,
                height: height,
                depth: depth
            }
        };

        CommandBuffer cmdBuffer;
        cmdBuffer.allocate(renderer, renderer.commandPoolTransient);

        cmdBuffer.begin(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
        cmdBuffer.cmdCopyBufferToImage(this, dstImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, [region]);
        cmdBuffer.end();

        QueueSubmitInfo submitInfo = {
            commandBuffers: [cmdBuffer]
        };
        renderer.device.graphicsQueue.submit(submitInfo);
        renderer.device.graphicsQueue.waitIdle();

        cmdBuffer.free();
    }
}

struct DeviceMemory
{
    private Renderer renderer;
    public VkDeviceMemory vkHandle;

    public void allocate(Renderer renderer, VkMemoryAllocateInfo allocInfo)
    {
        this.renderer = renderer;
        vkAllocateMemory(renderer.device.vkHandle, &allocInfo, null, &vkHandle).enforceVK();
    }

    public void free()
    {
        if (vkHandle != VK_NULL_HANDLE)
        {
            vkFreeMemory(renderer.device.vkHandle, vkHandle, null);
            vkHandle = VK_NULL_HANDLE;
        }
    }

    public void* map(VkDeviceSize offset = 0, VkDeviceSize size = VK_WHOLE_SIZE)
    {
        void* data;
        vkMapMemory(renderer.device.vkHandle, vkHandle, offset, size, 0, &data).enforceVK();
        return data;
    }

    public void unmap()
    {
        vkUnmapMemory(renderer.device.vkHandle, vkHandle);
    }
}
