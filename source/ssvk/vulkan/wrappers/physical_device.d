module ssvk.vulkan.wrappers.physical_device;

import std.experimental.logger;

import erupted;

import ssvk.vulkan;

struct SwapchainSupportDetails
{
    VkSurfaceCapabilitiesKHR surfaceCapabilities;
    VkSurfaceFormatKHR[] formats;
    VkPresentModeKHR[] presentModes;
}

struct PhysicalDevice
{
    public Renderer renderer;
    public VkPhysicalDevice vkHandle;

    public this(Renderer renderer, VkPhysicalDevice vkHandle)
    {
        this.renderer = renderer;
        this.vkHandle = vkHandle;
    }

    public VkPhysicalDeviceFeatures getFeatures()
    {
        VkPhysicalDeviceFeatures features;
        vkGetPhysicalDeviceFeatures(vkHandle, &features);
        return features;
    }

    public VkPhysicalDeviceMemoryProperties getMemoryProperties()
    {
        VkPhysicalDeviceMemoryProperties memoryProperties;
        vkGetPhysicalDeviceMemoryProperties(vkHandle, &memoryProperties);
        return memoryProperties;
    }

    public VkFormatProperties getFormatProperties(VkFormat format)
    {
        VkFormatProperties properties;
        vkGetPhysicalDeviceFormatProperties(vkHandle, format, &properties);
        return properties;
    }

    public VkPhysicalDeviceDescriptorIndexingFeatures getDescriptorIndexingFeatures()
    {
        VkPhysicalDeviceDescriptorIndexingFeatures descriptorIndexingFeatures;
        VkPhysicalDeviceFeatures2 features2 = {
            pNext: &descriptorIndexingFeatures
        };
        vkGetPhysicalDeviceFeatures2(vkHandle, &features2);
        return descriptorIndexingFeatures;
    }

    public VkQueueFamilyProperties[] getQueueFamilies()
    {
        uint32_t queueFamilyCount;
        VkQueueFamilyProperties[] queueFamilies;
        vkGetPhysicalDeviceQueueFamilyProperties(vkHandle, &queueFamilyCount, null);
        queueFamilies.length = queueFamilyCount;
        vkGetPhysicalDeviceQueueFamilyProperties(vkHandle, &queueFamilyCount, queueFamilies.ptr);
        return queueFamilies;
    }

    public bool getSurfaceSupport(uint32_t queueFamilyIndex, Surface surface)
    {
		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(vkHandle, queueFamilyIndex, surface.vkHandle, &presentSupport);
        return cast(bool) presentSupport;
    }

    public SwapchainSupportDetails getSwapchainSupportDetails(Surface surface)
    {
        SwapchainSupportDetails details;

        // general capabilities
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vkHandle, surface.vkHandle, &details.surfaceCapabilities);

        // supported formats
        uint32_t formatCount;
        vkGetPhysicalDeviceSurfaceFormatsKHR(vkHandle, surface.vkHandle, &formatCount, null);
        if (formatCount > 0)
        {
            details.formats.length = formatCount;
            vkGetPhysicalDeviceSurfaceFormatsKHR(vkHandle, surface.vkHandle, &formatCount, details.formats.ptr);
        }

        // present modes
        uint32_t presentModeCount;
        vkGetPhysicalDeviceSurfacePresentModesKHR(vkHandle, surface.vkHandle, &presentModeCount, null);
        if (presentModeCount > 0)
        {
            details.presentModes.length = presentModeCount;
            vkGetPhysicalDeviceSurfacePresentModesKHR(vkHandle, surface.vkHandle, &presentModeCount, details.presentModes.ptr);
        }

        return details;
    }

    public VkPhysicalDeviceRayTracingPropertiesNV getRaytracingProperties()
    {
        VkPhysicalDeviceRayTracingPropertiesNV raytracingProperties;
        VkPhysicalDeviceProperties2 properties = {
            pNext: &raytracingProperties
        };
        vkGetPhysicalDeviceProperties2(vkHandle, &properties);
        return raytracingProperties;
    }

    public VkExtensionProperties[] enumerateExtensions()
    {
        uint32_t extensionCount;
        VkExtensionProperties[] extensions;
        vkEnumerateDeviceExtensionProperties(vkHandle, null, &extensionCount, null);
        extensions.length = extensionCount;
        vkEnumerateDeviceExtensionProperties(vkHandle, null, &extensionCount, extensions.ptr);
        version(none) // debug
        {
	        info("Available device extensions:");
            dumpExtensions(extensions);
        }
        return extensions;
    }

    public VkLayerProperties[] enumerateLayers()
    {
        uint32_t layerCount;
        VkLayerProperties[] layers;
        vkEnumerateDeviceLayerProperties(vkHandle, &layerCount, null);
        layers.length = layerCount;
        vkEnumerateDeviceLayerProperties(vkHandle, &layerCount, layers.ptr);
        version(none) // debug
        {
	        info("Available device layers:");
            dumpLayers(layers);
        }
        return layers;
    }

    public uint32_t findMemoryType(VkMemoryRequirements requirements, VkMemoryPropertyFlags properties)
    {
        VkPhysicalDeviceMemoryProperties deviceProperties = getMemoryProperties();

        for (uint32_t i = 0; i < deviceProperties.memoryTypeCount; ++i)
        {
            if ((requirements.memoryTypeBits & (1 << i)) == 0) {
                continue; // not suitable for this buffer
            }
            if ((deviceProperties.memoryTypes[i].propertyFlags & properties) != properties) {
                continue; // does not fulfill required properties
            }
            return i;
        }

        throw new Exception("could not find a suitable memory type");
    }

    public VkFormat findSupportedFormat(const(VkFormat[]) candidates, VkImageTiling tiling,
            VkFormatFeatureFlags features)
    {
        // TODO: cache results
        
        foreach (format; candidates)
        {
            VkFormatProperties properties = getFormatProperties(format);
            if (tiling == VK_IMAGE_TILING_LINEAR && (properties.linearTilingFeatures & features)) {
                return format;
            }
            else if (tiling == VK_IMAGE_TILING_OPTIMAL && (properties.optimalTilingFeatures & features)) {
                return format;
            }
        }
        throw new Exception("could not find a suitable format");
    }

    public VkFormat findDepthFormat()
    {
        return findSupportedFormat([VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT],
                VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
    }

    public VkFormat findIntermediateImageFormatVec4()
    {
        return findSupportedFormat([VK_FORMAT_R32G32B32A32_SFLOAT], VK_IMAGE_TILING_OPTIMAL,
            VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT | VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT);
    }
}
