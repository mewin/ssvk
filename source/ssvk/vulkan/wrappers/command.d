module ssvk.vulkan.wrappers.command;

import std.typecons;

import erupted;

import ssvk.vulkan;

struct CommandPool
{
    private Renderer renderer;
    public VkCommandPool vkHandle;

    public void create(Renderer renderer, VkCommandPoolCreateInfo createInfo)
    {
        this.renderer = renderer;

        vkCreateCommandPool(renderer.device.vkHandle, &createInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        vkDestroyCommandPool(renderer.device.vkHandle, vkHandle, null);
    }
}

struct CommandBuffer
{
    private Renderer renderer;
    private CommandPool commandPool;
    public VkCommandBuffer vkHandle;

    ////////////////////////////////
    /// creation and destruction ///
    ////////////////////////////////
    public void allocate(Renderer renderer, CommandPool commandPool,
            VkCommandBufferLevel level = VK_COMMAND_BUFFER_LEVEL_PRIMARY)
    {
        this.renderer = renderer;
        this.commandPool = commandPool;

        VkCommandBufferAllocateInfo allocInfo = {
            commandPool: commandPool.vkHandle,
            level: level,
            commandBufferCount: 1
        };
        vkAllocateCommandBuffers(renderer.device.vkHandle, &allocInfo, &vkHandle).enforceVK();
    }

    public void free()
    {
        vkFreeCommandBuffers(renderer.device.vkHandle, commandPool.vkHandle, 1, &vkHandle);
    }

    public void reset(VkCommandBufferResetFlags flags = 0)
    {
        vkResetCommandBuffer(vkHandle, flags);
    }

    /////////////////
    /// recording ///
    /////////////////
    public void begin(VkCommandBufferUsageFlags flags = 0,
            Nullable!VkCommandBufferInheritanceInfo inheritanceInfo = Nullable!VkCommandBufferInheritanceInfo())
    {
        VkCommandBufferBeginInfo beginInfo = {
            flags: flags,
            pInheritanceInfo: inheritanceInfo.isNull() ? null : &inheritanceInfo.get()
        };
        vkBeginCommandBuffer(vkHandle, &beginInfo).enforceVK();
    }

    public void end()
    {
        vkEndCommandBuffer(vkHandle).enforceVK();
    }
}
