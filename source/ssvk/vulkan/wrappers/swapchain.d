module ssvk.vulkan.wrappers.swapchain;

import std.algorithm.comparison;
import std.exception;
import std.experimental.logger;

import erupted;

import ssvk.vulkan;

struct Swapchain
{
    private Renderer renderer;
    public VkSwapchainKHR vkHandle;
    public Image[] images;
    public ImageView[] imageViews;
    public VkSurfaceFormatKHR surfaceFormat;
    public VkExtent2D extent;

    public void create(Renderer renderer)
    {
        this.renderer = renderer;
        
        SwapchainSupportDetails ssd = renderer.device.physicalDevice.getSwapchainSupportDetails(renderer.surface);
        immutable(VkPresentModeKHR) presentMode = choosePresentMode(ssd);
        immutable(uint32_t) imageCount = chooseImageCount(ssd);
        extent = chooseSwapExtent(ssd);
        surfaceFormat = chooseSurfaceFormat(ssd);
        info("Chose surface format: ", surfaceFormat);

        VkSwapchainCreateInfoKHR swapchainCreateInfo = {
            surface: renderer.surface.vkHandle,
            minImageCount: imageCount,
            imageFormat: surfaceFormat.format,
            imageColorSpace: surfaceFormat.colorSpace,
            imageExtent: extent,
            imageArrayLayers: 1,
            imageUsage: VK_IMAGE_USAGE_STORAGE_BIT,
            preTransform: ssd.surfaceCapabilities.currentTransform,
            compositeAlpha: VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
            presentMode: presentMode,
            clipped: VK_TRUE
        };

        uint32_t[] sharedFamilyIndices;
        if (renderer.device.graphicsQueue.familyIndex != renderer.device.presentQueue.familyIndex)
        {
            swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            sharedFamilyIndices ~= [renderer.device.graphicsQueue.familyIndex, renderer.device.presentQueue.familyIndex];
            swapchainCreateInfo.queueFamilyIndexCount = cast(uint32_t) sharedFamilyIndices.length;
            swapchainCreateInfo.pQueueFamilyIndices = sharedFamilyIndices.ptr;
        }
        else {
            swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        }

        vkCreateSwapchainKHR(renderer.device.vkHandle, &swapchainCreateInfo, null, &vkHandle).enforceVK();

        // now retrieve the swapchain images
        uint32_t swapchainImageCount;
        VkImage[] imageHandles;
        vkGetSwapchainImagesKHR(renderer.device.vkHandle, vkHandle, &swapchainImageCount, null);
        imageHandles.length = swapchainImageCount;
        vkGetSwapchainImagesKHR(renderer.device.vkHandle, vkHandle, &swapchainImageCount, imageHandles.ptr);

        // and create images and image views from them
        images.length = swapchainImageCount;
        imageViews.length = swapchainImageCount;
        foreach (i, imageHandle; imageHandles)
        {
            // image object
            images[i].initialize(renderer, imageHandle);

            // image view
            VkImageViewCreateInfo imageViewCreateInfo = {
                image: images[i].vkHandle,
                viewType: VK_IMAGE_VIEW_TYPE_2D,
                format: surfaceFormat.format,
                components: {
                    r: VK_COMPONENT_SWIZZLE_IDENTITY,
                    g: VK_COMPONENT_SWIZZLE_IDENTITY,
                    b: VK_COMPONENT_SWIZZLE_IDENTITY,
                    a: VK_COMPONENT_SWIZZLE_IDENTITY
                },
                subresourceRange: {
                    aspectMask: VK_IMAGE_ASPECT_COLOR_BIT,
                    baseMipLevel: 0,
                    levelCount: 1,
                    baseArrayLayer: 0,
                    layerCount: 1
                }
            };
            imageViews[i].create(renderer, imageViewCreateInfo);
        }
    }

    public void cleanup()
    {
        foreach (imageView; imageViews) {
            imageView.cleanup();
        }
        vkDestroySwapchainKHR(renderer.device.vkHandle, vkHandle, null);
    }

    public VkResult acquireNextImage(uint64_t timeout, Semaphore semaphore, Fence fence, uint32_t* imageIndex)
    {
        return vkAcquireNextImageKHR(renderer.device.vkHandle, vkHandle, timeout, semaphore.vkHandle, fence.vkHandle,
                imageIndex).enforceVK!([VkResult.VK_SUCCESS, VkResult.VK_ERROR_OUT_OF_DATE_KHR, VkResult.VK_SUBOPTIMAL_KHR])();
    }

    private VkSurfaceFormatKHR chooseSurfaceFormat(ref SwapchainSupportDetails ssd)
    {
        VkSurfaceFormatKHR anyFormat = { format: VK_FORMAT_UNDEFINED };
        foreach (format; ssd.formats)
        {
            VkFormatProperties properties = renderer.device.physicalDevice.getFormatProperties(format.format);
            if (!(properties.optimalTilingFeatures & VK_FORMAT_FEATURE_STORAGE_IMAGE_BIT)) {
                continue;
            }
            if (anyFormat.format == VK_FORMAT_UNDEFINED) {
                anyFormat = format;
            }
            if (format.format == VK_FORMAT_B8G8R8A8_SRGB &&
                    format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                return format;
            }
        }

        enforce(anyFormat.format != VK_FORMAT_UNDEFINED);
        return anyFormat;
    }
    
    private VkPresentModeKHR choosePresentMode(ref SwapchainSupportDetails ssd)
    {
        foreach (presentMode; ssd.presentModes)
        {
            if (presentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
                return presentMode;
            }
        }

        return VK_PRESENT_MODE_FIFO_KHR;
    }

    private VkExtent2D chooseSwapExtent(ref SwapchainSupportDetails ssd)
    {
        if (ssd.surfaceCapabilities.currentExtent.width != uint32_t.max) {
            return ssd.surfaceCapabilities.currentExtent;
        }

        VkExtent2D extent = renderer.surface.getFramebufferSize();
        extent.width = clamp(extent.width, ssd.surfaceCapabilities.minImageExtent.width,
                ssd.surfaceCapabilities.maxImageExtent.width);
        extent.height = clamp(extent.height, ssd.surfaceCapabilities.minImageExtent.height,
                ssd.surfaceCapabilities.maxImageExtent.height);
        return extent;
    }

    private uint32_t chooseImageCount(ref SwapchainSupportDetails ssd)
    {
        uint32_t imageCount = ssd.surfaceCapabilities.minImageCount + 1;
        imageCount = min(imageCount, ssd.surfaceCapabilities.maxImageCount);
        return imageCount;
    }
}