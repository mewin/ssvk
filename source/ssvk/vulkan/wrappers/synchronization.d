module ssvk.vulkan.wrappers.synchronization;

import erupted;

import ssvk.vulkan;

struct Semaphore
{
    private Renderer renderer;
    public VkSemaphore vkHandle;

    public void create(Renderer renderer, VkSemaphoreCreateInfo createInfo = VkSemaphoreCreateInfo.init)
    {
        this.renderer = renderer;

        vkCreateSemaphore(renderer.device.vkHandle, &createInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        vkDestroySemaphore(renderer.device.vkHandle, vkHandle, null);
    }
}

struct Fence
{
    private Renderer renderer;
    public VkFence vkHandle;

    public void create(Renderer renderer, VkFenceCreateInfo createInfo = VkFenceCreateInfo.init)
    {
        this.renderer = renderer;

        vkCreateFence(renderer.device.vkHandle, &createInfo, null, &vkHandle).enforceVK();
    }

    public void cleanup()
    {
        vkDestroyFence(renderer.device.vkHandle, vkHandle, null);
    }

    public void wait(uint64_t timeout = uint64_t.max)
    {
        vkWaitForFences(renderer.device.vkHandle, 1, &vkHandle, VK_TRUE, timeout);
    }

    public void reset()
    {
        vkResetFences(renderer.device.vkHandle, 1, &vkHandle);
    }

    public T opCast(T)() const if (is(T == bool))
    {
        return vkHandle != VK_NULL_HANDLE;
    }
}
