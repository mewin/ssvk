module ssvk.vulkan.debug_utils_messenger;

debug:

import core.stdc.signal;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;
import std.algorithm;
import std.conv;
import std.experimental.logger;
import std.format;
import std.traits;

import bindbc.sdl;
import erupted;

import ssvk.vulkan;

private enum PRINT_BUFFER_LENGTH = 4096;
private enum SIGTRAP = 5;

int32_t[1024] ignoredMessageIds;

// I have no idea if this works
version (Windows) extern (Windows) void DebugBreak();

@nogc nothrow const(char*) formatMessageSeverity(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity)
{
    switch (messageSeverity)
    {
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
            return "verbose";
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
            return "info";
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
            return "warning";
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT: 
            return "error";
        default:
            return "<unknown>";
    }
}

@nogc nothrow private void wrapTextNoGC(ref char[PRINT_BUFFER_LENGTH] buffer, const(char*) text, uint width)
{
    int outPos = 0;
    int inPos = 0;

    while (text[inPos] != '\0' && outPos < PRINT_BUFFER_LENGTH)
    {
        int inPos2 = inPos;
        while (text[inPos2] != '\0')
        {
            int inPos3a = 1 + inPos2 + cast(int) strcspn(text + inPos2 + 1, " \t\n");
            int inPos3b = 2 + inPos2 + cast(int) strcspn(text + inPos2 + 1, "-");
            int inPos3 = min(inPos3a, inPos3b);

            if ((inPos3 - inPos) > width) {
                if (inPos2 == inPos) {
                    inPos2 = inPos + width;
                }
                break;
            }
            inPos2 = inPos3;
            if (text[inPos2] == '\n') {
                break;
            }
        }
        int len = min(inPos2 - inPos, PRINT_BUFFER_LENGTH - outPos);
        strncpy(buffer.ptr + outPos, text + inPos, inPos2 - inPos);
        buffer[outPos + len] = '\n';
        inPos = inPos2;
        outPos += len + 1;
    }
    buffer[outPos - 1] = '\0';
}

@nogc nothrow private void formatDebugMessage(ref char[PRINT_BUFFER_LENGTH] outBuffer,
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageTypes,
        const(VkDebugUtilsMessengerCallbackDataEXT*) pCallbackData)
{
    char[PRINT_BUFFER_LENGTH] wrappedMessage;

    wrapTextNoGC(wrappedMessage, pCallbackData.pMessage, 64);

    snprintf(outBuffer.ptr, PRINT_BUFFER_LENGTH, "Severity: %s\nTypes: %s\nID: %d (%s)\nMessage:\n%s",
        formatMessageSeverity(messageSeverity),
        "".ptr,
        pCallbackData.messageIdNumber, pCallbackData.pMessageIdName,
        wrappedMessage.ptr);
}

@nogc nothrow private bool isMessageIgnored(int32_t messageIdNumber)
{
    import std.algorithm.searching : canFind;

    return ignoredMessageIds[].canFind(messageIdNumber);
}

@nogc nothrow private void setMessageIgnored(int32_t messageIdNumber)
{
    import std.algorithm.searching : countUntil;

    ptrdiff_t idx = ignoredMessageIds[].countUntil(0);
    if (idx < 0) {
        return;
    }
    ignoredMessageIds[idx] = messageIdNumber;
}

extern(C)
@nogc nothrow private VkBool32 debugUtilsMessengerCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT           messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT                  messageTypes,
    const(VkDebugUtilsMessengerCallbackDataEXT)*     pCallbackData,
    void*                                            pUserData)
{
    enum Buttons
    {
        IGNORE,
        IGNORE_ALL,
        HALT,
        QUIT
    }

    if (isMessageIgnored(pCallbackData.messageIdNumber)) {
        return VK_FALSE;
    }

    char[PRINT_BUFFER_LENGTH] buffer;
    try
    {
        formatDebugMessage(buffer, messageSeverity, messageTypes, pCallbackData);
        switch (messageSeverity)
        {
            default:
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
                fputs(pCallbackData.pMessage, stderr);
                fputc('\n', stderr);
                fflush(stderr);
                break;
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
                fputs(pCallbackData.pMessage, stderr);
                fputc('\n', stderr);
                fflush(stderr);

                SDL_MessageBoxButtonData[4] buttons = [
                    {
                        flags: 0,
                        buttonid: Buttons.IGNORE,
                        text: "Ignore Now"
                    },
                    {
                        flags: 0,
                        buttonid: Buttons.IGNORE_ALL,
                        text: "Ignore All"
                    },
                    {
                        flags: SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT,
                        buttonid: Buttons.HALT,
                        text: "Halt"
                    },
                    {
                        flags: SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT,
                        buttonid: Buttons.QUIT,
                        text: "Quit"
                    }
                ];
                SDL_MessageBoxData msgBoxData = {
                    title: "Vulkan Message",
                    message: buffer.ptr,
                    numbuttons: cast(int) buttons.length,
                    buttons: buttons.ptr
                };
                int button;
                SDL_ShowMessageBox(&msgBoxData, &button);
                switch (button)
                {
                    case Buttons.IGNORE:
                        break;
                    case Buttons.IGNORE_ALL:
                        setMessageIgnored(pCallbackData.messageIdNumber);
                        break;
                    case Buttons.HALT:
                        version(Posix) raise(SIGTRAP);
                        version(Windows) DebugBreak();
                        break;
                    case Buttons.QUIT:
                    default:
                        exit(-1);
                        break;
                }
                break;
        }
    }
    catch(Exception)
    {

    }
    return VK_FALSE;
}

struct DebugUtilsMessenger
{
    private Renderer renderer;
    private VkDebugUtilsMessengerEXT vkHandle;

    public void create(Renderer renderer)
    {
        VkDebugUtilsMessengerCreateInfoEXT createInfo = {
            messageSeverity: VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
                    | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
            messageType: VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
                    | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
            pfnUserCallback: &debugUtilsMessengerCallback
        };
        vkCreateDebugUtilsMessengerEXT(renderer.instance.vkHandle, &createInfo, null, &vkHandle);
    }

    public void cleanup()
    {

    }
}
