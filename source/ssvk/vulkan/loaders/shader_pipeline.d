module ssvk.vulkan.loaders.shader_pipeline;

import std.algorithm;
import std.exception;
import std.file;
import std.json;

import erupted;

import ssvk.resource.shader;
import ssvk.loaders.shader;
import ssvk.vulkan;
import ssvk.vulkan.resource.shader;

enum DEFAULT_RAY_TRACING_RECURSION_DEPTH = 8;

private struct ShaderPipelineLoader
{
    Renderer renderer;
    ShaderData sd;
    ShaderModule[] shaderModules;
    VkPipelineShaderStageCreateInfo[] shaderStageCreateInfos;
    JSONValue json;

    void collectShaderModules()
    {  
        // create shader modules
        foreach (module_; sd.shader.modules)
        {
            ShaderModule shaderModule;
            shaderModule.create(renderer, module_.code);
            shaderModules ~= shaderModule;

            VkPipelineShaderStageCreateInfo shaderStageCreateInfo = {
                stage: stageToVulkan(module_.stage),
                _module: shaderModule.vkHandle,
                pName: "main"
            };
            shaderStageCreateInfos ~= shaderStageCreateInfo;
        }
    }

    void loadGraphicsPipeline()
    {
        assert (false, "graphics pipline currently dont work");
        version(none)
        {
            GraphicsPipelineCreateInfo createInfo = {
                // vertexInput (TODO)
                stages: shaderStageCreateInfos,
                renderPass: renderer.drawPass.renderPass,
                layout: sd.pipelineLayout
            };

            if ("input_assembly" in json && json["input_assembly"].type == JSONType.OBJECT)
            {
                JSONValue inputAssemblyJson = json["input_assembly"].object;
                if ("topology" in inputAssemblyJson && inputAssemblyJson["topology"].type == JSONType.STRING) {
                    createInfo.inputAssembly.topology = topologyFromJson(inputAssemblyJson["topology"].str);
                }
            }

            sd.pipeline.createGraphics(renderer, createInfo);
            sd.recreateInfo = createInfo;
        }
    }

    void loadRaytracingPipeline()
    {
        // shader groups
        VkRayTracingShaderGroupCreateInfoNV[] shaderGroupCreateInfos;
        foreach (i, module_; sd.shader.modules)
        {
            VkRayTracingShaderGroupCreateInfoNV shaderGroup = {
                type: VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_NV,
                generalShader: VK_SHADER_UNUSED_NV,
                closestHitShader: VK_SHADER_UNUSED_NV,
                anyHitShader: VK_SHADER_UNUSED_NV,
                intersectionShader: VK_SHADER_UNUSED_NV
            };
            switch (module_.stage)
            {
                case ShaderStage.RAY_GENERATION:
                    shaderGroup.generalShader = cast(int) i;
                    break;
                case ShaderStage.CLOSEST_HIT:
                    shaderGroup.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_NV;
                    shaderGroup.closestHitShader = cast(int) i;
                    break;
                case ShaderStage.MISS:
                    shaderGroup.generalShader = cast(int) i;
                    break;
                default:
                    assert(false, "What shader is this?");
            }
            shaderGroupCreateInfos ~= shaderGroup;
        }
        
        // create the pipeline
        RayTracingPipelineCreateInfo createInfo = {
            stages: shaderStageCreateInfos,
            groups: shaderGroupCreateInfos,
            maxRecursionDepth: DEFAULT_RAY_TRACING_RECURSION_DEPTH,
            layout: sd.pipelineLayout
        };
        if ("max_recursion_depth" in json && json["max_recursion_depth"].type == JSONType.INTEGER) {
            createInfo.maxRecursionDepth = cast(uint32_t) max(1, json["max_recursion_depth"].integer);
        }

        sd.pipeline.createRaytracing(renderer, createInfo);
        sd.recreateInfo = createInfo;
    }

    void loadComputePipeline()
    {
        enforce(shaderModules.length == 1, "compute pipelines must contain exactly one shader");

        VkComputePipelineCreateInfo createInfo = {
            stage: shaderStageCreateInfos[0],
            layout: sd.pipelineLayout.vkHandle
        };
        
        sd.pipeline.createCompute(renderer, createInfo);
        sd.recreateInfo = createInfo;
    }

    ShaderData load(const(string) filename)
    {
        string text = readText(filename);
        json = parseJSON(text);

        string[] shaderFiles;
        foreach (shaderFile; json["shaders"].array) {
            shaderFiles ~= shaderFile.str;
        }
        sd.initialize(renderer);
        sd.shader = loadShaderSPV(shaderFiles);
        sd.createDescriptorSetLayout();
        sd.createPipelineLayout();
        collectShaderModules();

        switch (json["type"].str)
        {
            case "graphics":
                loadGraphicsPipeline();
                break;
            case "ray_tracing":
                loadRaytracingPipeline();
                break;
            case "compute":
                loadComputePipeline();
                break;
            default:
                throw new Exception("unknown pipeline type: " ~ json["type"].str);
        }

        return sd;
    }
}

ShaderData loadShaderPipeline(Renderer renderer, const(string) filename)
{
    ShaderPipelineLoader loader;
    loader.renderer = renderer;
    return loader.load(filename);
}

// utility functions
VkPrimitiveTopology topologyFromJson(const(string) json)
{
    immutable(VkPrimitiveTopology[string]) VALUES = [
        "point_list" : VK_PRIMITIVE_TOPOLOGY_POINT_LIST,
        "line_list" : VK_PRIMITIVE_TOPOLOGY_LINE_LIST,
        "line_strip" : VK_PRIMITIVE_TOPOLOGY_LINE_STRIP,
        "triangle_list" : VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        "triangle_strip" : VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP,
        "triangle_fan" : VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN,
        "line_list_with_adjacency" : VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY,
        "line_strip_with_adjacency" : VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY,
        "triangle_list_with_adjacency" : VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY,
        "triangle_strip_with_adjacency" : VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY,
        "patch_list" : VK_PRIMITIVE_TOPOLOGY_PATCH_LIST
    ];
    
    return VALUES.get(json, VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST);
}
