module ssvk.vulkan.util;

import core.stdc.string : memcpy, strlen;
import std.conv;
import std.exception;
import std.experimental.logger;
import std.typecons;
import std.string;

import erupted;
import gfm.math;

import ssvk.core.types;
import ssvk.resource.bitmap;
import ssvk.vulkan;
import ssvk.vulkan.cmd.synchronization;

enum
{
	POSTPROCESS_BINDING_INPUT_IMAGE      = 2,
	POSTPROCESS_BINDING_OUTPUT_IMAGE     = 3,

	DRAWPASS_BINDING_MESHDATA_BUFFER     = 4,
	DRAWPASS_BINDING_VERTEXDATA_BUFFER   = 5,
	DRAWPASS_BINDING_MATERIALDATA_BUFFER = 6,
	DRAWPASS_BINDING_MATERIAL_TEXTURES   = 10,
}

struct QueueFamilyIndices
{
	Nullable!uint32_t graphicsFamily;
	Nullable!uint32_t presentFamily;
	Nullable!uint32_t computeFamily;

	bool isComplete() const {
		return !graphicsFamily.isNull() && !presentFamily.isNull() && !computeFamily.isNull();
	}
}

public VkResult enforceVK(VkResult[] results = [VkResult.VK_SUCCESS])(VkResult res)
{
	static foreach (result; results) {
		if (res == result) {
			return res;
		}
	}
	throw new Exception(res.to!string, __FILE__, __LINE__);
}

public QueueFamilyIndices findQueueFamilies(PhysicalDevice physicalDevice, Surface surface)
{
	QueueFamilyIndices qfi;

	// query queue families
	VkQueueFamilyProperties[] queueFamilies = physicalDevice.getQueueFamilies();

	foreach (i, queueFamily; queueFamilies)
	{
		uint32_t queueFamilyIndex = cast(uint32_t) i;
		immutable(bool) presentSupport = physicalDevice.getSurfaceSupport(queueFamilyIndex, surface);

		if (qfi.graphicsFamily.isNull() && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
			qfi.graphicsFamily = queueFamilyIndex;
		}
		if (qfi.computeFamily.isNull() && queueFamily.queueFlags & VK_QUEUE_COMPUTE_BIT) {
			qfi.computeFamily = queueFamilyIndex;
		}
		if (qfi.presentFamily.isNull() && presentSupport) {
			qfi.presentFamily = queueFamilyIndex;
		}
	}

	debug {
		info("Selected graphics queue family: ", qfi.graphicsFamily);
		info("Selected compute queue family: ", qfi.computeFamily);
		info("Selected present queue family: ", qfi.presentFamily);
	}

	return qfi;
}

public bool checkExtension(VkExtensionProperties[] extensions, string extensionName)
{
	foreach (VkExtensionProperties extension; extensions)
	{
		if (strip(extension.extensionName, "\0") == extensionName) {
			return true;
		}
	}
	return false;
}

public bool checkLayer(VkLayerProperties[] layers, string layerName)
{
	foreach (VkLayerProperties layer; layers)
	{
		if (strip(layer.layerName, "\0") == layerName) {
			return true;
		}
	}
	return false;
}

public void dumpExtensions(VkExtensionProperties[] extensions)
{
	foreach (VkExtensionProperties extension; extensions)
	{
		info(extension.extensionName[0..strlen(extension.extensionName.ptr)]);
	}
}
public void dumpLayers(VkLayerProperties[] layers)
{
	foreach (VkLayerProperties layer; layers)
	{
		info(layer.layerName[0..strlen(layer.layerName.ptr)]);
	}
}

bool hasStencilComponent(VkFormat format)
{
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

VkVertexInputBindingDescription getVertexBindingDescription()
{
	VkVertexInputBindingDescription bindingDescription = {
		binding: 0,
		stride: Vertex.sizeof,
		inputRate: VK_VERTEX_INPUT_RATE_VERTEX
	};

	return bindingDescription;
}

void allocateMemory(Renderer renderer, ref const(VkMemoryRequirements) memoryRequirements,
		VkMemoryPropertyFlags properties, ref DeviceMemory memory)
{
	uint32_t memoryTypeIndex = renderer.device.physicalDevice.findMemoryType(memoryRequirements,
			properties);
	VkMemoryAllocateInfo allocInfo = {
		allocationSize: memoryRequirements.size,
		memoryTypeIndex: memoryTypeIndex
	};
	memory.allocate(renderer, allocInfo);
}

void createAndFillBuffer(T)(Renderer renderer, const(T[]) data, VkBufferUsageFlags usage, ref Buffer targetBuffer,
		ref DeviceMemory targetMemory)
{
	createAndFillBuffer(renderer, data.ptr, data[0].sizeof * data.length, usage, targetBuffer, targetMemory);
}

void createAndFillBuffer(Renderer renderer, const(void*) data, VkDeviceSize bufferSize, VkBufferUsageFlags usage,
		ref Buffer targetBuffer, ref DeviceMemory targetMemory)
{
	// first create a staging buffer for copying the data
	Buffer stagingBuffer;
	DeviceMemory stagingBufferMemory;
	createBuffer(renderer, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			stagingBuffer, stagingBufferMemory);
	
	// fill it
	void* bufferData = stagingBufferMemory.map(0, bufferSize);
	memcpy(bufferData, data, bufferSize);
	stagingBufferMemory.unmap();

	// create the actual vertex buffer
	createBuffer(renderer, bufferSize, usage | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, targetBuffer, targetMemory);

	stagingBuffer.copyTo(targetBuffer, bufferSize);

	stagingBuffer.cleanup();
	stagingBufferMemory.free();
}

void createBuffer(Renderer renderer, VkDeviceSize bufferSize, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
		ref Buffer buffer, ref DeviceMemory memory)
{
	VkBufferCreateInfo createInfo = {
		size: bufferSize,
		usage: usage,
		sharingMode: VK_SHARING_MODE_EXCLUSIVE
	};
	buffer.create(renderer, createInfo);

	// allocate memory
	VkMemoryRequirements memoryRequirements = buffer.getMemoryRequirements();
	allocateMemory(renderer, memoryRequirements, properties, memory);
	buffer.bindMemory(memory);
}

void createImage(Renderer renderer, Bitmap bmp, VkImageTiling tiling, VkImageUsageFlags usage,
		VkMemoryPropertyFlags memProperties, ref Image image, ref DeviceMemory imageMemory)
{
	immutable(VkFormat) format = VK_FORMAT_R8G8B8A8_UNORM; // TODO: retrieve from bmp parameter
	// create a staging buffer
	auto pixels = bmp.getPixels();
	VkDeviceSize bufferSize = pixels.length * pixels[0].sizeof;
	Buffer stagingBuffer;
	DeviceMemory stagingBufferMemory;
	createBuffer(renderer, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
			VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
			stagingBuffer, stagingBufferMemory);
	
	// fill it
	void* bufferData = stagingBufferMemory.map(0, bufferSize);
	memcpy(bufferData, pixels.ptr, bufferSize);
	stagingBufferMemory.unmap();

	// create the image object
	createImage(renderer, bmp.extent.width, bmp.extent.height, format, tiling, usage, memProperties, image,
			imageMemory);
	
	// copy buffer data to image
	image.transitionLayout(format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
	stagingBuffer.copyToImage(image, bmp.extent.width, bmp.extent.height);
	image.transitionLayout(format, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

	// clear staging buffer & memory
	stagingBuffer.cleanup();
	stagingBufferMemory.free();
}

void createImage(Renderer renderer, uint width, uint height, VkFormat format, VkImageTiling tiling,
		VkImageUsageFlags usage, VkMemoryPropertyFlags memProperties, ref Image image,
		ref DeviceMemory imageMemory)
{
	// create the image object
	VkImageCreateInfo imageCreateInfo = {
		imageType: VK_IMAGE_TYPE_2D,
		extent: {
			width: width,
			height: height,
			depth: 1
		},
		mipLevels: 1,
		arrayLayers: 1,
		format: format,
		tiling: tiling,
		initialLayout: VK_IMAGE_LAYOUT_UNDEFINED,
		usage: usage,
		sharingMode: VK_SHARING_MODE_EXCLUSIVE,
		samples: VK_SAMPLE_COUNT_1_BIT,
		flags: 0
	};
	image.create(renderer, imageCreateInfo);

	// allocate memory
	VkMemoryRequirements memoryRequirements = image.getMemoryRequirements();
	uint32_t memoryTypeIndex = renderer.device.physicalDevice.findMemoryType(memoryRequirements,
			memProperties);
	VkMemoryAllocateInfo allocInfo = {
		allocationSize: memoryRequirements.size,
		memoryTypeIndex: memoryTypeIndex
	};
	imageMemory.allocate(renderer, allocInfo);
	image.bindMemory(imageMemory);
}

void createImageView(Renderer renderer, ref Image image, ref ImageView imageView, VkFormat format,
		VkImageAspectFlags aspect = VK_IMAGE_ASPECT_COLOR_BIT)
{
	VkImageViewCreateInfo createInfo = {
		image: image.vkHandle,
		viewType: VK_IMAGE_VIEW_TYPE_2D,
		format: format,
		subresourceRange: {
			aspectMask: aspect,
			baseMipLevel: 0,
			levelCount: 1,
			baseArrayLayer: 0,
			layerCount: 1
		}
	};
	imageView.create(renderer, createInfo);
}

void createTextureSampler(Renderer renderer, ref Sampler texSampler)
{
	VkSamplerCreateInfo createInfo = {
		magFilter: VK_FILTER_LINEAR,
		minFilter: VK_FILTER_LINEAR,
		addressModeU: VK_SAMPLER_ADDRESS_MODE_REPEAT,
		addressModeV: VK_SAMPLER_ADDRESS_MODE_REPEAT,
		addressModeW: VK_SAMPLER_ADDRESS_MODE_REPEAT,
		anisotropyEnable: VK_TRUE,
		maxAnisotropy: 16,
		borderColor: VK_BORDER_COLOR_INT_OPAQUE_BLACK,
		unnormalizedCoordinates: VK_FALSE,
		compareEnable: VK_FALSE,
		compareOp: VK_COMPARE_OP_ALWAYS,
		mipmapMode: VK_SAMPLER_MIPMAP_MODE_LINEAR,
		mipLodBias: 0.0,
		minLod: 0.0,
		maxLod: 0.0
	};

	texSampler.create(renderer, createInfo);
}

void memoryBarrier(CommandBuffer commandBuffer, Image image, VkImageLayout oldLayout,
		VkImageLayout newLayout, VkPipelineStageFlags srcStage, VkAccessFlags srcAccess,
		VkPipelineStageFlags dstStage, VkAccessFlags dstAccess)
{
	VkImageMemoryBarrier memoryBarrier = {
		oldLayout: oldLayout,
		newLayout: newLayout,
		srcQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED,
		dstQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED,
		image: image.vkHandle,
		subresourceRange: {
			aspectMask: VK_IMAGE_ASPECT_COLOR_BIT,
			baseMipLevel: 0,
			levelCount: 1,
			baseArrayLayer: 0,
			layerCount: 1
		},
		srcAccessMask: srcAccess,
		dstAccessMask: dstAccess
	};
	commandBuffer.cmdPipelineBarrier(srcStage, dstStage,
			0 /* dependencyFlags */, [], [], [memoryBarrier]);
}

void bufferMemoryBarrier(CommandBuffer commandBuffer, VkPipelineStageFlags srcStage, VkAccessFlags srcAccess,
		VkPipelineStageFlags dstStage, VkAccessFlags dstAccess, Buffer buffer, VkDeviceSize offset = 0,
		VkDeviceSize size = VK_WHOLE_SIZE)
{
	VkBufferMemoryBarrier memoryBarrier = {
		srcQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED,
		dstQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED,
		buffer: buffer.vkHandle,
		offset: offset,
		size: size,
		srcAccessMask: srcAccess,
		dstAccessMask: dstAccess
	};
	commandBuffer.cmdPipelineBarrier(srcStage, dstStage, 0, [], [memoryBarrier], []);
}

void createAccelerationStructure(Renderer renderer, VkAccelerationStructureTypeNV type, VkGeometryNV[] geometries,
		uint instances, ref AccelerationStructureNV accelerationStructure, ref DeviceMemory deviceMemory)
{
	// create acceleration structure
	VkAccelerationStructureCreateInfoNV createInfo = {
		compactedSize: 0,
		info: {
			type: type,
			flags: 0,
			instanceCount: instances,
			geometryCount: cast(uint32_t) geometries.length,
			pGeometries: geometries.ptr
		}
	};
	accelerationStructure.create(renderer, createInfo);

	// allocate memory
	VkAccelerationStructureMemoryRequirementsInfoNV memoryRequirementsInfo = {
		accelerationStructure: accelerationStructure.vkHandle,
		type: VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_OBJECT_NV
	};

	VkMemoryRequirements2 memoryRequirements = accelerationStructure.getMemoryRequirements(memoryRequirementsInfo);
	allocateMemory(renderer, memoryRequirements.memoryRequirements, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, deviceMemory);

	// bind memory
	VkBindAccelerationStructureMemoryInfoNV bindInfo = {
		accelerationStructure: accelerationStructure.vkHandle,
		memory: deviceMemory.vkHandle
	};
	accelerationStructure.bindMemory(bindInfo);
}

VkTransformMatrixNV convertTransformMatrix(mat4f transformMatrix)
{
	VkTransformMatrixNV result = {
		matrix: [
			transformMatrix.row(0).v,
			transformMatrix.row(1).v,
			transformMatrix.row(2).v
		]
	};

	return result;
}

VkDeviceSize getScratchBufferSize(AccelerationStructureNV accelerationStructure)
{
	VkAccelerationStructureMemoryRequirementsInfoNV memoryRequirementsInfo = {
		accelerationStructure: accelerationStructure.vkHandle,
		type: VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_BUILD_SCRATCH_NV
	};
	VkMemoryRequirements2 memoryRequirements = accelerationStructure.getMemoryRequirements(memoryRequirementsInfo);
	return memoryRequirements.memoryRequirements.size;
}
