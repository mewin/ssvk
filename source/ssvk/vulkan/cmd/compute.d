module ssvk.vulkan.cmd.compute;

import erupted;

import ssvk.vulkan;

pragma(inline, true);

void cmdDispatch(CommandBuffer commandBuffer, uint groupCountX, uint groupCountY, uint groupCountZ)
{
    vkCmdDispatch(commandBuffer.vkHandle, groupCountX, groupCountY, groupCountZ);
}
