module ssvk.vulkan.cmd.synchronization;

import erupted;

import ssvk.vulkan;

void cmdPipelineBarrier(CommandBuffer commandBuffer, VkPipelineStageFlags srcStageMask,
        VkPipelineStageFlags dstStageMask, VkDependencyFlags dependencyFlags,
        VkMemoryBarrier[] memoryBarriers, VkBufferMemoryBarrier[] bufferMemoryBarriers,
        VkImageMemoryBarrier[] imageMemoryBarriers)
{
    vkCmdPipelineBarrier(commandBuffer.vkHandle, srcStageMask, dstStageMask, dependencyFlags,
            cast(uint32_t) memoryBarriers.length, memoryBarriers.ptr, cast(uint32_t) bufferMemoryBarriers.length,
            bufferMemoryBarriers.ptr, cast(uint32_t) imageMemoryBarriers.length, imageMemoryBarriers.ptr);
}
