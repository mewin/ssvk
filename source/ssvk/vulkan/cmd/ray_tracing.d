module ssvk.vulkan.cmd.ray_tracing;

import std.typecons;

import erupted;

import ssvk.vulkan;

pragma(inline, true):

void cmdBuildAccelerationStructureNV(CommandBuffer commandBuffer, ref const(VkAccelerationStructureInfoNV) asInfo,
        Nullable!Buffer instanceData, VkDeviceSize instanceOffset, bool update, AccelerationStructureNV dst,
        Nullable!AccelerationStructureNV src, Buffer scratchBuffer, VkDeviceSize scratchBufferOffset = 0)
{
    vkCmdBuildAccelerationStructureNV(commandBuffer.vkHandle, &asInfo,
            (instanceData.isNull() ? VK_NULL_HANDLE : instanceData.get().vkHandle), instanceOffset, update,
            dst.vkHandle, (src.isNull() ? VK_NULL_HANDLE : src.get().vkHandle), scratchBuffer.vkHandle,
            scratchBufferOffset);
}

void cmdTraceRaysNV(CommandBuffer commandBuffer, Buffer raygenShaderBindingTableBuffer,
        VkDeviceSize raygenShaderBindingOffset, Buffer missShaderBindingTableBuffer,
        VkDeviceSize missShaderBindingOffset, VkDeviceSize missShaderBindingStride,
        Buffer hitShaderBindingTableBuffer, VkDeviceSize hitShaderBindingOffset,
        VkDeviceSize hitShaderBindingStride, Buffer callableShaderBindingTableBuffer,
        VkDeviceSize callableShaderBindingOffset, VkDeviceSize callableShaderBindingStride,
        uint32_t width, uint32_t height, uint32_t depth)
{
    vkCmdTraceRaysNV(commandBuffer.vkHandle,
            raygenShaderBindingTableBuffer.vkHandle, raygenShaderBindingOffset,
            missShaderBindingTableBuffer.vkHandle, missShaderBindingOffset, missShaderBindingStride,
            hitShaderBindingTableBuffer.vkHandle, hitShaderBindingOffset, hitShaderBindingStride,
            callableShaderBindingTableBuffer.vkHandle, callableShaderBindingOffset, callableShaderBindingStride,
            width, height, depth);
}
