module ssvk.vulkan.cmd.render;

import std.algorithm;
import std.array;

import erupted;

import ssvk.vulkan;

pragma(inline, true);

void cmdBeginRenderPass(CommandBuffer commandBuffer, VkRenderPassBeginInfo info,
        VkSubpassContents subpassContents = VK_SUBPASS_CONTENTS_INLINE)
{
    vkCmdBeginRenderPass(commandBuffer.vkHandle, &info, subpassContents);
}

void cmdNextSubpass(CommandBuffer commandBuffer, VkSubpassContents subpassContents = VK_SUBPASS_CONTENTS_INLINE)
{
    vkCmdNextSubpass(commandBuffer.vkHandle, subpassContents);
}

void cmdEndRenderPass(CommandBuffer commandBuffer)
{
    vkCmdEndRenderPass(commandBuffer.vkHandle);
}

void cmdBindDescriptorSets(CommandBuffer commandBuffer, VkPipelineBindPoint bindPoint, PipelineLayout pipelineLayout,
        uint32_t firstSet, VkDescriptorSet[] descriptorSets, uint32_t[] dynamicOffset)
{
    vkCmdBindDescriptorSets(commandBuffer.vkHandle, bindPoint, pipelineLayout.vkHandle, firstSet,
            cast(uint32_t) descriptorSets.length, descriptorSets.ptr,
            cast(uint32_t) dynamicOffset.length, dynamicOffset.ptr);
}

void cmdBindPipeline(CommandBuffer commandBuffer, VkPipelineBindPoint bindPoint, Pipeline pipeline)
{
    vkCmdBindPipeline(commandBuffer.vkHandle, bindPoint, pipeline.vkHandle);
}

void cmdBindIndexBuffer(CommandBuffer commandBuffer, Buffer buffer, VkDeviceSize offset, VkIndexType indexType)
{
    vkCmdBindIndexBuffer(commandBuffer.vkHandle, buffer.vkHandle, offset, indexType);
}

void cmdBindVertexBuffer(CommandBuffer commandBuffer, uint32_t firstBinding, VkBuffer[] buffers, VkDeviceSize[] offsets)
in(buffers.length == offsets.length)
{
    vkCmdBindVertexBuffers(commandBuffer.vkHandle, firstBinding, cast(uint32_t) buffers.length,
            buffers.ptr, offsets.ptr);
}

void cmdDraw(CommandBuffer commandBuffer, uint32_t vertexCount, uint32_t instanceCount = 1, uint32_t firstVertex = 0,
        uint32_t firstInstance = 0)
{
    vkCmdDraw(commandBuffer.vkHandle, vertexCount, instanceCount, firstVertex, firstInstance);
}

void cmdDrawIndexed(CommandBuffer commandBuffer, uint32_t indexCount, uint32_t instanceCount = 1,
        uint32_t firstIndex = 0, uint32_t vertexOffset = 0, uint32_t firstInstance = 0)
{
    vkCmdDrawIndexed(commandBuffer.vkHandle, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);
}

void cmdPushConstants(CommandBuffer commandBuffer, PipelineLayout pipelineLayout, VkShaderStageFlags stageFlags,
        uint32_t offset, uint32_t size, const(void)* values)
{
    vkCmdPushConstants(commandBuffer.vkHandle, pipelineLayout.vkHandle, stageFlags, offset, size, values);
}
