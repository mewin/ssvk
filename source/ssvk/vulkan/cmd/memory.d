module ssvk.vulkan.cmd.memory;

import erupted;

import ssvk.vulkan;

pragma(inline, true);

void cmdCopyBuffer(CommandBuffer commandBuffer, Buffer srcBuffer, Buffer dstBuffer, VkBufferCopy[] regions)
{
    vkCmdCopyBuffer(commandBuffer.vkHandle, srcBuffer.vkHandle, dstBuffer.vkHandle, cast(uint32_t) regions.length,
            regions.ptr);
}

void cmdCopyBufferToImage(CommandBuffer commandBuffer, Buffer srcBuffer, Image dstImage, VkImageLayout dstImageLayout,
        VkBufferImageCopy[] regions)
{
    vkCmdCopyBufferToImage(commandBuffer.vkHandle, srcBuffer.vkHandle, dstImage.vkHandle, dstImageLayout,
            cast(uint32_t) regions.length, regions.ptr);
}
