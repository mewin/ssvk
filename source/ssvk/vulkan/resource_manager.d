module ssvk.vulkan.resource_manager;

import erupted;

import ssvk.resource;
import ssvk.scene.point_light3d;
import ssvk.vulkan;
import ssvk.vulkan.resource;

struct ResourceMap(TResource, TVulkan)
{
    private Renderer renderer;
    private TVulkan[ulong] resources;

    public void create(Renderer renderer)
    {
        this.renderer = renderer;
    }

    public void cleanup()
    {
        foreach (ref data; resources)
        {
            data.cleanup();
        }
        resources.clear();
    }

    public TVulkan* opIndex(TResource resource)
    {
        if (resource.id !in resources) {
            createResource(resource);
        }
        return &resources[resource.id];
    }

    @property
    public inout(TVulkan[]) values() inout
    {
        return resources.values;
    }

    private void createResource(TResource resource)
    {
        TVulkan data;
        data.create(renderer, resource);
        resources[resource.id] = data;
    }
}

struct ResourceManager
{
    private Renderer renderer;
    private ResourceMap!(Mesh, MeshData) meshData;
    private ResourceMap!(Material, MaterialData) materialData;
    private ResourceMap!(Bitmap, TextureData) textureData;
    private ResourceMap!(PointLight3D, PointLightData) pointLightData;

    public void create(Renderer renderer)
    {
        this.renderer = renderer;

        meshData.create(renderer);
        materialData.create(renderer);
        textureData.create(renderer);
        pointLightData.create(renderer);
    }

    public MaterialData* getMaterialData(Material material)
    {
        return materialData[material];
    }

    public MeshData* getMeshData(Mesh mesh)
    {
        return meshData[mesh];
    }

    public TextureData* getTextureData(Bitmap bitmap)
    {
        return textureData[bitmap];
    }

    public PointLightData* getPointLightData(PointLight3D pointLight)
    {
        return pointLightData[pointLight];
    }

    public void cleanup()
    {
        meshData.cleanup();
        // materialData.cleanup();
        textureData.cleanup();
    }

    public TextureData[] getAllTextures()
    {
        return textureData.values;
    }
}
