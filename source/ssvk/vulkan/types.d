module ssvk.vulkan.types;

import std.typecons;

import erupted;
import gfm.math;

import ssvk.vulkan;

enum DebugRenderMode : uint
{
    NONE           = 0,
    ALBEDO         = 1,
    ROUGHNESS      = 2,
    METALLIC       = 3,
    UV0            = 4,
    UV1            = 5,
    BARYCENTRICS   = 6,
    MATERIAL_ID    = 7,
    INSTANCE_ID    = 8,
    PRIMITIVE_ID   = 9,
    NORMAL_MAP     = 10,
    DEPTH          = 11
}

enum DebugRenderFlagBits : uint32_t
{
    SKIP_NORMAL_MAP = (1 << 0),
    PPROCESS_SKIP   = (1 << 1),
    PPROCESS_SPLIT  = (1 << 2)
}
alias DebugRenderFlags = BitFlags!DebugRenderFlagBits;

struct PointLight
{
    enum COUNT = 8;

    vec3f color;
    float ambient;
    vec3f position;
    float range;
    float attenuation;
    float[3] __padding;
}

struct UBOGlobalState
{
    mat4f view;
    mat4f projection;
    PointLight[PointLight.COUNT] pointLights;
    vec3f eyePos;
    float time;
    DebugRenderMode debugRenderMode;
    DebugRenderFlags debugRenderFlags;
    uint environmentMapTextureIndex;
    float __padding;

    static assert(UBOGlobalState.sizeof <= 16_384); // min value for maxUniformBufferRange according to Vulkan specification
}

struct CameraState
{
    mat4f transform;
    // float fov;
    // float aspect;
    // float[2] __padding3;

    static assert(CameraState.sizeof % 4 == 0);
    static assert(CameraState.sizeof <= 128); // min value for maxPushConstantsSize according to Vulkan specification
}

struct MeshInfo
{
    uint32_t offset;
    uint32_t materialID;
    uint32_t[2] __padding;

    static assert(MeshInfo.sizeof % 16 == 0);
}

struct MaterialInfo
{
    uint32_t albedoTextureIndex;
    uint32_t normalTextureIndex;
    uint32_t roughnessMetallicTextureIndex;
    uint32_t ambientOcclusionTextureIndex;
    vec4f    albedoFactor;
    float    metallicFactor;
    float    roughnessFactor;
    float    refractiveIndex;
    float    __padding;
}
