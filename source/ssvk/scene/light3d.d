module ssvk.scene.light3d;

import ssvk.core.types.color;
import ssvk.scene.node3d;

class Light3D : Node3D
{
    mixin(implementBaseObject());
    
    public Color color = Color.WHITE;

    // public float energy = 1.0;
    // public float specular = 1.0;
}
