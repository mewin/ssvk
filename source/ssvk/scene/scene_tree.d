module ssvk.scene.scene_tree;

import std.exception;
import std.experimental.logger;
import std.range;

import ssvk.core.base_object;

class SceneTree : BaseObject
{
    mixin(implementBaseObject());

    private SceneNode rootNode_;

    public this()
    {
        rootNode_ = new SceneNode();
    }

    @property
    public inout(SceneNode) rootNode() inout
    {
        return rootNode_;
    }
}


class SceneNode : BaseObject
{
    mixin(implementBaseObject());
    
    private SceneNode parent_;
    private SceneNode[] children_;
    private string name_; // TODO: check for uniqueness on renaming using a setter (?)

    public this()
    {
    }

    @property
    public string name() const
    {
        if (name_.length > 0) {
            return name_;
        }
        return className;
    }

    @property
    public void name(string value)
    {
        name_ = value;
    }

    @property
    public inout(SceneNode) parent() inout
    {
        return parent_;
    }

    @property
    public inout(SceneNode[]) children() inout
    {
        return children_;
    }

    public void addChild(SceneNode child)
    in {
        assert(!(child is null));
    }
    body
    {
        if (child.parent == this) {
            return;
        }
        enforce(child.parent is null, "node already has a parent");
        child.parent_ = this;
        children_ ~= child;
    }

    public inout(T) firstNodeOfType(T)() inout
    {
        if (auto res = cast(T) this) {
            return res;
        }

        foreach (child; children_)
        {
            if (auto res = child.firstNodeOfType!(T)()) {
                return res;
            }
        }

        return null;
    }
    
    public inout(T)[] allNodesOfType(T)() inout
    {
        inout(T)[] result;
        if (auto res = cast(inout(T)) this) {
            result ~= res;
        }

        foreach (child; children_) {
            result ~= child.allNodesOfType!(T)();
        }

        return result;
    }

    public void prettyPrint() const
    {
        prettyPrintImpl(this, 0);
    }

    private void prettyPrintImpl(const(SceneNode) root, int indent) const
    {
        info(repeat(' ', indent), root.name);

        foreach (child; root.children)
        {
            prettyPrintImpl(child, indent + 2);
        }
    }
}
