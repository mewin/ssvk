module ssvk.scene.node3d;

import std.typecons;

import ssvk.core.types;
import ssvk.scene.scene_tree;

class Node3D : SceneNode
{
    mixin(implementBaseObject());
    
    public Transform3D transform;
    
    @property
    public Node3D parent3D() const
    {
        auto node = parent.rebindable;
        while (node !is null)
        {
            if (Node3D node3D = cast(Node3D) node) {
                return node3D;
            }
            node = node.parent;
        }
        return null;
    }

    /**
     * Direct Node3D children, skips non-3d nodes.
     *
     * Examples:
     * In this case:
     * Root
     *  +-- Non3D_A
     *  +-- Node3D_A
     *  +-- Node3D_B
     *  |    +-- Node3D_C
     *  +-- Non3D_B
     *       +-- Node3D_D
     * [Node3D_A, Node3D_B, Node3D_D] is returned.
     */
    @property
    public inout(Node3D)[] children3D() inout
    {
        inout(Node3D)[] result;
        collectChildren3D(this, result);
        return result;
    }

    @property
    public Transform3D globalTransform() const
    {
        auto p3d = parent3D;
        if (p3d !is null) {
            return transform ~ p3d.globalTransform;
        }
        return transform;
    }

    // TODO: setter for globalTransform

    /**
     * Returns the AABB containing any contained point of only this node in world space.
     */
    @property
    public AABB thisAABB() const
    {
        return AABB(transform.translation);
    }

    /**
     * Returns the AABB containing any contained point of this and all children in world space.
     */
    @property
    public final AABB aabb() const
    {
        AABB result = thisAABB;
        foreach (child; children3D)
        {
            result |= child.aabb;
        }
        return result;
    }

    private void collectChildren3D(inout(SceneNode) root, ref inout(Node3D)[] result) inout
    {
        foreach (child; root.children)
        {
            if (auto node3D = cast(inout(Node3D)) child)
            {
                result ~= node3D;
            }
            else
            {
                collectChildren3D(child, result);
            }
        }
    }
}
