module ssvk.scene.mesh_instance;

import gfm.math;

import ssvk.core.types;
import ssvk.resource.mesh;
import ssvk.scene.node3d;

class MeshInstance : Node3D
{
    mixin(implementBaseObject());
    
    public Mesh mesh;

    @property
    public override AABB thisAABB() const
    {
        if (mesh is null || mesh.vertices.length < 1) {
            return super.thisAABB;
        }
        mat4f transformMatrix = cast(mat4f) globalTransform;
        AABB result = AABB((transformMatrix * vec4f(mesh.vertices[0].position, 1.0)).xyz);

        foreach (vertex; mesh.vertices)
        {
            result |= (transformMatrix * vec4f(vertex.position, 1.0)).xyz;
        }

        return result;
    }
}
