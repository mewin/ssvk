module ssvk.scene.point_light3d;

import std.algorithm;

import ssvk.scene.light3d;

class PointLight3D : Light3D
{
    public float energy = 10.0;
    public float range = 40.0;
    public float attenuation_ = 0.5;

    @property
    public float attenuation() const
    {
        return attenuation_;
    }

    @property
    public void attenuation(float value)
    {
        assert(value >= 0.0 && value <= 1.0);
        attenuation_ = clamp(value, 0.0, 1.0);
    }
}
