module ssvk.core.plugin;

import ssvk.core.application;
import ssvk.core.base_object;

class Plugin : BaseObject
{
    mixin(implementBaseObject());
    
    public void initialize(Application application) {}
    public void cleanup() {}
    public void update(float delta) {}
}
