module ssvk.core.base_object;

import std.array;
import std.conv;
import std.exception;
import std.experimental.logger;
import std.functional;
import std.string;
import std.traits;
import std.variant;

import ssvk.core.utility.traits;
import ssvk.reflection.binding;
import ssvk.reflection.hints;

alias ObjectID = ulong;

private Variant[string] structToVariant(T)(T struc)
{
    Variant[string] unpacked;

    static foreach (member; Properties!T)
    {
        static if (__traits(getProtection, mixin("T." ~ member)) == "public" && !hasStaticMember!(T, member))
        {
            static if (is(typeof(mixin("struc." ~ member)) == struct)) {
                unpacked[member] = structToVariant(mixin("struc." ~ member));
            }
            else static if (__traits(isPOD, typeof(mixin("struc." ~ member)))) {
                unpacked[member] = Variant(mixin("struc." ~ member));
            }
        }
    }

    return unpacked;
}

private Variant[] variantArrayUnpackStructs(T...)(T args)
{
    Variant[] variants;

    static foreach (arg; args) {
        static if (is(typeof(arg) == struct)) {
            variants ~= Variant(structToVariant(arg));
        }
        else {
            variants ~= Variant(arg);
        }
    }

    return variants;
}

private string unwrapParameters(T...)()
{
    string[] parts;

    foreach (i, E; T) {
        parts ~= "values[" ~ to!string(i) ~ "].get!(" ~ fullyQualifiedName!(E) ~ ")";
    }

    return join(parts, ",");
}

private bool checkParamTypes(T...)(Variant[] values)
{
    foreach (i, E; T) {
        if (!values[i].convertsTo!(E)) {
            trace(i);
            return false;
        }
    }

    return true;
}

private string formatParamTypes(Variant[] values)
{
    string[] types;

    foreach (value; values) {
        types ~= value.type().toString();
    }

    return types.join(", ");
}

struct ParameterList
{
    Variant[] values;

    public void unwrap(D)(auto ref D fn)
    {
        static if (Parameters!fn.length > 0 && isImplicitlyConvertible!(ParameterList, Parameters!fn[0]))
        {
            fn(this);
        }
        else static if (Parameters!fn.length > 0 && isImplicitlyConvertible!(Variant[], Parameters!fn[0]))
        {
            fn(values);
        }
        else
        {
            if (values.length < Parameters!fn.length)
            {
                warning("Not calling wrapped function, not enough argument.");
                return;
            }
            if (!checkParamTypes!(Parameters!fn)(values))
            {
                warning("Not calling wrapped function, invalid parameter types.");
                warning("Expected: ", Parameters!fn.stringof);
                warning("Got: ", formatParamTypes(values));
                return;
            }
            mixin("fn(", unwrapParameters!(Parameters!fn)(), ");");
        }
    }
}

private struct RegisteredSignalHandler
{
    void delegate(ref ParameterList) wrappedFunction;
    bool fromReflection = false; // converting structs to Variant[string] must happen while the type is known => before wrapping it in a Variant
}

private ObjectID nextObjectID = 1; // TODO: make this shared/thread safe
private BaseObject[ObjectID] objectMap;

class BaseObject
{
    private RegisteredSignalHandler[][string] registeredSignalHandlers;
    private ObjectID id_;

    public this()
    {
        assert(!__ctfe, "BaseObjects must be created at runtime");
        
        id_ = nextObjectID;
        ++nextObjectID;

        objectMap[id_] = this;
    }

    public ~this()
    {
        objectMap.remove(id_);
    }

    @property
    public ObjectID id() const
    {
        return id_;
    }

    @property
    public string className() const
    {
        return this.classinfo.name[this.classinfo.name.lastIndexOf(".") + 1 .. $];
    }

    // TODO: owner parameter to auto-manage lifetime
    @(ReflectionHintTemplateTypes!(Variant[] delegate(Variant[]), true))
    public void connect(D, bool fromReflection = false)(string signal, D handler /*, BaseObject owner = null */)
    {
        auto asDelegate = toDelegate(handler);

        auto wrapper = (ref ParameterList parameters)
        {
            parameters.unwrap(asDelegate);
        };

        RegisteredSignalHandler rsh = {
            wrappedFunction: wrapper,
            fromReflection: fromReflection
        };
        registeredSignalHandlers.require(signal) ~= rsh;
    }

    public void emitSignal(T...)(string signal, T args)
    {
        if (signal !in registeredSignalHandlers) {
            return;
        }

        bool hasReflectionHandler = false;
        ParameterList params;
        params.values = variantArray(args);

        foreach (handler; registeredSignalHandlers[signal])
        {
            if (handler.fromReflection) {
                hasReflectionHandler = true;
            }
            else {
                handler.wrappedFunction(params);
            }
        }

        if (!hasReflectionHandler) {
            return;
        }

        params.values = variantArrayUnpackStructs(args);

        foreach (handler; registeredSignalHandlers[signal])
        {
            if (handler.fromReflection) {
                handler.wrappedFunction(params);
            }
        }
    }

    public BaseClass getClass() const
    {
        return wrapClass(this);
    }

    protected static string implementBaseObject()
    {
        return "
        public override ssvk.reflection.binding.BaseClass getClass() const
        {
            import ssvk.reflection.binding : wrapClass;
            return wrapClass(this);
        }
        ";
    }

    // static stuff
    public static BaseObject byID(ObjectID id)
    {
        return objectMap[id];
    }
}
