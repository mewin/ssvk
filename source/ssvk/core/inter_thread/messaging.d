module ssvk.core.inter_thread.messaging;

import std.concurrency;

import ssvk.core.base_object;

__gshared Tid mainThreadId;
__gshared ObjectID applicationId;

alias MessageID = ulong;

enum {
    ANY_MESSAGE_ID = 0
}

MessageID lastMessageID = 0;

shared class MessageException : Exception
{
    public const(MessageID) messageID;

    public this(MessageID messageID_, string file = __FILE__, size_t line = __LINE__)
    {
        import std.conv : to;
        
        super("Failed to handle message " ~ messageID_.to!string(), file, line);
        messageID = messageID_;
    }
}

mixin template MessageBase()
{
    MessageID messageID;
    Tid senderID;
}

struct MessageGetObjectProperty
{
    mixin MessageBase;

    ObjectID objectID;
    string propertyName;
    Variant value;
}

struct MessageSetObjectProperty
{
    mixin MessageBase;
    
    ObjectID objectID;
    string propertyName;
    Variant value;
}

struct MessageDone
{
    mixin MessageBase;
}

auto selectiveReceive(T)(MessageID id = ANY_MESSAGE_ID)
{
    T result;
    bool repeat = true;
    while (repeat)
    {
        receive(
            (T msg)
            {
                if (id == ANY_MESSAGE_ID || msg.messageID == id)
                {
                    result = msg;
                    repeat = false;
                }
                else {
                    send(thisTid, msg);
                }
            },
            (Variant other) {
                send(thisTid, other);
            }
        );
    }
    return result;
}

MessageID genMessageID()
out(result)
{
    assert(result != ANY_MESSAGE_ID);
}
body
{
    return ++lastMessageID;
}
