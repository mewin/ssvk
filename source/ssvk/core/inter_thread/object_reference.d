module ssvk.core.inter_thread.object_reference;

import std.concurrency;
import std.traits;

import ssvk.core.application;
import ssvk.core.base_object;
import ssvk.core.inter_thread.messaging;
import ssvk.core.utility.traits;

private string generateProperty(T : BaseObject, string prop)()
{
    alias TProp = Unqual!(typeof(mixin("T." ~ prop)));
    alias TRoot = RootType!TProp;

    static if (is(TProp : BaseObject))
    {
        return "ObjectReferenceRef!(" ~ fullyQualifiedName!(TProp) ~ ") " ~ prop ~ ";";
    }
    else static if (isBuiltinType!TRoot || is(TRoot == string))
    {
        return TProp.stringof ~ " " ~ prop ~ ";";
    }
    else static if (isDynamicArray!TProp)
    {
        return "DynamicArrayRef!(" ~ fullyQualifiedName!(T) ~ ", \"" ~ prop ~ "\") " ~ prop ~ ";";
    }
    else static if (__traits(isPOD, TRoot))
    {
        return ""; // fullyQualifiedName!(TProp) ~ " " ~ prop ~ ";";
    }
    else {
        return "";
    }
}

private string generateReferenceMembers(T : BaseObject)()
{
    import std.string : join;
    string[] lines;

    static foreach (prop; Properties!T)
    {
        static if (prop != "id") {
            lines ~= generateProperty!(T, prop)();
        }
    }

    return lines.join("\n");
}

struct ObjectReferenceRef(T : BaseObject)
{
    public ObjectID id;
}

struct DynamicArrayRef(T : BaseObject, string member)
{
    alias TProp = Unqual!(typeof(mixin("T." ~ member)));

    public ObjectID id;
}

struct ObjectReference(T : BaseObject)
{
    public ObjectID id;

    mixin(generateReferenceMembers!T());

    this(ObjectID id)
    {
        this.id = id;
    }

    void queryProperties(string[] propertyNames)()
    {
        MessageID[string] messageIds;

        static foreach (propertyName; propertyNames)
        {
            messageIds[propertyName] = sendPropertyQuery!(propertyName)();
        }

        static foreach (propertyName; propertyNames)
        {
            waitForPropertyQuery!(propertyName)(messageIds[propertyName]);
        }
    }

    MessageID sendPropertyQuery(string propertyName)()
    {
        MessageGetObjectProperty message =
        {
            messageID: genMessageID(),
            senderID: thisTid,
            objectID: id,
            propertyName: propertyName
        };
        send(mainThreadId, message);
        return message.messageID;
    }

    void waitForPropertyQuery(string propertyName)(MessageID messageID)
    {
        alias TProp = typeof(mixin("this." ~ propertyName));
        auto response = selectiveReceive!(MessageGetObjectProperty)(messageID);
        
        static if (is(TProp : ObjectReferenceRef!E, E)) {
            mixin("this." ~ propertyName ~ ".id = response.value.get!(ObjectID)();");
        }
        else {
            mixin("this." ~ propertyName ~ " = response.value.get!(" ~ TProp.stringof ~ ")();");
        }
    }

    void sendProperties(string[] propertyNames)()
    {
        MessageID[string] messageIds;

        static foreach (propertyName; propertyNames)
        {
            messageIds[propertyName] = sendPropertyUpdate!(propertyName)();
        }

        static foreach (propertyName; propertyNames)
        {
            waitForPropertyUpdate!(propertyName)(messageIds[propertyName]);
        }
    }

    MessageID sendPropertyUpdate(string propertyName)()
    {
        MessageSetObjectProperty message =
        {
            messageID: genMessageID(),
            senderID: thisTid,
            objectID: id,
            propertyName: propertyName,
            value: Variant(mixin("this." ~ propertyName))
        };
        send(mainThreadId, message);
        return message.messageID;
    }

    void waitForPropertyUpdate(string propertyName)(MessageID messageID)
    {
        selectiveReceive!(MessageDone)(messageID);
    }
}

ObjectReference!Application getApplicationReference()
{
    return ObjectReference!Application(applicationId);
}
