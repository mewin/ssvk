module ssvk.core.types.color;

import gfm.math;

struct Color
{
    public float r = 1.0;
    public float g = 1.0;
    public float b = 1.0;
    public float a = 1.0;

    enum
    {
        WHITE   = Color(1.0, 1.0, 1.0),
        BLACK   = Color(0.0, 0.0, 0.0),
        RED     = Color(1.0, 0.0, 0.0),
        GREEN   = Color(0.0, 1.0, 0.0),
        BLUE    = Color(0.0, 0.0, 1.0),
        YELLOW  = Color(1.0, 1.0, 0.0),
        MAGENTA = Color(1.0, 0.0, 1.0),
        CYAN    = Color(0.0, 1.0, 1.0),
        GRAY    = Color(0.5, 0.5, 0.5),
        GREY    = GRAY
    }

    public vec4f opCast(U)() const if (is(U == vec4f))
    {
        return vec4f(r, g, b, a);
    }
}
