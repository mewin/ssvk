module ssvk.core.types.transform3d;

import std.math;

import gfm.math;

import ssvk.core.types.math;
import ssvk.test;

struct Transform3D
{
    public mat4f basis = mat4f.identity;

    @property
    public vec3f translation() const
    {
        return basis.column(3).xyz;
    }

    @property
    public void translation(vec3f value)
    {
        basis.c[0][3] = value.x;
        basis.c[1][3] = value.y;
        basis.c[2][3] = value.z;
    }

    @property
    public vec3f scale() const
    {
        return vec3f(
            basis.column(0).magnitude(),
            basis.column(1).magnitude(),
            basis.column(2).magnitude()
        );
    }

    @property
    public void scale(vec3f value)
    {
        vec3f oldScale = scale();
        for (int row = 0; row < 3; ++row) {
            for (int col = 0; col < 3; ++col) {
                basis.c[row][col] *= value[col] * oldScale[col];
            }
        }
    }

    public auto apply(T)(T vec) const if (__traits(compiles, mat4f.identity * vec))
    {
        return basis * vec;
    }

    public mat4f opCast(U)() const if (is(U == mat4f))
    {
        return basis;
    }

    Transform3D opBinary(string op, R)(const R other) const if (op == "~" && is(R == Transform3D))
    {
        return fromMatrix(other.basis * basis);
    }

    public Transform3D rotated(vec3f axis, angle_rad angle)
    {
        return Transform3D.fromRotation(axis, angle) ~ this;
    }

    static Transform3D fromMatrix(const(mat4f) matrix)
    {
        return Transform3D(matrix);
    }

    static Transform3D fromRotation(vec3f axis, angle_rad angle)
    {
        return fromMatrix(mat4f.rotation(angle, axis));
    }

    static Transform3D fromTranslation(vec3f translation)
    {
        Transform3D trans;
        trans.translation = translation;
        return trans;
    }

    static Transform3D fromScale(vec3f scale)
    {
        Transform3D trans;
        trans.scale = scale;
        return trans;
    }

    static Transform3D lookAt(vec3f eye, vec3f center, vec3f up)
    {
        return fromMatrix(mat4f.lookAt(eye, center, up).inverse());
    }
}

/// ---------------------------------------
///  Tests
/// ---------------------------------------
@("Transform3D.basic")
unittest
{
    Transform3D rot = Transform3D.fromRotation(vec3f(0.0, 1.0, 0.0), 0.5 * PI);
    Transform3D trans = Transform3D.fromTranslation(vec3f(1.0, 0.0, 0.0));
    Transform3D scale = Transform3D.fromScale(vec3f(1.0, 2.0, 3.0));

    assertValid(rot.basis);
    assertValid(trans.basis);
    assertValid(scale.basis);

    assertApprox(trans.translation, vec3f(1.0, 0.0, 0.0));
    assertApprox(scale.scale, vec3f(1.0, 2.0, 3.0));

    vec4f p0 = vec4f(0.0, 0.0, 0.0, 1.0);
    vec4f p1 = vec4f(1.0, 0.0, 0.0, 1.0);

    assertApprox(cast(mat4f) rot * p0, vec4f(0.0, 0.0, 0.0, 1.0));
    assertApprox(cast(mat4f) trans * p0, vec4f(1.0, 0.0, 0.0, 1.0));
    assertApprox(cast(mat4f) rot * p1, vec4f(0.0, 0.0, -1.0, 1.0));
    assertApprox(cast(mat4f) trans * p1, vec4f(2.0, 0.0, 0.0, 1.0));
}

@("Transform3D.concatenation")
unittest
{
    Transform3D scale100;
    Transform3D scale01;
    scale100.scale = vec3f(100.0);
    scale01.scale = vec3f(0.1);

    Transform3D scale10A = scale01 ~ scale100;
    Transform3D scale10B = scale100 ~ scale01;

    assertApprox(scale10A.scale, vec3f(10.0));
    assertApprox(scale10B.scale, vec3f(10.0));

    // combinations of rotation and translation
    Transform3D rot = Transform3D.fromRotation(vec3f(0.0, 1.0, 0.0), 0.5 * PI);
    Transform3D trans = Transform3D.fromTranslation(vec3f(1.0, 0.0, 0.0));

    vec4f p0 = vec4f(0.0, 0.0, 0.0, 1.0);
    vec4f p1 = vec4f(1.0, 0.0, 0.0, 1.0);

    Transform3D rotTrans = rot ~ trans;
    Transform3D transRot = trans ~ rot;

    assertApprox(cast(mat4f) rotTrans * p0, vec4f(1.0, 0.0, 0.0, 1.0));
    assertApprox(cast(mat4f) transRot * p0, vec4f(0.0, 0.0, -1.0, 1.0));
}

@("Transform3D.fromMatrix")
unittest
{
    immutable(vec3f) translation = [10.0, -10.0, 20.0];
    immutable(mat4f) transMatrix = mat4f.translation(translation);
    immutable(Transform3D) transT = Transform3D.fromMatrix(transMatrix);
    assertApprox(transT.translation, translation);

    immutable(vec3f) scale = [0.5, 0.3, 0.1];
    immutable(mat4f) scaleMatrix = mat4f.scaling(scale);
    immutable(Transform3D) transS = Transform3D.fromMatrix(scaleMatrix);
    assertApprox(transS.scale, scale);

    immutable(vec4f) point = [1.0, -1.0, 2.3, 1.0];
    immutable(quatf) rotation = quatf.fromEulerAngles(0.0, 0.5 * PI, 0.0);
    immutable(mat4f) rotMat = cast(mat4f) rotation;
    immutable(Transform3D) transR = Transform3D.fromMatrix(rotMat);

    assertApprox(transR.apply(point), rotMat * point);
    assertApprox(transR.basis, cast(mat4f) rotation);
}

@("Transform3D.lookAt")
unittest
{
    immutable(vec3f) EYE = [0.0, 20.0, 10.0];
    immutable(vec3f) TARGET = [0.0, 0.0, 0.0];
    immutable(vec3f) UP = [0.0, 1.0, 0.0];

    Transform3D lookAt = Transform3D.lookAt(EYE, TARGET, UP);
    assertApprox(lookAt.scale, vec3f(1.0));
    assertApprox(lookAt.translation, EYE);
    // assert(matricesEqual(cast(mat4f) lookAt, mat4f.lookAt(EYE, TARGET, UP).inverse()));
    assertApprox(cast(mat4f) lookAt, mat4f.lookAt(EYE, TARGET, UP).inverse());
}
