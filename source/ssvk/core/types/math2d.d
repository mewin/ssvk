module ssvk.core.types.math2d;

struct Extent2D(T = int)
{
    public T width;
    public T height;
}

struct Point2D(T = int)
{
    public T x;
    public T y;
}

struct Distance2D(T = int)
{
    public T x;
    public T y;
}

alias Extent2DI = Extent2D!int;
alias Extent2DUI = Extent2D!uint;
alias Extent2DF = Extent2D!float;
alias Extent2DD = Extent2D!double;

alias Point2DI = Point2D!int;
alias Point2DUI = Point2D!uint;
alias Point2DF = Point2D!float;
alias Point2DD = Point2D!double;

alias Distance2DI = Distance2D!int;
alias Distance2DF = Distance2D!float;
alias Distance2DD = Distance2D!double;
