module ssvk.core.types.math3d;

import std.algorithm;

import gfm.math;

struct Vertex
{
    public vec3f position;
    public float __pad;
    public vec4f color;
    public vec3f normal;
    public float __pad2;
    public vec3f tangent;
    public float __pad3;
    public vec3f bitangent;
    public float __pad4;
    public vec4f portalOutputPosition;
    public vec3f portalOutputNormal;
    public float __pad5;
    public vec3f portalOutputTangent;
    public float __pad6;
    public vec3f portalOutputBitangent;
    public float __pad7;
    public vec2f uv0;
    public vec2f uv1;

    static assert(Vertex.sizeof % 16 == 0);
}

struct AABB
{
    public vec3f pointMin = vec3f(0.0);
    public vec3f pointMax = vec3f(0.0);

    public this(vec3f point)
    {
        this(point, point);
    }

    public this(vec3f pointMin, vec3f pointMax)
    {
        this.pointMin = pointMin;
        this.pointMax = pointMax;
    }

    @property
    public vec3f extents() const
    {
        return pointMax - pointMin;
    }

    @property
    public float maxExtent() const
    {
        return max(
            pointMax.x - pointMin.x,
            pointMax.y - pointMin.y,
            pointMax.z - pointMin.z
        );
    }

    public AABB opBinary(string op)(const(AABB) rhs) const if (op == "|")
    {
        return AABB(
            pointMin.minByElem(rhs.pointMin),
            pointMax.maxByElem(rhs.pointMax)
        );
    }

    public auto opOpAssign(string op)(const(AABB) value) if (op == "|")
    {
        pointMin = pointMin.minByElem(value.pointMin);
        pointMax = pointMax.maxByElem(value.pointMax);
        return this;
    }

    public auto opOpAssign(string op)(const(vec3f) value) if (op == "|")
    {
        pointMin = pointMin.minByElem(value);
        pointMax = pointMax.maxByElem(value);
        return this;
    }

    invariant
    {
        assert(pointMin.x <= pointMax.x);
        assert(pointMin.y <= pointMax.y);
        assert(pointMin.z <= pointMax.z);
    }
}