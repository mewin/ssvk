module ssvk.core.utility.math;

import std.math;

import ssvk.core.types.math;

float normalizeAngle(angle_rad angle)
{
    float res = angle;
    while (res > PI) {
        res -= 2.0 * PI;
    }
    while (res < -PI) {
        res += 2.0 * PI;
    }
    return res;
}
