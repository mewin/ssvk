module ssvk.core.utility.traits;

import std.algorithm.searching;
import std.traits;

private immutable(string[]) FILTERED_MEMBERS = [
    "_ctor", "_dtor"
];

bool isProperty(T, string member)()
{

    static if (__traits(compiles, typeof(mixin("T." ~ member)))
        && !__traits(isTemplate, mixin("T." ~ member)))
    {
        alias TMember = Unqual!(typeof(mixin("T." ~ member)));
        static if (isFunction!(TMember)) {
            return [__traits(getFunctionAttributes, mixin("T." ~ member))[]].canFind("@property");
        }
        else
        {
            return true;
        }
    }
    else
    {
        return false;
    }
}

template RootType(T)
{
    static if (is(Unqual!T : E[], E)) {
        alias RootType = RootType!E;
    }
    else static if (is(Unqual!T : E[K], E, K)) {
        alias RootType = RootType!E;
    }
    else {
        alias RootType = T;
    }
}

template Properties(T)
{
    static if (is (T == class) || is (T == struct))
    {
        enum Properties = ()
        {
            string[] props;

            static foreach (member; __traits(allMembers, T))
            {
                static if (!FILTERED_MEMBERS.canFind(member)
                    && isProperty!(T, member))
                {
                    props ~= member;
                }
            }

            return props;
        }();
    }
}

bool canSet(T, string member)()
{
    T object;
    alias TMember = typeof(mixin("object." ~ member));
    TMember val;
    return __traits(compiles, mixin("object." ~ member) = val);
}

bool canGet(T, string member)()
{
    T object;
    alias TMember = typeof(mixin("object." ~ member));
    TMember val;
    return __traits(compiles, val = mixin("object." ~ member));
}
