module ssvk.core.utility.bitmap;

import std.algorithm;
import std.exception;

import ssvk.core.types.color;
import ssvk.resource.bitmap;

void fill(T = ubyte, int channelCount = 4)(BitmapView!(T, channelCount) view, Color color)
{
    foreach (ref pixel; view.pixels)
    {
        pixel = color;
    }
}

void fill(Bitmap bitmap, Color color)
{
    mixin(visitBitmap!("fill", "color"));
}

Bitmap singleColorBitmap(Color color)
{
    Bitmap bitmap = new Bitmap(1, 1);

    bitmap.fill(color);

    return bitmap;
}

void swapChannels(T = ubyte, int channelCount = 4)(BitmapView!(T, channelCount) view, int channel0, int channel1)
{
    enforce(channel0 < channelCount && channel1 < channelCount);
    foreach (ref pixel; view.pixels)
    {
        swapAt(pixel.values, channel0, channel1);
    }
}

void swapChannels(Bitmap bitmap, uint channel0, uint channel1)
{
    mixin(visitBitmap!("swapChannels", "channel0, channel1"));
}
