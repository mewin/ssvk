module ssvk.core.utility.functional;

import std.array;
import std.conv;
import std.traits;

string unpackVariantArray(TParams...)()
{
    string[] parts;

    static foreach (i, TParam; TParams)
    {
        parts ~= ("params[" ~ i.to!string ~ "].get!(" ~ fullyQualifiedName!TParam ~ ")()");
    }

    return parts.join(",");
}
