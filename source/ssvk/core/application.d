module ssvk.core.application;

import core.time;
import core.stdc.string : memcpy;
import std.algorithm;
import std.concurrency;
import std.experimental.logger;
import std.typecons;

import erupted;
import gfm.math;

import ssvk.core.base_object;
import ssvk.core.plugin;
import ssvk.core.utility.math;
import ssvk.core.inter_thread.messaging;
import ssvk.resource.bitmap;
import ssvk.window;
import ssvk.scene.camera3d;
import ssvk.resource.material;
import ssvk.resource.mesh;
import ssvk.resource.shader;
import ssvk.scene.mesh_instance;
import ssvk.scene.scene_tree;
import ssvk.vulkan.renderer;

class Application : BaseObject
{
    mixin(implementBaseObject());

    public Renderer renderer = new Renderer();
    public Window window;
    public bool stop = false;
    public int totalTicks = 0;
    public float time = 0.0;

    public Plugin[] plugins;
    public SceneTree sceneTree;
    protected Camera3D camera;

    this()
    {
        // construct at runtime
        sceneTree = new SceneTree();
        camera = new Camera3D();
        window = new Window();
    }

    ////////////////////////
    /// public interface ///
    ////////////////////////
    public void initialize()
    {
        mainThreadId = thisTid;
        applicationId = id;

        createWindow();
        createRenderer();

        foreach (plugin; plugins)
        {
            plugin.initialize(this);
        }
    }

    public void tick(float delta)
    {
        renderer.update();
        renderer.drawFrame(sceneTree.rootNode, camera);

        foreach (plugin; plugins)
        {
            plugin.update(delta);
        }

        time += delta;
        ++totalTicks;

        handleMessages();
    }

    public void cleanup()
    {
        foreach (plugin; plugins)
        {
            plugin.cleanup();
        }
        renderer.cleanup();
    }

    public void run()
    {
        initialize();

        auto lastTick  = MonoTime.currTime;
        while (!stop)
        {
            auto now = MonoTime.currTime;
            auto tDiff = now - lastTick;
            lastTick = now;

            window.update();
            tick(0.001 * tDiff.total!"msecs");
        }
        cleanup();
    }

    //////////////////////
    /// initialization ///
    //////////////////////
    private void createWindow()
    {
        WindowCreateInfo createInfo = {

        };

        window.create(this, createInfo);
    }

    private void createRenderer()
    {
        RendererCreateInfo createInfo = {

        };
        
        renderer.create(this, createInfo);
    }

    private void handleMessages()
    {
        while (receiveTimeout(seconds(-1),
            &this.handleMessage
        )) {
            // receive more ...
        }
    }

    // TODO: move message handling to seperate file?
    private void handleMessage(Variant message)
    {
        if (message.convertsTo!MessageGetObjectProperty())
        {
            handleMessageGetObjectProperty(message.get!MessageGetObjectProperty());
        }
        else if (message.convertsTo!MessageSetObjectProperty())
        {
            handleMessageSetObjectProperty(message.get!MessageSetObjectProperty());
        }
        else
        {
            error("???");
        }
    }

    private void handleMessageGetObjectProperty(MessageGetObjectProperty message)
    {
        auto object = BaseObject.byID(message.objectID);

        if (object is null)
        {
            send(message.senderID, new shared MessageException(message.messageID));
            return;
        }

        auto clazz = object.getClass();
        if (message.propertyName !in clazz.properties)
        {
            send(message.senderID, new shared MessageException(message.messageID));
            return;
        }
        auto prop = &clazz.properties[message.propertyName];
        if (!prop.getter)
        {
            send(message.senderID, new shared MessageException(message.messageID));
            return;
        }
        Variant val = prop.getter(object);
        if (val.convertsTo!BaseObject()) { // response only contains object id
            message.value = val.get!(BaseObject)().id;
        }
        else {
            message.value = val;
        }
        
        send(message.senderID, message);
    }

    private void handleMessageSetObjectProperty(MessageSetObjectProperty message)
    {
        auto object = BaseObject.byID(message.objectID);

        if (object is null)
        {
            send(message.senderID, new shared MessageException(message.messageID));
            return;
        }

        auto clazz = object.getClass();
        if (message.propertyName !in clazz.properties)
        {
            send(message.senderID, new shared MessageException(message.messageID));
            return;
        }
        auto prop = &clazz.properties[message.propertyName];
        if (!prop.setter)
        {
            send(message.senderID, new shared MessageException(message.messageID));
            return;
        }
        try
        {
            prop.setter(object, message.value);
        }
        catch(Exception ex)
        {
            error("Error while processing MessageSetObjectProperty:");
            error(ex);
            send(message.senderID, new shared MessageException(message.messageID));
            return;
        }
        
        MessageDone response = {
            messageID: message.messageID
        };
        send(message.senderID, response);
    }
}
