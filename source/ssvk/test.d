module ssvk.test;

// TODO: choose a lib for better unit testing (or write own functions)

version(unittest):

import core.exception : AssertError;
import std.exception;
import std.format;
import std.functional;
import std.math;
import std.stdio;

import gfm.math;

bool vectorValid(T)(T vec)
{
    foreach (v; vec.v) {
        if (isNaN(v) || isInfinity(v)) {
            return false;
        }
    }
    return true;
}
alias matrixValid = vectorValid;

bool vectorsEqual(T)(T actual, T expected)
{
    return vectorValid(actual) && actual.distanceTo(expected) < 0.001;
}

bool compareMatrices(M : Matrix!(T, R, C), T, int R, int C)(M mat0, M mat1)
{
    for (int i = 0; i < C * R; ++i)
    {
        if (abs(mat0.v[i] - mat1.v[i]) > 0.001) {
            return false;
        }
    }
    return true;
}
bool matricesEqual(M : Matrix!(T, R, C), T, int R, int C)(M actual, M expected)
{
    return matrixValid(actual) && compareMatrices(actual, expected);
}

bool quatsEqual(T)(T actual, T expected)
{
    return vectorsEqual(actual.v, expected.v) || vectorsEqual(actual.v, -expected.v);
}

void assertEqual(alias cmp = "a == b", A, B)(A a, B b)
{
    assert(binaryFun!cmp(a, b), format("\nExpected value:\n%s\nActual value:\n%s\nComparator:\n" ~ cmp.stringof, b, a));
}

void assertApprox(A : B, B)(A a , B b, float epsilon = 1.0E-6) if (__traits(isFloating, A) && __traits(isFloating, B))
{
    assertEqual!((x, y) => abs(x - y) < epsilon)(a, b);
}

void assertApprox(T)(T a, T b) if (isVector!(T))
{
    assertEqual!vectorsEqual(a, b);
}

void assertApprox(T)(T a, T b) if (isMatrixInstantiation!(T))
{
    assertEqual!matricesEqual(a, b);
}

void assertApprox(T)(T a, T b) if (isQuaternionInstantiation!(T))
{
    assertEqual!quatsEqual(a, b);
}

private void _assertValid(alias cmp, T)(T a)
{
    assert(unaryFun!(cmp)(a), format("\nInvalid value: %s", a));
}

void assertValid(T)(T a) if (isMatrixInstantiation!(T))
{
    _assertValid!matrixValid(a);
}

@("Asserts.Approx")
unittest
{
    assertApprox(1.0, 1.000000001);
    assertApprox(vec3f(1.0, 0.0, 0.0), vec3f(1.00000001, -0.0000001, 0.0));
    assertThrown!AssertError(assertApprox(1.0, 1.001));
    assertThrown!AssertError(
        assertApprox(vec3f(1.0, 0.0, 0.0), vec3f(1.01, -0.01, 0.0)));
}
