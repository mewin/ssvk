module ssvk.loaders.shader;

import std.algorithm;
import std.array;
import std.exception;
import std.file;
import std.stdio;

import ssvk.resource.shader;

struct ShaderFiles
{
    string vertex;
    string fragment;
}

private const(uint[]) loadShaderCodeSPV(string shaderFileName)
{
    immutable(ulong) fileSize = getSize(shaderFileName);
    enforce(fileSize % uint.sizeof == 0);
    uint[] shaderCodeBuffer;
    shaderCodeBuffer.length = fileSize / uint.sizeof;

    File file = File(shaderFileName, "r");
    const(uint[]) shaderCodeSlice = file.rawRead(shaderCodeBuffer);
    return shaderCodeSlice;
}

Shader loadShaderSPV(const(string[]) files)
{
    const(uint[])[] code = files.map!(loadShaderCodeSPV).array();

    return new Shader(code);
}
