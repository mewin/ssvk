module ssvk.loaders.bitmap;

import std.string;

import bindbc.freeimage;

import ssvk.core.utility.bitmap;
import ssvk.resource.bitmap;

/// FreeImage
private ComponentType fiImageTypeToComponentType(FREE_IMAGE_TYPE type)
{
    switch (type)
    {
        case FREE_IMAGE_TYPE.FIT_BITMAP:
            return ComponentType.UINT8;
        case FREE_IMAGE_TYPE.FIT_RGB16:
        case FREE_IMAGE_TYPE.FIT_RGBA16:
            return ComponentType.UINT16;
        case FREE_IMAGE_TYPE.FIT_RGBF:
        case FREE_IMAGE_TYPE.FIT_RGBAF:
            return ComponentType.FLOAT32;
        default:
            throw new Exception("invalid FreeImage image type");
    }
}

private FIBITMAP* makeFIBitmapCompatible(FIBITMAP* bitmap)
{
    immutable(FREE_IMAGE_TYPE) imageType = FreeImage_GetImageType(bitmap);

    switch (imageType)
    {
        case FREE_IMAGE_TYPE.FIT_BITMAP:
        case FREE_IMAGE_TYPE.FIT_RGB16:
        case FREE_IMAGE_TYPE.FIT_RGBA16:
        case FREE_IMAGE_TYPE.FIT_RGBF:
        case FREE_IMAGE_TYPE.FIT_RGBAF:
            break;
        default:
            throw new Exception("incompatible image format");
    }

    immutable(FREE_IMAGE_COLOR_TYPE) colorType = FreeImage_GetColorType(bitmap);

    // normalize format
    if (colorType != FREE_IMAGE_COLOR_TYPE.FIC_RGB && colorType != FREE_IMAGE_COLOR_TYPE.FIC_RGBALPHA)
    {
        FIBITMAP* converted = FreeImage_ConvertTo32Bits(bitmap);
        if (converted == null) {
            throw new Exception("image conversion failed");
        }
        FreeImage_Unload(bitmap);
        bitmap = converted;
    }

    return bitmap;
}

Bitmap loadBitmap(string filename)
{
    immutable(char)* zstr = filename.toStringz();

    FREE_IMAGE_FORMAT format = FreeImage_GetFileType(zstr);
    if (format == FIF_UNKNOWN) {
        format = FreeImage_GetFIFFromFilename(zstr);
    }
    if (format == FIF_UNKNOWN) {
        throw new Exception("Could not detect image type from filename.");
    }
    FIBITMAP* bitmap = FreeImage_Load(format, zstr);
    if (!bitmap) {
        throw new Exception("Could not load image.");
    }

    try {
        bitmap = makeFIBitmapCompatible(bitmap);
    }
    catch(Exception ex) {
        FreeImage_Unload(bitmap);
        throw ex;
    }

    immutable(uint) width = FreeImage_GetWidth(bitmap);
    immutable(uint) height = FreeImage_GetHeight(bitmap);

    Bitmap result = new Bitmap(width, height);
    auto pixels = result.getPixels();
    // can directly convert
    FreeImage_ConvertToRawBits(&pixels[0].values[0], bitmap, result.pitch, result.bpp,
        FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, 1 // top down
    );

    immutable(bool) isBgr = (FI_RGBA_RED == 2); // FI_RGBA_RED is 0 for RGB and 2 for BGR
    static if (isBgr) { // must swap the channels
        result.swapChannels(CHANNEL_IDX_R, CHANNEL_IDX_B);
    }
    return result;
}
