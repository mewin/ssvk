module ssvk.loaders.scene;

import std.algorithm;
import std.conv;
import std.experimental.logger;
import std.path;
import std.traits;

import bindbc.assimp;
import gfm.math;

import ssvk.core.types;
import ssvk.core.utility.bitmap;
import ssvk.loaders.bitmap;
import ssvk.resource.bitmap;
import ssvk.resource.material;
import ssvk.resource.mesh;
import ssvk.scene.camera3d;
import ssvk.scene.mesh_instance;
import ssvk.scene.node3d;
import ssvk.scene.point_light3d;

private enum
{
    AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_BASE_COLOR_FACTOR = "$mat.gltf.pbrMetallicRoughness.baseColorFactor",
    AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_METALLIC_FACTOR   = "$mat.gltf.pbrMetallicRoughness.metallicFactor",
    AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_ROUGHNESS_FACTOR  = "$mat.gltf.pbrMetallicRoughness.roughnessFactor"
}

private struct AssimpLoader
{
    string filename;
    const(aiScene*) scene;
    Mesh[uint] convertedMeshes;
    Material[uint] convertedMaterials;

    private Transform3D convertAssimpTransform(ref const(aiMatrix4x4) assimpMatrix)
    {
        mat4f matrix;
        static if (is(ai_real == double))
        {
            static assert(aiMatrix4x4.sizeof == 16 * double.sizeof);
            mat4d matrixD = (cast(double*) &assimpMatrix)[0..16];
            matrix = matrixD;
        }
        else
        {
            static assert(aiMatrix4x4.sizeof == 16 * float.sizeof);
            matrix = (cast(float*) &assimpMatrix)[0..16];
        }

        return Transform3D.fromMatrix(matrix);
    }

    private vec3f convertAssimpVector(ref const(aiVector3D) assimpVector)
    {
        vec3f vector;
        vector.x = assimpVector.x;
        vector.y = assimpVector.y;
        vector.z = assimpVector.z;
        return vector;
    }

    private string convertAssimpString(ref const(aiString) assimpString)
    {
        return assimpString.data[0..assimpString.length].to!string;
    }

    private Mesh convertAssimpMesh(uint index)
    {
        if (index in convertedMeshes) {
            return convertedMeshes[index];
        }
        const(aiMesh*) assimpMesh = scene.mMeshes[index];

        Vertex[] vertices;
        vertices.length = assimpMesh.mNumVertices;
        
        for (uint i = 0; i < assimpMesh.mNumVertices; ++i) {
            vertices[i].position = convertAssimpVector(assimpMesh.mVertices[i]);
            vertices[i].color.x = 1.0;
            vertices[i].color.y = 1.0;
            vertices[i].color.z = 1.0;
            vertices[i].normal = convertAssimpVector(assimpMesh.mNormals[i]);
            vertices[i].tangent = convertAssimpVector(assimpMesh.mTangents[i]);
            vertices[i].bitangent = convertAssimpVector(assimpMesh.mBitangents[i]);
            vertices[i].uv0.x = assimpMesh.mTextureCoords[0][i].x;
            vertices[i].uv0.y = assimpMesh.mTextureCoords[0][i].y;
            if (assimpMesh.mNumUVComponents[1] > 1) {
                vertices[i].uv1.x = assimpMesh.mTextureCoords[1][i].x;
                vertices[i].uv1.y = assimpMesh.mTextureCoords[1][i].y;
            }
        }

        uint[] indices;
        indices.length = 3 * assimpMesh.mNumFaces;

        for (uint i = 0; i < assimpMesh.mNumFaces; ++i)
        {
            const(aiFace) face = assimpMesh.mFaces[i];
            assert(face.mNumIndices == 3);
            for (uint j = 0; j < face.mNumIndices; ++j) {
                indices[3 * i + j] = face.mIndices[j];
            }
        }

        Mesh mesh = new Mesh(vertices, indices);
        mesh.material = convertAssimpMaterial(assimpMesh.mMaterialIndex);

        convertedMeshes[index] = mesh;

        return mesh;
    }

    Material convertAssimpMaterial(uint index)
    {
        if (index in convertedMaterials) {
            return convertedMaterials[index];
        }

        const(aiMaterial*) assimpMaterial = scene.mMaterials[index];
        Bitmap albedoTexture = convertAssimpTexture(assimpMaterial, aiTextureType.DIFFUSE, 1); // AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_BASE_COLOR_TEXTURE
        Bitmap normalTexture = convertAssimpTexture(assimpMaterial, aiTextureType.NORMALS);
        Bitmap metallicRoughnessTexture = convertAssimpTexture(assimpMaterial, aiTextureType.UNKNOWN, 0); // AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_METALLICROUGHNESS_TEXTURE
        Bitmap ambientOcclusionTexture = convertAssimpTexture(assimpMaterial, aiTextureType.AMBIENT_OCCLUSION);

        if (albedoTexture is null) {
            albedoTexture = singleColorBitmap(Color.WHITE);
        }
        if (normalTexture is null) {
            normalTexture = singleColorBitmap(Color(0.5, 0.5, 1.0));
        }
        if (metallicRoughnessTexture is null) {
            metallicRoughnessTexture = singleColorBitmap(Color(1.0, 1.0, 0.0));
        }
        else {
            //    GLTF      |  SSVk
            // -------------+------------
            // R   -        | metallic
            // G roughness  | roughness
            // B metallic   |  -
            // A   -        |  -
            metallicRoughnessTexture.swapChannels(0, 2);
        }
        if (ambientOcclusionTexture is null) {
            ambientOcclusionTexture = singleColorBitmap(Color.WHITE);
        }

        // get parameters
        aiColor4D albedo = aiColor4D(1.0, 1.0, 1.0, 1.0);
        float metallic = 1.0;
        float roughness = 1.0;

        aiGetMaterialColor(assimpMaterial, AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_BASE_COLOR_FACTOR, 0, 0, &albedo);
        aiGetMaterialFloat(assimpMaterial, AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_METALLIC_FACTOR, 0, 0, &metallic);
        aiGetMaterialFloat(assimpMaterial, AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_ROUGHNESS_FACTOR, 0, 0, &roughness);

        Material material = new Material();
        material.boundTextures[BINDING_PBR_ALBEDO] = albedoTexture;
        material.boundTextures[BINDING_PBR_NORMAL] = normalTexture;
        material.boundTextures[BINDING_PBR_METALLIC_ROUGHNESS] = metallicRoughnessTexture;
        material.boundTextures[BINDINB_PBR_AMBIENT_OCCLUSION] = ambientOcclusionTexture;

        material.parameters[PARAMETER_PBR_ALBEDO] = vec4f(albedo.r, albedo.g, albedo.b, albedo.a);
        material.parameters[PARAMETER_PBR_METALLIC] = metallic;
        material.parameters[PARAMETER_PBR_ROUGHNESS] = roughness;

        convertedMaterials[index] = material;

        return material;
    }

    Bitmap convertAssimpTexture(const(aiMaterial*) assimpMaterial, aiTextureType textureType, uint index = 0)
    {
        uint textureCount = aiGetMaterialTextureCount(assimpMaterial, textureType);
        if (index >= textureCount) {
            return null;
        }

        aiString assimpPath;
        if (aiGetMaterialTexture(assimpMaterial, textureType, index, &assimpPath, null, null, null, null, null, null)
                != aiReturn.SUCCESS) {
            return null;
        }
        string path = buildPath(dirName(filename), assimpPath.data[0..assimpPath.length]);
        return loadBitmap(path);
    }

    private Node3D convertAssimpNode(const(aiNode*) node)
    {
        Node3D root = new Node3D();

        root.transform = convertAssimpTransform(node.mTransformation);

        for (uint i = 0; i < node.mNumMeshes; ++i)
        {
            uint meshIdx = node.mMeshes[i];
            Mesh mesh = convertAssimpMesh(meshIdx);
            MeshInstance meshInstance = new MeshInstance();
            meshInstance.mesh = mesh;
            meshInstance.name = convertAssimpString(scene.mMeshes[meshIdx].mName);
            root.addChild(meshInstance);
        }

        for (uint i = 0; i < node.mNumChildren; ++i)
        {
            const(aiNode*) child = node.mChildren[i];
            root.addChild(convertAssimpNode(child));
        }

        for (uint i = 0; i < scene.mNumLights; ++i)
        {
            const(aiLight*) assimpLight = scene.mLights[i];
            if (compareAssimpStrings(assimpLight.mName, node.mName)) {
                Node3D light = convertAssimpLight(assimpLight);
                if (light !is null) {
                    root.addChild(light);
                }
            }
        }

        return root;
    }

    private Node3D convertAssimpLight(const(aiLight*) assimpLight)
    {
        if (assimpLight.mType != aiLightSourceType.POINT) {
            return null; // TODO: other light types
        }

        PointLight3D light = new PointLight3D();

        light.transform.translation.x = assimpLight.mPosition.x;
        light.transform.translation.y = assimpLight.mPosition.y;
        light.transform.translation.z = assimpLight.mPosition.z;

        vec3f values = vec3f(assimpLight.mColorDiffuse.r, assimpLight.mColorDiffuse.g, assimpLight.mColorDiffuse.b);
        light.energy = max(values.x, values.y, values.z);
        values /= light.energy;

        light.color.r = values.x;
        light.color.g = values.y;
        light.color.b = values.z;

        return light;
    }

    private Camera3D convertAssimpCamera(const(aiCamera*) assimpCamera)
    {
        Camera3D camera = new Camera3D();

        camera.transform.lookAt(
            convertAssimpVector(assimpCamera.mPosition),
            convertAssimpVector(assimpCamera.mLookAt),
            convertAssimpVector(assimpCamera.mUp)
        );
        // TODO: other parameters (not part of the camera (yet))

        return camera;
    }

    private Node3D convertAssimpScene(const(aiScene*) assimpScene)
    {
        Node3D tree = convertAssimpNode(assimpScene.mRootNode);

        for (uint i = 0; i < assimpScene.mNumCameras; ++i)
        {
            Camera3D camera = convertAssimpCamera(assimpScene.mCameras[i]);
        }

        return tree;
    }

    private bool compareAssimpStrings(ref const(aiString) s1, ref const(aiString) s2)
    {
        return s1.length == s2.length
            && s1.data[0..s1.length] == s2.data[0..s2.length];
    }
}

Node3D loadScene(string filename)
{
    assert(aiImportFile, "Assimp not loaded.");

    const(aiScene*) scene = aiImportFile(filename.ptr, aiPostProcessSteps.Triangulate
            | aiPostProcessSteps.GenSmoothNormals  | aiPostProcessSteps.GenUVCoords
            | aiPostProcessSteps.CalcTangentSpace  | aiPostProcessSteps.ImproveCacheLocality
            | aiPostProcessSteps.OptimizeGraph     | aiPostProcessSteps.FindInvalidData
            | aiPostProcessSteps.TransformUVCoords | aiPostProcessSteps.FlipUVs);
    if (!scene || (scene.mFlags & AI_SCENE_FLAGS_INCOMPLETE) || !scene.mRootNode || scene.mNumMeshes < 1) {
        error(aiGetErrorString().to!string);
        throw new Exception("could not load model");
    }

    auto loader = AssimpLoader(filename, scene);

    info("Scene contains ", scene.mNumMeshes, " meshes.");
    info("Scene contains ", scene.mNumMaterials, " materials.");
    info("Scene contains ", scene.mNumTextures, " textures.");
    info("Scene contains ", scene.mNumCameras, " cameras.");
    info("Scene contains ", scene.mNumLights, " lights.");
    Node3D converted = loader.convertAssimpScene(scene);
    MeshInstance[] meshInstances = converted.allNodesOfType!MeshInstance();

    // as far as I know there is no concept of "portals" in Assimp/GLTF yet so just use the names to connect portals
    // createPortals(meshInstances);

    size_t totalVertices = 0;
    size_t totalIndices = 0;
    foreach (meshInstance; meshInstances) {
        totalVertices += meshInstance.mesh.vertices.length;
        totalIndices += meshInstance.mesh.indices.length;
    }
    info("Result contains ", meshInstances.length, " MeshInstances.");
    info("Total vertices: ", totalVertices);
    info("Total indices: ", totalIndices);

    return converted;
}
