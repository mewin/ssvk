import ssvk.bindings.assimp;
import ssvk.bindings.freeimage;
import ssvk.modules.lua.plugin;
import ssvk.modules.rc_server.plugin;

import test_application;

version(unittest) {
	// let silly take over
}
else {
	void main()
	{
		auto application = new TestApplication();
		application.plugins ~= new PluginAssimp();
		application.plugins ~= new PluginFreeImage();
		application.plugins ~= new PluginLua();
		application.plugins ~= new PluginRCServer();
		application.run();
	}
}