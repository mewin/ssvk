# SSVk

SSVk is a small 3D rendering engine I started as a hobby project and learning experience to get into Vulkan development (and also the D programming language).

This is just a hobby project and not even slightly usable for productive purposes. You probably don't want to use it (yet), but if you insist I provide some instructions below.

## Getting Started

Compiliation can be easily done by cloning the repository and using running `dub build`. Additionally you will need to compile the shaders in `assets/shaders` using glslangValidator to a `*.spv` file. The provided scons script does both automatically but SConstruct is not required for building.

There are currently no (real) scenes included to reduce the size of the repository. Therefore you will have to download a scene (preferably in GLTF format), put it into the assets folder and adjust the path in the `setupScene()` method in `source/test_application.d`.

These are the scenes I currently use for testing (as "autoconverted GLTF"):

* [Kasatka 71M - Three-bolt equipment](https://sketchfab.com/3d-models/kasatka-71m-three-bolt-equipment-ce8de196c58b4d09ba2474ad03beddb2)
* [Subway train interior](https://sketchfab.com/3d-models/subway-train-interior-f90d26d89ad44da4af508648f870a816)
* [Sci fi biolab corridor](https://sketchfab.com/3d-models/sci-fi-biolab-corridor-023906576a3640beaf38f99541b10830)

### Prerequisites

 * a working D development environment
 * glslangValidator for compiling the shaders
 * a scene you want to display
 * some installed libraries:
   * [SDL](https://libsdl.org/) for window creation and input
   * [Assimp](http://www.assimp.org/) for scene loading
   * [FreeImage](http://freeimage.sourceforge.net/) for image loading
   * [SPIRV-Cross](https://github.com/KhronosGroup/SPIRV-Cross) for parsing the compiled shaders

## Deployment

*You did read the first/second paragraph, didn't you?*

## Built With

* [DUB](https://dlang.org/) - compilation and dependency management
* [glslang](https://github.com/KhronosGroup/glslang) - shader compilation

## Contributing

It's a personal learning experience, so I do not see any reason for someone else to contribute code at the current point. But if you find bugs/have any critique, feel free to contact me via mail or create a ticket here.

## Versioning

I will start versioning once there is something I would call a working release.

## Authors

* **Patrick Wuttke** - *Initial work* - [mewin](https://mewin.de)

## License

This project is published under the terms of the GNU General Public License v3.0. See the LICENSE file for more information.

## Acknowledgments

* [vulkan-tutorial.com](https://vulkan-tutorial.com/) for providing a great tutorial for my first steps with Vulkan
* the people on Sketchfab who provide some pretty neat free 3D scenes I can use for testing
